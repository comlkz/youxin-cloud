package com.youxin.log;

import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * description: EnableUserLog <br>
 * date: 2020/3/12 10:30 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(LogAutoConfiguration.class)
public @interface EnableUserLog {
}
