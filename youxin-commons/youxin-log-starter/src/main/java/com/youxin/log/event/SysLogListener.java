package com.youxin.log.event;


import cn.hutool.core.util.StrUtil;

import com.youxin.context.BaseContextHandler;
import com.youxin.log.entity.SysLogDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;

import java.util.function.Consumer;


/**
 * 异步监听日志事件
 *
 * @author zuihou
 * @date 2019-07-01 15:13
 */

public class SysLogListener {

    private static final Logger log = LoggerFactory.getLogger(SysLogListener.class);

    private Consumer<SysLogDTO> consumer;

    public SysLogListener(Consumer<SysLogDTO> consumer) {
        this.consumer = consumer;
    }

    @Async(value = "asyncLogExecutor")
    @Order
    @EventListener(SysLogEvent.class)
    public void saveSysLog(SysLogEvent event) {
        SysLogDTO sysLog = (SysLogDTO) event.getSource();
        consumer.accept(sysLog);
    }

}
