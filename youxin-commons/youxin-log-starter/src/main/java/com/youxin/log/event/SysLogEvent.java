package com.youxin.log.event;


import com.youxin.log.entity.SysLogDTO;
import org.springframework.context.ApplicationEvent;

/**
 * 系统日志事件
 *
 * @author zuihou
 * @date 2019-07-01 15:13
 */
public class SysLogEvent extends ApplicationEvent {

    public SysLogEvent(SysLogDTO source) {
        super(source);
    }
}
