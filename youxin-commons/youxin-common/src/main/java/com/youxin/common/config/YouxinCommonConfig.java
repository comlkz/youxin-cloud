package com.youxin.common.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * description: YouxinCommonConfig <br>
 * date: 2020/2/29 17:02 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@EnableConfigurationProperties(YouxinCommonProperties.class)
@Configuration
public class YouxinCommonConfig {
}
