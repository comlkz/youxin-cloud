package com.youxin.common.constant;

public interface MqConstant {

    String CREATE_USER_QUEUE = "createUserQueue";

    String DELAY_EXCHANGE_NAME = "dtlExchange";

    String PER_QUEUE_TTL_EXCHANGE_NAME = "perQueueTTLExchange";

    String DELAY_QUEUE_PER_QUEUE_TTL_NAME = "deadQueue";

    String RED_PACK_TIMEOUT_SEND_QUEUE_NAME = "RedPackTimeOutSendQueue";


    String RED_PACK_TIMEOUT_PROCESS_QUEUE_NAME = "RedPackTimeOutProcessQueue";

    String MSG_SEND_QUEUE = "msgSendQueue";

    String PAY_NOTIFY_QUEUE = "payNotifyQueue";

    String PROCESS_EXCHANGE = "processExchange";

    String TOPIC_EXCHANGE="topicExchange";

    String MANAGER_CHANGE = "managerChange";

    int QUEUE_EXPIRATION = 24*60*60*1000;
}
