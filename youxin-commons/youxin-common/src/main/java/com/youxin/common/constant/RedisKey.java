package com.youxin.common.constant;

public interface RedisKey {
    /**
     * APP端
     */
    String PREFIX_USER_TOKEN = "com.guoliao.userpermission.token.";

    String PREFIX_USER_INFO = "com.youxin.token.info.";

    String USER_IM_TOKEN = "com.guoliao.userpermission.im.token.";

    String SMS_BETWEEN_PREFIX = "com.guoliao.sms.between.";

    String SMS_CODE_PREFIX = "com.guoliao.sms.code.";

    String SHIRO_KEY = "com.guoliao.shiro.";

    String USER_NO_GENERATOR = "com.guoliao.userpermission.no.generator";

    String GROUP_NO_GENERATOR = "com.guoliao.group.no.generator";

    String RED_PACK_PLAN = "com.guoliao.redpack.plan.";

    String RED_MAX_LIMIT = "com.guoliao.redpack.max.";

    String RED_SINGLE_COUNT_LIMIT = "com.guoliao.redpack.single.count.";

    String SYS_CONFIG_PREFIX = "com.guoliao.sys.config.";

    String USER_USE_STATIC = "com.guoliao.user.use.static";

    String USER_TRANSFER_GLOBAL = "com.guoliao.pay.user.transfer.global.";

    String PRINT_SCREEN = "com.guoliao.user.printscreen.";

    String PAY_RULE_KEY = "com.guoliao.pay.rules";

    /**
     * 后台
     */

    String ADMIN_USER_INFO = "com.guoliao.admin.info.";

    String PREFIX_CHAT_TOKEN = "com.guoliao.chat.token.";

    String CAPTCHA_CODE = "com.guoliao.chat.userpermission.captcha.";

    String SPECIAL_USER_IM_TOKEN = "com.guoliao.special.im.token.";

    String OAUTH2_CODE_KEY = "com.guoliao.oauth2.code.";

    String OAUTH2_TOKEN_KEY = "com.guoliao.oauth2.accessToken.";

    String CHANNEL_ORDER_NO = "com.guoliao.channel.order.no.";

    String CHANNEL_CHARGE_SMS= "com.guoliao.channel.charge.sms.";

    String CHANNEL_ORDER_TOKEN = "com.guoliao.chat.order.token.";

    String PRE_ORDER_KEY = "com.guoliao.pre.order.key.";
}
