package com.youxin.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * description: YouxinCommonProperties <br>
 * date: 2020/2/29 16:23 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@ConfigurationProperties(prefix = "youxin.common")
public class YouxinCommonProperties {

   private String serverUrl;

   private String filePath;

   private String downloadUrl;

    public String getServerUrl() {
        return serverUrl;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }
}
