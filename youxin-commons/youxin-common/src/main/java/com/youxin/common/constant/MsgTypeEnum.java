package com.youxin.common.constant;

public enum MsgTypeEnum {

    MODIFY_USER_INFO("MODIFY_USER_INFO", "修改用户信息"),
    USER_AUDIT("USER_AUDIT", "用户好友申请"),
    USER_APPLY("USER_APPLY", "用户审核通过"),
    JOIN_GROUP("JOIN_GROUP", "好友加入群组"),
    QUIT_GROUP("QUIT_GROUP", "退出群组"),
    KICK_GROUP("KICK_GROUP", "踢出群组"),
    MISS_GROUP("MISS_GROUP", "解散群组"),
    GROUP_AUDIT("GROUP_AUDIT", "申请入群处理完成"),
    NICKNAME_PROTECT("NICKNAME_PROTECT", "群成员昵称保护设置"),
    NICKNAME_GROUP("NICKNAME_PROTECT", "群成员昵称修改"),
    FRIEND_EXT_CHANGE("FRIEND_EXT_CHANGE", "好友拓展熟悉修改"),
    PRINT_SCREEN("PRINT_SCREEN", "截屏通知"),
    GROUP_EXT_CHANGE("GROUP_EXT_CHANGE", "群扩展属性修改");
    private String type;

    private String desc;

    MsgTypeEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
