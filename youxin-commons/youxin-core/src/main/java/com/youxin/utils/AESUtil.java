package com.youxin.utils;


import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

public class AESUtil {

    public static final String CHARSET = "UTF-8";
    public static final String AES_ALGORITHM = "AES/ECB/PKCS5Padding";

    public AESUtil() {
    }

    public static byte[] encrypt(byte[] data, byte[] key) {
        if (key.length != 16) {
            throw new RuntimeException("Invalid AES key length (must be 16 bytes)");
        } else {
            try {
                SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
                byte[] enCodeFormat = secretKey.getEncoded();
                SecretKeySpec seckey = new SecretKeySpec(enCodeFormat, "AES");
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(1, seckey);
                byte[] result = cipher.doFinal(data);
                return result;
            } catch (Exception var7) {
                throw new RuntimeException("encrypt fail!", var7);
            }
        }
    }

    public static byte[] decrypt(byte[] data, byte[] key) {
        if (key.length != 16) {
            throw new RuntimeException("Invalid AES key length (must be 16 bytes)");
        } else {
            try {
                SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
                byte[] enCodeFormat = secretKey.getEncoded();
                SecretKeySpec seckey = new SecretKeySpec(enCodeFormat, "AES");
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(2, seckey);
                byte[] result = cipher.doFinal(data);
                return result;
            } catch (Exception var7) {
                throw new RuntimeException("decrypt fail!", var7);
            }
        }
    }

    public static String encryptToBase64(String data, String key) {
        try {
            byte[] valueByte = encrypt(data.getBytes("UTF-8"), key.getBytes("UTF-8"));
            return Base64.getEncoder().encodeToString(valueByte);
        } catch (UnsupportedEncodingException var3) {
            throw new RuntimeException("encrypt fail!", var3);
        }
    }

    public static String decryptFromBase64(String data, String key) {
        try {
            byte[] originalData = Base64.getDecoder().decode(data.getBytes());
            byte[] valueByte = decrypt(originalData, key.getBytes("UTF-8"));
            return new String(valueByte, "UTF-8");
        } catch (UnsupportedEncodingException var4) {
            throw new RuntimeException("decrypt fail!", var4);
        }
    }

    public static String encryptWithKeyBase64(String data, String key) {
        try {
            byte[] valueByte = encrypt(data.getBytes("UTF-8"), Base64.getDecoder().decode(key.getBytes()));
            return new String(Base64.getEncoder().encode(valueByte));
        } catch (UnsupportedEncodingException var3) {
            throw new RuntimeException("encrypt fail!", var3);
        }
    }

    public static String decryptWithKeyBase64(String data, String key) {
        try {
            byte[] originalData = Base64.getDecoder().decode(data.getBytes());
            byte[] valueByte = decrypt(originalData, Base64.getDecoder().decode(key.getBytes()));
            return new String(valueByte, "UTF-8");
        } catch (UnsupportedEncodingException var4) {
            throw new RuntimeException("decrypt fail!", var4);
        }
    }

    public static byte[] genarateRandomKey() {
        KeyGenerator keygen = null;

        try {
            keygen = KeyGenerator.getInstance("AES/ECB/PKCS5Padding");
        } catch (NoSuchAlgorithmException var3) {
            throw new RuntimeException(" genarateRandomKey fail!", var3);
        }

        SecureRandom random = new SecureRandom();
        keygen.init(random);
        Key key = keygen.generateKey();
        return key.getEncoded();
    }

    public static String genarateRandomKeyWithBase64() {
        return Base64.getEncoder().encodeToString(genarateRandomKey());
    }
}
