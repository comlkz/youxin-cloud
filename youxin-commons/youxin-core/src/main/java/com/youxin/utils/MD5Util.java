package com.youxin.utils;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;

/**
 * @DateTime 2014年11月28日 下午2:11:10
 * @Company 华视传媒
 * @Author 刘兴密
 * @QQ 63972012
 * @Desc
 */
public class MD5Util {

    private static final Logger logger = LoggerFactory.getLogger(MD5Util.class);

    private final static String[] hexDigits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d",
            "e", "f"};

    private static final char[] DIGITS = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};


    /**
     * 转换字节数组为16进制字串
     *
     * @param b 字节数组
     * @return 16进制字串
     */
    public static String byteArrayToHexString(byte[] b) {
        StringBuilder resultSb = new StringBuilder();
        for (byte aB : b) {
            resultSb.append(byteToHexString(aB));
        }
        return resultSb.toString();
    }

    /**
     * 转换byte到16进制
     *
     * @param b 要转换的byte
     * @return 16进制格式
     */
    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0) {
            n = 256 + n;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigits[d1] + hexDigits[d2];
    }

    /**
     * MD5编码
     *
     * @param origin 原始字符串
     * @return 经过MD5加密之后的结果
     */
    public static String MD5(String origin) {
        String resultString = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            resultString = byteArrayToHexString(md.digest(origin.getBytes()));
        } catch (Exception e) {
            logger.error("MD5编码错误", e);
        }
        return resultString;
    }


    public static String MD5(String origin, String encoding) {
        String resultString = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            resultString = byteArrayToHexString(md.digest(origin.getBytes(encoding)));
        } catch (Exception e) {
            logger.error("MD5编码错误", e);
        }
        return resultString;
    }

    public static  String md5Salt(String originStr, String saltStr, int hashIterations){
        String resultString = null;
       try {
           byte[] salt = saltStr.getBytes();
           byte[] bytes = originStr.getBytes();
           MessageDigest digest = MessageDigest.getInstance("MD5");
           if (salt != null) {
               digest.reset();
               digest.update(salt);
           }

           byte[] hashed = digest.digest(bytes);
           int iterations = hashIterations - 1;

           for (int i = 0; i < iterations; ++i) {
               digest.reset();
               hashed = digest.digest(hashed);
           }
          // return new String(encode(hashed));
           return byteArrayToHexString(hashed);

       }catch (Exception e){
          e.printStackTrace();
       }
        return resultString;
    }


    public static char[] encode(byte[] data) {
        int l = data.length;
        char[] out = new char[l << 1];
        int i = 0;

        for(int var4 = 0; i < l; ++i) {
            out[var4++] = DIGITS[(240 & data[i]) >>> 4];
            out[var4++] = DIGITS[15 & data[i]];
        }

        return out;
    }

    public static int getLastNum(String str) {
        try {
            char[] charArray = str.toCharArray();
            for (int i = charArray.length - 1; i >= 0; i--) {
                if (Character.isDigit(charArray[i]) && Integer.valueOf(String.valueOf(charArray[i])) > 0) {
                    return Integer.valueOf(String.valueOf(charArray[i]));
                }
            }
        } catch (Exception e) {
            // logger.error(e.getMessage(), e);
        }
        return 0;
    }

    public static int getFirstNum(String str) {
        try {
            char[] charArray = str.toCharArray();
            for (char c : charArray) {
                if (Character.isDigit(c) && Integer.valueOf(String.valueOf(c)) > 0) {
                    return Integer.valueOf(String.valueOf(c));
                }
            }
        } catch (Exception e) {
            // logger.error(e.getMessage(), e);
        }
        return 0;
    }


}
