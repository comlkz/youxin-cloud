package com.youxin.exception;

/**
 * 异常接口类
 *
 * @author zuihou
 * @version 1.0,
 */
public interface BaseException {

    /**
     * 统一参数验证异常码
     */
    String BASE_VALID_PARAM = "0000001";

    /**
     * 返回异常信息
     *
     * @return
     */
    String getMessage();

    /**
     * 返回异常编码
     *
     * @return
     */
    String getCode();

}
