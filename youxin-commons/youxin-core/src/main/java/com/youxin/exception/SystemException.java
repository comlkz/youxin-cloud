package com.youxin.exception;

import java.util.function.Supplier;

/**
 * description: SystemException <br>
 * date: 2020/1/13 10:35 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class SystemException extends RuntimeException{

    private String msg;
    private String code = "000000";

    public static SystemException create(final Supplier<SystemException> supplier) {
        return supplier.get();
    }

    public SystemException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public SystemException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

    public SystemException(String code, String msg) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public SystemException(String msg, String code, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
