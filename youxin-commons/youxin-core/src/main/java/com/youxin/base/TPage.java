package com.youxin.base;

import java.io.Serializable;
import java.util.List;

public class TPage<T> implements Serializable {


    private static final long serialVersionUID = -8249490306440388786L;
    private List<T> records;
    private long total;

    public TPage() {

    }

    private TPage(List<T> data, long total) {
        this.records = data;
        this.total = total;
    }

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public static <T> TPage<T> page(List<T> data, long total){
        return new TPage(data,total);
    }
}
