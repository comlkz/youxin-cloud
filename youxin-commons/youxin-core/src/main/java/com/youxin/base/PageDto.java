package com.youxin.base;

public class PageDto<T> extends TPage {
    private String ext;

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}
