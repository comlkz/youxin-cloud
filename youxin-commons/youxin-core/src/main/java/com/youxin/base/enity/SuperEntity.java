package com.youxin.base.enity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.youxin.exception.BizException;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 超类基础实体
 *
 * @author zuihou
 * @date 2019/05/05
 */

public class SuperEntity<T> implements Serializable, Cloneable {
    public static final String FIELD_ID = "id";
    public static final String CREATE_TIME = "createTime";
    public static final String CREATE_USER = "createUser";

    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "主键")
    @NotNull(message = "id不能为空", groups = SuperEntity.Update.class)
    protected T id;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    protected LocalDateTime createTime;

    @ApiModelProperty(value = "创建人ID")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    protected T createUser;

    public SuperEntity(@NotNull(message = "id不能为空", groups = Update.class) T id, LocalDateTime createTime, T createUser) {
        this.id = id;
        this.createTime = createTime;
        this.createUser = createUser;
    }

    public SuperEntity() {
    }

    @Override
    public Object clone() {
        //支持克隆  提高性能  仅仅是浅克隆
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return new BizException("克隆失败");
        }
    }

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public T getCreateUser() {
        return createUser;
    }

    public void setCreateUser(T createUser) {
        this.createUser = createUser;
    }

    /**
     * 保存和缺省验证组
     */
    public interface Save extends Default {

    }

    /**
     * 更新和缺省验证组
     */
    public interface Update extends Default {

    }

    @Override
    public String toString() {
        return "SuperEntity{" +
                "id=" + id +
                ", createTime=" + createTime +
                ", createUser=" + createUser +
                '}';
    }
}
