package com.youxin.base;

import java.io.Serializable;

public class BaseResultCode implements Serializable {

    public static final String SUCCESS = "000000";

    /**
     * 参数错误
     */
    public static final String COMMON_FAIL = "000001";

    /**
     * 超出限制
     */
    public static final String EXCEED_LIMIT = "000002";

    /**
     * 请求过于频繁
     */
    public static final String REQUEST_FREQUENT = "000003";

    /**
     * 记录已存在
     */
    public static final String RECORD_EXISTS = "000004";

    /**
     * 系统内部异常
     */
    public static final String INTERNAL_ERROR = "000005";

    /**
     * 记录不存在
     */
    public static final String RECORD_NOT_EXISTS = "000006";

    /**
     * 安全认证不通过
     */
    public static final String SECURITY_ERROR = "000007";

    /**
     * 无权限
     */
    public static final String HAS_NO_PERMISSION = "000008";

    /**
     * 帐号被锁定
     */
    public static final String ACCOUNT_LOCK = "000010";
    /**
     * 登录需要验证码
     */
    public static final String SMS_CODE_NEED = "000011";


    /**
     * 密码不正确
     */
    public static final String PASSWORD_INVALID = "000012";

    /**
     * 余额不足
     */
    public static final String BALANCE_NOT_ENOUGH = "000013";

    /**
     * 超出单笔最大值
     */
    public static final String EXCEED_SINGLE_MAX = "000014";

    /**
     * 超出单笔最大值
     */
    public static final String EXCEED_DAY_MAX = "000015";

    /**
     * 用户未设置支付密码
     */
    public static final String PAY_PASS_NOT_SET = "000016";

    /**
     * 超出单笔最小值
     */
    public static final String EXCEED_SINGLE_MIN = "000017";
}
