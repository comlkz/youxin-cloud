package com.youxin.base;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Map;

public class TResult<T> implements Serializable {

    private static final long serialVersionUID = -7006386218675693726L;
    private String code;

    @ApiModelProperty(value = "提示消息")
    private String msg;

    @ApiModelProperty(value = "响应数据")
    private T data;

    @ApiModelProperty(value = "请求路径")
    private String path;
    /**
     * 附加数据
     */
    @ApiModelProperty(value = "附加数据")
    private Map<String, Object> extra;

    /**
     * 响应时间
     */
    @ApiModelProperty(value = "响应时间戳")
    private long timestamp = System.currentTimeMillis();

    public TResult(){

    }
    public TResult(T data){
        this.code = BaseResultCode.SUCCESS;
        this.data = data;
    }

    public TResult(String code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public TResult(String code, String msg, T data){
        this.code = code;
        this.msg = msg;
        this.data = data;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <String> TResult<String> success() {
        return new TResult("");
    }

    public static <T> TResult<T> success(T data) {
        return new TResult<T>(data);
    }

    public static <T> TResult<T> build(String code, String msg) {
        return new TResult<T>(code, msg);
    }

    public static <T> TResult<T> build(String code, String msg, T data) {
        return new TResult<T>(code, msg,data);
    }

    public static <E> TResult<E> result(String code, E data, String msg) {
        return new TResult<>(code, msg,data);
    }

    public String getPath() {
        return path;
    }

    public TResult setPath(String path) {
        this.path = path;
        return this;
    }

    public Map<String, Object> getExtra() {
        return extra;
    }

    public void setExtra(Map<String, Object> extra) {
        this.extra = extra;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
