package com.youxin.base;

import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;

public class MsgDto implements Serializable {

    private String data;

    private String type;

    public MsgDto(){

    }

    public MsgDto(String type,Object object){
        this.type = type;
        this.data = JSONObject.toJSONString(object);
    }
    public String getData() {
        return data;
    }


    public void setData(String data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
