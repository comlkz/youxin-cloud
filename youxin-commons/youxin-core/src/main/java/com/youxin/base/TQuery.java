package com.youxin.base;

import java.io.Serializable;

public class TQuery implements Serializable {

    private static final long serialVersionUID = -4821365817404121364L;
    /**
     * 当前页
     */
    private Integer currentPage = 1;
    /**
     * 每页条数
     */
    private Integer showCount = 10;

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getShowCount() {
        return showCount;
    }

    public void setShowCount(Integer showCount) {
        this.showCount = showCount;
    }
}
