package com.youxin.context;

/**
 * 常量工具类
 *
 * @author zuihou
 * @date 2018/12/21
 */
public class BaseContextConstants {
    /**
     *
     */
    public static final String TOKEN_NAME = "token";
    /**
     *
     */
    public static final String JWT_KEY_USER_NO = "userNo";

    public static final String JWT_KEY_DEVICE_ID = "deviceId";

    /**
     *
     */
    public static final String JWT_KEY_NAME = "name";
    /**
     *
     */
    public static final String JWT_KEY_ACCOUNT = "account";

    public static final String JWT_KEY_ID = "id";

    /**
     * 组织id
     */
    public static final String JWT_KEY_ORG_ID = "orgid";
    /**
     * 岗位id
     */
    public static final String JWT_KEY_STATION_ID = "stationid";
    /**
     * 租户
     */
    public static final String TENANT = "tenant";

    public static final String IS_BOOT = "boot";

    /**
     * 动态数据库名前缀。  每个项目配置死的
     */
    public static final String DATABASE_NAME = "database_name";

    public static final String ADMIN_KEY_USER_NAME = "userName";
}
