package com.youxin.user.dubbo;


import com.alibaba.fastjson.JSONObject;
import com.youxin.base.TResult;
import com.youxin.user.model.SysUser;
import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.rpc.service.GenericService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 用户操作API
 *
 * @author zuihou
 * @date 2019/07/10
 */
@Service
public class UserResolveApi {

    /**
     * 根据id 查询用户详情
     *
     * @param userNo
     * @param
     * @return
     */
    public SysUser getById(String userNo){
        ReferenceConfig<GenericService> reference = new ReferenceConfig<GenericService>();
        reference.setInterface("com.youxin.chat.user.client.UserClient");
// 声明为泛化接口
        reference.setGeneric(true);

// 用org.apache.dubbo.rpc.service.GenericService可以替代所有接口引用
        GenericService genericService = reference.get();

// 基本类型以及Date,List,Map等不需要转换，直接调用
        Object result = genericService.$invoke("getByUserNo", new String[] {"java.lang.String"}, new Object[] {userNo});
        SysUser user = JSONObject.parseObject(JSONObject.toJSONString(result),SysUser.class);
        return user;
    }
}
