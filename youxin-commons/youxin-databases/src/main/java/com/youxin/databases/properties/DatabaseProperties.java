package com.youxin.databases.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.youxin.databases.properties.DatabaseProperties.PREFIX;


/**
 * 客户端认证配置
 *
 * @author zuihou
 * @date 2018/11/20
 */
@ConfigurationProperties(prefix = PREFIX)
public class DatabaseProperties {
    public static final String PREFIX = "youxin.database";
    /**
     * 攻击 SQL 阻断解析器
     *
     * @return
     */
    public Boolean isBlockAttack = false;

    /**
     * 是否禁止写入
     */
    private Boolean isNotWrite = false;
    /**
     * 是否启用数据权限
     */
    private Boolean isDataScope = true;
    /**
     * 事务超时时间
     */
    private int txTimeout = 60 * 60;
    /**
     * 统一管理事务的方法名
     */
    private List<String> transactionAttributeList = new ArrayList<>(Arrays.asList("add*", "save*", "insert*",
            "create*", "update*", "edit*", "upload*", "delete*", "remove*",
            "clean*", "recycle*", "batch*", "mark*", "disable*", "enable*", "handle*", "syn*",
            "reg*", "gen*", "*Tx"
    ));
    /**
     * 事务扫描基础包
     */
    private String transactionScanPackage = "com.youxin";

    public Boolean getBlockAttack() {
        return isBlockAttack;
    }

    public void setBlockAttack(Boolean blockAttack) {
        isBlockAttack = blockAttack;
    }

    public Boolean getNotWrite() {
        return isNotWrite;
    }

    public void setNotWrite(Boolean notWrite) {
        isNotWrite = notWrite;
    }

    public Boolean getDataScope() {
        return isDataScope;
    }

    public void setDataScope(Boolean dataScope) {
        isDataScope = dataScope;
    }

    public int getTxTimeout() {
        return txTimeout;
    }

    public void setTxTimeout(int txTimeout) {
        this.txTimeout = txTimeout;
    }

    public List<String> getTransactionAttributeList() {
        return transactionAttributeList;
    }

    public void setTransactionAttributeList(List<String> transactionAttributeList) {
        this.transactionAttributeList = transactionAttributeList;
    }

    public String getTransactionScanPackage() {
        return transactionScanPackage;
    }

    public void setTransactionScanPackage(String transactionScanPackage) {
        this.transactionScanPackage = transactionScanPackage;
    }
}
