package com.youxin.admin.resolver;


import com.youxin.admin.annotation.LoginUser;
import com.youxin.admin.model.SysUser;
import com.youxin.context.BaseContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * Token转化SysUser
 *
 * @author zuihou
 * @date 2018/12/21
 */
public class ContextArgumentResolver implements HandlerMethodArgumentResolver {

    private static final Logger log = LoggerFactory.getLogger(ContextArgumentResolver.class);


    public ContextArgumentResolver() {

    }

    /**
     * 入参筛选
     *
     * @param mp 参数集合
     * @return 格式化后的参数
     */
    @Override
    public boolean supportsParameter(MethodParameter mp) {
        return mp.hasParameterAnnotation(LoginUser.class) && mp.getParameterType().equals(SysUser.class);
    }

    /**
     * @param methodParameter       入参集合
     * @param modelAndViewContainer model 和 view
     * @param nativeWebRequest      web相关
     * @param webDataBinderFactory  入参解析
     * @return 包装对象
     */
    @Override
    public Object resolveArgument(MethodParameter methodParameter,
                                  ModelAndViewContainer modelAndViewContainer,
                                  NativeWebRequest nativeWebRequest,
                                  WebDataBinderFactory webDataBinderFactory) {
        String account = BaseContextHandler.getAccount();
        Integer id = BaseContextHandler.getId();

        String name = BaseContextHandler.getName();
        //以下代码为 根据 @LoginUser 注解来注入 SysUser 对象
        SysUser user = new SysUser();
        user.setUserName(account);
        user.setNickName(name);
        user.setId(id);
        return user;
    }
}
