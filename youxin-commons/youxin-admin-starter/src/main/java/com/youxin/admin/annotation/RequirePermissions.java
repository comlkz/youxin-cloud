package com.youxin.admin.annotation;

import java.lang.annotation.*;

/**
 * description: RequirePermissions <br>
 * date: 2020/3/2 16:09 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequirePermissions {

    String value();
}
