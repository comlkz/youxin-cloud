package com.youxin.admin.model;


/**
 * @author zuihou
 * 用户实体
 */

public class SysUser {
    private static final long serialVersionUID = -5886012896705137070L;

    private Integer id;

    private String userName;//用户名称


    private String mobile;//用户手机号

    private String userNo;//用户标识

    private String nickName;//用户昵称

    private String avatar;//用户头像

    private Integer sex;//性别 0：未知 1：男 2：女

    private String area;//所在地区（以,分隔）

    private Integer userStatus;//用户状态  0：正常  -1：禁用

    /**
     * 用户类型 0：普通用户 1：系统用户 2：客服
     */
    private Integer userType;

    /**
     * 用户等级
     */
    private Integer userLevel;

    private String agentNo;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public String getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(String agentNo) {
        this.agentNo = agentNo;
    }


}
