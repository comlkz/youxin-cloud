package com.youxin.im;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * description: ImProperties <br>
 * date: 2020/2/26 16:43 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@ConfigurationProperties(prefix = "im")
public class ImProperties {

    private String appId;

    private String appSecret;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }
}
