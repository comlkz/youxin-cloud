package com.youxin.im;

import io.rong.RongCloud;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * description: ImAutoConfiguration <br>
 * date: 2020/2/26 16:42 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Configuration
@EnableConfigurationProperties(com.youxin.im.ImProperties.class)
public class ImAutoConfiguration {

    @Autowired
    private ImProperties imProperties;

    @Bean
    public RongCloud buildRongCloud(){
        return RongCloud.getInstance(imProperties.getAppId(),imProperties.getAppSecret());
    }
}
