package com.youxin.qiniu;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * description: ImProperties <br>
 * date: 2020/2/26 16:43 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@ConfigurationProperties(prefix = "qiniu")
public class QiniuProperties {

    private String accessKey;

    private String secretKey;

    private String bucket;

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }
}
