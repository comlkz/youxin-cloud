package com.youxin.qiniu;

import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
@EnableConfigurationProperties(com.youxin.qiniu.QiniuProperties.class)
public class QiniuConfiguration {

    @Resource
    private QiniuProperties qiniuProperties;

    @Bean
    public Auth createAuth(){
        return Auth.create(qiniuProperties.getAccessKey(), qiniuProperties.getSecretKey());
    }

    @Bean
    public UploadManager createUploadManager(){
        com.qiniu.storage.Configuration cfg = new com.qiniu.storage.Configuration(Region.autoRegion());
        UploadManager uploadManager = new UploadManager(cfg);
        return uploadManager;
    }
}
