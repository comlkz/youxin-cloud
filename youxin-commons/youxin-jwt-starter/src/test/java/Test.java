import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.youxin.auth.utils.JwtHelper;
import com.youxin.base.BaseResultCode;
import com.youxin.base.TResult;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

/**
 * description: Test <br>
 * date: 2020/3/8 15:56 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class Test {

    public static void main(String[] args) throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, true)
                //忽略未知字段
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        TResult result =TResult.build(BaseResultCode.COMMON_FAIL,"系统错误");
        System.out.println(mapper.writeValueAsString(result));
    }
}
