package com.youxin.auth.server.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.youxin.auth.server.properties.AuthServerProperties.PREFIX;


/**
 * 认证服务端 属性
 *
 * @author zuihou
 * @date 2018/11/20
 */

@ConfigurationProperties(prefix = PREFIX)
public class AuthServerProperties {
    public static final String PREFIX = "authentication";

    private TokenInfo user;

    public static class TokenInfo {
        /**
         * 过期时间
         */
        private Integer expire = 7200;
        /**
         * 加密 服务使用
         */
        private String priKey;
        /**
         * 解密
         */
        private String pubKey;

        public Integer getExpire() {
            return expire;
        }

        public void setExpire(Integer expire) {
            this.expire = expire;
        }

        public String getPriKey() {
            return priKey;
        }

        public void setPriKey(String priKey) {
            this.priKey = priKey;
        }

        public String getPubKey() {
            return pubKey;
        }

        public void setPubKey(String pubKey) {
            this.pubKey = pubKey;
        }
    }

    public TokenInfo getUser() {
        return user;
    }

    public void setUser(TokenInfo user) {
        this.user = user;
    }
}
