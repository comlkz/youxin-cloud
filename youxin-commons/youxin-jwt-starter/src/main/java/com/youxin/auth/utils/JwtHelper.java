package com.youxin.auth.utils;


import com.youxin.context.BaseContextConstants;
import com.youxin.exception.BizException;
import com.youxin.exception.code.ExceptionCode;
import com.youxin.utils.DateUtils;
import com.youxin.utils.NumberHelper;
import com.youxin.utils.StrHelper;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;
import java.util.Base64;

/**
 * Created by ace on 2017/9/10.
 *
 * @author zuihou
 * @date 2019-06-20 22:20
 */
public class JwtHelper {

    private static Logger log = LoggerFactory.getLogger(JwtHelper.class);
    private static final RsaKeyHelper RSA_KEY_HELPER = new RsaKeyHelper();

    private static final String SECRET = "secret";

    /**
     * 生成用户token
     *
     * @param jwtInfo
     * @param priKeyPath
     * @param expire
     * @return
     * @throws BizException
     */
    public static Token generateUserToken(JwtUserInfo jwtInfo,  int expire) throws BizException {
        JwtBuilder jwtBuilder = Jwts.builder()
                //设置主题
                .setSubject(String.valueOf(jwtInfo.getUserNo()))
                .claim(BaseContextConstants.JWT_KEY_USER_NO, jwtInfo.getUserNo())
                .claim(BaseContextConstants.JWT_KEY_DEVICE_ID, jwtInfo.getDeviceId());
        return generateToken(jwtBuilder,  expire);
    }



    /**
     * 获取token中的用户信息
     *
     * @param token      token
     * @return
     * @throws Exception
     */
    public static JwtUserInfo getJwtFromToken(String token) throws BizException {
        Jws<Claims> claimsJws = parserToken(token);
        Claims body = claimsJws.getBody();
        String strUserId = body.getSubject();
        String userNo = StrHelper.getObjectValue(body.get(BaseContextConstants.JWT_KEY_USER_NO));
        String deviceId = StrHelper.getObjectValue(body.get(BaseContextConstants.JWT_KEY_DEVICE_ID));
        return new JwtUserInfo(userNo, deviceId);
    }

    /**
     * 生成token
     *
     * @param builder
     * @param priKeyPath
     * @param expire
     * @return
     * @throws BizException
     */
    protected static Token generateToken(JwtBuilder builder,  int expire) throws BizException {
        try {
            //返回的字符串便是我们的jwt串了
          /*  String compactJws = builder.setExpiration(DateUtils.localDateTime2Date(LocalDateTime.now().plusSeconds(expire)))
                    //设置算法（必须）
                    .signWith(SignatureAlgorithm.RS256, RSA_KEY_HELPER.getPrivateKey(priKeyPath))
                    //这个是全部设置完成后拼成jwt串的方法
                    .compact();*/
            String compactJws = builder.setExpiration(DateUtils.localDateTime2Date(LocalDateTime.now().plusSeconds(expire)))
                    //设置算法（必须）
                    .signWith(SignatureAlgorithm.HS256, generalKey())
                    //这个是全部设置完成后拼成jwt串的方法
                    .compact();
            return new Token(compactJws, expire);
        } catch (Exception e) {
            log.error("errcode:{}, message:{}", ExceptionCode.JWT_GEN_TOKEN_FAIL.getCode(), e.getMessage());
            throw new BizException(ExceptionCode.JWT_GEN_TOKEN_FAIL.getCode(), ExceptionCode.JWT_GEN_TOKEN_FAIL.getMsg());
        }
    }


    /**
     * 公钥解析token
     *
     * @param token
     * @param pubKeyPath 公钥路径
     * @return
     * @throws Exception
     */
    private static Jws<Claims> parserToken(String token, String pubKeyPath) throws BizException {
        try {
            return Jwts.parser().setSigningKey(RSA_KEY_HELPER.getPublicKey(pubKeyPath)).parseClaimsJws(token);
        } catch (ExpiredJwtException ex) {
            //过期
            throw new BizException(ExceptionCode.JWT_TOKEN_EXPIRED.getCode(), ExceptionCode.JWT_TOKEN_EXPIRED.getMsg());
        } catch (SignatureException ex) {
            //签名错误
            throw new BizException(ExceptionCode.JWT_SIGNATURE.getCode(), ExceptionCode.JWT_SIGNATURE.getMsg());
        } catch (IllegalArgumentException ex) {
            //token 为空
            throw new BizException(ExceptionCode.JWT_ILLEGAL_ARGUMENT.getCode(), ExceptionCode.JWT_ILLEGAL_ARGUMENT.getMsg());
        } catch (Exception e) {
            log.error("errcode:{}, message:{}", ExceptionCode.JWT_PARSER_TOKEN_FAIL.getCode(), e.getMessage());
            throw new BizException(ExceptionCode.JWT_PARSER_TOKEN_FAIL.getCode(), ExceptionCode.JWT_PARSER_TOKEN_FAIL.getMsg());
        }
    }

    /**
     * 公钥解析token
     *
     * @param token
     * @param
     * @return
     * @throws Exception
     */
    public static Jws<Claims> parserToken(String token) throws BizException {
        try {
            return Jwts.parser().setSigningKey(generalKey()).parseClaimsJws(token);
        } catch (ExpiredJwtException ex) {
            //过期
            throw new BizException(ExceptionCode.JWT_TOKEN_EXPIRED.getCode(), ExceptionCode.JWT_TOKEN_EXPIRED.getMsg());
        } catch (SignatureException ex) {
            //签名错误
            throw new BizException(ExceptionCode.JWT_SIGNATURE.getCode(), ExceptionCode.JWT_SIGNATURE.getMsg());
        } catch (IllegalArgumentException ex) {
            //token 为空
            throw new BizException(ExceptionCode.JWT_ILLEGAL_ARGUMENT.getCode(), ExceptionCode.JWT_ILLEGAL_ARGUMENT.getMsg());
        } catch (Exception e) {
            log.error("errcode:{}, message:{}", ExceptionCode.JWT_PARSER_TOKEN_FAIL.getCode(), e.getMessage());
            throw new BizException(ExceptionCode.JWT_PARSER_TOKEN_FAIL.getCode(), ExceptionCode.JWT_PARSER_TOKEN_FAIL.getMsg());
        }
    }

    public static SecretKey generalKey() {
        // 本地的密码解码
        // 根据给定的字节数组使用AES加密算法构造一个密钥
        SecretKey key = new SecretKeySpec(SECRET.getBytes(), SignatureAlgorithm.HS256.getJcaName());
        return key;
    }
}
