package com.youxin.auth.client.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;


import static com.youxin.auth.client.properties.AuthClientProperties.PREFIX;


/**
 * 客户端认证配置
 *
 * @author zuihou
 * @date 2018/11/20
 */
@ConfigurationProperties(prefix = PREFIX)
public class AuthClientProperties {
    public static final String PREFIX = "authentication";

    private TokenInfo user;


    public static class TokenInfo {
        /**
         * 请求头名称
         */
        private String headerName;
        /**
         * 解密 网关使用
         */
        private String pubKey;

        public String getHeaderName() {
            return headerName;
        }

        public void setHeaderName(String headerName) {
            this.headerName = headerName;
        }

        public String getPubKey() {
            return pubKey;
        }

        public void setPubKey(String pubKey) {
            this.pubKey = pubKey;
        }
    }

    public TokenInfo getUser() {
        return user;
    }

    public void setUser(TokenInfo user) {
        this.user = user;
    }
}
