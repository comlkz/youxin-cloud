package com.youxin.auth.server.utils;


import com.youxin.auth.server.properties.AuthServerProperties;
import com.youxin.auth.utils.JwtHelper;
import com.youxin.auth.utils.JwtUserInfo;
import com.youxin.auth.utils.Token;
import com.youxin.exception.BizException;

/**
 * jwt token 工具
 *
 * @author zuihou
 * @date 2018/11/20
 */

public class JwtTokenServerUtils {
    /**
     * 认证服务端使用，如 authority-server
     * 生成和 解析token
     */
    private AuthServerProperties authServerProperties;

    public JwtTokenServerUtils(AuthServerProperties authServerProperties) {
        this.authServerProperties = authServerProperties;
    }

    /**
     * 生成token
     *
     * @param jwtInfo
     * @param expire
     * @return
     * @throws BizException
     */
    public Token generateUserToken(JwtUserInfo jwtInfo, Integer expire) throws BizException {
        AuthServerProperties.TokenInfo userTokenInfo = authServerProperties.getUser();
        if (expire == null || expire <= 0) {
            expire = userTokenInfo.getExpire();
        }
        return JwtHelper.generateUserToken(jwtInfo,  expire);
    }

    /**
     * 解析token
     *
     * @param token
     * @return
     * @throws BizException
     */
    public JwtUserInfo getUserInfo(String token) throws BizException {
        AuthServerProperties.TokenInfo userTokenInfo = authServerProperties.getUser();
        return JwtHelper.getJwtFromToken(token);
    }


}
