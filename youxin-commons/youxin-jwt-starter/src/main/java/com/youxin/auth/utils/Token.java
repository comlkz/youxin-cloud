package com.youxin.auth.utils;

import io.swagger.annotations.ApiModelProperty;


import java.io.Serializable;

/**
 * @author zuihou¬
 * @createTime 2017-12-15 11:22
 */

public class Token implements Serializable {
    private static final long serialVersionUID = -8482946147572784305L;
    /**
     * token
     */
    @ApiModelProperty(value = "token")
    private String token;
    /**
     * 有效时间：单位：秒
     */
    @ApiModelProperty(value = "有效期")
    private Integer expire;

    public Token(String token, Integer expire) {
        this.token = token;
        this.expire = expire;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getExpire() {
        return expire;
    }

    public void setExpire(Integer expire) {
        this.expire = expire;
    }
}
