package com.youxin.auth.client.utils;


import com.youxin.auth.client.properties.AuthClientProperties;
import com.youxin.auth.utils.JwtHelper;
import com.youxin.auth.utils.JwtUserInfo;
import com.youxin.exception.BizException;

/**
 * JwtToken 客户端工具
 *
 * @author zuihou
 * @date 2018/11/20
 */
public class JwtTokenClientUtils {
    /**
     * 用于 认证服务的 客户端使用（如 网关） ， 在网关获取到token后，
     * 调用此工具类进行token 解析。
     * 客户端一般只需要解析token 即可
     */
    private AuthClientProperties authClientProperties;

    public JwtTokenClientUtils(AuthClientProperties authClientProperties) {
        this.authClientProperties = authClientProperties;
    }

    /**
     * 解析token
     *
     * @param token
     * @return
     * @throws BizException
     */
    public JwtUserInfo getUserInfo(String token) throws BizException {
        return JwtHelper.getJwtFromToken(token);
    }
}
