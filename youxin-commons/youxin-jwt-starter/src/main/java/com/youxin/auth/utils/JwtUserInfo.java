package com.youxin.auth.utils;


import java.io.Serializable;

/**
 * jwt 存储的 内容
 *
 * @author zuihou
 * @date 2018/11/20
 */

public class JwtUserInfo implements Serializable {
    /**
     * 账号id
     */
    private String userNo;

    private String deviceId;

    public JwtUserInfo(String userNo,String deviceId) {
        this.userNo = userNo;
        this.deviceId = deviceId;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
