package com.youxin.chat.pay.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.youxin.common.config.YouxinCommonProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(value = {"com.youxin.chat.pay.**.mapper*"})
public class PayDatabaseConfig {

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }



}
