package com.youxin;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;
import com.alicp.jetcache.anno.config.EnableMethodCache;
import com.youxin.auth.server.EnableAuthServer;
import com.youxin.user.annotation.EnableLoginArgResolver;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * description: PayBootstrap <br>
 * date: 2020/2/29 11:03 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@SpringBootApplication
@EnableDubbo
@EnableAuthServer
@EnableDiscoveryClient
@EnableLoginArgResolver
@EnableMethodCache(basePackages = "com.youxin.chat.pay")
@EnableCreateCacheAnnotation
public class PayBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(PayBootstrap.class,args);
    }
}
