package com.youxin.chat.pay.config;

import com.youxin.chat.pay.interceptor.WalletHandlerInterceptor;
import com.youxin.user.interceptor.ContextHandlerInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * description: PayMvcConfigurer <br>
 * date: 2020/3/1 16:34 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Configuration
public class PayMvcConfigurer implements WebMvcConfigurer {

    @Resource
    private WalletHandlerInterceptor walletHandlerInterceptor;


    /**
     * 注册 拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        if (walletHandlerInterceptor != null) {
            String[] commonPathPatterns = getExcludeCommonPathPatterns();
            registry.addInterceptor(walletHandlerInterceptor)
                    .addPathPatterns("/**")
                    .order(11)
                    .excludePathPatterns(commonPathPatterns);
            WebMvcConfigurer.super.addInterceptors(registry);
        }
    }

    protected String[] getExcludeCommonPathPatterns() {
        String[] urls = {
        };
        return urls;

    }
}
