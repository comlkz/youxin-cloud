package com.youxin.chat.pay.controller;

import com.youxin.base.TResult;
import com.youxin.chat.pay.entity.req.UserOrderReq;
import com.youxin.chat.pay.entity.vo.*;
import com.youxin.chat.pay.service.OrderService;
import com.youxin.chat.pay.service.TransactionDetailService;
import com.youxin.chat.pay.service.UserRechargeService;
import com.youxin.chat.pay.service.UserWalletService;
import com.youxin.chat.pay.service.channel.entity.UserCardDto;
import com.youxin.user.annotation.LoginUser;
import com.youxin.user.model.SysUser;
import com.youxin.utils.IPUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * description: WalletController <br>
 * date: 2020/3/1 10:17 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Api(value = "钱包模块", tags = {"支付接口"})
@RestController
@RequestMapping("wallet")
public class WalletController {

    @Resource
    private UserWalletService userWalletService;

    @Resource
    private UserRechargeService userRechargeService;

    @Resource
    private TransactionDetailService transactionDetailService;

    @Resource
    private OrderService orderService;

    @GetMapping("view")
    @ApiOperation("用户钱包首页")
    public TResult<UserWalletVo> showUserWalletView(@ApiIgnore @LoginUser SysUser user) {
        UserWalletVo userWalletView = userWalletService.showUserWallet(user.getUserNo());
        return TResult.success(userWalletView);
    }

    @GetMapping("balance")
    @ApiOperation("用户余额查询")
    public TResult<UserBalanceVo> selectBalance(@ApiIgnore @LoginUser SysUser user) {
        UserBalanceVo userWalletDtos = userWalletService.selectBalance(user.getUserNo());
        return TResult.success(userWalletDtos);
    }

    @GetMapping("recharge/record")
    @ApiOperation("充值记录")
    public TResult<List<UserRechargeVo>> selectRechargeRecord(@ApiIgnore @LoginUser SysUser user) {
        List<UserRechargeVo> list = userRechargeService.selectRechargeRecord(user.getUserNo());
        return TResult.success(list);
    }

    @GetMapping("delectDetail")
    @ApiOperation("用户清空帐户记录")
    public TResult<String> delectDetail(@ApiIgnore @LoginUser SysUser user) {
        transactionDetailService.clearUserRecord(user.getUserNo());
        return TResult.success();
    }

    @PostMapping("paypaw")
    @ApiOperation("设置用户支付密码")
    public TResult<String> setUserPayPawInfo(@ApiIgnore @LoginUser SysUser user, @RequestBody PayPassReq payPawDto) {
        userWalletService.userpaypaw(payPawDto, user.getUserNo());
        return TResult.success();
    }

    @PostMapping("pay/forget")
    @ApiOperation("忘记支付密码")
    public TResult<String> forgetPayPaw(@ApiIgnore @LoginUser SysUser user, @RequestBody ForgetPayPassReq forgetPayPaw) {
        userWalletService.forgetPayPaw(user.getUserNo(), forgetPayPaw);
        return TResult.success();
    }

    @GetMapping("pay/forget/sms")
    @ApiOperation("忘记支付密码发送短信")
    public TResult<String> forgetPayPawSms(@ApiIgnore @LoginUser SysUser user) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        userWalletService.forgetPayPawSms(user.getUserNo(), IPUtil.ipToLong(IPUtil.getIpAddr(request)));
        return TResult.success();
    }

    @GetMapping("bank/list")
    @ApiOperation("获取银行列表")
    public TResult<List<BankInfoVo>> listBank() {
        List<BankInfoVo> list = userWalletService.listBank();
        return TResult.success(list);
    }

    @GetMapping("order/list")
    @ApiOperation("订单列表记录")
    public TResult<UserOrderVo> getCommonOrderList(@ApiIgnore @LoginUser SysUser user,UserOrderReq commonOrderRequest) {
        if (commonOrderRequest.getType() == -1) {
            commonOrderRequest.setType(null);
        }
        UserOrderVo orderDto = orderService.getCommonOrderList(user.getUserNo(), commonOrderRequest);
        return TResult.success(orderDto);
    }

    @GetMapping("card/list")
    @ApiOperation("已绑卡列表")
    public TResult<List<UserCardVo>> getUserCardList(@ApiIgnore @LoginUser SysUser user) {
        return TResult.success(userWalletService.getUserCardList(user.getUserNo()));
    }

    @GetMapping("order/detail/{orderNo}")
    @ApiOperation("订单详情记录")
    public TResult<OrderDetailVo> getCommonOrderDetail(@ApiIgnore @LoginUser SysUser user,@PathVariable String orderNo) {

        OrderDetailVo orderDetailDto = orderService.getCommonOrderDetail(user.getUserNo(), orderNo);
        return TResult.success(orderDetailDto);
    }

    @PostMapping("order/del")
    @ApiOperation("删除订单")
    public TResult<String> orderDel(@ApiIgnore @LoginUser SysUser user,@RequestBody List<String> idList) {
        orderService.orderDel(user.getUserNo(), idList);
        return TResult.success();
    }
}
