package com.youxin.chat.pay.controller;

import com.youxin.base.TResult;
import com.youxin.chat.pay.entity.req.TransferCodeReq;
import com.youxin.chat.pay.entity.req.TransferMoneyReq;
import com.youxin.chat.pay.entity.vo.TransferInfoVo;
import com.youxin.chat.pay.service.TransferService;
import com.youxin.common.config.YouxinCommonProperties;
import com.youxin.log.annotation.SysLog;
import com.youxin.user.annotation.LoginUser;
import com.youxin.user.model.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * description: TransferController <br>
 * date: 2020/2/29 16:13 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Api(value="个人付款码", tags={"支付接口"})
@RestController
@RequestMapping("transfer")
public class TransferController {

    @Resource
    private TransferService transferService;

    @Resource
    private YouxinCommonProperties youxinCommonProperties;

    @ApiOperation("生成转帐二维码")
    @SysLog("生成转帐二维码")
    @PostMapping("code")
    public TResult<String> showCode(@ApiIgnore @LoginUser SysUser user, @RequestBody @Validated TransferCodeReq transferCodeRequest) {
        transferCodeRequest.setUserNo(user.getUserNo());
        String code = transferService.buildTransferCode(transferCodeRequest);
        String qrCodeUrl = youxinCommonProperties.getServerUrl() + "transfer/scan/" + code;
        return TResult.success(qrCodeUrl);
    }

    @ApiOperation("扫描转帐二维码")
    @GetMapping("scan/{code}")
    public TResult<TransferInfoVo> scanCode(@PathVariable String code) {
        TransferInfoVo transferInfoVo = transferService.acquireTransferCodeInfo(code);
        return TResult.success(transferInfoVo);
    }

    @ApiOperation("执行转帐")
    @SysLog("执行转帐")
    @PostMapping("money")
    public TResult<TransferInfoVo> transferMoney(@ApiIgnore @LoginUser SysUser user,@RequestBody @Validated TransferMoneyReq transferMoneyRequest) {
        transferMoneyRequest.setUserNo(user.getUserNo());
        transferService.transferAmount(transferMoneyRequest);
        return TResult.success();
    }
}
