package com.youxin.chat.pay.provider;

import com.youxin.chat.pay.client.WalletClient;
import com.youxin.chat.pay.dto.PayRulesDto;
import com.youxin.chat.pay.service.PayRulesService;
import com.youxin.chat.pay.service.UserWalletService;
import com.youxin.exception.SystemException;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;

/**
 * description: WalletClientImpl <br>
 * date: 2020/3/5 11:08 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class WalletClientImpl implements WalletClient {

    @Resource
    private UserWalletService userWalletService;

    @Resource
    private PayRulesService payRulesService;

    @Override
    public void updateUserWalletStates(String userNo, Integer status) throws SystemException {
        userWalletService.updateUserWalletStates(userNo, status);
    }

    @Override
    public void saveRisk(PayRulesDto riskDto) {
        payRulesService.saveRisk(riskDto);
    }
}
