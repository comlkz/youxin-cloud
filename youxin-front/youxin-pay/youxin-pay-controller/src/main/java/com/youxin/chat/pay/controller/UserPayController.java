package com.youxin.chat.pay.controller;

import com.youxin.base.TResult;
import com.youxin.chat.pay.dto.PayRulesDto;
import com.youxin.chat.pay.entity.req.*;
import com.youxin.chat.pay.entity.vo.DefrayRateVo;
import com.youxin.chat.pay.service.UserPayService;
import com.youxin.log.annotation.SysLog;
import com.youxin.user.annotation.LoginUser;
import com.youxin.user.model.SysUser;
import com.youxin.utils.IPUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * description: UserPayController <br>
 * date: 2020/3/1 11:34 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */

@Api(value="支付信息", tags={"支付接口"})
@RestController
@RequestMapping("userPay")
public class UserPayController {

    @Resource
    private UserPayService userPayService;

    @ApiOperation("绑卡申请")
    @SysLog("绑卡申请")
    @PostMapping("card/bind/apply")
    public TResult<String> auditBindCard(@ApiIgnore @LoginUser SysUser user, @RequestBody @Validated UserCardReq userCardDto, HttpServletRequest request) {
        userCardDto.setUserIp(IPUtil.getIpAddr(request));
        return userPayService.auditBindCard(userCardDto, user.getUserNo());
    }

//    @ApiOperation("绑卡申请短信")
//    @PostMapping("card/bind/sms")
//    public TResult<String> sendBindSms(Long id) {
//        return TResult.success();
//    }

    @ApiOperation("绑卡短信验证")
    @SysLog("绑卡短信验证")
    @PostMapping("card/bind/validate")
    public TResult<String> validateBindSms(@ApiIgnore @LoginUser SysUser user, @RequestBody  @Validated UserCardSmsReq userCardSmsDto) {
        TResult result = userPayService.validateBindSms(userCardSmsDto, user.getUserNo());
        return result;
    }

    @ApiOperation("解约")
    @SysLog("解约")
    @PostMapping("card/termination")
    public TResult<String> terminationCard(@ApiIgnore @LoginUser SysUser user,@RequestBody TerminationCardReq terminationCardDto) {
        TResult result = userPayService.terminationCard(terminationCardDto, user.getUserNo());
        return result;
    }

    @ApiOperation("用户充值")
    @SysLog("用户充值")
    @PostMapping("pay")
    public TResult<String> doCharge(@ApiIgnore @LoginUser SysUser user, @RequestBody @Validated UserChargeReq userChargeDto, HttpServletRequest request) {
        TResult<String> result = userPayService.doCharge(userChargeDto, user.getUserNo(), IPUtil.getIpAddr(request));
        return result;
    }

    @ApiOperation("用户充值短信确认")
    @SysLog("用户充值短信确认")
    @PostMapping("pay/Sms")
    public TResult<String> doChargeSms(@ApiIgnore @LoginUser SysUser user,@RequestBody @Validated UserChargeSmsReq userChargeSmsDto) {
        TResult result = userPayService.doChargeSms(userChargeSmsDto, user.getUserNo());
        return result;
    }

    @ApiOperation("提现")
    @SysLog("提现")
    @PostMapping("defray")
    public TResult<String> defray(@ApiIgnore @LoginUser SysUser user,@RequestBody @Validated UserCashReq userCashReq, HttpServletRequest request) {
        userCashReq.setClientIp(IPUtil.getIpAddr(request));
        TResult result = userPayService.defray(userCashReq, user.getUserNo());
        return result;
    }

    @ApiOperation("提现费率")
    @GetMapping("defray/rate/{amount}")
    public TResult<DefrayRateVo> defrayRate(@ApiIgnore @LoginUser SysUser user, @PathVariable String amount) {
        DefrayRateVo defrayRateDto = userPayService.defrayRate(amount, user.getUserNo());
        return TResult.success(defrayRateDto);
    }

    @ApiOperation("支付规则")
    @GetMapping("getPayRules")
    public TResult<PayRulesDto> getPayRules(){
        PayRulesDto payRulesDto = userPayService.getPayRules();
        return TResult.success(payRulesDto);
    }
}
