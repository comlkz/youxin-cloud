package com.youxin.chat.pay.controller;


import com.youxin.base.TResult;
import com.youxin.chat.pay.service.UserPayService;
import com.youxin.chat.pay.service.channel.entity.ChannelNotifyRequest;
import com.youxin.chat.pay.utils.WebUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@ApiIgnore
@RestController
@RequestMapping("notify")
public class NotifyController {

    private Logger logger = LoggerFactory.getLogger(NotifyController.class);

    @Resource
    private UserPayService userPayService;


    @RequestMapping("{channelCode}/recharge/{orderNo}")
    public String orderNotify(@PathVariable String channelCode, @PathVariable String orderNo, HttpServletRequest request) {
        ChannelNotifyRequest notifyRequest = new ChannelNotifyRequest();
        notifyRequest.setOrderNo(orderNo);
        notifyRequest.setChannelCode(channelCode);
        String requestText;
        if ("YPL".equalsIgnoreCase(channelCode)) {
            requestText = WebUtil.getHttpBody(request);
            String ext = request.getHeader("x-efps-sign");
            notifyRequest.setExt(ext);
        } else if("AINONG".equalsIgnoreCase(channelCode)){
            requestText = WebUtil.getParamStringWithNull(request);

        }else {
            requestText = WebUtil.getParamString(request);
        }
        notifyRequest.setData(requestText);
        TResult<String> result = userPayService.rechargeNotify(notifyRequest);
        return result.getData();
    }

    @RequestMapping("{channelCode}/defray/{orderNo}")
    public String defrayNotify(@PathVariable String channelCode, @PathVariable String orderNo, HttpServletRequest request) {
        ChannelNotifyRequest notifyRequest = new ChannelNotifyRequest();
        notifyRequest.setOrderNo(orderNo);
        notifyRequest.setChannelCode(channelCode);
        String requestText;
        if ("YPL".equalsIgnoreCase(channelCode)) {
            requestText = WebUtil.getHttpBody(request);
            String ext = request.getHeader("x-efps-sign");
            notifyRequest.setExt(ext);
        } else if("AINONG".equalsIgnoreCase(channelCode)){
            requestText = WebUtil.getParamStringWithNull(request);

        }else
            {
            requestText = WebUtil.getParamString(request);
        }
        notifyRequest.setData(requestText);
        TResult<String> result = userPayService.defrayNotify(notifyRequest);
        return result.getData();
    }

}
