package com.youxin.chat.pay.provider;

import com.youxin.base.TResult;
import com.youxin.chat.pay.client.ChannelClient;
import com.youxin.chat.pay.dto.ChannelAccountDto;
import com.youxin.chat.pay.dto.ChannelBalanceDto;
import com.youxin.chat.pay.dto.ChannelBankDto;
import com.youxin.chat.pay.entity.vo.ChannelBalanceVo;
import com.youxin.chat.pay.service.ChannelAccountService;
import com.youxin.chat.pay.service.UserPayService;
import com.youxin.dozer.DozerUtils;
import com.youxin.exception.SystemException;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;

/**
 * description: ChannelClientImpl <br>
 * date: 2020/3/5 15:15 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class ChannelClientImpl implements ChannelClient {

    @Resource
    private ChannelAccountService channelAccountService;

    @Resource
    private UserPayService userPayService;

    @Resource
    private DozerUtils dozerUtils;

    @Override
    public void saveChannel(ChannelAccountDto channelAccountDto) {
        channelAccountService.saveChannel(channelAccountDto);
    }

    @Override
    public void saveChannelBank(ChannelBankDto channelBankDto) {
        channelAccountService.saveChannelBank(channelBankDto);
    }

    @Override
    public void removeChannelBank(Integer id) {
        channelAccountService.removeChannelBank(id);
    }

    @Override
    public ChannelBalanceDto balanceQuery(String channelAccount) throws SystemException {
        TResult<ChannelBalanceVo> result = userPayService.balanceQuery(channelAccount);
        ChannelBalanceDto balance = dozerUtils.map(result.getData(),ChannelBalanceDto.class);
        return balance;
    }
}
