package com.youxin.chat.pay.provider;

import com.youxin.chat.pay.client.OrderClient;
import com.youxin.chat.pay.dto.CashStatusChange;
import com.youxin.chat.pay.service.OrderService;
import com.youxin.exception.SystemException;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;

/**
 * description: OrderClientImpl <br>
 * date: 2020/3/5 11:53 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class OrderClientImpl implements OrderClient {

    @Resource
    private OrderService orderService;

    @Override
    public void changeCashStatus(CashStatusChange cashStatusChange) throws SystemException {
           orderService.changeCashStatus(cashStatusChange);
    }

    @Override
    public void syncCashStatus(Integer minute) throws SystemException {
        orderService.syncCashStatus(minute);
    }

    @Override
    public void syncOrderStatus(String orderNo) throws SystemException {
        orderService.syncOrderStatus(orderNo);
    }

    @Override
    public void syncCashStatus(String orderNo) throws SystemException {
        orderService.syncCashStatus(orderNo);
    }


}
