package com.youxin.chat.pay.controller;

import com.youxin.base.TResult;
import com.youxin.chat.pay.entity.req.RedPackAssignReq;
import com.youxin.chat.pay.entity.req.RedPackSendReq;
import com.youxin.chat.pay.entity.vo.RedPackReceiveVo;
import com.youxin.chat.pay.entity.vo.RedUnreceivedVo;
import com.youxin.chat.pay.service.RedPackService;
import com.youxin.log.annotation.SysLog;
import com.youxin.user.annotation.LoginUser;
import com.youxin.user.model.SysUser;
import com.youxin.utils.IPUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * description: RedPackController <br>
 * date: 2020/2/29 15:04 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Api(value="红包相关", tags={"支付接口"})

@RestController
@RequestMapping("redpack")
public class RedPackController {

    @Resource
    private RedPackService redPackService;

    @PostMapping("send")
    @SysLog("普通发送红包")
    @ApiOperation("普通发送红包(兼容定向红包)")
    public TResult<String> sendRedPack(@ApiIgnore @LoginUser(isFull = false) SysUser user, @RequestBody @Validated RedPackSendReq redPackSendDto) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        String id = redPackService.sendRedPack(redPackSendDto, user.getUserNo(), IPUtil.getIpAddr(request));
        return TResult.success(id);
    }

    @PostMapping("assign")
    @SysLog("定向发送红包")
    @ApiOperation("定向发送红包")
    public TResult<String> assignRedPack(@ApiIgnore @LoginUser(isFull = false) SysUser user, @RequestBody @Validated RedPackAssignReq redPackAssignDto) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        String id = redPackService.assignRedPack(redPackAssignDto, user.getUserNo(), IPUtil.getIpAddr(request));
        return TResult.success(id);
    }

    @PostMapping("acquire/{id}")
    @SysLog("领取红包")
    @ApiOperation("领取红包")
    public TResult<String> acquireRedPack(@ApiIgnore @LoginUser SysUser user,@PathVariable Long id) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        redPackService.acquireRedPack(id, user.getUserNo(), IPUtil.getIpAddr(request));
        return TResult.success();
    }

    @ApiOperation("查询收取红包详情")
    @GetMapping("sel/{redId}")
    public TResult<RedPackReceiveVo> getRedReceive(@ApiIgnore @LoginUser SysUser user, @PathVariable String redId) {
        RedPackReceiveVo detail = redPackService.getRedReceiveInfo(redId, user.getUserNo());
        return TResult.success(detail);
    }

    @ApiOperation("未领取的零钱红包")
    @GetMapping("unreceived/{groupNo}")
    public TResult<List<RedUnreceivedVo>> redUnreceivedList(@ApiIgnore @LoginUser SysUser user, @PathVariable String groupNo) {
        List<RedUnreceivedVo> list = redPackService.redUnreceivedList(user.getUserNo(), groupNo);
        return TResult.success(list);
    }
}
