package com.youxin.chat.pay.entity.req;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

public class RedPackAssignReq implements Serializable {
    @ApiModelProperty(value = "接收群组ID",required = true)
    private String sendTo;

    @ApiModelProperty(value = "接收对象编码（多个用英文逗号分隔）",required = true)
    private String sendToUserNo;

    @ApiModelProperty(value = "接收对象尤信号（多个用英文逗号分隔）",required = true)
    private String sendToUserName;

    @ApiModelProperty(value = "接收对象昵称（多个用英文逗号分隔）",required = true)
    private String sendToNickName;

    @ApiModelProperty(value = "红包类型 0：限额红包 1：拼手气红包",required = true)
    private Integer type;

    @ApiModelProperty(value = "金额（单位分）",required = true)
    @NotNull(message = "金额不能为空")
    @DecimalMin(value = "0",message = "金额不能为负数")
    @DecimalMax(value = "100000000",message = "金额不能为负数")
    private BigDecimal amount;

    @ApiModelProperty(value = "红包个数",required = true)
    private Integer num;

    @ApiModelProperty(value = "祝福语",required = true)
    private String redMsg;

    @ApiModelProperty(value = "支付密码",required = true)
    private String payPass;

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getRedMsg() {
        return redMsg;
    }

    public void setRedMsg(String redMsg) {
        this.redMsg = redMsg;
    }

    public String getPayPass() {
        return payPass;
    }

    public void setPayPass(String payPass) {
        this.payPass = payPass;
    }

    public String getSendToUserNo() {
        return sendToUserNo;
    }

    public void setSendToUserNo(String sendToUserNo) {
        this.sendToUserNo = sendToUserNo;
    }

    public String getSendToUserName() {
        return sendToUserName;
    }

    public void setSendToUserName(String sendToUserName) {
        this.sendToUserName = sendToUserName;
    }

    public String getSendToNickName() {
        return sendToNickName;
    }

    public void setSendToNickName(String sendToNickName) {
        this.sendToNickName = sendToNickName;
    }
}
