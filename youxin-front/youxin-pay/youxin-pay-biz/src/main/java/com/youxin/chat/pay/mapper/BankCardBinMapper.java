package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.BankCardBin;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface BankCardBinMapper extends BaseMapper<BankCardBin> {

    @Select("select * from t_bank_card_bin where card_bin_no=#{cardBinNo}")
    List<BankCardBin> selectByCardBinNo(@Param("cardBinNo")String cardBinNo);
}
