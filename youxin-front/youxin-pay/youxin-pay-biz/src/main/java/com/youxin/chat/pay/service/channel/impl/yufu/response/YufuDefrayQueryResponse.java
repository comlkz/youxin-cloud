package com.youxin.chat.pay.service.channel.impl.yufu.response;

public class YufuDefrayQueryResponse {

    private String respCode;

    private String respDesc;

    private String version;

    private String merchantId;

    private String merchantOrderId;

    private String bpSerialNum;

    private String transTime;

    /**
     * 01 受理失败
     * 02 受理成功
     * 03 支付中
     * 04 交易失败
     * 05 交易成功
     * 06 交易关闭
     */
    private String transStatus;

    private String isRefund;

    private String refundTime;

    private String msgExt;

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespDesc() {
        return respDesc;
    }

    public void setRespDesc(String respDesc) {
        this.respDesc = respDesc;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public void setMerchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public String getBpSerialNum() {
        return bpSerialNum;
    }

    public void setBpSerialNum(String bpSerialNum) {
        this.bpSerialNum = bpSerialNum;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public String getIsRefund() {
        return isRefund;
    }

    public void setIsRefund(String isRefund) {
        this.isRefund = isRefund;
    }

    public String getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(String refundTime) {
        this.refundTime = refundTime;
    }

    public String getMsgExt() {
        return msgExt;
    }

    public void setMsgExt(String msgExt) {
        this.msgExt = msgExt;
    }
}
