package com.youxin.chat.pay.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class WebUtil {

    private static final Logger logger = LoggerFactory.getLogger(WebUtil.class);

    public static String getParamString(HttpServletRequest request) {
        Map<String, String> map = new HashMap<>();
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String paramName = (String) paramNames.nextElement();

            String[] paramValues = request.getParameterValues(paramName);
            if (paramValues.length == 1) {
                String paramValue = paramValues[0];
                if (paramValue.length() != 0) {
                    map.put(paramName, paramValue);
                }
            }
        }
        StringBuffer sb = new StringBuffer();
        Set<Map.Entry<String, String>> set = map.entrySet();
        for (@SuppressWarnings("rawtypes") Map.Entry entry : set) {
            sb.append(entry.getKey() + ":" + entry.getValue());
        }
        String str = JSON.toJSONString(map);
        logger.info("request params:{}|", str);
        return str;
    }

    public static String getParamStringWithNull(HttpServletRequest request) {
        Map<String, String> map = new HashMap<>();
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String paramName = (String) paramNames.nextElement();

            String[] paramValues = request.getParameterValues(paramName);
            if (paramValues.length == 1) {
                String paramValue = paramValues[0];
                if (paramValue.length() != 0) {
                    map.put(paramName, paramValue);
                }else{
                    map.put(paramName,"");
                }
            }else{
                map.put(paramName,"");

            }
        }
        StringBuffer sb = new StringBuffer();
        Set<Map.Entry<String, String>> set = map.entrySet();
        for (@SuppressWarnings("rawtypes") Map.Entry entry : set) {
            sb.append(entry.getKey() + ":" + entry.getValue());
        }
        String str = JSON.toJSONString(map, SerializerFeature.WriteNullStringAsEmpty);
        logger.info("request params:{}|", str);
        return str;
    }

    public static String getHttpBody(HttpServletRequest request) {
        String data = "";

        try {
            InputStream in = request.getInputStream();
            InputStreamReader isr = new InputStreamReader(in, "utf-8");
            BufferedReader br = new BufferedReader(isr);

            for (String temp = ""; (temp = br.readLine()) != null; data = data + temp) {
                ;
            }
        } catch (IOException var7) {
            var7.printStackTrace();
        }

        if (data.indexOf("BODY_DATA") != -1) {
            try {
                data = URLDecoder.decode(data.substring("BODY_DATA=".length()), "utf-8");
            } catch (UnsupportedEncodingException var6) {
                var6.printStackTrace();
            }
        }

        return data;
    }
}
