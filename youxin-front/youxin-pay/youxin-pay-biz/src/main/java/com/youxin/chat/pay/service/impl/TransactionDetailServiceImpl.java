package com.youxin.chat.pay.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.chat.pay.mapper.TransactionDetailMapper;
import com.youxin.chat.pay.model.TransactionDetail;
import com.youxin.chat.pay.service.TransactionDetailService;
import org.springframework.stereotype.Service;

/**
 * description: TransactionDetailServiceImpl <br>
 * date: 2020/3/1 10:48 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class TransactionDetailServiceImpl extends ServiceImpl<TransactionDetailMapper, TransactionDetail>  implements TransactionDetailService {
    @Override
    public void clearUserRecord(String userNo) {
        TransactionDetail transactionDetail = new TransactionDetail();
        transactionDetail.setUserNo(userNo);
        transactionDetail.setStates("0");
        this.baseMapper.updateDetailState(transactionDetail);
    }
}
