package com.youxin.chat.pay.convert;



import com.youxin.chat.pay.entity.vo.BankInfoVo;
import com.youxin.chat.pay.model.BankInfo;
import org.springframework.util.StringUtils;

import java.util.Optional;

public class BankInfoConvert {

    public static BankInfo dtoToModel(BankInfoVo bankInfoDto) {
        BankInfo bankInfo = new BankInfo();
        if (!Optional.ofNullable(bankInfoDto).isPresent()) {
            return bankInfo;
        }
        bankInfo.setId(bankInfoDto.getId());
        bankInfo.setBankCode(bankInfoDto.getBankCode());
        bankInfo.setBankName(bankInfoDto.getBankName());

        bankInfo.setBankIcon(bankInfoDto.getBankIcon());
        bankInfo.setBankCover(bankInfoDto.getBankCover());
        return bankInfo;
    }

    public static BankInfoVo modelToDto(BankInfo bankInfo) {
        BankInfoVo bankInfoDto = new BankInfoVo();
        if (!Optional.ofNullable(bankInfo).isPresent()) {
            return bankInfoDto;
        }
        bankInfoDto.setId(bankInfo.getId());
        bankInfoDto.setBankCode(bankInfo.getBankCode());
        bankInfoDto.setBankName(bankInfo.getBankName());
        bankInfoDto.setBankIcon(bankInfo.getBankIcon());
        bankInfoDto.setBankCover(bankInfo.getBankCover());
        return bankInfoDto;
    }

    public static BankInfoVo modelToDto(BankInfo bankInfo,String fileUrl) {
        BankInfoVo bankInfoDto = new BankInfoVo();
        if (!Optional.ofNullable(bankInfo).isPresent()) {
            return bankInfoDto;
        }
        bankInfoDto.setId(bankInfo.getId());
        bankInfoDto.setBankCode(bankInfo.getBankCode());
        bankInfoDto.setBankName(bankInfo.getBankName());
        if(!StringUtils.isEmpty(bankInfo.getBankIcon())) {
            bankInfoDto.setBankIcon(fileUrl + bankInfo.getBankIcon());
        }
        if(!StringUtils.isEmpty(bankInfo.getBankCover())) {
            bankInfoDto.setBankCover(fileUrl + bankInfo.getBankCover());
        }
        return bankInfoDto;
    }
}
