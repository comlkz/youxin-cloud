package com.youxin.chat.pay.entity.search;

import java.io.Serializable;

public class UserCardSearch implements Serializable {

    private String userNo;

    private String channelAccount;

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getChannelAccount() {
        return channelAccount;
    }

    public void setChannelAccount(String channelAccount) {
        this.channelAccount = channelAccount;
    }
}
