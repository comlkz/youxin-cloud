package com.youxin.chat.pay.service.channel.impl.liantong.request;

import com.alibaba.fastjson.JSONObject;
import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.model.ChannelOrder;
import com.youxin.chat.pay.model.UserRecharge;
import com.youxin.chat.pay.service.channel.entity.ChannelPay;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LianTongPay {

    public static ChannelPay buildPayInfo(ChannelOrder channelOrder){
        ChannelPay channelPay = new ChannelPay();
        channelPay.setAmount(channelOrder.getRechargeAmount());
        channelPay.setCardId(channelOrder.getCardNo());
        channelPay.setUserNo(channelOrder.getUserNo());
        channelPay.setUserIp(channelOrder.getClientIp());
        channelPay.setOrderNo(channelOrder.getChannelOrderNo());
        channelPay.setContractId(channelOrder.getContractId());
        return channelPay;
    }

    public static JSONObject buildPayRequest(ChannelPay channelPay, ChannelAccount channelAccount){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version","1.0.0");
        jsonObject.put("busiType","PAY");
        jsonObject.put("merNo",channelAccount.getChannelAccount());
        jsonObject.put("merUserId",channelPay.getUserNo());
        jsonObject.put("merOrderNo",channelPay.getOrderNo());
        jsonObject.put("merOrderDate",new SimpleDateFormat("yyyyMMdd").format(new Date()));
        jsonObject.put("signType","RSA_SHA256");
        jsonObject.put("agreeNo",channelPay.getContractId());
        jsonObject.put("amount",channelPay.getAmount());
        jsonObject.put("reqTime",new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
        jsonObject.put("smsNeed","0");

        return jsonObject;
    }

    public static JSONObject buildPayExceptionRequest(String orderNo, ChannelAccount channelAccount){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version","1.0.0");
        jsonObject.put("busiType","PAYQ");
        jsonObject.put("merNo",channelAccount.getChannelAccount());
        jsonObject.put("merOrderNo",orderNo);
        jsonObject.put("merOrderDate",new SimpleDateFormat("yyyyMMdd").format(new Date()));
        jsonObject.put("signType","RSA_SHA256");

        return jsonObject;
    }
}
