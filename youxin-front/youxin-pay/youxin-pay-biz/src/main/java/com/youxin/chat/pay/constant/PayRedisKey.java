package com.youxin.chat.pay.constant;

import com.youxin.common.constant.RedisKey;

/**
 * description: PayRedisKey <br>
 * date: 2020/3/11 2:17 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface PayRedisKey extends RedisKey {

    String WALLET_STATUS = "com.youxin.chat.pay.wallet.status.";
}
