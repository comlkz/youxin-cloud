package com.youxin.chat.pay.service.channel.impl.ainong.request;

import com.alibaba.fastjson.JSONObject;
import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.model.UserRecharge;
import com.youxin.chat.pay.service.channel.entity.ChannelChargeSms;
import com.youxin.chat.pay.service.channel.entity.ChannelPay;
import com.youxin.utils.RandomUtil;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AinongPay {
    public static JSONObject buildReChargeRequest(ChannelPay channelPay, ChannelAccount channelAccount){
        JSONObject reqParams = new JSONObject();
        // 商户订单号
        reqParams.put("merOrderId", channelPay.getOrderNo());
        // 交易时间 yyyyMMddHHmmss
        reqParams.put("txnTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        // 金额单位：分
        reqParams.put("txnAmt", channelPay.getAmount().toString());
        reqParams.put("currency", "CNY");
        // 后台交易结果通知url（生产环境务必写正确）
        reqParams.put("backUrl", channelAccount.getNotifyUrl() + "/notify/" + channelAccount.getChannelCode() + "/recharge/" + channelPay.getOrderNo());

        // reqParams.put("payTimeOut", LocalDateTime.now().plusMinutes(30).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        reqParams.put("accNo", channelPay.getUserCard().getCardNo());
        reqParams.put("customerNm", channelPay.getUserCard().getOwnerName());
        reqParams.put("phoneNo", channelPay.getUserCard().getCardMobile());
        // 证件类型(01-居民身份证 02-军官证 03-护照 04-回乡证 05-台胞证 06-警官证 07-士兵证 99-其他证件)
        reqParams.put("certifTp", "01");
        reqParams.put("certifyId", channelPay.getUserCard().getCardId());

        // 主题
        reqParams.put("subject", RandomUtil.randomStr(20));
        // 预留
        reqParams.put("body", RandomUtil.randomStr(40));
        reqParams.put("merResv1", RandomUtil.randomStr(30));
        return reqParams;
    }

    public static JSONObject buildSmsRequest(ChannelChargeSms channelChargeSms, ChannelAccount channelAccount){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("tn", channelChargeSms.getExt().getString("tn"));
        jsonObject.put("smsCode", channelChargeSms.getValidateCode());
        return jsonObject;
    }

    public static JSONObject buildPayQueryRequest(String orderNo, ChannelAccount channelAccount){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("merOrderId", orderNo);

        return jsonObject;
    }
}
