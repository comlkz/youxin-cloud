package com.youxin.chat.pay.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class BankCardBin implements Serializable {

    private Integer id;

    private String cardBinNo;

    private String cardBinLen;

    private String bankShortName;

    private String bankFullName;

    private String bankCode;

    private String bankCodeEn;

    private Integer cardType;

    private String cardTypeDesc;

    private Integer cardLen;

    private Integer status;

    private LocalDateTime createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCardBinNo() {
        return cardBinNo;
    }

    public void setCardBinNo(String cardBinNo) {
        this.cardBinNo = cardBinNo;
    }

    public String getCardBinLen() {
        return cardBinLen;
    }

    public void setCardBinLen(String cardBinLen) {
        this.cardBinLen = cardBinLen;
    }

    public String getBankShortName() {
        return bankShortName;
    }

    public void setBankShortName(String bankShortName) {
        this.bankShortName = bankShortName;
    }

    public String getBankFullName() {
        return bankFullName;
    }

    public void setBankFullName(String bankFullName) {
        this.bankFullName = bankFullName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankCodeEn() {
        return bankCodeEn;
    }

    public void setBankCodeEn(String bankCodeEn) {
        this.bankCodeEn = bankCodeEn;
    }

    public Integer getCardType() {
        return cardType;
    }

    public void setCardType(Integer cardType) {
        this.cardType = cardType;
    }

    public String getCardTypeDesc() {
        return cardTypeDesc;
    }

    public void setCardTypeDesc(String cardTypeDesc) {
        this.cardTypeDesc = cardTypeDesc;
    }

    public Integer getCardLen() {
        return cardLen;
    }

    public void setCardLen(Integer cardLen) {
        this.cardLen = cardLen;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
}
