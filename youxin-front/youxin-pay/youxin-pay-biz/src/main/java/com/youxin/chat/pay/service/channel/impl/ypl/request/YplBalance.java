package com.youxin.chat.pay.service.channel.impl.ypl.request;

import com.alibaba.fastjson.JSONObject;
import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.utils.RandomUtil;


public class YplBalance {
    public static JSONObject buildBalanceRequest(ChannelAccount channelAccount) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("customerCode", channelAccount.getChannelAccount());
        jsonObject.put("accountType", "2");
        jsonObject.put("nonceStr", RandomUtil.randomStr(32));

        return jsonObject;
    }
}
