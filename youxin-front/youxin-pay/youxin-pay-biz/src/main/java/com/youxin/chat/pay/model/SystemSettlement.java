package com.youxin.chat.pay.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * description: SystemSettlement <br>
 * date: 2020/2/9 12:47 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class SystemSettlement {

    private Long id;

    private BigDecimal userRechargeAmount = new BigDecimal(0);

    private BigDecimal channelRechargeAmount = new BigDecimal(0);

    private BigDecimal rechargeServiceFee = new BigDecimal(0);

    private Integer rechargeCount = 0;

    private BigDecimal userDefrayAmount = new BigDecimal(0);

    private BigDecimal defrayServiceFee = new BigDecimal(0);

    private BigDecimal channelDefrayAmount = new BigDecimal(0);

    private BigDecimal channelDefrayFee = new BigDecimal(0);
    private Integer defrayCount = 0;

    private LocalDateTime createTime;

    private LocalDateTime staticTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getUserRechargeAmount() {
        return userRechargeAmount;
    }

    public void setUserRechargeAmount(BigDecimal userRechargeAmount) {
        this.userRechargeAmount = userRechargeAmount;
    }

    public BigDecimal getChannelRechargeAmount() {
        return channelRechargeAmount;
    }

    public void setChannelRechargeAmount(BigDecimal channelRechargeAmount) {
        this.channelRechargeAmount = channelRechargeAmount;
    }

    public BigDecimal getRechargeServiceFee() {
        return rechargeServiceFee;
    }

    public void setRechargeServiceFee(BigDecimal rechargeServiceFee) {
        this.rechargeServiceFee = rechargeServiceFee;
    }

    public Integer getRechargeCount() {
        return rechargeCount;
    }

    public void setRechargeCount(Integer rechargeCount) {
        this.rechargeCount = rechargeCount;
    }

    public BigDecimal getUserDefrayAmount() {
        return userDefrayAmount;
    }

    public void setUserDefrayAmount(BigDecimal userDefrayAmount) {
        this.userDefrayAmount = userDefrayAmount;
    }

    public BigDecimal getDefrayServiceFee() {
        return defrayServiceFee;
    }

    public void setDefrayServiceFee(BigDecimal defrayServiceFee) {
        this.defrayServiceFee = defrayServiceFee;
    }

    public BigDecimal getChannelDefrayAmount() {
        return channelDefrayAmount;
    }

    public void setChannelDefrayAmount(BigDecimal channelDefrayAmount) {
        this.channelDefrayAmount = channelDefrayAmount;
    }

    public Integer getDefrayCount() {
        return defrayCount;
    }

    public void setDefrayCount(Integer defrayCount) {
        this.defrayCount = defrayCount;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getStaticTime() {
        return staticTime;
    }

    public void setStaticTime(LocalDateTime staticTime) {
        this.staticTime = staticTime;
    }

    public BigDecimal getChannelDefrayFee() {
        return channelDefrayFee;
    }

    public void setChannelDefrayFee(BigDecimal channelDefrayFee) {
        this.channelDefrayFee = channelDefrayFee;
    }
}
