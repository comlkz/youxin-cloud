package com.youxin.chat.pay.entity.req;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

public class TransferCodeReq implements Serializable {

    @ApiModelProperty(value = "金额，单位分",required = true)
    @NotNull(message = "金额不能为空")
    @DecimalMin(value = "0",message = "金额不能为负数")
    @DecimalMax(value = "100000000",message = "金额不能为负数")
    private BigDecimal amount;

    private String userNo;

    @ApiModelProperty(value = "设备ID",required = true)
    private String deviceId;

    @ApiModelProperty(value = "设备类型 0：android 1:IOS",required = true)
    private Integer deviceType;

    @ApiModelProperty(value = "备注",required = false)
    private String remark;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
