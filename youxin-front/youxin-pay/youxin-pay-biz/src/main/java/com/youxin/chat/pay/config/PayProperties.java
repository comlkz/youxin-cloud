package com.youxin.chat.pay.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * description: PayProperties <br>
 * date: 2020/2/29 11:51 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Configuration
@ConfigurationProperties(prefix = "youxin.pay")
public class PayProperties {

    private ChannelProperties channel = new ChannelProperties();

    private RedPackProperties redPack = new RedPackProperties();


    public static class ChannelProperties {

        private String mobileAccount = "";

        private String payAccount = "";

        private String notifyUrl;

        public String getMobileAccount() {
            return mobileAccount;
        }

        public void setMobileAccount(String mobileAccount) {
            this.mobileAccount = mobileAccount;
        }

        public String getPayAccount() {
            return payAccount;
        }

        public void setPayAccount(String payAccount) {
            this.payAccount = payAccount;
        }

        public String getNotifyUrl() {
            return notifyUrl;
        }

        public void setNotifyUrl(String notifyUrl) {
            this.notifyUrl = notifyUrl;
        }
    }

    public static class RedPackProperties {
        private Integer maxCount = 100;

        private String maxAmount = String.valueOf(2000 * 100);

        private Long expireTime = 24 * 60 * 60 * 1000L;

        public Integer getMaxCount() {
            return maxCount;
        }

        public void setMaxCount(Integer maxCount) {
            this.maxCount = maxCount;
        }

        public String getMaxAmount() {
            return maxAmount;
        }

        public void setMaxAmount(String maxAmount) {
            this.maxAmount = maxAmount;
        }

        public Long getExpireTime() {
            return expireTime;
        }

        public void setExpireTime(Long expireTime) {
            this.expireTime = expireTime;
        }
    }

    public ChannelProperties getChannel() {
        return channel;
    }

    public RedPackProperties getRedPack() {
        return redPack;
    }
}
