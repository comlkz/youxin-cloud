package com.youxin.chat.pay.service.channel.entity;

import java.io.Serializable;

public class PayChannelDto implements Serializable {

    private String channelCode;

    private String channelName;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}
