package com.youxin.chat.pay.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface SequenceMapper {

    @Select(" select orderno(#{sequence})")
    String selectMaxOrderNo(@Param("sequence") String sequence);

}
