package com.youxin.chat.pay.service.channel.entity;

import java.io.Serializable;

public class ChannelBindCardSms implements Serializable {
    /**
     * 短信验证码
     */
    private String validateCode;


    /**
     * 拓展字段
     */
    private String ext;

    private String userNo;

    private String orderNo;

    public String getValidateCode() {
        return validateCode;
    }

    public void setValidateCode(String validateCode) {
        this.validateCode = validateCode;
    }



    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
