package com.youxin.chat.pay.entity.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class TransferInfoVo implements Serializable {

    @ApiModelProperty("用户编码")
    private String userNo;

    @ApiModelProperty("用户昵称")
    private String nickName;

    @ApiModelProperty("收款金额")
    private BigDecimal amount;

    @ApiModelProperty("用户头像")
    private String avatar;

    @ApiModelProperty("转帐编码")
    private String code;

    @ApiModelProperty("备注")
    private String remark;

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
