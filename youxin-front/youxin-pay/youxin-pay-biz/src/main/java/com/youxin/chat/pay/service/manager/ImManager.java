package com.youxin.chat.pay.service.manager;

import com.alibaba.fastjson.JSONObject;
import com.youxin.base.BaseResultCode;
import com.youxin.chat.pay.service.manager.message.PayNotifyMessage;
import com.youxin.chat.pay.service.manager.message.RedPackReceiveMessage;
import com.youxin.exception.SystemException;
import io.rong.RongCloud;
import io.rong.models.message.GroupMessage;
import io.rong.models.message.PrivateMessage;
import io.rong.models.message.SystemMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * description: ImManager <br>
 * date: 2020/2/29 15:54 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class ImManager {

    private static final Logger logger = LoggerFactory.getLogger(ImManager.class);
    @Resource
    private RongCloud rongCloud;

    public String getSystemNo(){
        return "10002";
    }

    public void sendGroupRedReceiveMsg(String fromUserId, String toUserId, Map<String, Object> map) throws SystemException {
        String type;
        String operation = "receiveRedpack";
        String redPackId = (String) map.get("redPackId");
        String msg = (String) map.get("msg");
        Map<String, String> user = (Map<String, String>) map.get("user");
        RedPackReceiveMessage redPacketMsg = new RedPackReceiveMessage(fromUserId, operation, redPackId, msg, user);
        GroupMessage messageModel = new GroupMessage();
        messageModel.setSenderId(fromUserId);
        messageModel.setTargetId(new String[]{toUserId});
        messageModel.setObjectName("GR:RedPacketReceiveMsg");
        messageModel.setContent(redPacketMsg);
        try {
            logger.info("发送红包消息，message={}", JSONObject.toJSONString(messageModel));

            rongCloud.message.group.send(messageModel);
        } catch (Exception e) {
            logger.error("发送红包消息，message={}", JSONObject.toJSONString(messageModel), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void sendPrivateRedReceiveMsg(String fromUserId, String toUserId, Map<String, Object> map) throws SystemException {

        String operation = "receiveRedpack";
        String redPackId = (String) map.get("redPackId");
        String msg = (String) map.get("msg");
        Map<String, String> user = (Map<String, String>) map.get("user");
        RedPackReceiveMessage redPacketMsg = new RedPackReceiveMessage(fromUserId, operation, redPackId, msg, user);
        PrivateMessage messageModel = new PrivateMessage();
        messageModel.setSenderId(fromUserId);
        messageModel.setTargetId(new String[]{toUserId});
        messageModel.setObjectName("GR:RedPacketReceiveMsg");
        messageModel.setContent(redPacketMsg);
        try {
            rongCloud.message.msgPrivate.send(messageModel);
            logger.info("发送红包消息，message={}", JSONObject.toJSONString(messageModel));

        } catch (Exception e) {
            logger.error("发送红包消息，message={}", JSONObject.toJSONString(messageModel), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void sendPayMsg(String toUserId, Map<String, Object> map,String extra) throws SystemException {
        String systemId = getSystemNo();
        PayNotifyMessage notifyMessage = new PayNotifyMessage(toUserId, "payNotify", map,extra);
        SystemMessage messageModel = new SystemMessage();
        messageModel.setSenderId(systemId);
        messageModel.setTargetId(new String[]{toUserId});
        messageModel.setObjectName("GR:PayNotifyMsg");
        messageModel.setContent(notifyMessage);
        try {
            rongCloud.message.system.send(messageModel);
        } catch (Exception e) {
            logger.error("发送系统通知消息，message={}", JSONObject.toJSONString(messageModel), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }
}
