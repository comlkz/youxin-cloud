package com.youxin.chat.pay.service.channel.impl.yufu.request;

public class YufuDefrayQueryRequest {

    /**
     * 固定值：1.0.0
     */
    private String version;

    /**
     * 商户代码
     */
    private String merchantId;

    /**
     * 商户订单号
     */
    private String merchantOrderId;

    /**
     * 业务流水号
     */
    private String bpSerialNum;

    private String msgExt;

    private String misc;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public void setMerchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public String getBpSerialNum() {
        return bpSerialNum;
    }

    public void setBpSerialNum(String bpSerialNum) {
        this.bpSerialNum = bpSerialNum;
    }

    public String getMsgExt() {
        return msgExt;
    }

    public void setMsgExt(String msgExt) {
        this.msgExt = msgExt;
    }

    public String getMisc() {
        return misc;
    }

    public void setMisc(String misc) {
        this.misc = misc;
    }
}
