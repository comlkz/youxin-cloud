package com.youxin.chat.pay.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.Set;

/**
 * Class Describe
 * <p>
 * User: yangguang Date: 17/7/29 Time: 上午11:46
 */
public class ParamStringUtils {
    private static Logger logger = LoggerFactory.getLogger(ParamStringUtils.class);

    public static String toSignStr(JSONObject jo, final String filterKey, String symbol) {
        if (jo == null) {
            return "";
        }
        Set<String> keys = jo.keySet();
        // List<String> orderKeys = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        // 除Sign外其他参数都按参数名排序进行参数值拼接后
        keys.stream().sorted()
                .filter(s -> (!(jo.get(s) instanceof JSONObject || jo.get(s) instanceof JSONArray
                        || (filterKey.toLowerCase().indexOf(s.toLowerCase()) > -1))
                        && !StringUtils.isEmpty(jo.getString(s)) && !"null".equalsIgnoreCase( jo.getString(s)))).
                forEach(s -> sb.append(s + "=" + jo.getString(s)).append(symbol));
        sb.deleteCharAt(sb.length() - 1);
        logger.info(
                "cmd=SignUtils:toSign msg=original Sign:" + sb.toString() + "  req param:" + JSON.toJSONString(jo));

        return sb.toString();
    }



    public static String toSignStrIncludEmpty(JSONObject jo, final String filterKey) {
        if (jo == null) {
            return "";
        }
        Set<String> keys = jo.keySet();
        // List<String> orderKeys = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        // 除Sign外其他参数都按参数名排序进行参数值拼接后
        keys.stream().sorted()
                .filter(s -> (!(jo.get(s) instanceof JSONObject || jo.get(s) instanceof JSONArray
                        || (filterKey.toLowerCase().indexOf(s.toLowerCase()) > -1)))).
                forEach(s -> sb.append(s + "=").append(jo.getString(s)).append("&"));
        sb.deleteCharAt(sb.length() - 1);
        logger.info(
                "cmd=SignUtils:toSign msg=original Sign:" + sb.toString() + "  req param:" + JSON.toJSONString(jo));

        return sb.toString();
    }

    public static String shBankSign(JSONObject jo, final String filterKey) {
        if (jo == null) {
            return "";
        }
        Set<String> keys = jo.keySet();
        StringBuilder sb = new StringBuilder();
        // 除Sign外其他参数都按参数名排序进行参数值拼接后
        keys.stream().sorted(String.CASE_INSENSITIVE_ORDER)
                .filter(s -> (!(jo.get(s) instanceof JSONObject || jo.get(s) instanceof JSONArray
                        || (filterKey.toLowerCase().indexOf(s.toLowerCase()) > -1))
                        && !StringUtils.isEmpty(jo.getString(s)) && !jo.getString(s).equalsIgnoreCase("null"))).
                forEach(s -> sb.append(jo.getString(s)));
        logger.info(
                "上海银行签名数据源 Sign:" + sb.toString() + "  req param:" + JSON.toJSONString(jo));
        return sb.toString();
    }


    public static String toSignStrIgnoreCase(JSONObject jo, String filterKey) {
        if (jo == null) {
            return "";
        }
        Set<String> keys = jo.keySet();
        // List<String> orderKeys = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        // 除Sign外其他参数都按参数名排序进行参数值拼接后
        keys.stream().sorted(Comparator.comparing(String::toLowerCase))
                .filter(s -> (!(jo.get(s) instanceof JSONObject || jo.get(s) instanceof JSONArray
                        || (filterKey.toLowerCase().indexOf(s.toLowerCase()) > -1))
                        && !StringUtils.isEmpty(jo.getString(s)) && !"null".equalsIgnoreCase( jo.getString(s)))).
                forEach(s -> sb.append(s + "=" + jo.getString(s)).append("&"));
        sb.deleteCharAt(sb.length() - 1);
        logger.info(
                "cmd=SignUtils:toSign msg=original Sign:" + sb.toString() + "  req param:" + JSON.toJSONString(jo));

        return sb.toString();
    }

    public static String toSignStr4Value(JSONObject jo, final String filterKey) {
        if (jo == null) {
            return "";
        }
        Set<String> keys = jo.keySet();
        // List<String> orderKeys = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        // 除Sign外其他参数都按参数名排序进行参数值拼接后
        keys.stream().sorted()
                .filter(s -> (!(jo.get(s) instanceof JSONObject || jo.get(s) instanceof JSONArray
                        || (filterKey.toLowerCase().indexOf(s.toLowerCase()) > -1))
                        && !StringUtils.isEmpty(jo.getString(s)))).
                forEach(s -> sb.append(jo.getString(s)));
        logger.info("cmd=SignUtils:toSignStr4Value msg=original Sign:" + sb.toString() + "  req param:" + JSON.toJSONString(jo));

        return sb.toString();
    }

}
