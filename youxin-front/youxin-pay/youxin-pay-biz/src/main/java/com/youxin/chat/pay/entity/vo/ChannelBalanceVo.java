package com.youxin.chat.pay.entity.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class ChannelBalanceVo implements Serializable {
    @ApiModelProperty(value = "余额")
    private String balance;
    @ApiModelProperty(value = "可用余额")
    private String availableBalance;
    @ApiModelProperty(value = "冻结金额")
    private String lockAmount;

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(String availableBalance) {
        this.availableBalance = availableBalance;
    }

    public String getLockAmount() {
        return lockAmount;
    }

    public void setLockAmount(String lockAmount) {
        this.lockAmount = lockAmount;
    }
}
