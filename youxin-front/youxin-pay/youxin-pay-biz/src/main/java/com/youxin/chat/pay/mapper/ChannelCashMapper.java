package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.youxin.chat.pay.model.ChannelCash;
import com.youxin.chat.pay.model.SystemSettlement;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;

public interface ChannelCashMapper extends BaseMapper<ChannelCash> {
    int doCashFinish(@Param("status") Integer status, @Param("cashNo") String cashNo);

}
