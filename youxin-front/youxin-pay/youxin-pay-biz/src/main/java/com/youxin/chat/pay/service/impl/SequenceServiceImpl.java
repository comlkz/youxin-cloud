package com.youxin.chat.pay.service.impl;

import com.youxin.chat.pay.mapper.SequenceMapper;
import com.youxin.chat.pay.service.SequenceService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * description: SequenceServiceImpl <br>
 * date: 2020/2/29 12:36 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class SequenceServiceImpl implements SequenceService {

    public static final String USER_ORDER_SEQ = "user_order_seq";

    public static final String CHANNEL_ORDER_SEQ = "channel_order_seq";

    public static final String MOBILE_CHARGE_SEQ = "mobile_charge_seq";

    public static final String USER_CASH_SEQ = "user_cash_seq";


    @Resource
    private SequenceMapper sequenceMapper;

    @Override
    public String buildUserOrderNo() {
        StringBuffer stringBuffer = new StringBuffer("PAY");
        String maxNo = sequenceMapper.selectMaxOrderNo(USER_ORDER_SEQ);
        String millis = LocalDateTime.now().format(DateTimeFormatter.ofPattern("SSS"));
        stringBuffer.append(maxNo).append(millis);
        return stringBuffer.toString();
    }

    @Override
    public String buildChannelOrderNo() {
        StringBuffer stringBuffer = new StringBuffer("CH");
        String maxNo = sequenceMapper.selectMaxOrderNo(CHANNEL_ORDER_SEQ);
        String millis = LocalDateTime.now().format(DateTimeFormatter.ofPattern("SSS"));
        stringBuffer.append(maxNo).append(millis);
        return stringBuffer.toString();
    }

    @Override
    public String buildThirdOrderNo(String prefix) {
        prefix = StringUtils.isEmpty(prefix) ? "TD" : prefix;
        StringBuffer stringBuffer = new StringBuffer(prefix);
        String maxNo = sequenceMapper.selectMaxOrderNo(MOBILE_CHARGE_SEQ);
        String millis = LocalDateTime.now().format(DateTimeFormatter.ofPattern("SSS"));
        stringBuffer.append(maxNo).append(millis);
        return stringBuffer.toString();
    }

    @Override
    public String buildUserCashNo() {
        StringBuffer stringBuffer = new StringBuffer("DF");
        String maxNo = sequenceMapper.selectMaxOrderNo(USER_CASH_SEQ);
        String millis = LocalDateTime.now().format(DateTimeFormatter.ofPattern("SSS"));
        stringBuffer.append(maxNo).append(millis);
        return stringBuffer.toString();
    }
}
