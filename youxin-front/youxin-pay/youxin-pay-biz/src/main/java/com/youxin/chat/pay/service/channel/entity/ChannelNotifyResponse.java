package com.youxin.chat.pay.service.channel.entity;

import java.io.Serializable;

public class ChannelNotifyResponse implements Serializable {

    private String responseText;

    /**
     *
     */
    private Integer status;

    private String orderNo;

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
