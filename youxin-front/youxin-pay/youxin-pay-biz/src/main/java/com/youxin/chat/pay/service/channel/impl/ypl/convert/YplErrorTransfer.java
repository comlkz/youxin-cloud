package com.youxin.chat.pay.service.channel.impl.ypl.convert;

import com.youxin.base.BaseResultCode;
import com.youxin.chat.pay.enums.PayResultEnum;
import com.youxin.chat.pay.service.channel.entity.ChannelResponse;

public class YplErrorTransfer {

    public static ChannelResponse buildErrorResult(String code, String errorMsg){
         if("0227".equalsIgnoreCase(code)){
             return ChannelResponse.build(PayResultEnum.CARD_BALANCE_NOT_ENOUGH.getCode(),PayResultEnum.CARD_BALANCE_NOT_ENOUGH.getMsg());
         }
        if("09190".equalsIgnoreCase(code)){
            return ChannelResponse.build(PayResultEnum.ACTION_NOT_SUPPORT.getCode(),PayResultEnum.ACTION_NOT_SUPPORT.getMsg());
        }
        if("0906".equalsIgnoreCase(code)){
            return ChannelResponse.build(PayResultEnum.CARD_BALANCE_NOT_ENOUGH.getCode(),PayResultEnum.CARD_BALANCE_NOT_ENOUGH.getMsg());
        }
        if("0916".equalsIgnoreCase(code)){
            return ChannelResponse.build(PayResultEnum.PAY_FAIL.getCode(),PayResultEnum.PAY_FAIL.getMsg());
        }
        if("09142".equalsIgnoreCase(code)){
            return ChannelResponse.build(PayResultEnum.MOBILE_NOT_MATCH.getCode(),PayResultEnum.MOBILE_NOT_MATCH.getMsg());

        }
        return ChannelResponse.build(BaseResultCode.COMMON_FAIL,errorMsg);
    }
}
