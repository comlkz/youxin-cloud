package com.youxin.chat.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.pay.model.TransactionDetail;

/**
 * description: TransactionDetailService <br>
 * date: 2020/3/1 10:48 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface TransactionDetailService extends IService<TransactionDetail> {

    void clearUserRecord(String userNo);
}
