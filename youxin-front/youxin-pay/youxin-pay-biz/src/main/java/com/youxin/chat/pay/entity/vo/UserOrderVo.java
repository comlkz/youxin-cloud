package com.youxin.chat.pay.entity.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

public class UserOrderVo implements Serializable {
    @ApiModelProperty(value = "收入")
    private String income;
    @ApiModelProperty(value = "支出")
    private String expend;
    @ApiModelProperty(value = "列表")
    private List<UserOrderItem> list;

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getExpend() {
        return expend;
    }

    public void setExpend(String expend) {
        this.expend = expend;
    }

    public List<UserOrderItem> getList() {
        return list;
    }

    public void setList(List<UserOrderItem> list) {
        this.list = list;
    }
}
