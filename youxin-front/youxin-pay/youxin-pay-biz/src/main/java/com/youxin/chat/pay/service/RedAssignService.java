package com.youxin.chat.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.pay.model.RedAssign;

/**
 * description: RedAssignService <br>
 * date: 2020/2/29 15:26 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface RedAssignService extends IService<RedAssign> {
}
