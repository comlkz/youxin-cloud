package com.youxin.chat.pay.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.youxin.chat.pay.entity.vo.UserRechargeVo;
import com.youxin.chat.pay.model.UserRecharge;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface UserRechargeMapper extends BaseMapper<UserRecharge> {

    int changeOrderStatus(@Param("orderNo") String orderNo, @Param("status") Integer status, @Param("initStatus") Integer initStatus);

   // IPage<UserRechargeDto> selectRechargeList(@Param("userRechargeDto") UserRechargeDto userRechargeDto, Page page);

    List<UserRechargeVo> selectRechargeRecord(@Param("userNo") String userNo);

    BigDecimal selectDayRechargeAmount(@Param("userNo") String userNo, @Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);

}
