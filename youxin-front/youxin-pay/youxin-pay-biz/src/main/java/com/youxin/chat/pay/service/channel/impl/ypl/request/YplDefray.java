package com.youxin.chat.pay.service.channel.impl.ypl.request;

import com.alibaba.fastjson.JSONObject;
import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.model.UserCash;
import com.youxin.chat.pay.service.channel.entity.ChannelDefray;
import com.youxin.chat.pay.service.channel.impl.ypl.util.YplSignUtil;
import com.youxin.utils.RandomUtil;


public class YplDefray {

    public static JSONObject buildDefrayRequest(ChannelDefray channelDefray, ChannelAccount channelAccount) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("outTradeNo", channelDefray.getOrderNo());
        jsonObject.put("customerCode", channelAccount.getChannelAccount());
        jsonObject.put("amount", channelDefray.getAmount());
        jsonObject.put("bankUserName", YplSignUtil.encryptByPublicKey(channelDefray.getReceiveName(), channelAccount.getRsaKeyPub()));
        jsonObject.put("bankCardNo", YplSignUtil.encryptByPublicKey(channelDefray.getCardNo(), channelAccount.getRsaKeyPub()));
        jsonObject.put("bankName", channelDefray.getBankName());
        jsonObject.put("bankAccountType", "2");
        jsonObject.put("payCurrency", "CNY");
        String notifyUrl = channelAccount.getNotifyUrl() + "/notify/" + channelAccount.getChannelCode() + "/defray/" + channelDefray.getOrderNo();
        jsonObject.put("notifyUrl", notifyUrl);
        jsonObject.put("nonceStr", RandomUtil.randomStr(32));

        return jsonObject;
    }

    public static JSONObject buildDefrayQueryRequest(UserCash userCash, ChannelAccount channelAccount) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("customerCode", channelAccount.getChannelAccount());
        jsonObject.put("outTradeNo", userCash.getCashNo());
        jsonObject.put("nonceStr", RandomUtil.randomStr(32));

        return jsonObject;
    }
}
