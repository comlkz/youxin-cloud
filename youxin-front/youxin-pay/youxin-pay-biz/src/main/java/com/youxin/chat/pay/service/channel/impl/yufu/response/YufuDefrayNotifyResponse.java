package com.youxin.chat.pay.service.channel.impl.yufu.response;

public class YufuDefrayNotifyResponse {

    /**
     * 接口版本号
     */
    private String version;

    /**
     * 商户代码
     */
    private String merchantId;

    /**
     * 商户订单号
     */
    private String merchantOrderId;

    /**
     * 商户订单时间
     */
    private String merchantOrderTime;

    /**
     * 通知类型 01-代付  02-退票
     */
    private String notifyType;

    /**
     * 交易时间
     */
    private String transTime;

    /**
     * 交易状态
     * 04 交易失败
     * 05 交易成功
     */
    private String transStatus;

    /**
     * 业务流水号
     */
    private String bpSerialNum;

    /**
     * 商户保留域
     */
    private String msgExt;

    private String respCode;

    private String respDesc;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public void setMerchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public String getMerchantOrderTime() {
        return merchantOrderTime;
    }

    public void setMerchantOrderTime(String merchantOrderTime) {
        this.merchantOrderTime = merchantOrderTime;
    }

    public String getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(String notifyType) {
        this.notifyType = notifyType;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public String getBpSerialNum() {
        return bpSerialNum;
    }

    public void setBpSerialNum(String bpSerialNum) {
        this.bpSerialNum = bpSerialNum;
    }

    public String getMsgExt() {
        return msgExt;
    }

    public void setMsgExt(String msgExt) {
        this.msgExt = msgExt;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespDesc() {
        return respDesc;
    }

    public void setRespDesc(String respDesc) {
        this.respDesc = respDesc;
    }
}
