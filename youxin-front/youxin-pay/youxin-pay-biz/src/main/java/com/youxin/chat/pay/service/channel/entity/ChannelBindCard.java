package com.youxin.chat.pay.service.channel.entity;

import java.io.Serializable;

public class ChannelBindCard implements Serializable {
    private String userNo;//用户标识

    private String cardNo;//用户卡号

    private String ownerName;//用户姓名

    private String cardId;//用户身分证号

    private String cardMobile;//绑卡手机号

    /**
     * 卡类型 0：储蓄卡  1：信用卡
     */
    private Integer cardType;

    /**
     * 协议ID
     */
    private String contractId;

    /**
     * 渠道帐号
     */
    private String channelAccount;

    /**
     * 对方ip
     */
    private String userIp;

    /**
     * 银行编码
     */
    private String bankNo;

    /**
     * 拓展字段
     */
    private String ext;

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardMobile() {
        return cardMobile;
    }

    public void setCardMobile(String cardMobile) {
        this.cardMobile = cardMobile;
    }

    public Integer getCardType() {
        return cardType;
    }

    public void setCardType(Integer cardType) {
        this.cardType = cardType;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getChannelAccount() {
        return channelAccount;
    }

    public void setChannelAccount(String channelAccount) {
        this.channelAccount = channelAccount;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }
}
