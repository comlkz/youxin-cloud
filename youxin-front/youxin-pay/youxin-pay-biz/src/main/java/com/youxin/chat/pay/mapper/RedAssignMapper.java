package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.RedAssign;

public interface RedAssignMapper extends BaseMapper<RedAssign> {
}
