package com.youxin.chat.pay.entity.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class ForgetPayPassReq implements Serializable {

    @ApiModelProperty(value = "验证码", required = true)
    private String code;
    @ApiModelProperty(value = "新支付密码", required = true)
    private String newPass;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }
}
