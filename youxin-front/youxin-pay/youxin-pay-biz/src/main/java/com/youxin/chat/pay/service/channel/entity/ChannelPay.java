package com.youxin.chat.pay.service.channel.entity;


import java.io.Serializable;
import java.math.BigDecimal;

public class ChannelPay implements Serializable {

    /**
     * 银行卡号
     */
    private String cardId;

    private BigDecimal amount;

    private String userIp;

    private String contractId;//协议号

    private String orderNo;//订单号

    private String userNo;

    private UserCardDto userCard;
    /**
     * 拓展字段
     */
    private String ext;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public UserCardDto getUserCard() {
        return userCard;
    }

    public void setUserCard(UserCardDto userCard) {
        this.userCard = userCard;
    }
}
