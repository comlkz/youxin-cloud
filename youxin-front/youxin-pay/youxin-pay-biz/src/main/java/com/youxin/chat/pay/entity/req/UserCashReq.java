package com.youxin.chat.pay.entity.req;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

public class UserCashReq implements Serializable {

    @ApiModelProperty(value = "提现金额",required = true)
    @NotNull(message = "提现金额不能为空")
    @DecimalMin(value = "0",message = "金额不能为负数")
    @DecimalMax(value = "100000000",message = "金额不能为负数")
    private BigDecimal amount;

    @ApiModelProperty(value = "提现银行卡ID",required = true)
    @NotNull(message = "提现银行卡ID不能为空")
    private String cardId;

    @ApiModelProperty(value = "提现备注",required = false)
    private String remark;

    @ApiModelProperty(hidden = true)
    private String clientIp;

    @ApiModelProperty(value = "支付密码",required = true)
    @NotNull(message = "支付密码不能为空")
    private String payPwd;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getPayPwd() {
        return payPwd;
    }

    public void setPayPwd(String payPwd) {
        this.payPwd = payPwd;
    }
}
