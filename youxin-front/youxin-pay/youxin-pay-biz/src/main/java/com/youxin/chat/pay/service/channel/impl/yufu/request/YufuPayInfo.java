package com.youxin.chat.pay.service.channel.impl.yufu.request;

public class YufuPayInfo {

    /**
     * 收款银行户名
     */
    private String accountName;

    /**
     * 收款银行帐号
     */
    private String accountNo;

    /**
     * 收款开户银行名 称
     */
    private String bankName;

    /**
     * 收款银行所在省 份
     */
    private String province;

    /**
     * 收款银行所在市
     */
    private String city;

    /**
     * 收款支行名称
     */
    private String branchName;

    /**
     * 金额 单位分
     */
    private String amt;

    /**
     * 账户属性 对公-01、对私-02
     */
    private String pblFlag;

    private String remark;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getPblFlag() {
        return pblFlag;
    }

    public void setPblFlag(String pblFlag) {
        this.pblFlag = pblFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
