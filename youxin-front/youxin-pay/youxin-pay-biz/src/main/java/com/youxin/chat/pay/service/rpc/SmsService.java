package com.youxin.chat.pay.service.rpc;

import com.youxin.chat.basic.client.ISmsClient;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

/**
 * description: SmsService <br>
 * date: 2020/2/29 12:11 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class SmsService {

    @Reference
    private ISmsClient smsClient;


    public void sendSms(Integer type, String phone, String content, Long ip){
        smsClient.sendSms(type, phone, content, ip);
    };

}
