package com.youxin.chat.pay.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.UserWallet;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;

public interface UserWalletMapper extends BaseMapper<UserWallet> {

    int doPaySuccess(@Param("rechargeAmount") BigDecimal rechargeAmount, @Param("userNo") String userNo);

    int doSendRedPack(@Param("amount") BigDecimal amount, @Param("userNo") String userNo);

    int doReceiveRedPack(@Param("amount") BigDecimal amount, @Param("userNo") String userNo);

    int doExpireRedPack(@Param("amount") BigDecimal amount, @Param("userNo") String userNo);

    int doCashAudit(@Param("amount") BigDecimal amount, @Param("userNo") String userNo);

    int doCashSuccess(@Param("amount") BigDecimal amount, @Param("userNo") String userNo);

    int doCashFail(@Param("amount") BigDecimal amount, @Param("userNo") String userNo);

    int doChargeSuccess(@Param("amount") BigDecimal amount, @Param("userNo") String userNo);

    int doIncreaseAmount(@Param("amount") BigDecimal amount, @Param("userNo") String userNo);

    int doDecreaseAmount(@Param("amount") BigDecimal amount, @Param("userNo") String userNo);

    @Select("select * from t_user_wallet where user_no=#{userNo}")
    UserWallet selectUserWalleByUserNo(@Param("userNo")String userNo);

    @Select("select wallet_status from t_user_wallet where user_no=#{userNo}")
    Integer selectWalletStatus(@Param("userNo")String userNo);

    @Update("update t_user_wallet set wallet_status=#{status} where user_no=#{userNo}")
    int updateWalletStatus(@Param("userNo")String userNo,@Param("status")Integer status);

    @Update("update t_user_wallet set pay_pwd=#{pass} where user_no=#{userNo}")
    int updateWalletPass(@Param("userNo")String userNo,@Param("pass")String pass);
}
