package com.youxin.chat.pay.entity.search;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class CashSearch implements Serializable {

    private String userNo;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private List<Integer> statuses;

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public List<Integer> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<Integer> statuses) {
        this.statuses = statuses;
    }
}
