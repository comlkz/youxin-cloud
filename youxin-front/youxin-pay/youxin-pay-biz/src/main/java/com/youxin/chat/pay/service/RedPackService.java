package com.youxin.chat.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.pay.entity.req.RedPackAssignReq;
import com.youxin.chat.pay.entity.req.RedPackSendReq;
import com.youxin.chat.pay.entity.vo.RedPackReceiveVo;
import com.youxin.chat.pay.entity.vo.RedUnreceivedVo;
import com.youxin.exception.SystemException;

import java.util.List;

/**
 * description: RedPackService <br>
 * date: 2020/2/29 15:06 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface RedPackService {

    String sendRedPack(RedPackSendReq redPackSendDto, String userNo, String ip) throws SystemException;

    String assignRedPack(RedPackAssignReq redPackAssignDto, String userNo, String ip) throws SystemException;

    void acquireRedPack(Long id, String userNo,String ip) throws SystemException;

    RedPackReceiveVo getRedReceiveInfo(String redId, String userNo) throws SystemException;

    List<RedUnreceivedVo> redUnreceivedList(String userNo, String groupNo) throws SystemException;

}
