package com.youxin.chat.pay.service.channel.impl.yufu.request;

import com.alibaba.fastjson.JSONObject;
import com.youxin.chat.pay.model.ChannelAccount;

public class YufuBalance {
    public static JSONObject buildBalanceRequest(ChannelAccount channelAccount){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version","1.0.0");
        jsonObject.put("merchantId",channelAccount.getChannelAccount());
        return jsonObject;
    }
}
