package com.youxin.chat.pay.entity.req;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RedPackSendReq implements Serializable {

    @ApiModelProperty(value = "接收对象",required = true)
    @NotNull(message = "接收对象不能为空")
    private String sendTo;

    @ApiModelProperty(value = "红包类型 0：普通红包 1：拼手气红包  2：单个红包",required = true)
    @NotNull(message = "红包类型不能为空")
    private Integer type;

    @ApiModelProperty(value = "金额（单位分）",required = true)
    @NotNull(message = "金额不能为空")
    @DecimalMin(value = "0",message = "金额不能为负数")
    @DecimalMax(value = "100000000",message = "金额不能为负数")
    private BigDecimal amount;

    @ApiModelProperty(value = "红包个数",required = true)
    @NotNull(message = "红包个数不能为空")
    private Integer num;

    @ApiModelProperty(value = "祝福语",required = true)
    private String redMsg;

    @ApiModelProperty(value = "支付密码",required = true)
    @NotNull(message = "支付密码不能为空")
    private String payPass;

    @ApiModelProperty(value = "红包接收人")
    private List<String> sendToUserNos;

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getRedMsg() {
        return redMsg;
    }

    public void setRedMsg(String redMsg) {
        this.redMsg = redMsg;
    }

    public String getPayPass() {
        return payPass;
    }

    public void setPayPass(String payPass) {
        this.payPass = payPass;
    }

    public List<String> getSendToUserNos() {
        return sendToUserNos;
    }

    public void setSendToUserNos(List<String> sendToUserNos) {
        this.sendToUserNos = sendToUserNos;
    }
}
