package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.entity.dto.CardBindInfo;
import com.youxin.chat.pay.entity.search.UserCardSearch;
import com.youxin.chat.pay.entity.vo.UserCardVo;
import com.youxin.chat.pay.model.UserCard;
import com.youxin.chat.pay.service.channel.entity.UserCardDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.io.Serializable;
import java.util.List;

public interface UserCardMapper extends BaseMapper<UserCard> {

    List<UserCardVo> getUserCardList(@Param("userNo") String userNo);

    List<CardBindInfo> getCardListByParam(@Param("record") UserCardSearch userCardSearch);

    @Select("select * from t_user_card where card_no=#{cardNo} and user_no=#{userNo} and is_deleted = 0")
    UserCard selectByCardNoAndUserNo(@Param("userNo") String userNo, @Param("cardNo") String cardNo);

   @Update("update t_user_card set is_deleted = 1 where id=#{id}")
    int deleteById(@Param("id") Serializable id);
}
