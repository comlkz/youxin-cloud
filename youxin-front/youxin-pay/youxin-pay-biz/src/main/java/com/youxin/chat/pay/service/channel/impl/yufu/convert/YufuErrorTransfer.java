package com.youxin.chat.pay.service.channel.impl.yufu.convert;


import com.youxin.base.BaseResultCode;
import com.youxin.chat.pay.service.channel.entity.ChannelResponse;

public class YufuErrorTransfer {
    public static ChannelResponse buildPayErrorResult(String code, String errorMsg) {
        if ("1000019".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1000019", "商户订单金额为空");
        }
        if ("1000055".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1000055", "商城优惠金额不小于商户订单金额");
        }
        if ("1000037".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1000037", "不支持当前商户交易");
        }
        if ("1000062".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1000062", "风险审核中");
        }
        if ("1000039".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1000039", "商户订单金额不正确");
        }
        if ("1000117".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1000117", "发送短信异常");
        }
        if ("1000119".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1000119", "短信验证码错误");
        }
        if ("1000121".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1000121", "不支持的卡类型");
        }
        if ("1000122".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1000122", "非此用户绑定的卡");
        }
        if ("1000123".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1000123", "卡信息有误，请核对后重新输入");
        }
        if ("0013020".equalsIgnoreCase(code)) {
            return ChannelResponse.build("0013020", "该卡已被绑定");
        }
        if ("0013032".equalsIgnoreCase(code)) {
            return ChannelResponse.build("0013032", "证件号错误");
        }
        if ("0010006".equalsIgnoreCase(code)) {
            return ChannelResponse.build("0010006", "短信验证码错误");
        }
        if ("0030014".equalsIgnoreCase(code)) {
            return ChannelResponse.build("0030014", "身份证号码错误");
        }
        return ChannelResponse.build(BaseResultCode.COMMON_FAIL, errorMsg);

    }

    public static ChannelResponse buildDefrayErrorResult(String code, String errorMsg) {
        if ("RT-0002".equalsIgnoreCase(code)) {
            return ChannelResponse.build("RT-0002", "账户属性(对公)暂不支持");
        }
        if ("RT-0005".equalsIgnoreCase(code)) {
            return ChannelResponse.build("RT-0005", "缺少配置(计费方式)");
        }
        if ("RT-0009".equalsIgnoreCase(code)) {
            return ChannelResponse.build("RT-0009", "交易订单不存在");
        }
        if ("RT-0010".equalsIgnoreCase(code)) {
            return ChannelResponse.build("RT-0010", "平台限额超限/产品限额超限");
        }
        if ("RT-0011".equalsIgnoreCase(code)) {
            return ChannelResponse.build("RT-0011", "账户余额不足");
        }
        if ("RT-0012".equalsIgnoreCase(code)) {
            return ChannelResponse.build("RT-0012", "账户已冻结");
        }
        if ("RT-0013".equalsIgnoreCase(code)) {
            return ChannelResponse.build("RT-0013", "黑名单风险受限");
        }
        if ("1040401016".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1040401016", "请填写金额");
        }
        if ("1040401049".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1040401049", "代付明细信息必填");
        }
        if ("1040401050".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1040401050", "账户名格式错误");
        }
        if ("1040401051".equalsIgnoreCase(code)) {
            return ChannelResponse.build("1040401051", "银行卡格式有误");
        }
        if ("104020001".equalsIgnoreCase(code)) {
            return ChannelResponse.build("104020001", "虚拟账户不存在");
        }
        if ("104020002".equalsIgnoreCase(code)) {
            return ChannelResponse.build("104020002", "查询无结果");
        }
        if ("0090015".equalsIgnoreCase(code)) {
            return ChannelResponse.build("0090015", "商户支付授权地址异常");
        }
        return ChannelResponse.build(BaseResultCode.COMMON_FAIL, errorMsg);

    }
}
