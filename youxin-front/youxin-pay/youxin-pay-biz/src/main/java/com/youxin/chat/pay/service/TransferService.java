package com.youxin.chat.pay.service;

import com.youxin.chat.pay.entity.req.TransferCodeReq;
import com.youxin.chat.pay.entity.req.TransferMoneyReq;
import com.youxin.chat.pay.entity.vo.TransferInfoVo;
import com.youxin.exception.SystemException;

/**
 * description: TransferService <br>
 * date: 2020/2/29 16:15 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface TransferService {

    String buildTransferCode(TransferCodeReq request) throws SystemException;

    /**
     * 获取二维码转帐信息
     * @param code
     * @return
     */
    TransferInfoVo acquireTransferCodeInfo(String code);

    /**
     * 转帐金额
     * @param request
     * @throws SystemException
     */
    void transferAmount(TransferMoneyReq request)throws SystemException;
}
