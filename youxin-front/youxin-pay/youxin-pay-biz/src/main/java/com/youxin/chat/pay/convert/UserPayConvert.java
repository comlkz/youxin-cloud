package com.youxin.chat.pay.convert;

import com.alibaba.fastjson.JSONObject;

import com.youxin.chat.pay.dto.PayRulesDto;
import com.youxin.chat.pay.entity.req.*;
import com.youxin.chat.pay.enums.OrderSourceEnum;
import com.youxin.chat.pay.enums.OrderStatusEnum;
import com.youxin.chat.pay.model.*;
import com.youxin.chat.pay.service.channel.entity.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class UserPayConvert {

    public static UserRecharge buildRecharge(UserChargeReq userChargeDto, UserCard userCard, String orderNo, String userNo, String userIp) {
        UserRecharge userRecharge = new UserRecharge();
        userRecharge.setUserNo(userNo);
        userRecharge.setRechargeNo(orderNo);
        userRecharge.setRechargeAmount(userChargeDto.getAmount());
        userRecharge.setPayType("quickPay");
        userRecharge.setCardNo(userCard.getCardNo());
        userRecharge.setAccountName(userCard.getOwnerName());
        userRecharge.setAccountMobile(userCard.getCardMobile());
        userRecharge.setIdNo(userCard.getCardId());
        userRecharge.setRechargeStatus(0);
        userRecharge.setClientIp(userIp);
        userRecharge.setCreateTime(LocalDateTime.now());
        userRecharge.setUpdateTime(LocalDateTime.now());
        return userRecharge;
    }

    public static ChannelOrder buildChannelOrder(UserRecharge userRecharge, ChannelAccount channelAccount, String channelOrderNo) {
        ChannelOrder channelOrder = new ChannelOrder();
        channelOrder.setRechrageNo(userRecharge.getRechargeNo());
        channelOrder.setChannelOrderNo(channelOrderNo);
        channelOrder.setUserNo(userRecharge.getUserNo());
        channelOrder.setRechargeAmount(userRecharge.getRechargeAmount());
        BigDecimal serviceFee = userRecharge.getRechargeAmount().multiply(channelAccount.getChannelRate()).divide(new BigDecimal(100));
        if (serviceFee.compareTo(new BigDecimal(1)) < 0) {
            serviceFee = new BigDecimal(1);
        }
        BigDecimal payAmount = userRecharge.getRechargeAmount().subtract(serviceFee);
        channelOrder.setPayAmount(payAmount);
        channelOrder.setChannelRate(channelAccount.getChannelRate());
        channelOrder.setPayType("quick_pay");
        channelOrder.setCardNo(userRecharge.getCardNo());
        channelOrder.setAccountName(userRecharge.getAccountName());
        channelOrder.setAccountMobile(userRecharge.getAccountMobile());
        channelOrder.setIdNo(userRecharge.getIdNo());
        channelOrder.setChannelAccount(channelAccount.getChannelAccount());
        channelOrder.setChannelAlias(channelAccount.getChannelAlias());
        channelOrder.setPayStatus(OrderStatusEnum.NOT_PAY.getStatus());
        channelOrder.setHandleType(0);
        channelOrder.setCreateTime(LocalDateTime.now());
        channelOrder.setUpdateTime(LocalDateTime.now());
        return channelOrder;
    }

    public static CommonOrder buildPayCommonOrder(UserRecharge userRecharge) {
        CommonOrder commonOrder = new CommonOrder();
        commonOrder.setOrderNo(userRecharge.getRechargeNo());
        commonOrder.setOrderRemark(OrderSourceEnum.PAY.getDesc());
        commonOrder.setOrderSource(OrderSourceEnum.PAY.getStatus());
        commonOrder.setClientIp(userRecharge.getClientIp());
        commonOrder.setOrderStatus(OrderStatusEnum.NOT_PAY.getStatus());
        commonOrder.setOrderAmount(userRecharge.getRechargeAmount());
        commonOrder.setUserNo(userRecharge.getUserNo());
        commonOrder.setCreateTime(LocalDateTime.now());
        commonOrder.setUpdateTime(LocalDateTime.now());
        commonOrder.setFeeType(0);
        return commonOrder;
    }

    public static ChannelBindCard buildBindCardInfo(UserCardReq userCardDto, String userNo, ChannelAccount channelAccount, String bankNo) {

        ChannelBindCard channelBindCard = new ChannelBindCard();
        channelBindCard.setCardId(userCardDto.getCardId());
        channelBindCard.setCardMobile(userCardDto.getCardMobile());
        channelBindCard.setCardNo(userCardDto.getCardNo());
        channelBindCard.setCardType(userCardDto.getCardType());
        channelBindCard.setUserNo(userNo);
        channelBindCard.setOwnerName(userCardDto.getOwnerName());
        channelBindCard.setChannelAccount(channelAccount.getChannelAccount());
        channelBindCard.setUserIp(userCardDto.getUserIp());
        channelBindCard.setBankNo(bankNo);
        return channelBindCard;
    }

    public static ChannelBindCardSms buildBindCardSms(UserCardSmsReq userCardSmsDto) {
        ChannelBindCardSms channelBindCardSms = new ChannelBindCardSms();
        channelBindCardSms.setValidateCode(userCardSmsDto.getValidateCode());
        channelBindCardSms.setOrderNo(userCardSmsDto.getId());
        return channelBindCardSms;
    }

    public static ChannelTerminationCard buildTerminationInfo(UserCard userCard, String contractId, ChannelAccount channelAccount) {
        ChannelTerminationCard channelTerminationCard = new ChannelTerminationCard();
        channelTerminationCard.setCardNo(userCard.getCardNo());
        channelTerminationCard.setUserNo(userCard.getUserNo());
        channelTerminationCard.setChannelAccount(channelAccount.getChannelAccount());
        channelTerminationCard.setContractId(contractId);
        return channelTerminationCard;
    }

    public static ChannelDefray buildDefrayInfo(ChannelCash channelCash, UserCard userCard, String bankCode, String bankLinked, String bankName) {
        ChannelDefray channelDefray = new ChannelDefray();
        channelDefray.setOrderNo(channelCash.getCashNo());
        channelDefray.setAmount(channelCash.getReqAmount());
        channelDefray.setCardNo(userCard.getCardNo());
        channelDefray.setBankMobile(userCard.getCardMobile());
        channelDefray.setIdCard(userCard.getCardId());
        channelDefray.setReceiveName(userCard.getOwnerName());
        channelDefray.setBankName(bankName);
        channelDefray.setBankCode(bankCode);
        channelDefray.setBankLinked(bankLinked);
        return channelDefray;
    }



    public static ChannelCash buildDefrayChannelCash(UserCard userCard, UserCash userCash, String userNo, String orderNo, ChannelAccount channelAccont) {
        ChannelCash channelCash = new ChannelCash();
        channelCash.setUserNo(userNo);
        channelCash.setCashNo(orderNo);
        channelCash.setCashStatus(OrderStatusEnum.NOT_PAY.getStatus());
        //代付费用 = 用户提现金额 - （提现金额 * 提现比例） - 最小手续费getCashRate
        BigDecimal cashAmount = userCash.getCashAmount().subtract(userCash.getCashAmount().multiply(userCash.getCashRate()).divide(new BigDecimal(100))).subtract(userCash.getDefrayFee());
        channelCash.setCashAmount(cashAmount.setScale(0, BigDecimal.ROUND_DOWN));
        channelCash.setReqAmount(cashAmount.setScale(0, BigDecimal.ROUND_DOWN));
        channelCash.setCardNo(userCard.getCardNo());
        channelCash.setAccountName(userCard.getOwnerName());
        channelCash.setAccountMobile(userCard.getCardMobile());
        channelCash.setChannelAccount(channelAccont.getChannelAccount());
        channelCash.setServiceFee(channelAccont.getServiceFee());
        return channelCash;
    }

    public static UserCash builddefrayUserCash(UserCashReq userCashReq, ChannelAccount channelAccount, UserCard userCard, String userNo, String orderNo, PayRulesDto payRulesDto) {
        UserCash userCash = new UserCash();
        userCash.setCashNo(orderNo);
        userCash.setUserNo(userNo);
        userCash.setCashAmount(userCashReq.getAmount());
        userCash.setCardNo(userCard.getCardNo());
        userCash.setUserIp(userCashReq.getClientIp());
        userCash.setCreateTime(LocalDateTime.now());
        userCash.setCashRate(payRulesDto.getCashRate());
        userCash.setCashStatus(0);
        userCash.setDefrayFee(payRulesDto.getMinDefrayFee());
        return userCash;
    }

    public static UserCard buildUserCard(ChannelBindCard channelBindCard, String userNo, String bankCode, String userIp) {
        UserCard userCard = new UserCard();
        userCard.setBankCode(bankCode);
        userCard.setCardId(channelBindCard.getCardId());
        userCard.setCardMobile(channelBindCard.getCardMobile());
        userCard.setCardNo(channelBindCard.getCardNo());
        userCard.setCardType(Integer.valueOf(channelBindCard.getCardType()));
        userCard.setCreateTime(LocalDateTime.now());
        userCard.setOwnerName(channelBindCard.getOwnerName());
        userCard.setUserNo(userNo);
        userCard.setUserIp(userIp);
        userCard.setBindStatus(1);
        userCard.setUpdateTime(LocalDateTime.now());
        userCard.setChannelAccount(channelBindCard.getChannelAccount());
        return userCard;
    }

    public static ChannelCard buildChannelCard(String id, String agreementId, String channelBankCode, ChannelAccount channelAccount, String ext) {
        ChannelCard channelCard = new ChannelCard();
        channelCard.setUserCardId(id);
        channelCard.setContractId(agreementId);
        channelCard.setChannelCode(channelAccount.getChannelCode());
        channelCard.setChannelAccount(channelAccount.getChannelAccount());
        channelCard.setExpand(ext);
        return channelCard;
    }

    public static ChannelChargeSms buildChannelChargeSms(UserChargeSmsReq userChargeSmsDto, String userNo, String channelOrderNo, JSONObject ext) {
        ChannelChargeSms channelChargeSms = new ChannelChargeSms();
        channelChargeSms.setValidateCode(userChargeSmsDto.getValidateCode());
        channelChargeSms.setUserNo(userNo);
        channelChargeSms.setOrderNo(channelOrderNo);
        channelChargeSms.setExt(ext);
        return channelChargeSms;
    }

    public static PayCertification buildPayCertification(ChannelBindCard channelBindCard) {
        PayCertification payCertification = new PayCertification();
        payCertification.setUserNo(channelBindCard.getUserNo());
        payCertification.setCardId(channelBindCard.getCardId());
        payCertification.setMobile(channelBindCard.getCardMobile());
        payCertification.setUserName(channelBindCard.getOwnerName());
        payCertification.setCreateTime(LocalDateTime.now());

        return payCertification;
    }
}
