package com.youxin.chat.pay.entity.req;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

public class UserChargeReq implements Serializable {

    @ApiModelProperty(value = "银行卡id",required = true)
    @NotNull(message = "银行卡ID不能为空")
    private String cardId;
    @ApiModelProperty(value = "金额",required = true)
    @DecimalMin(value = "0",message = "金额不合法")
    private BigDecimal amount;
    @ApiModelProperty(value = "支付密码",required = false)
    private String payPwd;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPayPwd() {
        return payPwd;
    }

    public void setPayPwd(String payPwd) {
        this.payPwd = payPwd;
    }
}
