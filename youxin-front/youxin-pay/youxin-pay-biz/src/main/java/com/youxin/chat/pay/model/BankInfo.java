package com.youxin.chat.pay.model;

public class BankInfo {

    private Integer id;

    private String bankCode;

    private String bankName;

    private String bankIcon;

    private String bankCover;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankIcon() {
        return bankIcon;
    }

    public void setBankIcon(String bankIcon) {
        this.bankIcon = bankIcon;
    }

    public String getBankCover() {
        return bankCover;
    }

    public void setBankCover(String bankCover) {
        this.bankCover = bankCover;
    }
}
