package com.youxin.chat.pay.convert;


import com.youxin.chat.pay.model.ChannelOrder;
import com.youxin.chat.pay.service.channel.entity.ChannelPay;
import com.youxin.chat.pay.service.channel.entity.UserCardDto;

public class ChannelPayConvert {

    public static ChannelPay transOrderToChannelPay(ChannelOrder channelOrder, String contractId, UserCardDto userCardDto, String ext) {
        ChannelPay channelPay = new ChannelPay();
        channelPay.setCardId(channelOrder.getCardNo());
        channelPay.setAmount(channelOrder.getRechargeAmount());
        channelPay.setUserIp(channelOrder.getClientIp());
        channelPay.setContractId(channelOrder.getContractId());
        channelPay.setOrderNo(channelOrder.getChannelOrderNo());
        channelPay.setUserNo(channelOrder.getUserNo());
        channelPay.setContractId(contractId);
        channelPay.setUserCard(userCardDto);
        channelPay.setExt(ext);
        return channelPay;
    }
}
