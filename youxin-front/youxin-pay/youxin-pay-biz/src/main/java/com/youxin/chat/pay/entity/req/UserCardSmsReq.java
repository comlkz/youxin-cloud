package com.youxin.chat.pay.entity.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class UserCardSmsReq implements Serializable {

    @ApiModelProperty(value = "短信验证码",required = true)
    private String validateCode;

    private String userIp;
    @ApiModelProperty(value = "申请返回ID",required = true)
    private String id;

    public String getValidateCode() {
        return validateCode;
    }

    public void setValidateCode(String validateCode) {
        this.validateCode = validateCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }
}
