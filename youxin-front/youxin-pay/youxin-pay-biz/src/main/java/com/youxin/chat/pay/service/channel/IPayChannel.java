package com.youxin.chat.pay.service.channel;


import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.model.ChannelOrder;
import com.youxin.chat.pay.model.UserCash;
import com.youxin.chat.pay.model.UserRecharge;
import com.youxin.chat.pay.service.channel.entity.*;

public interface IPayChannel {

    ChannelResponse bindCard(String orderNo, ChannelBindCard channelBindCard, ChannelAccount channelAccount);

    ChannelResponse validateSms(ChannelBindCardSms channelBindCardSms, ChannelAccount channelAccount);

    boolean support(String channelCode);

    ChannelResponse pay(ChannelPay channelPay, ChannelAccount channelAccount);

    ChannelResponse termination(ChannelTerminationCard channelTerminationCard, ChannelAccount channelAccount);

    ChannelNotifyResponse orderNotify(ChannelNotifyRequest request, ChannelAccount channelAccount);

    ChannelNotifyResponse defrayNotify(ChannelNotifyRequest request, ChannelAccount channelAccount);

    ChannelResponse payQuery(ChannelOrder channelOrder, ChannelAccount channelAccount);

    ChannelResponse defray(ChannelDefray channelDefray, ChannelAccount channelAccount);

    ChannelResponse defrayQuery(UserCash userCash, ChannelAccount channelAccount);

    ChannelResponse doChargeSms(ChannelChargeSms channelChargeSms, ChannelAccount channelAccount);

    ChannelResponse balanceQuery(ChannelAccount channelAccount);
}
