package com.youxin.chat.pay.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.chat.pay.mapper.RedAssignMapper;
import com.youxin.chat.pay.model.RedAssign;
import com.youxin.chat.pay.service.RedAssignService;
import org.springframework.stereotype.Service;

/**
 * description: RedAssignServiceImpl <br>
 * date: 2020/2/29 15:26 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class RedAssignServiceImpl extends ServiceImpl<RedAssignMapper, RedAssign> implements RedAssignService {
}
