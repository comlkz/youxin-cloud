package com.youxin.chat.pay.service.rpc;

import com.youxin.chat.user.client.GroupClient;
import com.youxin.chat.user.client.UserClient;
import com.youxin.chat.user.dto.UserInfoDto;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * description: UserService <br>
 * date: 2020/2/29 15:32 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class UserService {

    @Reference
    private UserClient userClient;

    @Reference
    private GroupClient groupClient;

    public UserInfoDto getByUserNo(String userNo){
        return userClient.getByUserNo(userNo);
    }
    public List<UserInfoDto> getByUserNos(List<String> userNos){
        return userClient.getByUserNos(userNos);
    }

    public int countMemberByGroupNoAndUserNo(String groupNo, String userNo){
        return groupClient.countMemberByGroupNoAndUserNo(groupNo, userNo);
    }

    public boolean checkUserInBlackList(String userNo, String groupNo){
        return groupClient.checkUserInBlackList(userNo, groupNo);
    }

    public boolean isGroupAdmin(String userNo, String groupNo){
        return groupClient.checkOperate(userNo, groupNo);
    }
}
