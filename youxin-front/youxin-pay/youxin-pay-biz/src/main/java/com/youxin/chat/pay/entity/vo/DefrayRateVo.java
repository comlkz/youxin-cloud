package com.youxin.chat.pay.entity.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class DefrayRateVo implements Serializable {
    @ApiModelProperty(value = "费率（百分比）")
    private String rate;
    @ApiModelProperty(value = "实际提出金额")
    private BigDecimal cashAmount;
    @ApiModelProperty(value = "最低提出金额")
    private BigDecimal minCashAmount;

    @ApiModelProperty(value = "最大提出金额")
    private BigDecimal maxCashAmount;

    @ApiModelProperty(value = "手续费（单位分）")
    private BigDecimal serviceFee;

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public BigDecimal getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(BigDecimal cashAmount) {
        this.cashAmount = cashAmount;
    }

    public BigDecimal getMinCashAmount() {
        return minCashAmount;
    }

    public void setMinCashAmount(BigDecimal minCashAmount) {
        this.minCashAmount = minCashAmount;
    }

    public BigDecimal getMaxCashAmount() {
        return maxCashAmount;
    }

    public void setMaxCashAmount(BigDecimal maxCashAmount) {
        this.maxCashAmount = maxCashAmount;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }
}
