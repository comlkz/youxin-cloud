package com.youxin.chat.pay.service.manager;

import com.youxin.base.BaseResultCode;
import com.youxin.chat.pay.context.TransactionContext;
import com.youxin.chat.pay.enums.OrderStatusEnum;
import com.youxin.chat.pay.mapper.*;
import com.youxin.chat.pay.model.*;
import com.youxin.exception.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * description: UserRechargeManager <br>
 * date: 2020/3/1 12:37 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class UserRechargeManager {

    private static final Logger logger = LoggerFactory.getLogger(UserRechargeManager.class);


    @Resource
    private UserRechargeMapper userRechargeMapper;

    @Resource
    private ChannelOrderMapper channelOrderMapper;

    @Resource
    private CommonOrderMapper commonOrderMapper;

    @Resource
    private UserWalletMapper userWalletMapper;

    @Resource
    private TransactionDetailMapper transactionDetailMapper;

    @Transactional
    public void saveRechargeInfo(UserRecharge userRecharge, ChannelOrder channelOrder, CommonOrder commonOrder) {
        userRechargeMapper.insert(userRecharge);
        channelOrderMapper.insert(channelOrder);
        commonOrderMapper.insert(commonOrder);
    }

    @Transactional
    public int doPaySuccess(ChannelOrder channelOrder) {
        int count = channelOrderMapper.changeOrderStatus(channelOrder.getChannelOrderNo(), OrderStatusEnum.PAY_SUCCESS.getStatus(), OrderStatusEnum.NOT_PAY.getStatus());
        if (count < 1) {
            logger.error("该渠道订单状态已修改,orderNo={}", channelOrder.getChannelOrderNo());
            throw new SystemException(BaseResultCode.RECORD_EXISTS, "记录已被修改");
        }
        count = userRechargeMapper.changeOrderStatus(channelOrder.getRechrageNo(), OrderStatusEnum.PAY_SUCCESS.getStatus(), OrderStatusEnum.NOT_PAY.getStatus());
        if (count < 1) {
            logger.error("该充值订单状态已修改,rechargeNo={}", channelOrder.getRechrageNo());
            throw new SystemException(BaseResultCode.RECORD_EXISTS, "记录已被修改");
        }
        count = userWalletMapper.doPaySuccess(channelOrder.getRechargeAmount(), channelOrder.getUserNo());
        if (count < 1) {
            logger.error("修改用户钱包信息失败,userNo={}", channelOrder.getUserNo());
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "修改用户钱包失败");
        }
        UserWallet userWallet = userWalletMapper.selectById(channelOrder.getUserNo());
        TransactionDetail transactionDetail = TransactionContext.buildTransactionDetail(channelOrder,userWallet.getAgentNo());
        count = transactionDetailMapper.insert(transactionDetail);
        if (count < 1) {
            logger.error("修改交易记录信息失败,userNo={},rechargeNo={}", channelOrder.getUserNo(), channelOrder.getRechrageNo());
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "修改交易记录信息失败");
        }
        commonOrderMapper.changeOrderStatus(OrderStatusEnum.PAY_SUCCESS.getStatus(), channelOrder.getRechrageNo());
        return count;
    }

    @Transactional
    public int doPayFail(ChannelOrder channelOrder) {

        int count = channelOrderMapper.changeOrderStatus(channelOrder.getChannelOrderNo(), OrderStatusEnum.PAY_FAIL.getStatus(), OrderStatusEnum.NOT_PAY.getStatus());
        if (count < 1) {
            logger.error("该渠道订单状态已修改,orderNo={}", channelOrder.getChannelOrderNo());
            throw new SystemException(BaseResultCode.RECORD_EXISTS, "记录已被修改");
        }
        count = userRechargeMapper.changeOrderStatus(channelOrder.getRechrageNo(), OrderStatusEnum.PAY_FAIL.getStatus(), OrderStatusEnum.NOT_PAY.getStatus());
        if (count < 1) {
            logger.error("该充值订单状态已修改,rechargeNo={}", channelOrder.getRechrageNo());
            throw new SystemException(BaseResultCode.RECORD_EXISTS, "记录已被修改");
        }
        count = commonOrderMapper.changeOrderStatus(OrderStatusEnum.PAY_FAIL.getStatus(), channelOrder.getRechrageNo());
        return count;
    }
}
