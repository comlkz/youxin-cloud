package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.ChannelOrder;
import com.youxin.chat.pay.model.SystemSettlement;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;

public interface ChannelOrderMapper extends BaseMapper<ChannelOrder> {

    int changeOrderStatus(@Param("orderNo") String orderNo, @Param("status") Integer status, @Param("initStatus") Integer initStatus);

    @Select("select * from t_channel_order where channel_order_no=#{channelOrderNo}")
    ChannelOrder selectByChannelOrderNo(@Param("channelOrderNo")String channelOrderNo);

    SystemSettlement staticOrderData(@Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);
}
