package com.youxin.chat.pay.context;

import com.youxin.chat.pay.enums.FeeTypeEnum;
import com.youxin.chat.pay.enums.OrderSourceEnum;
import com.youxin.chat.pay.model.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * description: TransactionContext <br>
 * date: 2020/2/29 15:19 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class TransactionContext {

    public static TransactionDetail buildTransactionDetail(RedSend redSend, String agentNo) {
        TransactionDetail detail = new TransactionDetail();
        detail.setUserNo(redSend.getUserNo());
        detail.setOrderNo(String.valueOf(redSend.getId()));
        detail.setUserTransAmount(redSend.getRedAmount());
        detail.setOpType(OrderSourceEnum.RED_SEND.getStatus());
        detail.setFeeType(FeeTypeEnum.OUT_MONEY.ordinal());
        detail.setAgentNo(agentNo);
        detail.setCreateTime(LocalDateTime.now());
        detail.setUpdateTime(LocalDateTime.now());
        return detail;
    }

    public static TransactionDetail buildTransactionDetail(String id, String userNo, BigDecimal amount, String agentNo) {
        TransactionDetail detail = new TransactionDetail();
        detail.setUserNo(userNo);
        detail.setOrderNo(String.valueOf(id));
        detail.setUserTransAmount(amount);
        detail.setOpType(OrderSourceEnum.RED_RECEIVE.getStatus());
        detail.setFeeType(FeeTypeEnum.IN_MONEY.ordinal());
        detail.setCreateTime(LocalDateTime.now());
        detail.setUpdateTime(LocalDateTime.now());
        detail.setAgentNo(agentNo);
        return detail;
    }

    public static  TransactionDetail buildTransactionDetail(ChannelOrder channelOrder, String agentNo) {
        TransactionDetail detail = new TransactionDetail();
        detail.setUserNo(channelOrder.getUserNo());
        detail.setOrderNo(channelOrder.getRechrageNo());
        detail.setChannelOrderNo(channelOrder.getChannelOrderNo());
        detail.setUserTransAmount(channelOrder.getRechargeAmount());
        detail.setChannelTransAmount(channelOrder.getPayAmount());
        detail.setUserServiceFee(new BigDecimal(0));
        detail.setChannelServiceFee(channelOrder.getRechargeAmount().subtract(channelOrder.getPayAmount()));
        detail.setOpType(OrderSourceEnum.PAY.ordinal());
        detail.setChannelAccount(channelOrder.getChannelAccount());
        detail.setChannelAlias(channelOrder.getChannelAlias());
        detail.setFeeType(FeeTypeEnum.IN_MONEY.ordinal());
        detail.setAgentNo(agentNo);
        detail.setCreateTime(LocalDateTime.now());
        detail.setUpdateTime(LocalDateTime.now());

        return detail;
    }

    public static  TransactionDetail buildTransactionDetail(UserCash userCash, ChannelCash channelCash, String agentNo) {
        TransactionDetail detail = new TransactionDetail();
        detail.setUserNo(userCash.getUserNo());
        detail.setOrderNo(userCash.getCashNo());
        detail.setChannelOrderNo(userCash.getCashNo());
        detail.setUserTransAmount(userCash.getCashAmount());
        detail.setChannelTransAmount(channelCash.getReqAmount());
        detail.setUserServiceFee(userCash.getCashAmount().subtract(channelCash.getCashAmount()));
        detail.setChannelServiceFee(channelCash.getServiceFee());
        detail.setOpType(OrderSourceEnum.DEFRAY.ordinal());
        detail.setChannelAccount(channelCash.getChannelAccount());
        detail.setChannelAlias(channelCash.getChannelAlias());
        detail.setFeeType(FeeTypeEnum.OUT_MONEY.ordinal());
        detail.setCreateTime(LocalDateTime.now());
        detail.setUpdateTime(LocalDateTime.now());
        detail.setAgentNo(agentNo);
        return detail;
    }

    public static  TransactionDetail buildExpireDetail(String id, String userNo, BigDecimal amount,String agentNo) {
        TransactionDetail detail = new TransactionDetail();
        detail.setUserNo(userNo);
        detail.setOrderNo(String.valueOf(id));
        detail.setUserTransAmount(amount);
        detail.setOpType(OrderSourceEnum.RED_REFUND.getStatus());
        detail.setFeeType(FeeTypeEnum.IN_MONEY.ordinal());
        detail.setCreateTime(LocalDateTime.now());
        detail.setUpdateTime(LocalDateTime.now());
        detail.setAgentNo(agentNo);
        return detail;
    }
}
