package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.BankInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface BankInfoMapper extends BaseMapper<BankInfo> {
    List<BankInfo> getList(@Param("channelCode") String channelCode);
}
