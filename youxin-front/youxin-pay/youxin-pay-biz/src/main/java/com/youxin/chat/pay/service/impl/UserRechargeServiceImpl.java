package com.youxin.chat.pay.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.chat.pay.entity.vo.UserRechargeVo;
import com.youxin.chat.pay.mapper.UserRechargeMapper;
import com.youxin.chat.pay.model.UserRecharge;
import com.youxin.chat.pay.service.UserRechargeService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * description: UserRechargeServiceImpl <br>
 * date: 2020/3/1 10:40 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class UserRechargeServiceImpl extends ServiceImpl<UserRechargeMapper, UserRecharge> implements UserRechargeService {
    @Override
    public List<UserRechargeVo> selectRechargeRecord(String userNo) {
       return  this.baseMapper.selectRechargeRecord(userNo);
    }
}
