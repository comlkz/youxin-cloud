package com.youxin.chat.pay.service.channel.impl.liantong.request;

import com.alibaba.fastjson.JSONObject;

import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.service.channel.entity.ChannelTerminationCard;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LianTongTermination {


    public static JSONObject auditTerminationRequest(ChannelTerminationCard channelTerminationCard, ChannelAccount channelAccount, String orderNo){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version", "1.0.0");
        jsonObject.put("busiType", "SIGN");
        jsonObject.put("merNo", channelTerminationCard.getChannelAccount());
        jsonObject.put("merOrderDate", new SimpleDateFormat("yyyyMMdd").format(new Date()));
        jsonObject.put("merOrderNo", orderNo);
        jsonObject.put("signType", "RSA_SHA256");
        jsonObject.put("agreeNo", channelTerminationCard.getContractId());
        return jsonObject;
    }
}
