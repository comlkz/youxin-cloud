package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.ChannelCard;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ChannelCardMapper extends BaseMapper<ChannelCard> {

    @Select("select * from t_channel_card where user_card_id=#{cardId} and channel_account=#{channelAccount}")
    ChannelCard   selectByCardIdAndChannelAccount(@Param("cardId")String cardId,@Param("channelAccount")String channelAccount);

    @Select("select * from t_channel_card where user_card_id=#{cardId} ")
    List<ChannelCard> selectByCardId(@Param("cardId")String cardId);
}
