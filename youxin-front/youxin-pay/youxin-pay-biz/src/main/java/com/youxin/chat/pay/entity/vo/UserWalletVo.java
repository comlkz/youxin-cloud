package com.youxin.chat.pay.entity.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class UserWalletVo implements Serializable {

    /**
     * 是否设置支付密码（0：未设置 1：已设置）
     */
    @ApiModelProperty(value = "是否设置支付密码（0：未设置 1：已设置）")
    private Integer hasPayPass;

    /**
     * 是否绑卡 0：未绑卡 1：已绑卡
     */
    @ApiModelProperty(value = "是否绑卡 0：未绑卡 1：已绑卡")

    private Integer hasBindCard;

    /**
     * 余额（单位：分）
     */
    @ApiModelProperty(value = "余额（单位：分）")
    private BigDecimal balance;
    @ApiModelProperty(value = "状态0:：正常 1：被冻结")
    private Integer walletStatus;

    public Integer getHasPayPass() {
        return hasPayPass;
    }

    public void setHasPayPass(Integer hasPayPass) {
        this.hasPayPass = hasPayPass;
    }

    public Integer getHasBindCard() {
        return hasBindCard;
    }

    public void setHasBindCard(Integer hasBindCard) {
        this.hasBindCard = hasBindCard;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Integer getWalletStatus() {
        return walletStatus;
    }

    public void setWalletStatus(Integer walletStatus) {
        this.walletStatus = walletStatus;
    }
}
