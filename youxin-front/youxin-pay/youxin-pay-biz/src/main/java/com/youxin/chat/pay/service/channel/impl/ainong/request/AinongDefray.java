package com.youxin.chat.pay.service.channel.impl.ainong.request;

import com.alibaba.fastjson.JSONObject;
import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.model.UserCash;
import com.youxin.chat.pay.service.channel.entity.ChannelDefray;
import com.youxin.utils.RandomUtil;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AinongDefray {

    public static JSONObject buildDefrayRequest(ChannelDefray channelDefray, ChannelAccount channelAccount){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("merOrderId", channelDefray.getOrderNo());
        jsonObject.put("txnTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        jsonObject.put("txnAmt", channelDefray.getAmount().longValue());
        jsonObject.put("currency", "CNY");
        String notifyUrl = channelAccount.getNotifyUrl() + "/notify/" + channelAccount.getChannelCode() + "/defray/" + channelDefray.getOrderNo();
        jsonObject.put("backUrl", notifyUrl);
        jsonObject.put("subject", RandomUtil.randomStr(20));
        jsonObject.put("body", RandomUtil.randomStr(40));
        jsonObject.put("accNo", channelDefray.getCardNo());
        jsonObject.put("accType", "01");
        jsonObject.put("customerNm", channelDefray.getReceiveName());
        jsonObject.put("unionBankCd", "");
        jsonObject.put("merResv1", RandomUtil.randomStr(30));
        return jsonObject;
    }

    public static JSONObject buildDefrayQueryRequest(UserCash userCash, ChannelAccount channelAccount){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("merOrderId", userCash.getCashNo());

        return jsonObject;
    }
}
