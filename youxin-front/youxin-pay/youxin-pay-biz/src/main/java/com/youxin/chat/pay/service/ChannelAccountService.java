package com.youxin.chat.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.pay.dto.ChannelAccountDto;
import com.youxin.chat.pay.dto.ChannelBankDto;
import com.youxin.chat.pay.model.ChannelAccount;

/**
 * description: ChannelAccountService <br>
 * date: 2020/3/5 15:06 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface ChannelAccountService extends IService<ChannelAccount> {

    void saveChannel(ChannelAccountDto channelAccountDto);

    void saveChannelBank(ChannelBankDto channelBankDto);

    void removeChannelBank(Integer id);
}
