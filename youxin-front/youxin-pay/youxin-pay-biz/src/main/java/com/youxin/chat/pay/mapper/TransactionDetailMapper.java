package com.youxin.chat.pay.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.youxin.chat.pay.model.AgentStatic;
import com.youxin.chat.pay.model.TransactionDetail;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface TransactionDetailMapper extends BaseMapper<TransactionDetail> {

    @Update("UPDATE t_transaction_detail set states = #{transactionDetail.states} WHERE userNo = #{transactionDetail.userNo}")
    void updateDetailState(@Param("transactionDetail") TransactionDetail transactionDetail);

   // IPage<TransactionDetailDto> selectListByParam(Page page, @Param("record") TransactionSearch search);

    // List<AgentStatic> staticAgentTransaction(@Param("record") AgentStaticSearch search);
}
