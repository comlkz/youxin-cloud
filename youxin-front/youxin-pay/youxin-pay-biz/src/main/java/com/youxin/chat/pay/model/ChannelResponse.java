package com.youxin.chat.pay.model;

public class ChannelResponse {

    /**
     * 基于baseResultCode
     */
    private String code;

    private String result;

    private String errorMsg;

    private Integer status;

    private String ext;

    private String agreementId;

    public ChannelResponse() {
    }

    public ChannelResponse(String code,String msg) {
        this.code = code;
        this.errorMsg = msg;
    }

    public static ChannelResponse build(String code,String msg){
        return new ChannelResponse(code,msg);
    }

    public ChannelResponse(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(String agreementId) {
        this.agreementId = agreementId;
    }
}
