package com.youxin.chat.pay.service.channel.impl.ypl.request;

import com.alibaba.fastjson.JSONObject;
import com.youxin.chat.pay.service.channel.entity.ChannelTerminationCard;
import com.youxin.utils.RandomUtil;

public class YplTermination {

    public static JSONObject buildTerminationRequest(ChannelTerminationCard channelTerminationCard){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("protocol",channelTerminationCard.getContractId());
        jsonObject.put("customerCode",channelTerminationCard.getChannelAccount());
        jsonObject.put("memberId",channelTerminationCard.getUserNo());
        jsonObject.put("nonceStr", RandomUtil.randomStr(32));

        return jsonObject;
    }
}
