package com.youxin.chat.pay.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@ApiModel(description = "用户余额表")
public class UserBalanceVo implements Serializable {

    @ApiModelProperty(value = "用户唯一标识", required = true)
    private String userNo;//用户唯一标识
    @ApiModelProperty(value = "余额", required = false)
    private BigDecimal userBalance;
    @ApiModelProperty(value = "冻结金额", required = false)
    //冻结金额(用户提现，先将提现的金额划到冻结资金，等银行回调成功，处理成功之后将锁定金额归0，一个账户同一时间只能有一笔未成功的提现单产生)
    private BigDecimal lockAmount;
    @ApiModelProperty(value = "进账总额", required = false)
    private BigDecimal payAmount;//进账总额
    @ApiModelProperty(value = "提现总额", required = false)
    private BigDecimal cashAmount;//提现总额
    @ApiModelProperty(value = "修改时间", required = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;//修改时间
    @ApiModelProperty(value = "是否删除", required = false)
    private Integer isDeleted;
    @ApiModelProperty(value = "手续费总额", required = false)
    private BigDecimal serviceFee;
    @ApiModelProperty(value = "状态0:：正常 1：被冻结", required = false)
    private Integer walletStatus;

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public BigDecimal getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(BigDecimal userBalance) {
        this.userBalance = userBalance;
    }

    public BigDecimal getLockAmount() {
        return lockAmount;
    }

    public void setLockAmount(BigDecimal lockAmount) {
        this.lockAmount = lockAmount;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public BigDecimal getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(BigDecimal cashAmount) {
        this.cashAmount = cashAmount;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public Integer getWalletStatus() {
        return walletStatus;
    }

    public void setWalletStatus(Integer walletStatus) {
        this.walletStatus = walletStatus;
    }
}
