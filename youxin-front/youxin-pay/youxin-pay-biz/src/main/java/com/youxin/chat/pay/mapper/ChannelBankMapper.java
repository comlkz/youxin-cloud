package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.ChannelBank;
import org.apache.ibatis.annotations.Param;

public interface ChannelBankMapper extends BaseMapper<ChannelBank> {

    ChannelBank selectByChannelCodeAndBankCode(@Param("channelCode")String channelCode,@Param("bankCode")String bankCode,@Param("type")int type);
}
