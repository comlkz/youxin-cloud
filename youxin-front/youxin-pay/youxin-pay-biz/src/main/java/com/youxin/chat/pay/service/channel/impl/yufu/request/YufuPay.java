package com.youxin.chat.pay.service.channel.impl.yufu.request;

import com.alibaba.fastjson.JSONObject;

import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.model.ChannelOrder;
import com.youxin.chat.pay.model.UserRecharge;
import com.youxin.chat.pay.enums.PayChannelEnum;
import com.youxin.chat.pay.service.channel.entity.ChannelChargeSms;
import com.youxin.chat.pay.service.channel.entity.ChannelDefray;
import com.youxin.chat.pay.service.channel.entity.ChannelPay;
import com.youxin.utils.RandomUtil;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class YufuPay {
    public static JSONObject buildPayRequest(ChannelPay channelPay, ChannelAccount channelAccount) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version", "2.2.0");
        jsonObject.put("merchantId", channelAccount.getChannelAccount());
        jsonObject.put("merchantOrderId", channelPay.getOrderNo());
        jsonObject.put("merchantOrderTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        jsonObject.put("merchantOrderAmt", channelPay.getAmount());
        jsonObject.put("merchantOrderCurrency", "156");
        String notifyUrl = channelAccount.getNotifyUrl() + "/notify/" + channelAccount.getChannelCode() + "/recharge/" + channelPay.getOrderNo();
        jsonObject.put("backUrl", notifyUrl);
        jsonObject.put("verifyId",channelPay.getContractId());
        jsonObject.put("gwType", "04");
        jsonObject.put("merchantUserId", channelPay.getExt());
        jsonObject.put("merchantOrderDesc", RandomUtil.randomStr(16));
        jsonObject.put("userType","01");
        Map rcExt = new HashMap();
        rcExt.put("goodsCtgy","199,198,197");
        jsonObject.put("rcExt", JSONObject.toJSONString(rcExt));
        Map map = new HashMap();
        map.put("merchantId", channelAccount.getChannelAccount());
        map.put("merchantName", channelAccount.getChannelAlias());
        map.put("orderAmt", channelPay.getAmount());
        map.put("sumGoodsName", RandomUtil.randomStr(16));
        Map<String,String> payCardMap = new HashMap<>();
        payCardMap.put("cardNo",channelPay.getUserCard().getCardNo());
        payCardMap.put("cardType","p1");
        payCardMap.put("bankNo",channelPay.getUserCard().getBankCode());

        payCardMap.put("certNo",channelPay.getUserCard().getCardId());
        payCardMap.put("certType","01");
        payCardMap.put("name",channelPay.getUserCard().getOwnerName());
        payCardMap.put("phone",channelPay.getUserCard().getCardMobile());
        if(StringUtils.isEmpty(channelPay.getContractId())){
            jsonObject.put("payCardList", JSONObject.toJSONString(Arrays.asList(payCardMap)));
        }
        List list = new ArrayList();
        list.add(map);
        jsonObject.put("merchantSettleInfo", JSONObject.toJSONString(list));

        return jsonObject;
    }

    public static JSONObject doChargeSmsRequest(ChannelChargeSms channelChargeSms, String channelAccount) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version", "2.2.0");
        jsonObject.put("merchantId", channelAccount);
        jsonObject.put("token", channelChargeSms.getExt().getString("token"));
        jsonObject.put("smsCode", channelChargeSms.getValidateCode());

        return jsonObject;
    }

    public static JSONObject buildPayExceptionRequest(ChannelOrder channelOrder, ChannelAccount channelAccount){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version","2.2.0");
        jsonObject.put("transType","01");
        jsonObject.put("merchantId",channelAccount.getChannelAccount());
        jsonObject.put("oriMerchantOrderId", channelOrder.getChannelOrderNo());
        jsonObject.put("oriMerchantOrderTime",channelOrder.getCreateTime().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        return jsonObject;
    }

    public static YufuCreateDefrayRequest buildDefrayRequest(ChannelDefray channelDefray, ChannelAccount channelAccount){
        YufuCreateDefrayRequest request = new YufuCreateDefrayRequest();
        request.setVersion("1.0.0");
        request.setMerchantId(channelAccount.getChannelAccount());
        request.setMerchantOrderId(channelDefray.getOrderNo());
        request.setMerchantOrderTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        YufuPayInfo payInfo = new YufuPayInfo();
        payInfo.setAccountName(channelDefray.getReceiveName());
        payInfo.setAccountNo(channelDefray.getCardNo());
        payInfo.setBankName(channelDefray.getBankName());
        payInfo.setAmt(String.valueOf(channelDefray.getAmount().intValue()));
        payInfo.setPblFlag("02");
        payInfo.setRemark( RandomUtil.randomStr(5));
        request.setPayInfo(JSONObject.toJSONString(payInfo));
        request.setBackUrl(channelAccount.getNotifyUrl() + "/notify/" + PayChannelEnum.YUFU.getChannelCode() + "/defray/" + channelDefray.getOrderNo());
        return request;
    }

    public static YufuDefrayQueryRequest buildDefrayQueryRequest(String orderNo,ChannelAccount channelAccount){
        YufuDefrayQueryRequest request = new YufuDefrayQueryRequest();
        request.setVersion("1.0.0");
        request.setMerchantId(channelAccount.getChannelAccount());
        request.setMerchantOrderId(orderNo);
        return request;
    }

}
