package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.youxin.chat.pay.entity.req.UserOrderReq;
import com.youxin.chat.pay.entity.vo.UserOrderItem;
import com.youxin.chat.pay.model.CommonOrder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;

public interface CommonOrderMapper extends BaseMapper<CommonOrder> {

    int changeOrderStatus(@Param("status") Integer status, @Param("orderNo") String orderNo);

    List<UserOrderItem> getCommonOrderList(Page<UserOrderItem> page, @Param("userNo") String userNo, @Param("commonOrderRequest") UserOrderReq commonOrderRequest);

    BigDecimal getOrderAmount(@Param("feeType") int feeType, @Param("userNo") String userNo, @Param("commonOrderRequest") UserOrderReq commonOrderRequest);

    void updateDisplayByIds(@Param("display") int display, @Param("list") List<String> ids);

    @Select("select * from t_common_order where order_no=#{orderNo}")
    CommonOrder selectByOrderNo(@Param("orderNo") String orderNo);
    //BigDecimal sumOrderAmountByParam(@Param("record") CommonOrderSearch search);
}
