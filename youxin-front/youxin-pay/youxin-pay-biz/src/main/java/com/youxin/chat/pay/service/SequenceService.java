package com.youxin.chat.pay.service;

/**
 * description: SequenceService <br>
 * date: 2020/2/29 12:36 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface SequenceService {

    String buildUserOrderNo();

    String buildChannelOrderNo();

    String buildThirdOrderNo(String prefix);

    String buildUserCashNo();

}
