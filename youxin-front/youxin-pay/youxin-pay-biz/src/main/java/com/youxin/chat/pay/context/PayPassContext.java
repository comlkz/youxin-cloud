package com.youxin.chat.pay.context;


import com.youxin.utils.MD5Util;

public class PayPassContext {

    public static boolean validatePass(String userNo, String validatePass, String pass) {
        String salt = getSalt(userNo);
        String inputPass = MD5Util.md5Salt(validatePass, salt, 1);
        return inputPass.equalsIgnoreCase(pass);
    }

    private static String getSalt(String userNo) {
        int endIndex = userNo.length();
        int beginIndex = 0;

        if (endIndex > 6) {
            beginIndex = endIndex - 6;
        }
        String salt = userNo.substring(beginIndex, endIndex);
        return salt;
    }
}
