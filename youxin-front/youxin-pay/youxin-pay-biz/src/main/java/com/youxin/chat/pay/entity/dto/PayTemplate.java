package com.youxin.chat.pay.entity.dto;

import java.io.Serializable;

/**
 * description: PayTemplate <br>
 * date: 2020/2/1 11:33 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class PayTemplate implements Serializable {

    /**
     * key值
     */
    private String key;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String value;

    /**
     * 排序号
     */
    private int orderNum;

    public PayTemplate(String key, String title, String value, int orderNum) {
        this.key = key;
        this.title = title;
        this.value = value;
        this.orderNum = orderNum;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }
}
