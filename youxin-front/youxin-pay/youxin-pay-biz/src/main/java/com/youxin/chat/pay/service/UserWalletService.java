package com.youxin.chat.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.pay.entity.vo.*;
import com.youxin.chat.pay.model.UserWallet;
import com.youxin.chat.pay.service.channel.entity.UserCardDto;

import java.util.List;

/**
 * description: UserWalletService <br>
 * date: 2020/3/1 10:18 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface UserWalletService extends IService<UserWallet> {

    UserWalletVo showUserWallet(String userNo);

    UserBalanceVo selectBalance(String userNo);

    void userpaypaw(PayPassReq payPawDto, String userNo);

    void forgetPayPaw(String userNo, ForgetPayPassReq forgetPayPaw);

    void forgetPayPawSms(String userNo, Long ip);

    List<BankInfoVo> listBank();

    List<UserCardVo> getUserCardList(String userNo);

    void updateUserWalletStates(String userNo, Integer status);

    int getWalletStatus(String userNo);
}
