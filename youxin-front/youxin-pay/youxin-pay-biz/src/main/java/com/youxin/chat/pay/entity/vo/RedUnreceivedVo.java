package com.youxin.chat.pay.entity.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class RedUnreceivedVo implements Serializable {
    @ApiModelProperty(value = "用户编码")
    private String userNo;
    @ApiModelProperty(value = "群编码")
    private String groupNo;
    @ApiModelProperty(value = "祝福语")
    private String redMsg;
    @ApiModelProperty(value = "红包类型 0：普通红包 1：拼手气红包  2：单个红包 3: 定向红包")
    private Integer redType;
    @ApiModelProperty(value = "红包状态 0：正在领取  1：领取完成 2：红包过期")
    private Integer redStatus;
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "剩余金额（单位分）")
    private BigDecimal leftAmount;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getRedMsg() {
        return redMsg;
    }

    public void setRedMsg(String redMsg) {
        this.redMsg = redMsg;
    }

    public Integer getRedType() {
        return redType;
    }

    public void setRedType(Integer redType) {
        this.redType = redType;
    }

    public Integer getRedStatus() {
        return redStatus;
    }

    public void setRedStatus(Integer redStatus) {
        this.redStatus = redStatus;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public BigDecimal getLeftAmount() {
        return leftAmount;
    }

    public void setLeftAmount(BigDecimal leftAmount) {
        this.leftAmount = leftAmount;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
