package com.youxin.chat.pay.service;


import com.youxin.base.TResult;
import com.youxin.chat.pay.dto.PayRulesDto;
import com.youxin.chat.pay.entity.req.*;
import com.youxin.chat.pay.entity.vo.ChannelBalanceVo;
import com.youxin.chat.pay.entity.vo.DefrayRateVo;
import com.youxin.chat.pay.service.channel.entity.ChannelNotifyRequest;
import com.youxin.exception.SystemException;

public interface UserPayService {

    TResult<String> auditBindCard(UserCardReq userCardDto, String userNo) throws SystemException;

    TResult<String> validateBindSms(UserCardSmsReq userCardSmsDto, String userNo) throws SystemException;

    TResult<String> doCharge(UserChargeReq userChargeDto, String userNo, String userIp) throws SystemException;

    TResult<String> terminationCard(TerminationCardReq terminationCardDto, String userNo) throws SystemException;

    TResult<String> payQuery(String rechargeNo) throws SystemException;

    TResult<String> defray(UserCashReq userCashReq, String userNo) throws SystemException;

    TResult<String> defrayQuery(String cashNo) throws SystemException;

    TResult<String> rechargeNotify(ChannelNotifyRequest notifyRequest) throws SystemException;

    TResult<String> defrayNotify(ChannelNotifyRequest notifyRequest) throws SystemException;

    TResult<String> doChargeSms(UserChargeSmsReq userChargeSmsDto, String userNo) throws SystemException;

    DefrayRateVo defrayRate(String amount, String userNo) throws SystemException;

    TResult<ChannelBalanceVo> balanceQuery(String merchantId) throws SystemException;

    PayRulesDto getPayRules() throws SystemException;
}
