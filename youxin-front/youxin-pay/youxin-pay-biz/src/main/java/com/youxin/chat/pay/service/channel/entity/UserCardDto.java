package com.youxin.chat.pay.service.channel.entity;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class UserCardDto implements Serializable {

    @ApiModelProperty(value = "id",required = true)
    private String id;

    @ApiModelProperty(value = "银行卡号",required = true)
    @NotNull(message = "银行卡号不能为空")
    private String cardNo;

    @ApiModelProperty(value = "姓名",required = true)
    @NotNull(message = "姓名不能为空")
    private String ownerName;

    @ApiModelProperty(value = "身份证号",required = true)
    @NotNull(message = "身份证号不能为空")
    private String cardId;

    @ApiModelProperty(value = "手机号",required = true)
    @NotNull(message = "手机号不能为空")
    private String cardMobile;

    @ApiModelProperty(value = "卡类型 0：储蓄卡  1：信用卡",required = false)
    private Integer cardType;

    @ApiModelProperty(value = "用户IP",required = false)
    private String userIp;

    @ApiModelProperty(value = " 银行名称",required = false)
    private String bankName;

    @ApiModelProperty(value = " 银行logo",required = false)
    private String bankIcon;

    @ApiModelProperty(value = " 银行大背景",required = false)
    private String bankCover;

    private String bankCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardMobile() {
        return cardMobile;
    }

    public void setCardMobile(String cardMobile) {
        this.cardMobile = cardMobile;
    }

    public Integer getCardType() {
        return cardType;
    }

    public void setCardType(Integer cardType) {
        this.cardType = cardType;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankIcon() {
        return bankIcon;
    }

    public void setBankIcon(String bankIcon) {
        this.bankIcon = bankIcon;
    }

    public String getBankCover() {
        return bankCover;
    }

    public void setBankCover(String bankCover) {
        this.bankCover = bankCover;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
}
