package com.youxin.chat.pay.service.channel.impl.yufu.request;

import com.alibaba.fastjson.JSONObject;
import com.youxin.chat.pay.service.channel.entity.ChannelTerminationCard;

public class YufuTermination {
    public static JSONObject buildTerminationRequest(ChannelTerminationCard channelTerminationCard){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version","2.2.0");
        jsonObject.put("merchantId",channelTerminationCard.getChannelAccount());
        jsonObject.put("merchantUserId",channelTerminationCard.getUserNo());
        jsonObject.put("signId", channelTerminationCard.getContractId());

        return jsonObject;
    }
}
