package com.youxin.chat.pay.service.manager;

import com.youxin.chat.pay.context.CommonOrderContext;
import com.youxin.chat.pay.enums.FeeTypeEnum;
import com.youxin.chat.pay.enums.PayResultEnum;
import com.youxin.chat.pay.mapper.CommonOrderMapper;
import com.youxin.chat.pay.mapper.UserTransferMapper;
import com.youxin.chat.pay.mapper.UserWalletMapper;
import com.youxin.chat.pay.model.CommonOrder;
import com.youxin.chat.pay.model.UserTransfer;
import com.youxin.chat.pay.service.SequenceService;
import com.youxin.exception.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * description: TransferManager <br>
 * date: 2020/2/29 16:30 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class TransferManager {

    private static final Logger logger = LoggerFactory.getLogger(TransferManager.class);


    @Resource
    private UserWalletMapper userWalletMapper;

    @Resource
    private SequenceService sequenceService;

    @Resource
    private UserTransferMapper userTransferMapper;

    @Resource
    private CommonOrderMapper commonOrderMapper;


    @Transactional
    public String transferMoney(String fromUserNo, String toUserNo, BigDecimal amount, String remark, String code) {
        int count = userWalletMapper.doDecreaseAmount(amount,fromUserNo);
        if(count < 1){
            logger.error("转帐异常，用户余额不足,userNo={},amount={}",fromUserNo,amount);
            throw new SystemException(PayResultEnum.PAY_FAIL.getCode(),"转帐失败，用户余额不足");
        }
        count = userWalletMapper.doIncreaseAmount(amount,toUserNo);
        if(count < 1){
            logger.error("转帐异常,userNo={},amount={}",fromUserNo,amount);
            throw new SystemException(PayResultEnum.PAY_FAIL.getCode(),"转帐失败，请联系客服");
        }
        String orderNo = sequenceService.buildThirdOrderNo("TR");
        UserTransfer userTransfer = buildUserTransfer(fromUserNo, toUserNo, amount,remark,code);
        userTransfer.setOrderNo(orderNo);
        userTransfer.setFeeType(1);
        userTransferMapper.insert(userTransfer);
        String fromOrderNo = userTransfer.getId();
        CommonOrder fromCommonOrder = CommonOrderContext.buildCommonOrder(fromOrderNo,fromUserNo,amount, FeeTypeEnum.OUT_MONEY.ordinal());
        commonOrderMapper.insert(fromCommonOrder);
        userTransfer.setFeeType(0);
        userTransfer.setId(null);
        userTransferMapper.insert(userTransfer);

        CommonOrder toCommonOrder = CommonOrderContext.buildCommonOrder(orderNo,toUserNo,amount,FeeTypeEnum.IN_MONEY.ordinal());

        commonOrderMapper.insert(toCommonOrder);
        return orderNo;
    }

    private UserTransfer buildUserTransfer(String fromUserNo, String toUserNo, BigDecimal amount,String remark,String code){
        UserTransfer userTransfer = new UserTransfer();
        userTransfer.setFromUserNo(fromUserNo);
        userTransfer.setToUserNo(toUserNo);
        userTransfer.setPayCode(code);
        userTransfer.setAmount(amount);
        userTransfer.setCreateTime(LocalDateTime.now());
        userTransfer.setRemark(remark);
        return userTransfer;
    }
}
