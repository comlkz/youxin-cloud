package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.UserTransfer;

public interface UserTransferMapper extends BaseMapper<UserTransfer> {
}
