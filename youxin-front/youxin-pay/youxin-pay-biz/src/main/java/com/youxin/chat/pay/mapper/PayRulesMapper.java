package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.PayRules;

public interface PayRulesMapper extends BaseMapper<PayRules> {
}
