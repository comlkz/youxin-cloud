package com.youxin.chat.pay.service.channel.impl.liantong.request;

import com.alibaba.fastjson.JSONObject;
import com.youxin.chat.pay.service.channel.entity.ChannelBindCard;
import com.youxin.chat.pay.service.channel.entity.ChannelBindCardSms;
import com.youxin.utils.AESUtil;


import java.text.SimpleDateFormat;
import java.util.Date;

public class LianTongBindCard {


    public static JSONObject auditBindCardRequest(ChannelBindCard userCard, String userIp, String orderNo, String key) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version", "1.0.0");
        jsonObject.put("busiType", "SIGN");
        jsonObject.put("merNo", userCard.getChannelAccount());
        jsonObject.put("merUserId", userCard.getUserNo());
        jsonObject.put("merOrderNo", orderNo);
        jsonObject.put("merOrderDate", new SimpleDateFormat("yyyyMMdd").format(new Date()));
        jsonObject.put("signType", "RSA_SHA256");
        jsonObject.put("payTool", "AUTHPAY");
        jsonObject.put("bankAcctNo", AESUtil.encryptToBase64(userCard.getCardNo(), key));
        jsonObject.put("bankAcctName", AESUtil.encryptToBase64(userCard.getOwnerName(), key));
        jsonObject.put("idType", "01");
        jsonObject.put("idNo", AESUtil.encryptToBase64(userCard.getCardId(), key));
        jsonObject.put("mobileNo", AESUtil.encryptToBase64(userCard.getCardMobile(), key));
        jsonObject.put("userIp", userIp);
        return jsonObject;
    }

    public static JSONObject validateBindSmsRequest(ChannelBindCardSms userCardSmsDto, String channelAccount) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version", "1.0.0");
        jsonObject.put("busiType", "SIGNMSG");
        jsonObject.put("merNo", channelAccount);
        jsonObject.put("merOrderNo", userCardSmsDto.getOrderNo());
        jsonObject.put("merOrderDate", new SimpleDateFormat("yyyyMMdd").format(new Date()));
        jsonObject.put("signType", "RSA_SHA256");
        jsonObject.put("smsCode", userCardSmsDto.getValidateCode());
        return jsonObject;
    }


}
