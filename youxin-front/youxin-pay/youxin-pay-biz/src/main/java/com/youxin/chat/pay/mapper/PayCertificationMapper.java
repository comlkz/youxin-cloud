package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.PayCertification;

public interface PayCertificationMapper extends BaseMapper<PayCertification> {
}
