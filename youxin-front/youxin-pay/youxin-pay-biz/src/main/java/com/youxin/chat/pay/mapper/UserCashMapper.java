package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.chat.pay.entity.search.CashSearch;
import com.youxin.chat.pay.model.SystemSettlement;
import com.youxin.chat.pay.model.UserCash;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface UserCashMapper extends BaseMapper<UserCash> {
   // IPage<UserCashDto> selectCashList(@Param("userCashDto") UserCashDto userCashDto, Page page);

   // UserCashDto selectCash(@Param("cashNo") String cashNo);

    @Select("select * from t_user_cash where cash_no=#{cashNo}")
    UserCash selectByCashNo(@Param("cashNo")String cashNo);

    BigDecimal selectMaxAmountByDay(@Param("record") CashSearch cashSearch);

    int selectCountByDay(@Param("record") CashSearch cashSearch);

    int doCashFinish(@Param("status") Integer status, @Param("cashNo") String cashNo);

    SystemSettlement staticCashData(@Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);

    @Select("select cash_no from t_user_cash where cash_status=0 and create_time <#{time}")
    List<String> selectProcessCash(@Param("time")LocalDateTime time);
}
