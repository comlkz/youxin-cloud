package com.youxin.chat.pay.service.channel.impl.ainong.convert;


import com.youxin.base.BaseResultCode;
import com.youxin.chat.pay.service.channel.entity.ChannelResponse;

public class AinongErrorTransfer {
    public static ChannelResponse buildErrorResult(String code, String errorMsg) {
        if ("ES0005".equalsIgnoreCase(code)) {
            return ChannelResponse.build("ES0005", "访问限流");
        }
        if ("ES0011".equalsIgnoreCase(code)) {
            return ChannelResponse.build("ES0011", "平台维护中");
        }
        if ("ES0012".equalsIgnoreCase(code)) {
            return ChannelResponse.build("ES0012", "业务请求受限");
        }
        if ("ES9999".equalsIgnoreCase(code)) {
            return ChannelResponse.build("ES9999", "系统繁忙");
        }

        return ChannelResponse.build(BaseResultCode.COMMON_FAIL, errorMsg);
    }

}
