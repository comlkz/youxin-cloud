package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.PaymentCode;

public interface PaymentCodeMapper extends BaseMapper<PaymentCode> {
}
