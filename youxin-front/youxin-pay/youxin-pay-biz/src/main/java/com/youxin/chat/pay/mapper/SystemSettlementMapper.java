package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.SystemSettlement;

/**
 * description: SystemSettlementMapper <br>
 * date: 2020/2/9 13:00 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface SystemSettlementMapper extends BaseMapper<SystemSettlement> {
}
