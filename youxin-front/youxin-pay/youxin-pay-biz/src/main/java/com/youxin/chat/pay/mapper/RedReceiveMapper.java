package com.youxin.chat.pay.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.entity.vo.RedReceiveItem;
import com.youxin.chat.pay.model.RedReceive;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface RedReceiveMapper extends BaseMapper<RedReceive> {

    @Select("select  USER_NO, NICK_NAME, AMOUNT,AVATAR, RECEIVE_TIME, UPDATE_TIME from t_red_receive where RED_ID=#{redId}")
    List<RedReceiveItem> selectRedReceive(@Param("redId") String redId);

    @Select("select count(1) from t_red_receive where red_id=#{redId} and user_no=#{userNo}")
    int countByRedIdAndUserNo(@Param("redId")String redId,@Param("userNo") String userNo);
}
