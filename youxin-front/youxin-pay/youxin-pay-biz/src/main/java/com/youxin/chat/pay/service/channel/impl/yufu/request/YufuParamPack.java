package com.youxin.chat.pay.service.channel.impl.yufu.request;

public class YufuParamPack {
    private String data;
    private String enc;
    private String sign;

    public YufuParamPack(String data, String enc, String sign) {
        this.data = data;
        this.enc = enc;
        this.sign = sign;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getEnc() {
        return enc;
    }

    public void setEnc(String enc) {
        this.enc = enc;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
