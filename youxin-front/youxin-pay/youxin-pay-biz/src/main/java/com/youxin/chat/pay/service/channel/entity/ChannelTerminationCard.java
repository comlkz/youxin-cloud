package com.youxin.chat.pay.service.channel.entity;

import java.io.Serializable;

public class ChannelTerminationCard implements Serializable {
    private String userNo;

    private String cardNo;

    /**
     * 协议ID
     */
    private String contractId;

    /**
     * 渠道帐号
     */
    private String channelAccount;

    /**
     * 对方ip
     */
    private String userIp;

    /**
     * 拓展字段
     */
    private String ext;

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getChannelAccount() {
        return channelAccount;
    }

    public void setChannelAccount(String channelAccount) {
        this.channelAccount = channelAccount;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}
