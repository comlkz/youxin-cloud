package com.youxin.chat.pay.service;

/**
 * description: ITemplateService <br>
 * date: 2020/2/1 11:49 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface TemplateService {

    /**
     * 发送红包过期消息
     * @param orderNo
     */
    void sendRedPackExpireMessage(String orderNo);

    /**
     * 发送充值成功消息
     * @param orderNo
     */
    void sendChargeSuccMessage(String orderNo);


    /**
     * 发送提现成功消息
     * @param orderNo
     */
    void sendDefraySuccMessage(String orderNo);

    /**
     * 发送付款成功消息
     * @param orderNo
     */
    void sendPaySuccMessage(String orderNo);

    /**
     * 发送到帐成功消息
     * @param orderNo
     */
    void sendReceiveMessage(String orderNo);
}
