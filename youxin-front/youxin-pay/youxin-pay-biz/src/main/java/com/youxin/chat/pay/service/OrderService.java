package com.youxin.chat.pay.service;

import com.youxin.chat.pay.dto.CashStatusChange;
import com.youxin.chat.pay.entity.req.UserOrderReq;
import com.youxin.chat.pay.entity.vo.OrderDetailVo;
import com.youxin.chat.pay.entity.vo.UserOrderVo;

import java.util.List;

/**
 * description: OrderService <br>
 * date: 2020/3/1 11:07 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface OrderService {

    UserOrderVo getCommonOrderList(String userNo, UserOrderReq commonOrderRequest);

    OrderDetailVo getCommonOrderDetail(String userNo, String orderNo);

    void orderDel(String userNo, List<String> idList);

    void changeCashStatus(CashStatusChange cashStatusChange);

    void syncCashStatus(Integer minute);

    void syncOrderStatus(String orderNo);

    void syncCashStatus(String orderNo);

}
