package com.youxin.chat.pay.context;



import com.youxin.chat.pay.entity.req.UserCashReq;
import com.youxin.chat.pay.enums.FeeTypeEnum;
import com.youxin.chat.pay.enums.OrderSourceEnum;
import com.youxin.chat.pay.enums.OrderStatusEnum;
import com.youxin.chat.pay.model.CommonOrder;
import com.youxin.chat.pay.model.RedReceive;
import com.youxin.chat.pay.model.RedSend;
import com.youxin.chat.pay.model.UserRecharge;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class CommonOrderContext {

    public static CommonOrder buildPayCommonOrder(UserRecharge userRecharge, String agentNo) {
        CommonOrder commonOrder = new CommonOrder();
        commonOrder.setOrderNo(userRecharge.getRechargeNo());
        commonOrder.setOrderRemark(OrderSourceEnum.PAY.getDesc());
        commonOrder.setOrderSource(OrderSourceEnum.PAY.getStatus());
        commonOrder.setClientIp(userRecharge.getClientIp());
        commonOrder.setOrderStatus(OrderStatusEnum.NOT_PAY.getStatus());
        commonOrder.setOrderAmount(userRecharge.getRechargeAmount());
        commonOrder.setUserNo(userRecharge.getUserNo());
        commonOrder.setAgentNo(agentNo);
        commonOrder.setCreateTime(LocalDateTime.now());
        commonOrder.setUpdateTime(LocalDateTime.now());
        commonOrder.setFeeType(0);
        return commonOrder;
    }

    public static CommonOrder buildCommonOrder(String orderNo, String userNo, BigDecimal amount, int type) {
        CommonOrder commonOrder = new CommonOrder();
        commonOrder.setUserNo(userNo);
        commonOrder.setOrderNo(orderNo);
        OrderSourceEnum statusEnum;
        if(type == 0) {
            statusEnum = OrderSourceEnum.PAY_CODE_RECEIVE;
        }else{
            statusEnum = OrderSourceEnum.PAY_CODE_TRANSFER;

        }
        commonOrder.setOrderSource(statusEnum.getStatus());
        commonOrder.setOrderRemark(statusEnum.getDesc());
        commonOrder.setOrderAmount(amount);
        commonOrder.setOrderStatus(OrderStatusEnum.PAY_SUCCESS.getStatus());
        commonOrder.setFeeType(type);
        //   commonOrder.setClientIp(ip);
        commonOrder.setOpTime(LocalDateTime.now());
        commonOrder.setCreateTime(LocalDateTime.now());
        commonOrder.setUpdateTime(LocalDateTime.now());
        return commonOrder;
    }

   public static CommonOrder buildDefrayCommonOrder(UserCashReq userCashReq, String userNo, String orderNo, String agentNo) {
        CommonOrder commonOrder = new CommonOrder();
        commonOrder.setOrderNo(orderNo);
        commonOrder.setOrderRemark(OrderSourceEnum.DEFRAY.getDesc());
        commonOrder.setOrderSource(OrderSourceEnum.DEFRAY.getStatus());
        commonOrder.setClientIp(userCashReq.getClientIp());
        commonOrder.setOrderStatus(OrderStatusEnum.NOT_PAY.getStatus());
        commonOrder.setOrderAmount(userCashReq.getAmount());
        commonOrder.setUserNo(userNo);
        commonOrder.setFeeType(1);
        commonOrder.setAgentNo(agentNo);
        commonOrder.setCreateTime(LocalDateTime.now());
        commonOrder.setUpdateTime(LocalDateTime.now());
        return commonOrder;
    }

    public static CommonOrder buildCommonOrder(RedSend redSend, String agentNo) {
        CommonOrder commonOrder = new CommonOrder();
        commonOrder.setUserNo(redSend.getUserNo());
        commonOrder.setOrderNo(redSend.getId());
        commonOrder.setOrderSource(OrderSourceEnum.RED_SEND.getStatus());
        commonOrder.setOrderRemark(OrderSourceEnum.RED_SEND.getDesc());
        commonOrder.setOrderAmount(redSend.getRedAmount());
        commonOrder.setOrderStatus(OrderStatusEnum.PAY_SUCCESS.getStatus());
        commonOrder.setFeeType(FeeTypeEnum.OUT_MONEY.ordinal());
        commonOrder.setClientIp(redSend.getClientIp());
        commonOrder.setAgentNo(agentNo);
        commonOrder.setOpTime(LocalDateTime.now());
        commonOrder.setCreateTime(LocalDateTime.now());
        commonOrder.setUpdateTime(LocalDateTime.now());
        return commonOrder;
    }

    public static  CommonOrder buildCommonOrder(RedReceive redReceive, String ip, String agentNo) {
        CommonOrder commonOrder = new CommonOrder();
        commonOrder.setUserNo(redReceive.getUserNo());
        commonOrder.setOrderNo(redReceive.getId());
        commonOrder.setOrderSource(OrderSourceEnum.RED_RECEIVE.getStatus());
        commonOrder.setOrderRemark(OrderSourceEnum.RED_RECEIVE.getDesc());
        commonOrder.setOrderAmount(redReceive.getAmount());
        commonOrder.setOrderStatus(OrderStatusEnum.PAY_SUCCESS.getStatus());
        commonOrder.setFeeType(FeeTypeEnum.IN_MONEY.ordinal());
        commonOrder.setClientIp(ip);
        commonOrder.setOpTime(LocalDateTime.now());
        commonOrder.setAgentNo(agentNo);
        commonOrder.setCreateTime(LocalDateTime.now());
        commonOrder.setUpdateTime(LocalDateTime.now());
        return commonOrder;
    }

    public static CommonOrder buildExpireCommonOrder(RedSend redSend, String refundOrderNo, BigDecimal leftAmount) {
        CommonOrder commonOrder = new CommonOrder();
        commonOrder.setUserNo(redSend.getUserNo());
        commonOrder.setOrderNo(refundOrderNo);
        commonOrder.setOrderSource(OrderSourceEnum.RED_REFUND.getStatus());
        commonOrder.setOrderRemark(OrderSourceEnum.RED_REFUND.getDesc());
        commonOrder.setOrderAmount(leftAmount);
        commonOrder.setOrderStatus(OrderStatusEnum.PAY_SUCCESS.getStatus());
        commonOrder.setFeeType(FeeTypeEnum.IN_MONEY.ordinal());
        commonOrder.setClientIp(redSend.getClientIp());
        commonOrder.setOpTime(LocalDateTime.now());
        commonOrder.setCreateTime(LocalDateTime.now());
        commonOrder.setUpdateTime(LocalDateTime.now());
        return commonOrder;
    }
}
