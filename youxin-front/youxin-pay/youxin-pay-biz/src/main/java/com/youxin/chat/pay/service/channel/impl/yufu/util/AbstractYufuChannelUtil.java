package com.youxin.chat.pay.service.channel.impl.yufu.util;

import com.youxin.chat.pay.service.channel.impl.yufu.request.YufuParamPack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class AbstractYufuChannelUtil {
    private static final Logger logger = LoggerFactory.getLogger(AbstractYufuChannelUtil.class);

    public static Map<String,Object> transferPackToMap(String channelAccount, YufuParamPack request){
        Map<String,Object> map = new HashMap<>();
        try {
           /* map.put("merchantId", channelAccount);
            map.put("data", URLEncoder.encode(request.getData(), "utf-8"));
            map.put("enc", URLEncoder.encode(request.getEnc(), "utf-8"));
            map.put("sign", URLEncoder.encode(request.getSign(), "utf-8"));*/
            map.put("merchantId", channelAccount);
            map.put("data",request.getData());
            map.put("enc",request.getEnc());
            map.put("sign",request.getSign());

        }catch (Exception e){
            logger.error("url编码错误",e);
        }
        return map;
    }
}
