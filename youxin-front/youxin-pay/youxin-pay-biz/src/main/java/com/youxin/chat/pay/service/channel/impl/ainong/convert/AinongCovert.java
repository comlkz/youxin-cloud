package com.youxin.chat.pay.service.channel.impl.ainong.convert;

import com.alibaba.fastjson.JSONObject;

public class AinongCovert {

    public static JSONObject buildParams(JSONObject data, String channelAccount, String traceNo) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version", "2.0.0");
        jsonObject.put("merId", channelAccount);
        //String traceNo = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) + RandomUtil.randomStr(5);
        jsonObject.put("traceNo", traceNo);
        jsonObject.put("signMethod", "RSA");
        jsonObject.put("data", data);
        return jsonObject;
    }

}
