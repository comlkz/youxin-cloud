package com.youxin.chat.pay.entity.req;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class UserChargeSmsReq implements Serializable {
    @ApiModelProperty(value = "短信验证码",required = true)
    @NotNull(message = "短信验证码不能为空")
    private String validateCode;

    @ApiModelProperty(value = "申请返回ID",required = true)
    @NotNull(message = "申请返回ID不能为空")
    private String id;

    public String getValidateCode() {
        return validateCode;
    }

    public void setValidateCode(String validateCode) {
        this.validateCode = validateCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
