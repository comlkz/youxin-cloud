package com.youxin.chat.pay.service.manager.message;

import io.rong.messages.BaseMessage;
import io.rong.util.GsonUtil;

import java.util.Map;

public class RedPackReceiveMessage extends BaseMessage {
    //操作人用户 Id
    private String operatorUserId;

    //操作名称
    private String operation;

    private String redPackId;

    private String msg;

    private  Map<String, String> user;

    //可以放置任意的数据内容，也可以去掉此属性
    private String extra;

    private transient static final String TYPE = "GR:RedPacketReceiveMsg";

    public RedPackReceiveMessage(String operatorUserId, String operation, String redPackId, String msg, Map<String, String> user) {
        this.operatorUserId = operatorUserId;
        this.operation = operation;
        this.redPackId = redPackId;
        this.msg = msg;
        this.user = user;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public String toString() {

        return GsonUtil.toJson(this, RedPackReceiveMessage.class);
    }

    public String getOperatorUserId() {
        return operatorUserId;
    }

    public void setOperatorUserId(String operatorUserId) {
        this.operatorUserId = operatorUserId;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getRedPackId() {
        return redPackId;
    }

    public void setRedPackId(String redPackId) {
        this.redPackId = redPackId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Map<String, String> getUser() {
        return user;
    }

    public void setUser(Map<String, String> user) {
        this.user = user;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
