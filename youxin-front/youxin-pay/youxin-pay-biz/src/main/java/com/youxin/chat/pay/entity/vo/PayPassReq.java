package com.youxin.chat.pay.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "设置支付密码参数")
public class PayPassReq implements Serializable {

    @ApiModelProperty(value = "旧密码",required = false)
    private String oldPass;
    @ApiModelProperty(value = "新密码",required = true)
    private String newPass;

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

}
