package com.youxin.chat.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.pay.dto.PayRulesDto;
import com.youxin.chat.pay.model.PayRules;

/**
 * description: PayRulesService <br>
 * date: 2020/2/29 11:43 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface PayRulesService extends IService<PayRules> {

    PayRulesDto info();

    void saveRisk(PayRulesDto riskDto);
}
