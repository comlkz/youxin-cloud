package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.AgentStatic;

/**
 * description: 代理统计mapper <br>
 * date: 2020/1/6 14:26 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface AgentStaticMapper extends BaseMapper<AgentStatic> {
}
