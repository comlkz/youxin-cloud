package com.youxin.chat.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.pay.entity.vo.UserRechargeVo;
import com.youxin.chat.pay.model.UserRecharge;

import java.util.List;

/**
 * description: UserRechargeService <br>
 * date: 2020/3/1 10:39 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface UserRechargeService extends IService<UserRecharge> {

    List<UserRechargeVo> selectRechargeRecord(String userNo) ;

}
