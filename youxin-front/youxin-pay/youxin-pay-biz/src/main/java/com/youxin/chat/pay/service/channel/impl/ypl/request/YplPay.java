package com.youxin.chat.pay.service.channel.impl.ypl.request;

import com.alibaba.fastjson.JSONObject;

import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.service.channel.entity.ChannelChargeSms;
import com.youxin.chat.pay.service.channel.entity.ChannelPay;
import com.youxin.utils.RandomUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YplPay {


    public static JSONObject buildPayRequest(ChannelPay channelPay, ChannelAccount channelAccount) {
        JSONObject jsonObject = new JSONObject();
        Map<String, Object> orderInfo = new HashMap<>();
        orderInfo.put("Id", channelPay.getOrderNo());
        orderInfo.put("businessType", "100099");
        List<Map<String, String>> goods = new ArrayList<>();
        Map<String, String> goodMap = new HashMap<>();
        goodMap.put("name", RandomUtil.randomStr(15));
        goodMap.put("number", "1");
        goodMap.put("amount", String.valueOf(channelPay.getAmount()));
        goods.add(goodMap);
        orderInfo.put("goodsList", goods);
        jsonObject.put("outTradeNo", channelPay.getOrderNo());
        jsonObject.put("customerCode", channelAccount.getChannelAccount());
        jsonObject.put("memberId", channelPay.getUserNo());
        jsonObject.put("protocol", channelPay.getContractId());
        jsonObject.put("orderInfo", orderInfo);
        jsonObject.put("payAmount", channelPay.getAmount());
        String notifyUrl = channelAccount.getNotifyUrl() + "/notify/" + channelAccount.getChannelCode() + "/recharge/" + channelPay.getOrderNo();
        jsonObject.put("notifyUrl", notifyUrl);
        jsonObject.put("payCurrency", "CNY");
        jsonObject.put("transactionStartTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        jsonObject.put("nonceStr", RandomUtil.randomStr(32));

        return jsonObject;
    }

    public static JSONObject doChargeSmsRequest(ChannelChargeSms channelChargeSms) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", channelChargeSms.getExt().getString("token"));
        jsonObject.put("customerCode", channelChargeSms.getExt().getString("customerCode"));
        jsonObject.put("memberId", channelChargeSms.getExt().getString("memberId"));
        jsonObject.put("protocol", channelChargeSms.getExt().getString("protocol"));
        jsonObject.put("smsCode", channelChargeSms.getValidateCode());
        jsonObject.put("nonceStr", RandomUtil.randomStr(32));

        return jsonObject;
    }
}
