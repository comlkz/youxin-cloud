package com.youxin.chat.pay.entity.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class UserOrderReq implements Serializable {

    @ApiModelProperty(value = "-1:全部 0:用户充值 1:用户提现 2:红包发送 3:红包接收 4:红包退款 5:手机充值", required = true)
    private Integer type;
    @ApiModelProperty(value = "开始时间", required = false)
    private String start;
    @ApiModelProperty(value = "结束时间", required = false)
    private String end;
    @ApiModelProperty(value = "查询年月", required = false)
    private String time;

    private Integer currentPage = 1;

    private Integer showCount = 500;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getShowCount() {
        return showCount;
    }

    public void setShowCount(Integer showCount) {
        this.showCount = showCount;
    }
}
