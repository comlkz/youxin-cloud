package com.youxin.chat.pay.service.channel.impl.yufu.request;

import com.alibaba.fastjson.JSONObject;
import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.service.channel.entity.ChannelBindCard;
import com.youxin.chat.pay.service.channel.entity.ChannelBindCardSms;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YufuBindCard {
    public static JSONObject auditBindCardRequest(String orderNo, ChannelBindCard channelBindCard, ChannelAccount channelAccount) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version", "2.2.0");
        jsonObject.put("merchantId", channelAccount.getChannelAccount());
        jsonObject.put("gwType", "04");
        jsonObject.put("merchantUserId", orderNo);
        Map map = new HashMap();
        map.put("cardNo", channelBindCard.getCardNo());
        map.put("cardType", "P1");
        map.put("bankNo", channelBindCard.getBankNo());
        map.put("certNo", channelBindCard.getCardId());
        map.put("certType", "01");
        map.put("name", channelBindCard.getOwnerName());
        map.put("phone", channelBindCard.getCardMobile());
        List list = new ArrayList();
        list.add(map);
        jsonObject.put("signCardList", JSONObject.toJSONString(list));

        return jsonObject;
    }

    public static JSONObject validateBindSmsRequest(ChannelBindCardSms channelBindCardSms, ChannelAccount channelAccount) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("version", "2.2.0");
        jsonObject.put("merchantId", channelAccount.getChannelAccount());
        jsonObject.put("signToken", channelBindCardSms.getExt());
        jsonObject.put("smsCode", channelBindCardSms.getValidateCode());

        return jsonObject;
    }
}
