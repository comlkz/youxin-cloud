package com.youxin.chat.pay.service.channel.impl.test;

import com.youxin.base.BaseResultCode;
import com.youxin.chat.pay.enums.OrderStatusEnum;
import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.model.ChannelOrder;
import com.youxin.chat.pay.model.UserCash;
import com.youxin.chat.pay.model.UserRecharge;
import com.youxin.chat.pay.service.channel.IPayChannel;
import com.youxin.chat.pay.enums.PayChannelEnum;
import com.youxin.chat.pay.service.channel.entity.*;
import org.springframework.stereotype.Service;

@Service
public class TestPayChannel  implements IPayChannel {


    @Override
    public ChannelResponse bindCard(String orderNo, ChannelBindCard channelBindCard, ChannelAccount channelAccount) {
        return new ChannelResponse(BaseResultCode.SUCCESS);
    }

    @Override
    public ChannelResponse validateSms(ChannelBindCardSms channelBindCardSms, ChannelAccount channelAccount) {
        return new ChannelResponse(BaseResultCode.SUCCESS);
    }

    @Override
    public boolean support(String channelCode) {
        return PayChannelEnum.TEST.getChannelCode().equalsIgnoreCase(channelCode);
    }

    @Override
    public ChannelResponse pay(ChannelPay channelPay, ChannelAccount channelAccount) {
        return new ChannelResponse(BaseResultCode.SUCCESS);
    }

    @Override
    public ChannelResponse termination(ChannelTerminationCard channelTerminationCard, ChannelAccount channelAccount) {
        return new ChannelResponse(BaseResultCode.SUCCESS);
    }

    @Override
    public ChannelNotifyResponse orderNotify(ChannelNotifyRequest request, ChannelAccount channelAccount) {
        ChannelNotifyResponse channelResponse =  new ChannelNotifyResponse();
        channelResponse.setStatus(OrderStatusEnum.PAY_SUCCESS.getStatus());
        return channelResponse;
    }

    @Override
    public ChannelNotifyResponse defrayNotify(ChannelNotifyRequest request, ChannelAccount channelAccount) {
        return null;
    }

    @Override
    public ChannelResponse payQuery(ChannelOrder channelOrder, ChannelAccount channelAccount) {
        return new ChannelResponse(BaseResultCode.SUCCESS);
    }

    @Override
    public ChannelResponse defray(ChannelDefray channelDefray, ChannelAccount channelAccount) {
        return new ChannelResponse(BaseResultCode.SUCCESS);
    }

    @Override
    public ChannelResponse defrayQuery(UserCash userCash, ChannelAccount channelAccount) {
        return new ChannelResponse(BaseResultCode.SUCCESS);
    }

    @Override
    public ChannelResponse doChargeSms(ChannelChargeSms channelChargeSms, ChannelAccount channelAccount) {
        return new ChannelResponse(BaseResultCode.SUCCESS);
    }

    @Override
    public ChannelResponse balanceQuery(ChannelAccount channelAccount) {
        return null;
    }
}
