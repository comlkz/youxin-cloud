package com.youxin.chat.pay.service.channel.impl.liantong.request;

import com.alibaba.fastjson.JSONObject;

import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.model.UserCash;
import com.youxin.chat.pay.service.channel.entity.ChannelDefray;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LianTongDefray {

    public static JSONObject buildDefrayRequest(ChannelDefray channelDefray, ChannelAccount channelAccount, String backUrl) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("interfaceVersion", "1.0.0.2");
        jsonObject.put("tranType", "DF01");
        jsonObject.put("merNo", channelAccount.getChannelAccount());
        jsonObject.put("orderDate", new SimpleDateFormat("yyyyMMdd").format(new Date()));
        jsonObject.put("reqTime", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
        jsonObject.put("orderNo", channelDefray.getOrderNo());
        jsonObject.put("amount", channelDefray.getAmount());
        jsonObject.put("bizCode", "B0001");
        jsonObject.put("payeeAcc", channelDefray.getCardNo());
        jsonObject.put("woType", "1");
        jsonObject.put("signType", "RSA_SHA256");
        jsonObject.put("callbackUrl", backUrl);

        return jsonObject;
    }

    public static JSONObject buildDefrayQueryRequest(UserCash userCash, ChannelAccount channelAccount){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("interfaceVersion", "1.0.0.0");
        jsonObject.put("tranType", "DF01");
        jsonObject.put("merNo", channelAccount.getChannelAccount());
        jsonObject.put("orderDate", new SimpleDateFormat("yyyyMMdd").format(new Date()));
        jsonObject.put("reqTime", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
        jsonObject.put("orderNo", userCash.getCashNo());
        jsonObject.put("signType", "RSA_SHA256");

        return jsonObject;

    }
}
