package com.youxin.chat.pay.service.channel.impl.ypl.request;

import com.alibaba.fastjson.JSONObject;
import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.service.channel.entity.ChannelBindCard;
import com.youxin.chat.pay.service.channel.entity.ChannelBindCardSms;
import com.youxin.chat.pay.service.channel.impl.ypl.util.YplSignUtil;
import com.youxin.utils.RandomUtil;


public class YplBindCard {
    public static JSONObject auditBindCardRequest(String orderNo, ChannelBindCard channelBindCard, ChannelAccount channelAccount) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("mchtOrderNo", orderNo);
        jsonObject.put("customerCode", channelAccount.getChannelAccount());
        jsonObject.put("memberId", channelBindCard.getUserNo());
        jsonObject.put("userName", YplSignUtil.encryptByPublicKey(channelBindCard.getOwnerName(),channelAccount.getRsaKeyPub()));
        jsonObject.put("phoneNum",  YplSignUtil.encryptByPublicKey(channelBindCard.getCardMobile(),channelAccount.getRsaKeyPub()));
        jsonObject.put("bankCardNo",  YplSignUtil.encryptByPublicKey(channelBindCard.getCardNo(),channelAccount.getRsaKeyPub()));
        jsonObject.put("bankCardType", "debit");
        jsonObject.put("certificatesType", "01");
        jsonObject.put("certificatesNo",YplSignUtil.encryptByPublicKey(channelBindCard.getCardId(),channelAccount.getRsaKeyPub()));
        jsonObject.put("nonceStr", RandomUtil.randomStr(32));

        return jsonObject;
    }

    public static JSONObject validateBindSmsRequest(ChannelBindCardSms channelBindCardSms, ChannelAccount channelAccount) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("smsNo", channelBindCardSms.getExt());
        jsonObject.put("customerCode", channelAccount.getChannelAccount());
        jsonObject.put("memberId", channelBindCardSms.getUserNo());
        jsonObject.put("smsCode", channelBindCardSms.getValidateCode());
        jsonObject.put("nonceStr", RandomUtil.randomStr(32));

        return jsonObject;
    }
}
