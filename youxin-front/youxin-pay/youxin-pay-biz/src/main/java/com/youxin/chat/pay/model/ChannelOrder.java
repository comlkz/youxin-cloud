package com.youxin.chat.pay.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

public class ChannelOrder {

    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 渠道订单号
     */
    private String channelOrderNo;

    private String userNo;

    private String rechrageNo;//充值编号

    private BigDecimal rechargeAmount;//充值金额（单位：分）

    private BigDecimal payAmount;//支付金额（单位：分）

    private BigDecimal channelRate;//渠道费率（含%）

    private String payType;//支付方式

    private String cardNo;//银行卡号

    private String accountName;//银行卡姓名

    private String accountMobile;//手机号

    private String idNo;//身分证号

    private String clientIp;//用户IP

    private String contractId;//协议ID

    private String channelAccount;//渠道帐号

    private String channelAlias;//渠道别名

    private Date succTime;//成功时间

    private Integer payStatus;//支付状态 0：未支付  1：支付成功

    private Integer handleType;//处理方式 0：系统  1：人工

    private String handleUser;//处理人

    private LocalDateTime createTime;

    private LocalDateTime updateTime;//修改时间

    private String requestContext;//请求参数

    private String notifyContext;//通知参数

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRechrageNo() {
        return rechrageNo;
    }

    public void setRechrageNo(String rechrageNo) {
        this.rechrageNo = rechrageNo == null ? null : rechrageNo.trim();
    }

    public BigDecimal getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(BigDecimal rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public BigDecimal getChannelRate() {
        return channelRate;
    }

    public void setChannelRate(BigDecimal channelRate) {
        this.channelRate = channelRate;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType == null ? null : payType.trim();
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo == null ? null : cardNo.trim();
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName == null ? null : accountName.trim();
    }

    public String getAccountMobile() {
        return accountMobile;
    }

    public void setAccountMobile(String accountMobile) {
        this.accountMobile = accountMobile == null ? null : accountMobile.trim();
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo == null ? null : idNo.trim();
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp == null ? null : clientIp.trim();
    }

    public String getChannelAccount() {
        return channelAccount;
    }

    public void setChannelAccount(String channelAccount) {
        this.channelAccount = channelAccount == null ? null : channelAccount.trim();
    }

    public String getChannelAlias() {
        return channelAlias;
    }

    public void setChannelAlias(String channelAlias) {
        this.channelAlias = channelAlias == null ? null : channelAlias.trim();
    }

    public Date getSuccTime() {
        return succTime;
    }

    public void setSuccTime(Date succTime) {
        this.succTime = succTime;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Integer getHandleType() {
        return handleType;
    }

    public void setHandleType(Integer handleType) {
        this.handleType = handleType;
    }

    public String getHandleUser() {
        return handleUser;
    }

    public void setHandleUser(String handleUser) {
        this.handleUser = handleUser == null ? null : handleUser.trim();
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getRequestContext() {
        return requestContext;
    }

    public void setRequestContext(String requestContext) {
        this.requestContext = requestContext;
    }

    public String getNotifyContext() {
        return notifyContext;
    }

    public void setNotifyContext(String notifyContext) {
        this.notifyContext = notifyContext;
    }

    public String getChannelOrderNo() {
        return channelOrderNo;
    }

    public void setChannelOrderNo(String channelOrderNo) {
        this.channelOrderNo = channelOrderNo;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }
}
