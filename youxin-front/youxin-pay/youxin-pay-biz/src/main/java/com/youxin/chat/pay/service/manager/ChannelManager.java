package com.youxin.chat.pay.service.manager;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CacheRefresh;
import com.alicp.jetcache.anno.Cached;
import com.youxin.chat.pay.mapper.ChannelAccountMapper;
import com.youxin.chat.pay.model.ChannelAccount;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * description: ChannelManager <br>
 * date: 2020/3/17 13:18 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Component
public class ChannelManager {

    @Resource
    private ChannelAccountMapper channelAccountMapper;


    @Cached(name = "pay.channel.account.",key = "#accountNo",expire = 1,timeUnit = TimeUnit.DAYS)
    @CacheRefresh(refresh = 1,timeUnit = TimeUnit.HOURS)
    public ChannelAccount getChannelByAccount(String accountNo) {
        ChannelAccount channelAccount = channelAccountMapper.getChannelByAccount(accountNo);
        return channelAccount;
    }

    @CacheInvalidate(name = "pay.channel.account.",key = "#channelAccount.channelAccount")
    public void saveChannel(ChannelAccount channelAccount){
        if(channelAccount.getId() == null){
            channelAccountMapper.insert(channelAccount);
        }else{
            channelAccountMapper.updateById(channelAccount);
        }
    }
}
