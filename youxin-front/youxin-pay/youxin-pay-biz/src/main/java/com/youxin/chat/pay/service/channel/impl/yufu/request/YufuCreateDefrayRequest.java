package com.youxin.chat.pay.service.channel.impl.yufu.request;

public class YufuCreateDefrayRequest {

    private String version;

    /**
     * 商户代码
     * 商户代码必须为整 数 , 且 长 度 须 在 1-24之间
     */
    private String merchantId;

    /**
     * 商户订单号
     */
    private String merchantOrderId;

    /**
     * 商户订单时间
     * yyyyMMddHHmmss
     */
    private String merchantOrderTime;

    /**
     * 代付明细信息
     * JSON 格式
     */
    private String payInfo;

    /**
     * 后台通知 URL
     */
    private String backUrl;

    /**
     * 商户保留域
     */
    private String msgExt;

    private String misc;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public void setMerchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public String getMerchantOrderTime() {
        return merchantOrderTime;
    }

    public void setMerchantOrderTime(String merchantOrderTime) {
        this.merchantOrderTime = merchantOrderTime;
    }

    public String getPayInfo() {
        return payInfo;
    }

    public void setPayInfo(String payInfo) {
        this.payInfo = payInfo;
    }

    public String getBackUrl() {
        return backUrl;
    }

    public void setBackUrl(String backUrl) {
        this.backUrl = backUrl;
    }

    public String getMsgExt() {
        return msgExt;
    }

    public void setMsgExt(String msgExt) {
        this.msgExt = msgExt;
    }

    public String getMisc() {
        return misc;
    }

    public void setMisc(String misc) {
        this.misc = misc;
    }
}
