package com.youxin.chat.pay.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.youxin.chat.pay.entity.vo.RedUnreceivedVo;
import com.youxin.chat.pay.model.RedSend;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface RedSendMapper extends BaseMapper<RedSend> {

    int doReceive(@Param("amount") BigDecimal amount, @Param("finish") Integer finish, @Param("id") String id);

    int doExpire(@Param("redStatus") Integer redStatus, @Param("refundOrderNo") String refundOrderNo, @Param("id") String id);

    List<RedUnreceivedVo> redUnreceivedList(@Param("groupNo") String groupNo);
}
