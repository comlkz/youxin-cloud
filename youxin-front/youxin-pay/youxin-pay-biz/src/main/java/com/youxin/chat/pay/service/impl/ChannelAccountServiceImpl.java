package com.youxin.chat.pay.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.chat.pay.dto.ChannelAccountDto;
import com.youxin.chat.pay.dto.ChannelBankDto;
import com.youxin.chat.pay.mapper.ChannelAccountMapper;
import com.youxin.chat.pay.mapper.ChannelBankMapper;
import com.youxin.chat.pay.model.ChannelAccount;
import com.youxin.chat.pay.model.ChannelBank;
import com.youxin.chat.pay.service.ChannelAccountService;
import com.youxin.chat.pay.service.manager.ChannelManager;
import com.youxin.dozer.DozerUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * description: ChannelAccountServiceImpl <br>
 * date: 2020/3/5 15:07 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class ChannelAccountServiceImpl extends ServiceImpl<ChannelAccountMapper, ChannelAccount> implements ChannelAccountService {

    @Resource
    private DozerUtils dozerUtils;

    @Resource
    private ChannelBankMapper channelBankMapper;

    @Resource
    private ChannelManager channelManager;

    @Override
    public void saveChannel(ChannelAccountDto channelAccountDto){
        ChannelAccount channelAccount = dozerUtils.map(channelAccountDto,ChannelAccount.class);
        channelManager.saveChannel(channelAccount);
    }

    @Override
    public void saveChannelBank(ChannelBankDto channelBankDto) {
        ChannelBank channelBank = dozerUtils.map(channelBankDto,ChannelBank.class);
        if(channelBank.getId() == null) {
            channelBankMapper.insert(channelBank);
        }else{
            channelBankMapper.updateById(channelBank);
        }
    }

    @Override
    public void removeChannelBank(Integer id) {
        channelBankMapper.deleteById(id);
    }


}
