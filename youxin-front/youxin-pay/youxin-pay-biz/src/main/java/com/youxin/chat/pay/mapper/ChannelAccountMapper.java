package com.youxin.chat.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.pay.model.ChannelAccount;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface ChannelAccountMapper extends BaseMapper<ChannelAccount> {

    @Select("select * from t_channel_account where channel_account=#{channelAccount} and account_status = 1")
    ChannelAccount getChannelByAccount(@Param("channelAccount")String channelAccount);
}
