package com.youxin.chat.pay.entity.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class TerminationCardReq implements Serializable {
    @ApiModelProperty(value = "卡id",required = true)
    private String id;
    @ApiModelProperty(value = "支付密码",required = true)
    private String payPass;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPayPass() {
        return payPass;
    }

    public void setPayPass(String payPass) {
        this.payPass = payPass;
    }
}
