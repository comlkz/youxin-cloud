package com.youxin.chat.pay.service.channel.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class ChannelDefray implements Serializable {

    /**
     * 提现订单号
     */
    private String orderNo;
    /**
     * 代付金额 单位(分)
     */
    private BigDecimal amount;



    /**
     * 收款人姓名
     */
    private String receiveName;


    /**
     * 收款银行编码
     */
    private String bankCode;

    /**
     * 银行名称
     */
    private String bankName;


    /**
     * 卡号
     */
    private String cardNo;
    /**
     * 联行号
     */
    private String bankLinked;


    /**
     * 扩展字段
     */
    private String extendParams;



    /**
     * 银行预留手机号
     */
    private String bankMobile;

    /**
     * 身份证号
     */
    private String idCard;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getReceiveName() {
        return receiveName;
    }

    public void setReceiveName(String receiveName) {
        this.receiveName = receiveName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getBankLinked() {
        return bankLinked;
    }

    public void setBankLinked(String bankLinked) {
        this.bankLinked = bankLinked;
    }

    public String getExtendParams() {
        return extendParams;
    }

    public void setExtendParams(String extendParams) {
        this.extendParams = extendParams;
    }

    public String getBankMobile() {
        return bankMobile;
    }

    public void setBankMobile(String bankMobile) {
        this.bankMobile = bankMobile;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
}
