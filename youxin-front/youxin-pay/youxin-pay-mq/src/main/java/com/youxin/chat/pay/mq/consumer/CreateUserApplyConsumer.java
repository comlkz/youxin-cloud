package com.youxin.chat.pay.mq.consumer;


import com.youxin.common.constant.MqConstant;
import com.youxin.chat.pay.mapper.UserWalletMapper;
import com.youxin.chat.pay.model.UserWallet;
import com.youxin.chat.pay.service.rpc.UserService;
import com.youxin.chat.user.dto.UserInfoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Component
public class CreateUserApplyConsumer {


    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private UserWalletMapper userWalletMapper;

    @Resource
    private UserService userService;

    @RabbitListener(queues = MqConstant.CREATE_USER_QUEUE)
    @RabbitHandler
    public void get(String data) {
        logger.info("收到用户创建成功消息,data={}",data);
        try {
            UserWallet userWallet = new UserWallet();
            userWallet.setUserNo(data);
            UserInfoDto userInfoDto = userService.getByUserNo(data);
            if(userInfoDto != null) {
                userWallet.setPayAmount(new BigDecimal(0));
                userWallet.setCashAmount(new BigDecimal(0));
                userWallet.setServiceFee(new BigDecimal(0));
                userWallet.setLockAmount(new BigDecimal(0));
                userWallet.setUserBalance(new BigDecimal(0));
                userWallet.setAgentNo(userInfoDto.getAgentNo());
                userWallet.setCreateTime(LocalDateTime.now());
                userWallet.setUpdateTime(LocalDateTime.now());
                userWalletMapper.insert(userWallet);
            }
        }catch (Exception e){
            logger.error("用户创建成功处理失败，data={},error={}",data,e);
        }
    }
}
