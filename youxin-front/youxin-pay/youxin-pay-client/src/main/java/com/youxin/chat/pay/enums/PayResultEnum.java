package com.youxin.chat.pay.enums;

public enum PayResultEnum {

    CARD_BALANCE_NOT_ENOUGH("000100","银行卡余额不足"),

    ACTION_NOT_SUPPORT("000101","暂不支持此交易"),

    USER_STATUS_ERROR("000102","用户状态异常"),

    PAY_FAIL("000103","支付失败"),

    MOBILE_NOT_MATCH("000104","与银行预留手机号码不一致");

    private String code;

    private String msg;


     PayResultEnum(String code, String msg){
        this.code = code;
        this.msg = msg;
    }


    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
