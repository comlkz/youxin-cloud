package com.youxin.chat.pay.client;

import com.youxin.base.TResult;
import com.youxin.chat.pay.dto.ChannelAccountDto;
import com.youxin.chat.pay.dto.ChannelBalanceDto;
import com.youxin.chat.pay.dto.ChannelBankDto;
import com.youxin.exception.SystemException;

/**
 * description: ChannelClient <br>
 * date: 2020/3/5 15:15 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface ChannelClient {

    void saveChannel(ChannelAccountDto channelAccountDto);

    void saveChannelBank(ChannelBankDto channelBankDto);

    void removeChannelBank(Integer id);

    ChannelBalanceDto balanceQuery(String channelAccount) throws SystemException;
}
