package com.youxin.chat.pay.enums;

public enum OrderStatusEnum {

    NOT_PAY(0, "未支付"), PAY_SUCCESS(1, "支付成功"),PAY_FAIL(-1, "支付失败"),
    EXPIRED(-2,"已过期");

    private Integer status;
    private String desc;

    OrderStatusEnum(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }


    public Integer getStatus() {
        return this.status;
    }

    public String getDesc() {
        return this.desc;
    }

    public static OrderStatusEnum forStatus(Integer status){
        for(OrderStatusEnum item : OrderStatusEnum.values()){
            if(item.getStatus().equals(status)) {
                return item;
            }
        }
        return null;
    }
}
