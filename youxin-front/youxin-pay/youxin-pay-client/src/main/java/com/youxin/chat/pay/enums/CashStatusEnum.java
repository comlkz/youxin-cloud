package com.youxin.chat.pay.enums;

public enum CashStatusEnum {

    AUDIT(0,"申请中"),SUCCESS(1,"提现成功"),FAIL(2,"提现失败");
    private Integer status;
    private String desc;

    CashStatusEnum(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return this.status;
    }
}






