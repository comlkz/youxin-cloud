package com.youxin.chat.pay.enums;

public enum OrderSourceEnum {

    PAY(0, "用户充值"), DEFRAY(1, "用户提现"),
    RED_SEND(2, "红包发送"),
    RED_RECEIVE(3, "红包接收"),
    RED_REFUND(4, "红包退款"),
    MOBILE_RECHARGE(5,"手机充值"),
    PAY_CODE_RECEIVE(6,"二维码收款"),
    PAY_CODE_TRANSFER(7,"二维码付款");

    private Integer status;

    private String desc;

    OrderSourceEnum(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return this.status;
    }

    public String getDesc() {
        return desc;
    }

    public static OrderSourceEnum forCode(Integer state){
        for(OrderSourceEnum item : OrderSourceEnum.values()){
            if(item.getStatus().equals(state)) {
                return item;
            }
        }
        return null;
    }
}
