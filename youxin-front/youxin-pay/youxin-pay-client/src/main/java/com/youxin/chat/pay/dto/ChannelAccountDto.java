package com.youxin.chat.pay.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ChannelAccountDto implements Serializable {

    private Long id;

    /**
     * 渠道帐号
     */
    private String channelAccount;

    /**
     * 渠道别名
     */
    private String channelAlias;

    /**
     * 渠道编码
     */
    private String channelCode;

    /**
     * 渠道类型 0：快捷渠道  1：话费充值渠道
     */
    private Integer channelType;

    /**
     * 密钥
     */
    private String secretKey;

    /**
     * 账号状态 -1禁用 1:启用
     */
    private Integer accountStatus;

    /**
     * 通道url
     */
    private String channelUrl;

    private String notifyUrl;
    /**
     * 上游通道公钥
     */
    private String rsaKeyPub;

    /**
     * 商户私钥
     */
    private String rsaKeyPri;

    /**
     * 扩展信息，用json格式
     */
    private String extInfo;

    /**
     * 支付费率（包含%）
     */
    private BigDecimal channelRate;

    /**
     * 代付费用（单位分）
     */
    private BigDecimal serviceFee;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChannelAccount() {
        return channelAccount;
    }

    public void setChannelAccount(String channelAccount) {
        this.channelAccount = channelAccount;
    }

    public String getChannelAlias() {
        return channelAlias;
    }

    public void setChannelAlias(String channelAlias) {
        this.channelAlias = channelAlias;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public Integer getChannelType() {
        return channelType;
    }

    public void setChannelType(Integer channelType) {
        this.channelType = channelType;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public Integer getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(Integer accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getChannelUrl() {
        return channelUrl;
    }

    public void setChannelUrl(String channelUrl) {
        this.channelUrl = channelUrl;
    }

    public String getRsaKeyPub() {
        return rsaKeyPub;
    }

    public void setRsaKeyPub(String rsaKeyPub) {
        this.rsaKeyPub = rsaKeyPub;
    }

    public String getRsaKeyPri() {
        return rsaKeyPri;
    }

    public void setRsaKeyPri(String rsaKeyPri) {
        this.rsaKeyPri = rsaKeyPri;
    }

    public String getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }

    public BigDecimal getChannelRate() {
        return channelRate;
    }

    public void setChannelRate(BigDecimal channelRate) {
        this.channelRate = channelRate;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }
}
