package com.youxin.chat.pay.enums;

public enum RedStatusEnum {

    PROCESSING(0,"正在领取"),
    FINISH(1,"领取完成"),
    EXPIRE(2,"领取过期");
    private Integer status;

    private String desc;

    RedStatusEnum(Integer status, String desc){
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus(){
        return this.status;
    }
}
