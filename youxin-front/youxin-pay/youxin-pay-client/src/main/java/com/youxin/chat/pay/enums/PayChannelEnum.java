package com.youxin.chat.pay.enums;

public enum PayChannelEnum {

    LIANTONG("LIANTONG", "联通"),
    TEST("TEST", "测试"),
    YPL("YPL", "易票联"),
    YUFU("YUFU","裕福"),
    AINONG("AINONG","爱农");

    private String channelCode;

    private String channelName;

    PayChannelEnum(String channelCode, String channelName) {
        this.channelCode = channelCode;
        this.channelName = channelName;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public String getChannelName() {
        return channelName;
    }
}
