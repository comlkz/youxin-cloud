package com.youxin.chat.pay.client;

import com.youxin.chat.pay.dto.CashStatusChange;
import com.youxin.exception.SystemException;

/**
 * description: OrderClient <br>
 * date: 2020/3/5 11:52 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface OrderClient {

    void changeCashStatus(CashStatusChange cashStatusChange) throws SystemException;

    void syncCashStatus(Integer minute) throws SystemException;

    void syncOrderStatus(String orderNo) throws SystemException;

    void syncCashStatus(String orderNo) throws SystemException;

}
