package com.youxin.chat.pay.dto;

import java.io.Serializable;

public class CashStatusChange implements Serializable {

    private Integer status;

    private String orderNo;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
