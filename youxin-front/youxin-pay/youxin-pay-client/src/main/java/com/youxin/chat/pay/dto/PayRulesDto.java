package com.youxin.chat.pay.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayRulesDto implements Serializable {

    @ApiModelProperty(value = "单笔最大提现金额（单位 分）")
    private BigDecimal perCashAmount =  new BigDecimal(1000000);

    @ApiModelProperty(value = "当日最大提现金额（单位 分）")
    private BigDecimal maxDayCashAmount  =  new BigDecimal(1000000000);

    @ApiModelProperty(value = "当日最大提现次数（单位 次）")
    private Integer maxDayCount =  new Integer(10);

    @ApiModelProperty(value = "单笔最小提现金额（单位 分）")
    private BigDecimal perMinCashAmount = new BigDecimal(200);

    @ApiModelProperty(value = "单笔最大充值金额（单位 分）")
    private BigDecimal rechargeOnceLimit =  new BigDecimal(1000000);

    @ApiModelProperty(value = "单笔最小充值金额（单位 分）")
    private BigDecimal rechargeOnceMin = new BigDecimal(100);

    @ApiModelProperty(value = "当日最大充值金额（单位 分）")
    private BigDecimal rechargeDayLimit = new BigDecimal(1000000000);

    @ApiModelProperty(value = "默认渠道帐号")
    private String channelAccount = "";

    @ApiModelProperty(value = "提现费率（含 %）")
    private BigDecimal cashRate = new BigDecimal(0.6);

    @ApiModelProperty(value = "提现最小手费费（单位 分）")
    private BigDecimal minDefrayFee = new BigDecimal(100);

    @ApiModelProperty(value = "单个红包限额（单位 分）")
    private BigDecimal redPackMax = new BigDecimal(20000);


    public BigDecimal getPerCashAmount() {
        return perCashAmount;
    }

    public void setPerCashAmount(BigDecimal perCashAmount) {
        this.perCashAmount = perCashAmount;
    }

    public BigDecimal getMaxDayCashAmount() {
        return maxDayCashAmount;
    }

    public void setMaxDayCashAmount(BigDecimal maxDayCashAmount) {
        this.maxDayCashAmount = maxDayCashAmount;
    }

    public Integer getMaxDayCount() {
        return maxDayCount;
    }

    public void setMaxDayCount(Integer maxDayCount) {
        this.maxDayCount = maxDayCount;
    }

    public BigDecimal getRechargeOnceLimit() {
        return rechargeOnceLimit;
    }

    public void setRechargeOnceLimit(BigDecimal rechargeOnceLimit) {
        this.rechargeOnceLimit = rechargeOnceLimit;
    }

    public BigDecimal getRechargeDayLimit() {
        return rechargeDayLimit;
    }

    public void setRechargeDayLimit(BigDecimal rechargeDayLimit) {
        this.rechargeDayLimit = rechargeDayLimit;
    }

    public String getChannelAccount() {
        return channelAccount;
    }

    public void setChannelAccount(String channelAccount) {
        this.channelAccount = channelAccount;
    }

    public BigDecimal getCashRate() {
        return cashRate;
    }

    public void setCashRate(BigDecimal cashRate) {
        this.cashRate = cashRate;
    }

    public BigDecimal getMinDefrayFee() {
        return minDefrayFee;
    }

    public void setMinDefrayFee(BigDecimal minDefrayFee) {
        this.minDefrayFee = minDefrayFee;
    }

    public BigDecimal getPerMinCashAmount() {
        return perMinCashAmount;
    }

    public void setPerMinCashAmount(BigDecimal perMinCashAmount) {
        this.perMinCashAmount = perMinCashAmount;
    }

    public BigDecimal getRechargeOnceMin() {
        return rechargeOnceMin;
    }

    public void setRechargeOnceMin(BigDecimal rechargeOnceMin) {
        this.rechargeOnceMin = rechargeOnceMin;
    }

    public BigDecimal getRedPackMax() {
        return redPackMax;
    }

    public void setRedPackMax(BigDecimal redPackMax) {
        this.redPackMax = redPackMax;
    }
}

