package com.youxin.chat.pay.client;

import com.youxin.chat.pay.dto.PayRulesDto;
import com.youxin.exception.SystemException;

/**
 * description: WalletClient <br>
 * date: 2020/3/5 11:07 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface WalletClient {

    void updateUserWalletStates(String userNo, Integer status) throws SystemException;

    void saveRisk(PayRulesDto riskDto);
}
