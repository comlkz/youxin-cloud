package com.youxin.chat.pay.enums;

public enum RedPackTypeEnum {

    NORMAL(0, "普通红包"),
    RANDOM(1, "随机红包"),
    SINGLE(2, "单个红包"),
    ASSIGN(3, "定向红包"),
    ASSIGNRANDOM(4, "定向随机红包");
    private Integer type;

    private String desc;

    RedPackTypeEnum(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public Integer getType() {
        return this.type;
    }
}
