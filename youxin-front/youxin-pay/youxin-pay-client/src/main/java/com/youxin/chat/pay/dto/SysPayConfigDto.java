package com.youxin.chat.pay.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class SysPayConfigDto implements Serializable {

    private String channelAccount;

    private BigDecimal defrayRate;

    public String getChannelAccount() {
        return channelAccount;
    }

    public void setChannelAccount(String channelAccount) {
        this.channelAccount = channelAccount;
    }

    public BigDecimal getDefrayRate() {
        return defrayRate;
    }

    public void setDefrayRate(BigDecimal defrayRate) {
        this.defrayRate = defrayRate;
    }
}
