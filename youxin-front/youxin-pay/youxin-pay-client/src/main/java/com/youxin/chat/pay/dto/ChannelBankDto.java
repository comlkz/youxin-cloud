package com.youxin.chat.pay.dto;

import java.io.Serializable;

public class ChannelBankDto implements Serializable {

    private Integer id;

    /**
     * 渠道编码
     */
    private String channelCode;

    /**
     * 本地银行编码
     */
    private String bankCode;

    /**
     * 银行名称
     */
    private String bankName;


    /**
     * 渠道银行编码
     */
    private String channelBankCode;

    /**
     * 是否支持支付 0：不支持  1：支持
     */
    private Integer supportPay;

    /**
     * 是否支付代付 0：不支持 1：支持
     */
    private Integer supportDefray;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getChannelBankCode() {
        return channelBankCode;
    }

    public void setChannelBankCode(String channelBankCode) {
        this.channelBankCode = channelBankCode;
    }

    public Integer getSupportPay() {
        return supportPay;
    }

    public void setSupportPay(Integer supportPay) {
        this.supportPay = supportPay;
    }

    public Integer getSupportDefray() {
        return supportDefray;
    }

    public void setSupportDefray(Integer supportDefray) {
        this.supportDefray = supportDefray;
    }
}
