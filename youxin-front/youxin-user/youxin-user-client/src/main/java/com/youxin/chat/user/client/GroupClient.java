package com.youxin.chat.user.client;

/**
 * description: GroupClient <br>
 * date: 2020/2/29 15:40 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface GroupClient {

    int countMemberByGroupNoAndUserNo(String groupNo,String userNo);

    boolean checkUserInBlackList(String userNo, String groupNo);

    boolean checkOperate(String userNo, String groupNo);

    /**
     * 发送群警告
     * @param groupNo
     */
    void sendGroupWarning(String groupNo);

    /**
     * 冻结群组
     * @param groupNo
     */
    void lockGroup(String groupNo);
}
