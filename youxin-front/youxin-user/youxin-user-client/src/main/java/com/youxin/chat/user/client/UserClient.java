package com.youxin.chat.user.client;

import com.youxin.chat.user.dto.UserAddDto;
import com.youxin.chat.user.dto.UserInfoDto;

import java.util.List;

/**
 * description: UserClient <br>
 * date: 2020/2/28 16:06 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface UserClient {

    UserInfoDto getByUserNo(String userNo);

    List<UserInfoDto> getByUserNos(List<String> userNos);

    void updateUserStates(String id, Integer status);

    void saveUser(UserAddDto userAddDto);

    UserInfoDto getUserInfoByToken(String token);

}
