package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

public class GroupBlackAddReq implements Serializable {
    @ApiModelProperty(value = "群编码", required = true)
    private String groupNo;
    @ApiModelProperty(value = "用户编码列表", required = true)
    private List<String> userNos;

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public List<String> getUserNos() {
        return userNos;
    }

    public void setUserNos(List<String> userNos) {
        this.userNos = userNos;
    }
}
