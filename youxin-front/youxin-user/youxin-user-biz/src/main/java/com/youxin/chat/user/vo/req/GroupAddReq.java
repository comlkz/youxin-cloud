package com.youxin.chat.user.vo.req;

import java.io.Serializable;
import java.util.List;

public class GroupAddReq implements Serializable {

    private List<String> userNos;

    private String groupNo;

    private String groupCode;

    private Integer type;

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public List<String> getUserNos() {
        return userNos;
    }

    public void setUserNos(List<String> userNos) {
        this.userNos = userNos;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "GroupAddDto{" +
                "userNos=" + userNos +
                ", groupNo='" + groupNo + '\'' +
                ", groupCode='" + groupCode + '\'' +
                ", type=" + type +
                '}';
    }
}
