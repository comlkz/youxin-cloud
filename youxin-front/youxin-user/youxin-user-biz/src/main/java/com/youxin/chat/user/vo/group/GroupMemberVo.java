package com.youxin.chat.user.vo.group;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class GroupMemberVo implements Serializable {

    @ApiModelProperty(value = "成员主键ID",required = true)
    private String id;

    @ApiModelProperty(value = "群聊ID",required = true)
    private String groupNo;//群聊ID

    @ApiModelProperty(value = "群成员编码",required = true)
    private String groupUser;//群成员编码

    @ApiModelProperty(value = "群成员昵称",required = true)
    private String groupNickName;//群成员昵称

    @ApiModelProperty(value = "群成员备注",required = true)
    private String groupNote;

    @ApiModelProperty(value = "群成员头像",required = true)
    private String avatar;//群成员头像

    @ApiModelProperty(value = "成员类型 0：普通成员 1：群主 2：管理员",required = true)
    private Byte userType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupUser() {
        return groupUser;
    }

    public void setGroupUser(String groupUser) {
        this.groupUser = groupUser;
    }

    public String getGroupNickName() {
        return groupNickName;
    }

    public void setGroupNickName(String groupNickName) {
        this.groupNickName = groupNickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Byte getUserType() {
        return userType;
    }

    public void setUserType(Byte userType) {
        this.userType = userType;
    }

    public String getGroupNote() {
        return groupNote;
    }

    public void setGroupNote(String groupNote) {
        this.groupNote = groupNote;
    }
}
