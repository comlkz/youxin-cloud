package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.user.model.AgentInfo;
import org.apache.ibatis.annotations.Param;

public interface AgentInfoMapper extends BaseMapper<AgentInfo> {

    void updateStatusByAgentNo(@Param("status") Integer status, @Param("agentNo") String agentNo);

}
