package com.youxin.chat.user.enums;

/**
 * description: UserCollectEnum <br>
 * date: 2020/2/20 14:43 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public enum UserCollectEnum {

    MESSAGE(1,"消息"),
    IMAGE(2,"图片"),
    LINK(3,"链接"),
    VIDEO(4,"语音"),
    GAME(5,"游戏");

    private String desc;

    private Integer type;

    UserCollectEnum(Integer type, String desc){
        this.desc = desc;
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getType() {
        return type;
    }
}
