package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class GroupAuditReq implements Serializable {
    @ApiModelProperty(value = "群聊ID",required = true)
    private String groupId;
    @ApiModelProperty(value = "状态0：禁用，1：开启",required = true)
    private Integer audit;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getAudit() {
        return audit;
    }

    public void setAudit(Integer audit) {
        this.audit = audit;
    }
}
