package com.youxin.chat.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.user.model.UserAdd;
import com.youxin.chat.user.vo.user.UserAddVo;
import com.youxin.exception.SystemException;

import java.util.List;

/**
 * description: UserAddService <br>
 * date: 2020/2/25 12:06 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface UserAddService extends IService<UserAdd> {

    List<UserAddVo> listAddUsers(String userNo) throws SystemException;

    void emptyList(String userNo) throws SystemException;

}
