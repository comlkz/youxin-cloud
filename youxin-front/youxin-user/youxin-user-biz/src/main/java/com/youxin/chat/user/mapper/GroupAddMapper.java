package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.user.model.GroupAdd;
import com.youxin.chat.user.vo.group.GroupAddListVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GroupAddMapper extends BaseMapper<GroupAdd> {

    List<GroupAddListVo> listGroupAdd(@Param("groupNo") String groupNo);

}
