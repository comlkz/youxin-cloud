package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class FriendRemarkReq implements Serializable {
    @ApiModelProperty(value = "好友id",required = true)
    private String friendNo;
    @ApiModelProperty(value = "备注",required = true)
    private String friendNickName;

    public String getFriendNo() {
        return friendNo;
    }

    public void setFriendNo(String friendNo) {
        this.friendNo = friendNo;
    }

    public String getFriendNickName() {
        return friendNickName;
    }

    public void setFriendNickName(String friendNickName) {
        this.friendNickName = friendNickName;
    }
}
