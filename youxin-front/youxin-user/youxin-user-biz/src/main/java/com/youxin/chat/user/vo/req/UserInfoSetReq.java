package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * description: UserInfoSetReq <br>
 * date: 2020/2/26 16:54 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class UserInfoSetReq implements Serializable {

    @ApiModelProperty(value = "用户昵称", required = false)
    private String nickName;
    @ApiModelProperty(value = "用户头像", required = false)
    private String avatar;
    @ApiModelProperty(value = "性别  0：未知 1：男 2：女", required = false)
    private  Integer sex;

    @ApiModelProperty(value = "所在地区", required = false)
    private String area;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "UserInfoSetReq{" +
                "nickName='" + nickName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", sex=" + sex +
                ", area='" + area + '\'' +
                '}';
    }
}
