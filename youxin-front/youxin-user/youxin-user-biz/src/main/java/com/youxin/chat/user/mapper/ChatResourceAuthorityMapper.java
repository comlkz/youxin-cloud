package com.youxin.chat.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.user.model.ChatResourceAuthority;

public interface ChatResourceAuthorityMapper extends BaseMapper<ChatResourceAuthority> {

}
