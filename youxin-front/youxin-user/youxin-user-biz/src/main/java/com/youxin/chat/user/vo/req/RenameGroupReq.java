package com.youxin.chat.user.vo.req;

import java.io.Serializable;

public class RenameGroupReq implements Serializable{

    private String groupNo;

    private String groupName;

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
