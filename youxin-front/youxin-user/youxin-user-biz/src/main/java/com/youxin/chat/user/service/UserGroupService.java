package com.youxin.chat.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.user.model.UserGroup;
import com.youxin.chat.user.vo.group.GroupItemVo;
import com.youxin.chat.user.vo.req.GroupNickNameReq;
import com.youxin.chat.user.vo.req.GroupPreferenceReq;
import com.youxin.exception.SystemException;

import java.util.List;

/**
 * description: UserGroupService <br>
 * date: 2020/2/28 14:02 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface UserGroupService extends IService<UserGroup> {

    void setPreference(String userNo, GroupPreferenceReq userGroupDto) throws SystemException;

    List<GroupItemVo> getAddressGroup(String userNo) throws SystemException;

    void updateNickName(String userNo, GroupNickNameReq groupNickNameDto) throws SystemException;

}
