package com.youxin.chat.user.model;

/**
 * description: GroupExt <br>
 * date: 2020/1/8 11:00 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class GroupExt {

    /**
     * 关闭 -1
     * 即刻清理 0
     * 其余时间（单位秒）
     */
    private Integer  clearTime;

    /**
     * 0:不开启
     * 1：开启
     */
    private Integer printScreenNotify;

    public Integer getClearTime() {
        return clearTime;
    }

    public void setClearTime(Integer clearTime) {
        this.clearTime = clearTime;
    }

    public Integer getPrintScreenNotify() {
        return printScreenNotify;
    }

    public void setPrintScreenNotify(Integer printScreenNotify) {
        this.printScreenNotify = printScreenNotify;
    }

    @Override
    public String toString() {
        return "GroupExt{" +
                "clearTime=" + clearTime +
                ", printScreenNotify=" + printScreenNotify +
                '}';
    }
}
