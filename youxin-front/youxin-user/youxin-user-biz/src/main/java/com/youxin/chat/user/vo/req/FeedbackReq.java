package com.youxin.chat.user.vo.req;

import java.io.Serializable;

public class FeedbackReq implements Serializable {
    private String feedback;

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
