package com.youxin.chat.user.vo.req;

/**
 * description: RegisterCodeReq <br>
 * date: 2020/3/14 9:35 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class RegisterCodeReq  {

    private String mobile;

    private String code;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
