package com.youxin.chat.user.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

@ApiModel(description = "用户信息参数")
public class UserDetailVo implements Serializable {
    private static final long serialVersionUID = 8775569977659550551L;

    private String id;

    @ApiModelProperty(value = "用户名称", required = true)
    private String userName;
    @ApiModelProperty(value = "用户标识", required = true)
    private String userNo;
    @ApiModelProperty(value = "用户昵称", required = false)
    private String nickName;
    @ApiModelProperty(value = "用户头像", required = false)
    private String avatar;
    @ApiModelProperty(value = "性别  0：未知 1：男 2：女", required = false)
    private  Integer sex;
    @ApiModelProperty(value = "所在地区", required = false)
    private String area;
    @ApiModelProperty(value = "用户状态", required = false)
    private Integer userStatus;
    @ApiModelProperty(value = "手机号", required = false)
    private String mobile;
    @ApiModelProperty(value = "支付标识 0：没有支付密码 1：有支付密码", required = false)
    private Integer payStatus;

    @ApiModelProperty(value = "用户类型 0：普通用户 1：系统用户 2：客服", required = false)
    private Integer userType;

    @ApiModelProperty(value = "代理编号", required = false)
    private String agentNo;

    @ApiModelProperty(value = "用户等级", required = false)
    private Integer userLevel;

    @ApiModelProperty(value = "活跃时间", required = false)
    private Date lastUseTime;

    @ApiModelProperty(value = "创建时间", required = false)
    private Date createTime;

    @ApiModelProperty(value = "是否完善用户信息 0：否 1：是")
    private Integer hasPerfect;

    private Integer deviceType;

    private String deviceId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public Date getLastUseTime() {
        return lastUseTime;
    }

    public void setLastUseTime(Date lastUseTime) {
        this.lastUseTime = lastUseTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(String agentNo) {
        this.agentNo = agentNo;
    }

    public Integer getHasPerfect() {
        return hasPerfect;
    }

    public void setHasPerfect(Integer hasPerfect) {
        this.hasPerfect = hasPerfect;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
