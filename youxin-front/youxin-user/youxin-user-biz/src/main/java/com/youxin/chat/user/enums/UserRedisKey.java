package com.youxin.chat.user.enums;

import com.youxin.common.constant.RedisKey;

/**
 * description: UserRedisKey <br>
 * date: 2020/3/11 2:01 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface UserRedisKey extends RedisKey {

    String USER_INFO = "com.youxin.chat.user.info.";

    String REGISTER_CODE = "com.youxin.chat.user.register.";
}
