package com.youxin.chat.user.vo.group;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * description: GroupView <br>
 * date: 2020/1/14 15:56 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class GroupByCodeVo implements Serializable {

    @ApiModelProperty("群编号")
    private String groupNo;

    @ApiModelProperty("群头像")
    private String groupAvatar;

    private int userNums;

    private String groupName;

    @ApiModelProperty("是否已加入 0:未加入 1:已加入")
    private Integer hasJoined;

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupAvatar() {
        return groupAvatar;
    }

    public void setGroupAvatar(String groupAvatar) {
        this.groupAvatar = groupAvatar;
    }

    public int getUserNums() {
        return userNums;
    }

    public void setUserNums(int userNums) {
        this.userNums = userNums;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getHasJoined() {
        return hasJoined;
    }

    public void setHasJoined(Integer hasJoined) {
        this.hasJoined = hasJoined;
    }
}
