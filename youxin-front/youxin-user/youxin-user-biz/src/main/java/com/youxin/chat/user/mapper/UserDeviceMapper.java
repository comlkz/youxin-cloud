package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.user.model.UserDevice;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserDeviceMapper extends BaseMapper<UserDevice> {

    int updateLastLoginTime(@Param("record") UserDevice userDevice);

    @Update("update t_user_device set access_token= null,im_token = null where user_no=#{userNo}")
    int updateAccessTokenAndImTokenNull(@Param("userNo") String userNo);

    @Select("select * from t_user_device where access_token=#{accessToken}")
    UserDevice selectByAccessToken(@Param("accessToken") String accessToken);

    @Update("update t_user_device set im_token = #{imToken} where user_no=#{userNo}")
    int updateImTokenByUserNo(@Param("userNo") String userNo,@Param("imToken") String imToken);

    @Select("select * from t_user_device where user_no=#{userNo} and access_token is not null")
    UserDevice selectActiveByUserNo(@Param("userNo") String userNo);
}
