package com.youxin.chat.user.convert;


import com.youxin.chat.user.model.UserInfo;
import com.youxin.chat.user.vo.user.UserDetailVo;

import java.util.Optional;

public class UserInfoConvert {

    public static UserDetailVo modelToDto(UserInfo userInfo, String fileUrl) {
        UserDetailVo userInfoDto = new UserDetailVo();
        if (!Optional.ofNullable(userInfo).isPresent()) {
            return userInfoDto;
        }
        userInfoDto.setId(userInfo.getId());
        userInfoDto.setUserName(userInfo.getUserName());
        userInfoDto.setMobile(userInfo.getMobile());
        userInfoDto.setUserNo(userInfo.getUserNo());
        userInfoDto.setNickName(userInfo.getNickName());
        userInfoDto.setAvatar(fileUrl + userInfo.getAvatar());
        userInfoDto.setSex(userInfo.getSex());
        userInfoDto.setArea(userInfo.getArea());
        userInfoDto.setUserStatus(userInfo.getUserStatus());
        userInfoDto.setUserType(userInfo.getUserType());
        userInfoDto.setUserLevel(userInfo.getUserLevel());
        userInfoDto.setAgentNo(userInfo.getAgentNo());
        return userInfoDto;
    }
}
