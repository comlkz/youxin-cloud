package com.youxin.chat.user.enums;

public enum UserSourceEnum {

    MOBILE(0,"源自手机号"),
    USER_NAME(1,"源自帐号"),
    CODE(2,"源自扫码");
    private Integer source;

    private String desc;

    UserSourceEnum(Integer source, String desc) {
        this.source = source;
        this.desc = desc;
    }

    public Integer getSource(){
        return this.source;
    }

    public String getDesc(){
        return this.desc;
    }
}
