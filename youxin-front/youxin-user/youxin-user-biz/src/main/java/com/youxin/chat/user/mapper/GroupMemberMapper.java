package com.youxin.chat.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.youxin.chat.user.model.GroupMember;
import com.youxin.chat.user.vo.group.GroupMemberVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface GroupMemberMapper extends BaseMapper<GroupMember> {

    @Select("select count(1) from t_group_member where group_no=#{groupNo} and group_user=#{userNo}")
    int countMemberByGroupNoAndUserNo(@Param("groupNo") String groupNo, @Param("userNo")String userNo);

    @Select("select * from t_group_member where group_no=#{groupNo} and group_user=#{userNo}")
    GroupMember selectByGroupNoAndUserNo(@Param("groupNo") String groupNo, @Param("userNo")String userNo);

    void insertBatch(@Param("list") List<GroupMember> list);

    int deleteByGroupNoAndUserNos(@Param("groupNo") String groupNo, @Param("userNos") List<String> userNos);

    int deleteByGroupNo(@Param("groupNo") String groupNo);


    int updateNickName(@Param("groupMember") GroupMember groupMember);

    Integer selectUserType(@Param("userNo") String userNo, @Param("groupNo") String groupNo);

    int cancelGroupMaster(@Param("groupNo") String groupNo);

    int setGroupMaster(@Param("userNo") String userNo, @Param("groupNo") String groupNo);

    int updateGroupUserType(@Param("groupNo") String groupNo, @Param("userNos") List<String> userNos, @Param("userType") Integer userType);


    List<String> getManagerList(@Param("groupNo") String groupNo);

    String getManagerId(@Param("groupNo") String groupNo);

    List<String> getGroupMember(@Param("groupNo") String groupNo);

    List<GroupMemberVo> selectByGroupNo(@Param("groupNo") String groupNo);

    List<GroupMemberVo> listGroupManagers(@Param("groupNo") String groupNo);

}
