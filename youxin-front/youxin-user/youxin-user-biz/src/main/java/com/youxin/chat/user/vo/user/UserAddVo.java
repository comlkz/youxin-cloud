package com.youxin.chat.user.vo.user;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

public class UserAddVo implements Serializable {

    private static final long serialVersionUID = -6214948926429971822L;
    private Long id;

    /**
     * 用户编码
     */
    @ApiModelProperty(value = "用户编码")
    private String userNo;

    /**
     * 被申请人用户编码
     */
    @ApiModelProperty(value = "待添加用户编码")
    private String addUserNo;

    /**
     * 邀请人昵称
     */
    @ApiModelProperty(value = "邀请人昵称")
    private String nickName;

    @ApiModelProperty(value = "邀请人头像")
    private String avatar;

    /**
     *被申请人昵称
     */
    @ApiModelProperty(value = "被申请人昵称")
    private String addNickName;

    /**
     * 被申请人头像
     */
    @ApiModelProperty(value = "被申请人头像")
    private String addAvatar;

    /**
     * 添加时间
     */
    @ApiModelProperty(value = "添加时间")
    private LocalDateTime addTime;

    /**
     * 申请状态，0：待处理  1：添加成功  -1：拒绝
     */
    @ApiModelProperty(value = "申请状态，0：待处理  1：添加成功  -1：拒绝")
    private int addStatus;

    /**
     * 申请类型
     * 0：添加他人为好友 ；
     * 1：被他人添加为好友
     */
    @ApiModelProperty(value = "申请类型，0：添加他人为好友 1：被他人添加为好友 ")
    private int auditType;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 完成时间
     */
    @ApiModelProperty(value = "完成时间")
    private LocalDateTime finishTime;

    /**
     * 设备类型  0：android 1:IOS
     */
    @ApiModelProperty(value = "设备类型  0：android 1:IOS")
    private Integer deviceType;

    /**
     * 添加方式 0：手机号申请 1：帐号申请  2：扫码申请
     */
    @ApiModelProperty(value = "添加方式 0：手机号申请 1：帐号申请  2：扫码申请")
    private Integer addSource;


    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getAddUserNo() {
        return addUserNo;
    }

    public void setAddUserNo(String addUserNo) {
        this.addUserNo = addUserNo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAddNickName() {
        return addNickName;
    }

    public void setAddNickName(String addNickName) {
        this.addNickName = addNickName;
    }

    public String getAddAvatar() {
        return addAvatar;
    }

    public void setAddAvatar(String addAvatar) {
        this.addAvatar = addAvatar;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    public int getAddStatus() {
        return addStatus;
    }

    public void setAddStatus(int addStatus) {
        this.addStatus = addStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public LocalDateTime getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(LocalDateTime finishTime) {
        this.finishTime = finishTime;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public int getAuditType() {
        return auditType;
    }

    public void setAuditType(int auditType) {
        this.auditType = auditType;
    }

    public Integer getAddSource() {
        return addSource;
    }

    public void setAddSource(Integer addSource) {
        this.addSource = addSource;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}
