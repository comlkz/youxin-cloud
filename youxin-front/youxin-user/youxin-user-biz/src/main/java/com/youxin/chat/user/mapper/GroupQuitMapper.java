package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.user.model.GroupQuit;
import com.youxin.chat.user.vo.group.GroupQuitVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GroupQuitMapper extends BaseMapper<GroupQuit> {

    void insertBatch(List<GroupQuit> list);

    List<GroupQuitVo> quitGroupList(@Param("groupNo") String groupNo);

}
