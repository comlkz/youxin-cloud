package com.youxin.chat.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.youxin.chat.user.model.Group;
import com.youxin.chat.user.vo.group.GroupItemVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface GroupMapper extends BaseMapper<Group> {

    int deleteByGroupNo(@Param("groupNo") String groupNo);

    List<GroupItemVo> getAddressGroup(@Param("userNo") String userNo);

    void updateGroupNotice(@Param("group") Group group);

    int changeGroupMaster(@Param("groupNo") String groupNo, @Param("userNo") String userNo);

    @Select("select * from t_group where group_no=#{groupNo} and group_status=0")
    Group getByGroupNo(@Param("groupNo") String groupNo);

    @Update("update t_group set append_status=#{status} where group_no=#{groupNo}")
    int updateAppendStatus(@Param("groupNo") String groupNo,@Param("status")Integer status);

}
