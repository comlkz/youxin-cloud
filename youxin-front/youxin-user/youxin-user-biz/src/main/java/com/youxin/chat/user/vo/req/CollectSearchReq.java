package com.youxin.chat.user.vo.req;


import com.youxin.base.TQuery;

/**
 * description: CollectSearch <br>
 * date: 2020/2/20 13:18 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class CollectSearchReq extends TQuery {

    private String key;

    private String userNo;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }
}
