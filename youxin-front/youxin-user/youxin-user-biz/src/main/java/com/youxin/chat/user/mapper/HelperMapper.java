package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.user.model.Helper;


import java.util.List;

public interface HelperMapper extends BaseMapper<Helper> {
}
