package com.youxin.chat.user.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

@ApiModel(description = "用户好友参数")
public class UserFriendVo implements Serializable {
    private static final long serialVersionUID = 4668318070795438009L;

    @ApiModelProperty(value = "用户编码",required = true)
    private String userNo;
    @ApiModelProperty(value = "好友用户编码",required = true)
    private String friendNo;
    @ApiModelProperty(value = "备注昵称",required = false)
    private String friendNickName;
    @ApiModelProperty(value = "好友备注",required = false)
    private String friendNote;//好友备注
    @ApiModelProperty(value = "好友头像",required = false)
    private String friendAvatar;
    @ApiModelProperty(value = "好友状态 0：正常 1：拉黑",required = false)
    private Integer friendStatus;
    @ApiModelProperty(value = "创建时间",required = false)
    private LocalDateTime createTime;
    @ApiModelProperty(value = "修改时间",required = false)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "星标好友 0：否 1：是",required = false)
    private Integer starFriend;
    @ApiModelProperty(value = "免打扰 0：否 1：是",required = false)
    private Integer notDisturb;
    @ApiModelProperty(value = "好友原昵称",required = false)
    private String nickName;
    @ApiModelProperty(value = "性别 0：未知 1：男 2：女",required = false)
    private Integer sex;
    @ApiModelProperty(value = "地区",required = false)
    private String area;
    @ApiModelProperty(value = "禁群消息定时清理 -1：关闭，0：即刻清理，其余时间（单位秒）",required = false)
    private Integer clearTime;
    @ApiModelProperty(value = "截屏通知 0：不开启，1：开启",required = false)
    private Integer printScreenNotify;

    @ApiModelProperty(value = "禁群消息定时清理时刻 yyyy-MM-dd HH:mm:ss")
    private String clearTimePoint;

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getFriendNo() {
        return friendNo;
    }

    public void setFriendNo(String friendNo) {
        this.friendNo = friendNo;
    }

    public String getFriendNickName() {
        return friendNickName;
    }

    public void setFriendNickName(String friendNickName) {
        this.friendNickName = friendNickName;
    }

    public String getFriendAvatar() {
        return friendAvatar;
    }

    public void setFriendAvatar(String friendAvatar) {
        this.friendAvatar = friendAvatar;
    }

    public Integer getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(Integer friendStatus) {
        this.friendStatus = friendStatus;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStarFriend() {
        return starFriend;
    }

    public void setStarFriend(Integer starFriend) {
        this.starFriend = starFriend;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getFriendNote() {
        return friendNote;
    }

    public void setFriendNote(String friendNote) {
        this.friendNote = friendNote;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getNotDisturb() {
        return notDisturb;
    }

    public void setNotDisturb(Integer notDisturb) {
        this.notDisturb = notDisturb;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getClearTime() {
        return clearTime;
    }

    public void setClearTime(Integer clearTime) {
        this.clearTime = clearTime;
    }

    public Integer getPrintScreenNotify() {
        return printScreenNotify;
    }

    public void setPrintScreenNotify(Integer printScreenNotify) {
        this.printScreenNotify = printScreenNotify;
    }

    public String getClearTimePoint() {
        return clearTimePoint;
    }

    public void setClearTimePoint(String clearTimePoint) {
        this.clearTimePoint = clearTimePoint;
    }
}
