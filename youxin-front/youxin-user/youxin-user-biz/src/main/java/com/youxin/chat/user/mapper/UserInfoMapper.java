package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.youxin.chat.user.model.UserInfo;
import com.youxin.chat.user.vo.user.UserSearchVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface UserInfoMapper extends BaseMapper<UserInfo> {


    @Select("select * from t_user_info where user_no = #{userNo}")
    UserInfo selectByUserNo(@Param("userNo")String userNo);

    @Select("select USER_NAME from t_user_info where USER_TYPE = '2'")
    Set<String> selectAssistant();

    @Select("SELECT USER_NO,USER_NAME,NICK_NAME,AVATAR,SEX,AREA, MOBILE, 1 as source from t_user_info where mobile= #{searchKey}")
    UserSearchVo selectUserByMobile(@Param("searchKey") String searchKey);

    @Select("SELECT USER_NO,USER_NAME,NICK_NAME,AVATAR,SEX,AREA, MOBILE, 1 as source from t_user_info where user_name= #{searchKey}")
    UserSearchVo selectUserByUserName(@Param("searchKey") String searchKey);

    void updateUseTimeByUserNo(@Param("useTime") LocalDateTime useTime, @Param("userNo") String userNo);

}
