package com.youxin.chat.user.model;

import java.time.LocalDateTime;

/**
 * description: GroupCode <br>
 * date: 2020/1/14 10:39 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class GroupCode {

    private Integer id;

    private String groupNo;

    private String groupCode;

    private String createUser;

    private LocalDateTime createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
}
