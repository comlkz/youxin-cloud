package com.youxin.chat.user.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

public class UserAdd {

    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 用户编码
     */
    private String userNo;

    /**
     * 待添加用户编码
     */
    private String addUserNo;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     *待添加用户昵称
     */
    private String addNickName;

    /**
     * 待添加用户头像
     */
    private String addAvatar;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 申请状态，0：待处理  1：添加成功  -1：拒绝
     */
    private int addStatus;

    /**
     * 备注
     */
    private String remark;

    /**
     * 完成时间
     */
    private LocalDateTime finishTime;

    /**
     * 设备类型  0：android 1:IOS
     */
    private Integer deviceType;

    /**
     * 添加方式 0：手机号申请 1：帐号申请  2：扫码申请
     */
    private Integer addSource;



    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }


    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }



    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public LocalDateTime getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(LocalDateTime finishTime) {
        this.finishTime = finishTime;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getAddUserNo() {
        return addUserNo;
    }

    public void setAddUserNo(String addUserNo) {
        this.addUserNo = addUserNo;
    }

    public String getAddNickName() {
        return addNickName;
    }

    public void setAddNickName(String addNickName) {
        this.addNickName = addNickName;
    }

    public String getAddAvatar() {
        return addAvatar;
    }

    public void setAddAvatar(String addAvatar) {
        this.addAvatar = addAvatar;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    public int getAddStatus() {
        return addStatus;
    }

    public void setAddStatus(int addStatus) {
        this.addStatus = addStatus;
    }

    public Integer getAddSource() {
        return addSource;
    }

    public void setAddSource(Integer addSource) {
        this.addSource = addSource;
    }
}
