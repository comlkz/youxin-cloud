package com.youxin.chat.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.user.model.Group;
import com.youxin.chat.user.vo.group.*;
import com.youxin.chat.user.vo.req.*;
import com.youxin.chat.user.vo.user.UserDetailVo;
import com.youxin.exception.SystemException;

import java.util.List;

/**
 * description: GroupService <br>
 * date: 2020/2/27 14:20 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface GroupService extends IService<Group> {

    GroupDetailVo addGroup(String userNo, GroupAddReq groupAddDto) throws SystemException;

    /*
     * description: 添加群组 <br>
     * version: 1.0 <br>
     * date: 2020/1/8 11:19 <br>
     * author: llkj <br>
     *
     * @param userNo 用户编码
     * @param groupNo 群组编码
     * @param userList 用户更表
     * @param type  邀请方式 0：普通邀请 1：扫码申请
     * @return void
     */
    void joinGroup(String userNo, String groupNo, List<String> userList, int type) throws SystemException;

    /*
     * description: 添加群组 <br>
     * version: 1.0 <br>
     * date: 2020/1/14 10:52 <br>
     * author: llkj <br>
     *
     * @param userNo
     * @param groupAddDto
     * @return void
     */
    int joinGroup(String userNo, GroupAddReq groupAddDto) throws SystemException;

    void quitGroup(QuitGroupReq quitGroupDto, String userNo) throws SystemException;

    void dismissGroup(String groupNo, String userNo) throws SystemException;

    List<GroupMemberVo> listGroupMembers(String userNo, String groupNo) throws SystemException;

    GroupDetailVo acquireGroupInfo(String userNo, String groupNo) throws SystemException;

    GroupBasicVo getBasicGroupInfo(String userNo, String groupNo) throws SystemException;

    GroupByCodeVo getGroupByGroupCode(String groupCode,String userNo);

    void updateGroupNotice(String userNo, GroupNoticeReq groupNoticeDto) throws SystemException;

    void transferMaster(String userNo, String oldUserNo, String groupNo) throws SystemException;

    void setGroupManager(String userNo, String oldUserNo, String groupNo) throws SystemException;

    void removeGroupManager(String userNo, String oldUserNo, String groupNo) throws SystemException;

    List<GroupMemberVo> listGroupManagers(String groupNo) throws SystemException;

    void setBanned(GroupBannedReq bannedDto, String userNo) throws SystemException;

    void setGroupProtect(GroupProtectReq protectDto, String userNo) throws SystemException;

    void setQRStatus(GroupQRStatusReq qrStatusDto, String userNo) throws SystemException;

    void setNicknameProtect(GroupNicknameProtectReq nicknameProtectDto, String managerId) throws SystemException;

    void setGroupAudit(GroupAuditReq groupAuditDto, String userNo) throws SystemException;

    List<GroupAddListVo> listGroupAdd(String groupNo, String userNo) throws SystemException;

    void handleGroupAdd(String userNo, String auditUserNo, String auditGroupNo, int action) throws SystemException;

    List<GroupBlacklistVo> blacklistGroup(String groupNo, String userNo) throws SystemException;

    void blacklistGroupAdd(GroupBlackAddReq groupBlackAdd, String userNo) throws SystemException;

    void blacklistGroupDel(String groupNo,String rmUserNo, String userNo) throws SystemException;

    List<GroupQuitVo> quitGroupList(String groupNo, String userNo) throws SystemException;

    void renameGroup(String groupNo, String groupName, String userNo) throws SystemException;

    void setGroupExt(GroupAttrReq groupExt,String userNo) throws SystemException;

    void printScreenMsg(GroupPrintScreenReq printScreenMsgDto, String userNo) throws SystemException;

    List<UserDetailVo> groupUserActive(String userNo, String groupNo, int type) throws SystemException;

    /**
     * 发送群警告
     * @param groupNo
     */
    void sendGroupWarning(String groupNo);

    /**
     * 冻结群组
     * @param groupNo
     */
    void lockGroup(String groupNo);
}
