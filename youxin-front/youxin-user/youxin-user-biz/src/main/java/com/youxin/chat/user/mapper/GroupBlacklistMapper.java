package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.youxin.chat.user.model.GroupBlacklist;
import com.youxin.chat.user.vo.group.GroupBlacklistVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface GroupBlacklistMapper extends BaseMapper<GroupBlacklist> {

    List<GroupBlacklistVo> blacklistGroup(@Param("groupNo") String groupNo, @Param("userNo") String userNo);

    @Select("select count(1) from t_group_blacklist where group_no=#{groupNo} and user_no=#{userNo}")
    int countByGroupNoAndUserNo(@Param("groupNo")String groupNo,@Param("userNo")String userNo);

    int insertBatch(@Param("list") List<GroupBlacklist> list);

    @Delete("delete from t_group_blacklist where group_no=#{groupNo} and user_no=#{userNo}")
    void deleteByGroupNoAndUserNo(@Param("groupNo") String groupNo, @Param("userNo") String userNo);
}
