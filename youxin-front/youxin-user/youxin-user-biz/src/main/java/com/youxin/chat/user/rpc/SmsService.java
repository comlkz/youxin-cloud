package com.youxin.chat.user.rpc;

import com.youxin.chat.basic.client.ISmsClient;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

/**
 * description: SmsService <br>
 * date: 2020/2/29 10:22 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class SmsService {

    @Reference(timeout = 5000, retries = 1)
    private ISmsClient smsClient;

    public void sendSms(Integer type, String phone, String content, Long ip) {
        smsClient.sendSms(type, phone, content, ip);
    }

}
