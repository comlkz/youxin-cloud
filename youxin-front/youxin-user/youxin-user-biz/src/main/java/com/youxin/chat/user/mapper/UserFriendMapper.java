package com.youxin.chat.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.youxin.chat.user.model.UserFriend;
import com.youxin.chat.user.vo.user.UserFriendVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserFriendMapper extends BaseMapper<UserFriend> {


    UserFriend selectFriendDetails(@Param("userNo") String userNo, @Param("friendNo") String friendNo);

    void deleteFriend(@Param("userNo") String userNo, @Param("friendNo") String friendNo);

    void remarkFriend(@Param("userFriend") UserFriend userFriend);

    @Select("select ID, USER_NO, FRIEND_NO, ifnull(FRIEND_NOTE,FRIEND_NICK_NAME) as FRIEND_NICK_NAME, FRIEND_AVATAR, FRIEND_STATUS,STAR_FRIEND, CREATE_TIME, UPDATE_TIME, STATES from t_user_friend where USER_NO = #{userNo}\n" +
            "order by STAR_FRIEND desc,FRIEND_NICK_NAME ASC")
    List<UserFriendVo> listFriend(@Param("userNo") String userNo);

    void setFriendDisturb(@Param("userNo") String userNo, @Param("type") Integer type,@Param("friendid")Integer friendid);

    List<UserFriendVo> blackList(@Param("userNo") String userNo);
}
