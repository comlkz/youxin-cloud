package com.youxin.chat.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.user.dto.UserAddDto;
import com.youxin.chat.user.dto.UserInfoDto;
import com.youxin.chat.user.model.UserInfo;
import com.youxin.chat.user.vo.req.*;
import com.youxin.chat.user.vo.user.*;
import com.youxin.exception.SystemException;

import java.util.List;
import java.util.Set;

/**
 * description: UserService <br>
 * date: 2020/2/25 11:36 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface UserService extends IService<UserInfo> {

    void setLoginPass(String userNo, LoginPassReq loginPass) throws SystemException;

    void perfectUserInfo(String userNo, UserInfoSetReq req);

    LoginResp mobileLogin(LoginMobileReq req);

    void register(RegisterReq registerReq) throws SystemException;

    /**
     * 验证注册短信验证码
     * @param registerCodeReq
     * @return
     */
    String validateRegisterCode(RegisterCodeReq registerCodeReq);

    void sendSms(RegisterSmsReq smsReqDto, Long ip) throws SystemException;

    void forgetLoginPass(ForgetPassReq forgetPassReq) throws SystemException;

    void logOut(String userNo);

    UserDetailVo selectUserDetails(String userNo) throws SystemException;

    UserDetailVo getUserInfo(String userNo) throws SystemException;

    List<UserDetailVo> listSpecialUsers() throws SystemException;

    List<UserMatchVo> matchConcatUser(String userNo, List<UserMatchReq> mobileList) throws SystemException;

    Set selectAssistant() throws SystemException;

    void saveUserName(String userNo, String account) throws SystemException;

    void shareMsg(ShareMsgReq shareMsgDto, String userId) throws SystemException;

    void saveActiveRecord(String userNo);

    List<UserCollectVo> collectList(String userNo, CollectSearchReq search) throws SystemException;

    void collectAdd(String userNo, UserCollectReq userCollectAdd) throws SystemException;

    void collectDel(String userNo, int id) throws SystemException;

   void setFriendAttr(FriendExtReq friendExtDto, String userNo) throws SystemException;

    void updateUserStates(String id, Integer status);

    void saveUser(UserAddDto userAddDto);

    UserInfoDto getUserInfoByToken(String token);
}
