package com.youxin.chat.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.chat.user.mapper.ChatComplaintMapper;
import com.youxin.chat.user.model.ChatComplaint;
import com.youxin.chat.user.service.ComplaintService;
import com.youxin.chat.user.vo.req.ComplaintReq;
import com.youxin.dozer.DozerUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * description: ComplaintServiceImpl <br>
 * date: 2020/3/10 10:15 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class ComplaintServiceImpl extends ServiceImpl<ChatComplaintMapper, ChatComplaint> implements ComplaintService {


    @Resource
    private DozerUtils dozerUtils;

    @Override
    public void setComplaint(String userNo, ComplaintReq complaint) {
        ChatComplaint chatComplaint = dozerUtils.map(complaint,ChatComplaint.class);
        chatComplaint.setUserNo(userNo);
        this.baseMapper.insert(chatComplaint);
    }
}
