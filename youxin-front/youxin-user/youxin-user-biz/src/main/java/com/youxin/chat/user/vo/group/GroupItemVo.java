package com.youxin.chat.user.vo.group;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class GroupItemVo implements Serializable {

    @ApiModelProperty(value = "群编码")
    private String groupNo;

    @ApiModelProperty(value = "群聊名称")
    private String groupName;//群聊名称

    @ApiModelProperty(value = "群公告")
    private String groupNotice;//群公告

    @ApiModelProperty(value = "群头像")
    private String groupAvatar;



    @ApiModelProperty(value = "是否加入通讯录 0：否 1：是", required = true)
    private Integer isJoinAddress;

    @ApiModelProperty(value = "是否名打扰 0：否 1：是", required = true)
    private Integer isNotDisturb;

    @ApiModelProperty(value = "是否置顶 0：否 1：是", required = true)
    private Integer isTop;


    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupNotice() {
        return groupNotice;
    }

    public void setGroupNotice(String groupNotice) {
        this.groupNotice = groupNotice;
    }



    public String getGroupAvatar() {
        return groupAvatar;
    }

    public void setGroupAvatar(String groupAvatar) {
        this.groupAvatar = groupAvatar;
    }

    public Integer getIsJoinAddress() {
        return isJoinAddress;
    }

    public void setIsJoinAddress(Integer isJoinAddress) {
        this.isJoinAddress = isJoinAddress;
    }

    public Integer getIsNotDisturb() {
        return isNotDisturb;
    }

    public void setIsNotDisturb(Integer isNotDisturb) {
        this.isNotDisturb = isNotDisturb;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }
}
