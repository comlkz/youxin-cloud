package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class UserApplyReq implements Serializable {
    private static final long serialVersionUID = -8490479948047877984L;

    @ApiModelProperty(value = "用户编码",required = true)
    private String userNo;

    @ApiModelProperty(value = "添加方式 0：手机号申请 1：帐号申请  2：扫码申请 3：通过群添加",required = true)
    private Integer source;

    @ApiModelProperty(value = "手机号",required = false)
    private String mobile;

    @ApiModelProperty(value = "备注",required = false)
    private String remark;

    @ApiModelProperty(value = "设备类型 0：android 1:IOS",required = false)
    private Integer deviceType;

    @ApiModelProperty(value = "添加来自群ID",required = false)
    private String groupId;

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
