package com.youxin.chat.user.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;

@TableName("t_group")
public class Group {

    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    private String groupNo;

    private String groupName;//群聊名称

    private String groupNotice;//群公告

    private String groupOwner;//群主编号

    private String createUser;//创建人

    private String groupAvatar;//群头像

    private Integer protectedMode;//保护模式0：开启，1：关闭

    private Integer qrStatus;//二维码状态0：开启，1：禁用

    private Integer banned;//禁言状态0：允许，1：禁言

    private Integer nicknameProtect;//群成员昵称保护0：开启，1：关闭

    private Integer audit;//审核状态0：禁用，1：开启

    //@TableField(el = "ext,typeHandler=com.guoliao.chat.common.config.mybatis.MySqlJsonHandler")
    private String ext;

    private Integer appendStatus;//新增状态1：有新增 0：无新增


    private Integer groupStatus;//群组状态 0：正常 -1：冻结


    private LocalDateTime createTime;

    private LocalDateTime updateTime;//修改时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id ;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName == null ? null : groupName.trim();
    }

    public String getGroupNotice() {
        return groupNotice;
    }

    public void setGroupNotice(String groupNotice) {
        this.groupNotice = groupNotice == null ? null : groupNotice.trim();
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner == null ? null : groupOwner.trim();
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getGroupAvatar() {
        return groupAvatar;
    }

    public void setGroupAvatar(String groupAvatar) {
        this.groupAvatar = groupAvatar;
    }

    public Integer getProtectedMode() {
        return protectedMode;
    }

    public void setProtectedMode(Integer protectedMode) {
        this.protectedMode = protectedMode;
    }

    public Integer getQrStatus() {
        return qrStatus;
    }

    public void setQrStatus(Integer qrStatus) {
        this.qrStatus = qrStatus;
    }

    public Integer getBanned() {
        return banned;
    }

    public void setBanned(Integer banned) {
        this.banned = banned;
    }

    public Integer getNicknameProtect() {
        return nicknameProtect;
    }

    public void setNicknameProtect(Integer nicknameProtect) {
        this.nicknameProtect = nicknameProtect;
    }

    public Integer getAudit() {
        return audit;
    }

    public void setAudit(Integer audit) {
        this.audit = audit;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public Integer getAppendStatus() {
        return appendStatus;
    }

    public void setAppendStatus(Integer appendStatus) {
        this.appendStatus = appendStatus;
    }

    public Integer getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(Integer groupStatus) {
        this.groupStatus = groupStatus;
    }
}
