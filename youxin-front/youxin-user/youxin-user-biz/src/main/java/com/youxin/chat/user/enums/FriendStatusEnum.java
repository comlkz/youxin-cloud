package com.youxin.chat.user.enums;

public enum FriendStatusEnum {

    NORMAL(0,"正常"),

    BLACK(1,"拉黑");

    private Integer status;

    private String desc;

    FriendStatusEnum(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus(){
        return this.status;
    }

    public String getDesc(){
        return this.desc;
    }

}
