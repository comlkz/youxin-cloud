package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class GroupBannedReq implements Serializable {

    @ApiModelProperty(value = "群聊ID",required = true)
    private String groupId;
    @ApiModelProperty(value = "禁言状态0：允许，1：禁言",required = true)
    private Integer banned;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getBanned() {
        return banned;
    }

    public void setBanned(Integer banned) {
        this.banned = banned;
    }
}
