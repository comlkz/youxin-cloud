package com.youxin.chat.user.vo.req;

import java.io.Serializable;

public class GroupUserReq implements Serializable {

    private String groupNo;

    private String userNo;

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }
}
