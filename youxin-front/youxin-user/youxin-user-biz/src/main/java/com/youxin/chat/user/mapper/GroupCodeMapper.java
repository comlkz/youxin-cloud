package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.user.model.Group;
import com.youxin.chat.user.model.GroupCode;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * description: GroupCodeMapper <br>
 * date: 2020/1/14 10:40 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface GroupCodeMapper extends BaseMapper<GroupCode> {

    @Select("select * from t_group_code where group_code=#{groupCode}")
    GroupCode getByGroupCode(@Param("groupCode") String groupCode);

    @Delete("delete from t_group_code where group_no=#{groupNo}")
    void deleteByGroupNo(@Param("groupNo") String groupNo);
}
