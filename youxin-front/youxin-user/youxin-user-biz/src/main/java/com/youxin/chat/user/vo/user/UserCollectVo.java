package com.youxin.chat.user.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

@ApiModel(description = "用户收藏表")
public class UserCollectVo implements Serializable {

    private Integer id;
    @ApiModelProperty(value = "用户编码")
    private String userNo;
    @ApiModelProperty(value = "来源")
    private String contentFrom;
    @ApiModelProperty(value = "类型 1：消息 2：图片 3：链接 4：语音")
    private Integer contentType;

    @ApiModelProperty(value = "文件大小")
    private Integer fileSize;
    @ApiModelProperty(value = "内容")
    private String content;
    @ApiModelProperty(value = "内容链接")
    private String contentUrl;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getContentFrom() {
        return contentFrom;
    }

    public void setContentFrom(String contentFrom) {
        this.contentFrom = contentFrom;
    }

    public Integer getContentType() {
        return contentType;
    }

    public void setContentType(Integer contentType) {
        this.contentType = contentType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }
}
