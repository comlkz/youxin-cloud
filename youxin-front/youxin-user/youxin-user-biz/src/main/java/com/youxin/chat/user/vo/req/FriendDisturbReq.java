package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class FriendDisturbReq implements Serializable {

    private Integer friendId;
    @ApiModelProperty(value = "免打扰 0：否 1：是",required = true)
    private Integer type;

    public Integer getFriendId() {
        return friendId;
    }

    public void setFriendId(Integer friendId) {
        this.friendId = friendId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
