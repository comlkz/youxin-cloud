package com.youxin.chat.user.manager;

import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CacheRefresh;
import com.alicp.jetcache.anno.Cached;
import com.alicp.jetcache.anno.CreateCache;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.youxin.chat.user.mapper.UserAddMapper;
import com.youxin.chat.user.mapper.UserFriendMapper;
import com.youxin.chat.user.mapper.UserInfoMapper;
import com.youxin.chat.user.model.UserAdd;
import com.youxin.chat.user.model.UserFriend;
import com.youxin.chat.user.model.UserInfo;
import com.youxin.chat.user.vo.user.UserFriendVo;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * description: UserFriendManager <br>
 * date: 2020/2/26 16:27 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class UserFriendManager {

    @Resource
    private UserFriendMapper userFriendMapper;

    @Resource
    private UserAddMapper userAddMapper;

    @Resource
    private UserInfoMapper userInfoMapper;

    @CreateCache(name = "user.friend.list.")
    private Cache<String, Object> jetcache;

    @Transactional
    public List<Integer> addFriend(String userNo, String applyUserNo, Integer source) {
        List<Integer> result = new ArrayList<>();
        UserFriend selfSide = userFriendMapper.selectOne(new LambdaQueryWrapper<UserFriend>().eq(UserFriend::getUserNo, userNo)
                .eq(UserFriend::getFriendNo, applyUserNo));
        UserFriend friendSide = userFriendMapper.selectOne(new LambdaQueryWrapper<UserFriend>().eq(UserFriend::getUserNo, applyUserNo)
                .eq(UserFriend::getFriendNo, userNo));
        UserInfo userInfo = userInfoMapper.selectOne(new LambdaQueryWrapper<UserInfo>().eq(UserInfo::getUserNo, userNo));
        UserInfo applyUserInfo = userInfoMapper.selectOne(new LambdaQueryWrapper<UserInfo>().eq(UserInfo::getUserNo, applyUserNo));
        UserAdd userAdd = userAddMapper.selectOne(new LambdaQueryWrapper<UserAdd>().eq(UserAdd::getUserNo, applyUserNo).eq(UserAdd::getAddUserNo, userNo).eq(UserAdd::getAddStatus, 0));
        userAdd.setAddStatus(source);
        userAddMapper.updateById(userAdd);
        if (selfSide == null) {
            UserFriend userFriend = new UserFriend();
            userFriend.setUserNo(userNo);
            userFriend.setFriendNo(applyUserInfo.getUserNo());
            userFriend.setFriendNickName(applyUserInfo.getNickName());
            userFriend.setFriendAvatar(applyUserInfo.getAvatar());
            userFriend.setFriendStatus(0);
            userFriend.setCreateTime(LocalDateTime.now());
            userFriend.setUpdateTime(LocalDateTime.now());
            userFriendMapper.insert(userFriend);
            result.add(0);
        }

        if (friendSide == null) {
            UserFriend userFriend = new UserFriend();
            userFriend.setUserNo(applyUserNo);
            userFriend.setFriendNo(userInfo.getUserNo());
            userFriend.setFriendNickName(userInfo.getNickName());
            userFriend.setFriendAvatar(userInfo.getAvatar());
            userFriend.setFriendStatus(0);
            userFriend.setCreateTime(LocalDateTime.now());
            userFriend.setUpdateTime(LocalDateTime.now());
            userFriendMapper.insert(userFriend);
            result.add(1);
        }
        try {
            jetcache.remove(userNo);
            jetcache.remove(applyUserNo);
        } catch (Exception e) {

        }
        return result;
    }

    @Transactional
    public void deleteFriend(String userNo, String friendNo) {
        userFriendMapper.deleteFriend(userNo, friendNo);
        userFriendMapper.deleteFriend(friendNo, userNo);
        try {
            jetcache.remove(userNo);
            jetcache.remove(friendNo);
        } catch (Exception e) {

        }
    }

    @Cached(name = "user.friend.list.", key = "#userNo", timeUnit = TimeUnit.HOURS, expire = 10)
    @CacheRefresh(refresh = 30, timeUnit = TimeUnit.MINUTES)
    public List<UserFriendVo> listFriend(String userNo) {
        return userFriendMapper.listFriend(userNo);
    }

    @CacheInvalidate(name = "user.friend.list.", key = "#userFriend.userNo")
    public void remarkFriend(UserFriend userFriend) {

        userFriendMapper.remarkFriend(userFriend);
    }


    @CacheInvalidate(name = "user.friend.list.", key = "#userFriend.userNo")
    public void starFriend(UserFriend userFriend) {
        userFriendMapper.update(userFriend,
                new LambdaQueryWrapper<UserFriend>().eq(UserFriend::getUserNo, userFriend.getUserNo()).eq(UserFriend::getFriendNo, userFriend.getFriendNo()));

    }
}
