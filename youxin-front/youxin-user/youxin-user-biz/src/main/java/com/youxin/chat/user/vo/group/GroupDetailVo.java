package com.youxin.chat.user.vo.group;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

public class GroupDetailVo implements Serializable {

    @ApiModelProperty(value = "群ID", required = true)
    private String groupNo;

    @ApiModelProperty(value = "群成员", required = true)
    private List<GroupMemberVo> groupMembers;

    @ApiModelProperty(value = "群名称", required = true)
    private String groupName;

    @ApiModelProperty(value = "群公告", required = true)
    private String groupNotice;

    @ApiModelProperty(value = "是否加入通讯录 0：否 1：是", required = true)
    private Integer isJoinAddress;

    @ApiModelProperty(value = "是否名打扰 0：否 1：是", required = true)
    private Integer isNotDisturb;

    @ApiModelProperty(value = "是否置顶 0：否 1：是", required = true)
    private Integer isTop;

    @ApiModelProperty(value = "群主编码", required = true)
    private String adminNo;

    @ApiModelProperty(value = "群主昵称", required = true)
    private String adminNickName;


    @ApiModelProperty(value = "是否加入群组，0：否 1：是")
    private Integer joinInGroup;

    @ApiModelProperty(value = "群头像")
    private String groupAvatar;

    @ApiModelProperty(value = "群成员数目")
    private Integer memberNum;
    private String nickName;

    @ApiModelProperty(value = "保护模式0：开启，1：关闭")
    private Integer protectedMode;
    @ApiModelProperty(value = "二维码状态0：开启，1：禁用")
    private Integer qrStatus;
    @ApiModelProperty(value = "禁言状态0：允许，1：禁言")
    private Integer banned;

    @ApiModelProperty(value = "审核状态0：禁用，1：开启")
    private Integer audit;

    @ApiModelProperty(value = "群成员昵称保护0：开启，1：关闭",required = true)
    private Integer nicknameProtect;

    @ApiModelProperty(value = "禁群消息定时清理 -1：关闭，0：即刻清理，其余时间（单位秒）")
    private int clearTime;

    @ApiModelProperty(value = "禁群消息定时清理时刻 yyyy-MM-dd HH:mm:ss")
    private String clearTimePoint;

    @ApiModelProperty(value = "截屏通知 0：不开启，1：开启")
    private int printScreenNotify;

    @ApiModelProperty(value = "群二维码编码")
    private String groupCode;

    @ApiModelProperty(value = "新增状态1：有新增 0：无新增")
    private Integer appendStatus;

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }


    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupNotice() {
        return groupNotice;
    }

    public void setGroupNotice(String groupNotice) {
        this.groupNotice = groupNotice;
    }

    public Integer getIsJoinAddress() {
        return isJoinAddress;
    }

    public void setIsJoinAddress(Integer isJoinAddress) {
        this.isJoinAddress = isJoinAddress;
    }

    public Integer getIsNotDisturb() {
        return isNotDisturb;
    }

    public void setIsNotDisturb(Integer isNotDisturb) {
        this.isNotDisturb = isNotDisturb;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public List<GroupMemberVo> getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(List<GroupMemberVo> groupMembers) {
        this.groupMembers = groupMembers;
    }

    public String getAdminNo() {
        return adminNo;
    }

    public void setAdminNo(String adminNo) {
        this.adminNo = adminNo;
    }

    public String getAdminNickName() {
        return adminNickName;
    }

    public void setAdminNickName(String adminNickName) {
        this.adminNickName = adminNickName;
    }

    public Integer getJoinInGroup() {
        return joinInGroup;
    }

    public void setJoinInGroup(Integer joinInGroup) {
        this.joinInGroup = joinInGroup;
    }

    public String getGroupAvatar() {
        return groupAvatar;
    }

    public void setGroupAvatar(String groupAvatar) {
        this.groupAvatar = groupAvatar;
    }

    public Integer getMemberNum() {
        return memberNum;
    }

    public void setMemberNum(Integer memberNum) {
        this.memberNum = memberNum;
    }

    public Integer getProtectedMode() {
        return protectedMode;
    }

    public void setProtectedMode(Integer protectedMode) {
        this.protectedMode = protectedMode;
    }

    public Integer getQrStatus() {
        return qrStatus;
    }

    public void setQrStatus(Integer qrStatus) {
        this.qrStatus = qrStatus;
    }

    public Integer getBanned() {
        return banned;
    }

    public void setBanned(Integer banned) {
        this.banned = banned;
    }

    public Integer getNicknameProtect() {
        return nicknameProtect;
    }

    public void setNicknameProtect(Integer nicknameProtect) {
        this.nicknameProtect = nicknameProtect;
    }

    public int getClearTime() {
        return clearTime;
    }

    public void setClearTime(int clearTime) {
        this.clearTime = clearTime;
    }

    public int getPrintScreenNotify() {
        return printScreenNotify;
    }

    public void setPrintScreenNotify(int printScreenNotify) {
        this.printScreenNotify = printScreenNotify;
    }

    public String getClearTimePoint() {
        return clearTimePoint;
    }

    public void setClearTimePoint(String clearTimePoint) {
        this.clearTimePoint = clearTimePoint;
    }

    public Integer getAudit() {
        return audit;
    }

    public void setAudit(Integer audit) {
        this.audit = audit;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public Integer getAppendStatus() {
        return appendStatus;
    }

    public void setAppendStatus(Integer appendStatus) {
        this.appendStatus = appendStatus;
    }
}
