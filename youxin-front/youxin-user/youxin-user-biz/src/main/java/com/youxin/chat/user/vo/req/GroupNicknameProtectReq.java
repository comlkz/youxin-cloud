package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class GroupNicknameProtectReq implements Serializable {
    @ApiModelProperty(value = "群聊ID",required = true)
    private String groupId;
    @ApiModelProperty(value = "群成员昵称保护0：开启，1：关闭",required = true)
    private Integer nicknameProtect;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getNicknameProtect() {
        return nicknameProtect;
    }

    public void setNicknameProtect(Integer nicknameProtect) {
        this.nicknameProtect = nicknameProtect;
    }
}
