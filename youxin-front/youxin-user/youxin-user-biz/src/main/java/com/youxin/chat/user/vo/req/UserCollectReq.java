package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class UserCollectReq implements Serializable {
    @ApiModelProperty(value = "来源", required = true)
    private String contentFrom;
    @ApiModelProperty(value = "类型 1：消息 2：图片 3：链接 4：语音 5:游戏", required = true)
    private Integer contentType;
    @ApiModelProperty(value = "内容", required = true)
    private String content;

    @ApiModelProperty(value = "内容链接", required = true)
    private String contentUrl;

    @ApiModelProperty(value = "文件大小（单位：字节）", required = true)
    private long fileSize;

    public String getContentFrom() {
        return contentFrom;
    }

    public void setContentFrom(String contentFrom) {
        this.contentFrom = contentFrom;
    }

    public Integer getContentType() {
        return contentType;
    }

    public void setContentType(Integer contentType) {
        this.contentType = contentType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }
}
