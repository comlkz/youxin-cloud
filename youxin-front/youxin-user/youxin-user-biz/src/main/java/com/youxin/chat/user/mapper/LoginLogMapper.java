package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.user.model.LoginLog;

public interface LoginLogMapper extends BaseMapper<LoginLog> {

}
