package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class FriendStarReq implements Serializable {
    @ApiModelProperty(value = "好友编码",required = true)
    private String friendNo;
    @ApiModelProperty(value = "星标好友 0：否 1：是",required = true)
    private Integer starFriend;

    public String getFriendNo() {
        return friendNo;
    }

    public void setFriendNo(String friendNo) {
        this.friendNo = friendNo;
    }

    public Integer getStarFriend() {
        return starFriend;
    }

    public void setStarFriend(Integer starFriend) {
        this.starFriend = starFriend;
    }
}
