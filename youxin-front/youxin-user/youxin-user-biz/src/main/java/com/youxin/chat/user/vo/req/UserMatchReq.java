package com.youxin.chat.user.vo.req;

import java.io.Serializable;

public class UserMatchReq implements Serializable {

    private String mobile;

    private String trueName;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }
}
