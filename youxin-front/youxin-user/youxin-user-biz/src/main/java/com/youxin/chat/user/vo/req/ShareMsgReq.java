package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

public class ShareMsgReq implements Serializable {

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "图片地址")
    private String imageUri;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "链接地址")
    private String url;

    @ApiModelProperty(value = "发送用户")
    private List<ShareMsgUserItem> accounts;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<ShareMsgUserItem> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<ShareMsgUserItem> accounts) {
        this.accounts = accounts;
    }
}


