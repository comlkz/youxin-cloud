package com.youxin.chat.user.model;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

public class UserFriend {
    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    private String userNo;//用户编码

    private String friendNo;//好友用户编码

    private String friendNickName;//好友昵称

    private String friendNote;//好友备注

    private String friendAvatar;//好友头像

    private Integer friendStatus;//好友状态 0：正常 1：拉黑

    /**
     * 星标好友 0:否 1：是
     */
    private Integer starFriend;

    private Integer notDisturb;//免打扰 0：否 1：是

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private String states;//标识状态

    private String ext;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo == null ? null : userNo.trim();
    }

    public String getFriendNo() {
        return friendNo;
    }

    public void setFriendNo(String friendNo) {
        this.friendNo = friendNo == null ? null : friendNo.trim();
    }

    public String getFriendNickName() {
        return friendNickName;
    }

    public void setFriendNickName(String friendNickName) {
        this.friendNickName = friendNickName == null ? null : friendNickName.trim();
    }

    public String getFriendAvatar() {
        return friendAvatar;
    }

    public void setFriendAvatar(String friendAvatar) {
        this.friendAvatar = friendAvatar == null ? null : friendAvatar.trim();
    }

    public Integer getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(Integer friendStatus) {
        this.friendStatus = friendStatus;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public Integer getStarFriend() {
        return starFriend;
    }

    public void setStarFriend(Integer starFriend) {
        this.starFriend = starFriend;
    }

    public String getFriendNote() {
        return friendNote;
    }

    public void setFriendNote(String friendNote) {
        this.friendNote = friendNote;
    }

    public Integer getNotDisturb() {
        return notDisturb;
    }

    public void setNotDisturb(Integer notDisturb) {
        this.notDisturb = notDisturb;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}
