package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class GroupNoticeReq implements Serializable {
    @ApiModelProperty(value = "群聊ID",required = true)
    private String groupNo;
    @ApiModelProperty(value = "群公告",required = true)
    private String groupNotice;

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupNotice() {
        return groupNotice;
    }

    public void setGroupNotice(String groupNotice) {
        this.groupNotice = groupNotice;
    }
}
