package com.youxin.chat.user.vo.user;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class UserSearchVo implements Serializable {

    @ApiModelProperty(value = "用户名称", required = true)
    private String userName;
    @ApiModelProperty(value = "用户标识", required = true)
    private String userNo;
    @ApiModelProperty(value = "用户昵称", required = false)
    private String nickName;
    @ApiModelProperty(value = "用户头像", required = false)
    private String avatar;
    @ApiModelProperty(value = "性别 0:未知，1:男,2:女", required = false)
    private  Integer sex;
    @ApiModelProperty(value = "所在地区", required = false)
    private String area;

    @ApiModelProperty(value = "手机号", required = false)
    private String mobile;

    @ApiModelProperty(value = "是否是好友，0:否，1:是")
    private Integer isFriend;


    @ApiModelProperty(value = "用户来源，0：用户名搜索，1：手机号搜索", required = false)
    private Integer source;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getIsFriend() {
        return isFriend;
    }

    public void setIsFriend(Integer isFriend) {
        this.isFriend = isFriend;
    }

    @Override
    public String toString() {
        return "UserSearchVo{" +
                "userName='" + userName + '\'' +
                ", userNo='" + userNo + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", sex=" + sex +
                ", area='" + area + '\'' +
                ", mobile='" + mobile + '\'' +
                ", isFriend=" + isFriend +
                ", source=" + source +
                '}';
    }
}
