package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class GroupQRStatusReq implements Serializable {
    @ApiModelProperty(value = "群聊ID",required = true)
    private String groupId;
    @ApiModelProperty(value = "二维码状态0：开启，1：禁用",required = true)
    private Integer qrStatus;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getQrStatus() {
        return qrStatus;
    }

    public void setQrStatus(Integer qrStatus) {
        this.qrStatus = qrStatus;
    }
}
