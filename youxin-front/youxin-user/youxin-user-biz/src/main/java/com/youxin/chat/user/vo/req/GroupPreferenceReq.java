package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class GroupPreferenceReq implements Serializable {

    @ApiModelProperty(value = "群ID",required = true)
    private String groupNo;
    @ApiModelProperty(value = "是否置顶", required = false)
    private Integer isTop;
    @ApiModelProperty(value = "免打扰", required = false)
    private Integer notDisturb;
    @ApiModelProperty(value = "是否加入群组 0：否 1：是",required = false)
    private Integer isAddressList;
    @ApiModelProperty(value = "背景图片",required = false)
    private String backgroundImg;

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public Integer getNotDisturb() {
        return notDisturb;
    }

    public void setNotDisturb(Integer notDisturb) {
        this.notDisturb = notDisturb;
    }

    public Integer getIsAddressList() {
        return isAddressList;
    }

    public void setIsAddressList(Integer isAddressList) {
        this.isAddressList = isAddressList;
    }

    public String getBackgroundImg() {
        return backgroundImg;
    }

    public void setBackgroundImg(String backgroundImg) {
        this.backgroundImg = backgroundImg;
    }
}
