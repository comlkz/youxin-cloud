package com.youxin.chat.user.model;

import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

@TableName("t_login_log")
public class LoginLog {
    private Integer id;

    private String username;//用户名

    private Date loginTime;//登录时间

    private String location;//登录地点

    private String ip;//IP地址

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location == null ? null : location.trim();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }
}
