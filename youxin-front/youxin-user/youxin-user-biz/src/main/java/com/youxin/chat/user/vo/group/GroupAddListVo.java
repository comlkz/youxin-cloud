package com.youxin.chat.user.vo.group;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

public class GroupAddListVo implements Serializable {
    private static final long serialVersionUID = -6214948926429971822L;
    private Long id;

    /**
     * 用户编码
     */
    @ApiModelProperty(value = "用户编码")
    private String userNo;

    /**
     * 被申请人群编码
     */
    @ApiModelProperty(value = "待添加群编码")
    private String addGroupNo;

    /**
     * 邀请人昵称
     */
    @ApiModelProperty(value = "申请人昵称")
    private String nickName;

    @ApiModelProperty(value = "申请人头像")
    private String avatar;

    /**
     *被申请人昵称
     */
    @ApiModelProperty(value = "被申请群昵称")
    private String addNickName;

    /**
     * 被申请人头像
     */
    @ApiModelProperty(value = "被申请群头像")
    private String addAvatar;

    /**
     * 添加时间
     */
    @ApiModelProperty(value = "添加时间")
    private LocalDateTime addTime;

    /**
     * 申请状态，0：待处理  1：添加成功  -1：拒绝
     */
    @ApiModelProperty(value = "申请状态，0：待处理  1：添加成功  -1：拒绝")
    private int addStatus;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 完成时间
     */
    @ApiModelProperty(value = "完成时间")
    private LocalDateTime finishTime;

    /**
     * 添加方式 0：普通邀请 1：扫码申请
     */
    @ApiModelProperty(value = "添加方式 0：普通邀请 1：扫码申请")
    private Integer addSource;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getAddGroupNo() {
        return addGroupNo;
    }

    public void setAddGroupNo(String addGroupNo) {
        this.addGroupNo = addGroupNo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAddNickName() {
        return addNickName;
    }

    public void setAddNickName(String addNickName) {
        this.addNickName = addNickName;
    }

    public String getAddAvatar() {
        return addAvatar;
    }

    public void setAddAvatar(String addAvatar) {
        this.addAvatar = addAvatar;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    public int getAddStatus() {
        return addStatus;
    }

    public void setAddStatus(int addStatus) {
        this.addStatus = addStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public LocalDateTime getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(LocalDateTime finishTime) {
        this.finishTime = finishTime;
    }

    public Integer getAddSource() {
        return addSource;
    }

    public void setAddSource(Integer addSource) {
        this.addSource = addSource;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}
