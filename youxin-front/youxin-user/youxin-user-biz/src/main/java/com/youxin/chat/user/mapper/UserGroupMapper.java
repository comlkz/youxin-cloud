package com.youxin.chat.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.user.model.UserGroup;
import com.youxin.chat.user.vo.group.GroupItemVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserGroupMapper extends BaseMapper<UserGroup> {

    void insertBatch(@Param("list") List<UserGroup> list);

    int deleteByGroupNoAndUserNos(@Param("groupNo") String groupNo, @Param("userNos") List<String> userNos);

    int deleteByGroupNo(@Param("groupNo") String groupNo);

    UserGroup selectByGroupNo(@Param("groupNo") String groupNo, @Param("userNo") String userNo);

    void updateByGroupNo(@Param("userGroup") UserGroup userGroup);

}
