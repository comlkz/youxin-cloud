package com.youxin.chat.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.user.model.UserFriend;
import com.youxin.chat.user.vo.req.FriendDisturbReq;
import com.youxin.chat.user.vo.req.UserApplyReq;
import com.youxin.chat.user.vo.user.UserFriendVo;
import com.youxin.chat.user.vo.user.UserSearchVo;
import com.youxin.exception.SystemException;

import java.util.List;

/**
 * description: UserFriendService <br>
 * date: 2020/2/25 14:12 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface UserFriendService extends IService<UserFriend> {

    void applyUser(String userNo, UserApplyReq userApplyReq) throws SystemException;

    void handleApply(String userNo, String applyUserNo, Integer action) throws SystemException;

    UserSearchVo findUserByKey(String userNo, String searchKey) throws SystemException;

    List<UserFriendVo> listFriend(String userNo) throws SystemException;

    UserFriendVo selectFriendDetails(String userNo, String friendNo) throws SystemException;

    void blackUser(String userNo, String friendNo, Integer status) throws SystemException;

    void deleteFriend(String userNo, String friendNo) throws SystemException;

    void starFriend(String userNo, String friendNo, Integer type) throws SystemException;

    void remarkFriend(String userNo, String friendNo, String remark) throws SystemException;

    void setFriendDisturb(String userNO, FriendDisturbReq friendDisturbDto) throws SystemException;

    List<UserFriendVo> blackList(String userNo) throws SystemException;

}
