package com.youxin.chat.user.vo.user;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class UserMatchVo implements Serializable {

    private String mobile;

    private String trueName;

    @ApiModelProperty("好友状态 0：未注册；1：待添加 ；2：已添加")
    private Integer status;

    @ApiModelProperty("用户编码")
    private String userNo;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }
}
