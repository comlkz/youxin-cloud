package com.youxin.chat.user.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.time.LocalDateTime;

public class UserInfo implements Serializable {


    private static final long serialVersionUID = -7720703795451729171L;
    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    private String userName;//用户名称

    private String password;//用户密码

    private String mobile;//用户手机号

    private String userNo;//用户标识

    private String nickName;//用户昵称

    private String avatar;//用户头像

    private Integer sex;//性别 0：未知 1：男 2：女

    private String area;//所在地区（以,分隔）

    private String payPassword;//支付密码

    private Integer userStatus;//用户状态  0：正常  -1：禁用

    private Integer hasPerfect;

    /**
     * 代理商编码
     */
    private String agentNo;

    /**
     * 用户类型 0：普通用户 1：系统用户 2：客服
     */
    private Integer userType;

    /**
     * 用户等级
     */
    private Integer userLevel;


    private LocalDateTime lastUseTime;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public LocalDateTime getLastUseTime() {
        return lastUseTime;
    }

    public void setLastUseTime(LocalDateTime lastUseTime) {
        this.lastUseTime = lastUseTime;
    }

    public String getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(String agentNo) {
        this.agentNo = agentNo;
    }

    public Integer getHasPerfect() {
        return hasPerfect;
    }

    public void setHasPerfect(Integer hasPerfect) {
        this.hasPerfect = hasPerfect;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "id='" + id + '\'' +
                ", userName='" + userName + '\'' +
                ", mobile='" + mobile + '\'' +
                ", userNo='" + userNo + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", sex=" + sex +
                ", area='" + area + '\'' +
                ", payPassword='" + payPassword + '\'' +
                ", userStatus=" + userStatus +
                ", hasPerfect=" + hasPerfect +
                ", agentNo='" + agentNo + '\'' +
                ", userType=" + userType +
                ", userLevel=" + userLevel +
                ", lastUseTime=" + lastUseTime +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
