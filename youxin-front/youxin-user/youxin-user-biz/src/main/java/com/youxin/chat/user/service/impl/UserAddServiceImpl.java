package com.youxin.chat.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.chat.basic.client.IFileClient;
import com.youxin.chat.basic.dto.constant.FileTypeEnum;
import com.youxin.chat.user.mapper.UserAddMapper;
import com.youxin.chat.user.model.UserAdd;
import com.youxin.chat.user.rpc.FileService;
import com.youxin.chat.user.service.UserAddService;
import com.youxin.chat.user.vo.user.UserAddVo;
import com.youxin.common.config.YouxinCommonProperties;
import com.youxin.dozer.DozerUtils;
import com.youxin.exception.SystemException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * description: UserAddServiceImpl <br>
 * date: 2020/2/25 12:06 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class UserAddServiceImpl extends ServiceImpl<UserAddMapper, UserAdd> implements UserAddService {

    @Resource
    private DozerUtils dozer;

    @Resource
    private FileService fileService;

    @Resource
    private YouxinCommonProperties youxinCommonProperties;

    @Override
    public List<UserAddVo> listAddUsers(String userNo) throws SystemException {
        List<UserAdd> list = this.baseMapper.selectList(new LambdaQueryWrapper<UserAdd>().eq(UserAdd::getAddUserNo, userNo).eq(UserAdd::getAddStatus, 0));
        List<UserAddVo> userAddVos = list.stream().map(item -> {
            UserAddVo vo = dozer.map(item, UserAddVo.class);
            vo.setAuditType(1);
            vo.setAvatar(youxinCommonProperties.getDownloadUrl() + item.getAvatar());
            vo.setAddAvatar(youxinCommonProperties.getDownloadUrl() + item.getAddAvatar());
            return vo;
        }).collect(Collectors.toList());
        return userAddVos;
    }

    @Override
    public void emptyList(String userNo) throws SystemException {
        this.baseMapper.emptyList(userNo);
    }
}
