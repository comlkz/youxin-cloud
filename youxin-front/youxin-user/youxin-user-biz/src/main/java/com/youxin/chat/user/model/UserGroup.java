package com.youxin.chat.user.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

public class UserGroup {

    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    private String groupNo;//群组ID

    private Integer isTop;//是否置顶 0：不置顶  1：置顶

    private Integer notDisturb;//免打扰 0：否 1：是

    private String userNo;//用户编码

    private String backgroundImg;//背景图片

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Integer isAddressList;//是否添加到通讯录0：否 1：是

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id ;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public Integer getNotDisturb() {
        return notDisturb;
    }

    public void setNotDisturb(Integer notDisturb) {
        this.notDisturb = notDisturb;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo == null ? null : userNo.trim();
    }

    public String getBackgroundImg() {
        return backgroundImg;
    }

    public void setBackgroundImg(String backgroundImg) {
        this.backgroundImg = backgroundImg == null ? null : backgroundImg.trim();
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIsAddressList() {
        return isAddressList;
    }

    public void setIsAddressList(Integer isAddressList) {
        this.isAddressList = isAddressList;
    }
}
