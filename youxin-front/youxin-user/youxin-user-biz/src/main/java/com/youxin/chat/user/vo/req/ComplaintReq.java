package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class ComplaintReq implements Serializable {
    @ApiModelProperty(value = "举报投诉内容",required = true)
    private String complaint;
    @ApiModelProperty(value = "截图的ID，用英文逗号分隔",required = true)
    private String pictureId;
    @ApiModelProperty(value = "投诉类型 0：发布色情 1：存在反动 2：欺诈骗钱 3：赌博行为",required = true)
    private Integer type;
    @ApiModelProperty(value = "被投诉的id（群id、人id）",required = true)
    private String complaintId;

    public String getComplaint() {
        return complaint;
    }

    public void setComplaint(String complaint) {
        this.complaint = complaint;
    }

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getComplaintId() {
        return complaintId;
    }

    public void setComplaintId(String complaintId) {
        this.complaintId = complaintId;
    }
}
