package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "登录密码参数")
public class LoginPassReq implements Serializable {

    @ApiModelProperty(value = "新密码",required = true)
    //@Pattern(regexp = "",message = "密码格式不正确")
    private String password;

    @ApiModelProperty(value = "原始密码",required = true)
    private String oldPassword;

    private String code;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
