package com.youxin.chat.user.manager.message;

import io.rong.messages.BaseMessage;
import io.rong.util.GsonUtil;

import java.util.Map;

public class FriendExtMessage extends BaseMessage {
    //操作人用户 Id
    private String operatorUserId;

    //操作名称
    private String operation;

    //群组中各种通知的操作数据
    private Map<String, Object> data;

    private String message;

    private String extra;

    private transient static final String TYPE = "RC:InfoNtf";

    public FriendExtMessage(String operatorUserId, String message, String extra, String operation) {
        this.operatorUserId = operatorUserId;
        this.message = message;
        this.extra = extra;
        this.operation = operation;
    }

    @Override
    public String getType() {
        return "RC:InfoNtf";
    }

    @Override
    public String toString() {
        return GsonUtil.toJson(this, FriendExtMessage.class);
    }

    public String getOperatorUserId() {
        return operatorUserId;
    }

    public void setOperatorUserId(String operatorUserId) {
        this.operatorUserId = operatorUserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public static String getTYPE() {
        return TYPE;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
