package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.youxin.chat.user.model.ActiveUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActiveUserMapper extends BaseMapper<ActiveUser> {

}
