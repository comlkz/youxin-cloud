package com.youxin.chat.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.chat.user.mapper.ChatFeedbackMapper;
import com.youxin.chat.user.model.ChatFeedback;
import com.youxin.chat.user.service.FeedbackService;
import com.youxin.chat.user.vo.req.FeedbackReq;
import com.youxin.dozer.DozerUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * description: FeedbackServiceImpl <br>
 * date: 2020/3/10 10:16 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class FeedbackServiceImpl extends ServiceImpl<ChatFeedbackMapper, ChatFeedback> implements FeedbackService {

    @Resource
    private DozerUtils dozerUtils;

    @Override
    public void setFeedback(String userNo,FeedbackReq feedbackReq) {
        ChatFeedback chatFeedback = dozerUtils.map(feedbackReq,ChatFeedback.class);
        chatFeedback.setUserNo(userNo);
        this.baseMapper.insert(chatFeedback);
    }
}
