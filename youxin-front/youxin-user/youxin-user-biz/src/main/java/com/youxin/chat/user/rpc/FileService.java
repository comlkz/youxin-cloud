package com.youxin.chat.user.rpc;

import com.youxin.chat.basic.client.IFileClient;
import com.youxin.chat.basic.dto.file.FileDto;
import com.youxin.chat.basic.dto.file.SysFileDto;
import com.youxin.common.config.YouxinCommonProperties;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * description: FileService <br>
 * date: 2020/2/25 13:05 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class FileService {

    @Reference
    private IFileClient fileClient;

    @Resource
    private YouxinCommonProperties youxinCommonProperties;

    public String getImageUrl(int type,String str){
        return youxinCommonProperties.getDownloadUrl() + str;
    }

    public String getFileUrl(){
        return fileClient.getFileUrl();
    }

    public String primaryId(String prefix){
        return fileClient.primaryId(prefix);
    }

    public void saveFile(SysFileDto sysFileDto){
        fileClient.saveFile(sysFileDto);
    }

    public List<SysFileDto> getFilesByIds( List<String> fileIds){
        return fileClient.getFilesByIds(fileIds);
    }

    public String getRealPath(){
        return youxinCommonProperties.getFilePath();
    }

    public String add(FileDto fileDto, Integer type) {
        return fileClient.add(fileDto, type);
    }

    public void removeFile(String fileId){
         fileClient.removeFile(fileId);
    }

}
