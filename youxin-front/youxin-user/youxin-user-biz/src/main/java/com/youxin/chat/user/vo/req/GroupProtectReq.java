package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class GroupProtectReq implements Serializable {
    @ApiModelProperty(value = "群聊ID",required = true)
    private String groupId;
    @ApiModelProperty(value = "保护模式0：开启，1：关闭",required = true)
    private Integer protect;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getProtect() {
        return protect;
    }

    public void setProtect(Integer protect) {
        this.protect = protect;
    }
}
