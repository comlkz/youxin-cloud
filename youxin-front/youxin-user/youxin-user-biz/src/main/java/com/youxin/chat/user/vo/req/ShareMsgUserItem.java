package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class ShareMsgUserItem implements Serializable {

    @ApiModelProperty(value = "用户或群组编码")
    private String account;

    @ApiModelProperty(value = "类型 0：用户 1：群组")
    private Integer type;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}
