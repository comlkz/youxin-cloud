package com.youxin.chat.user.service.impl;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.base.BaseResultCode;
import com.youxin.base.MsgDto;
import com.youxin.chat.basic.dto.constant.FileTypeEnum;
import com.youxin.chat.user.enums.GroupStatusEnum;
import com.youxin.chat.user.manager.UserManager;
import com.youxin.common.config.YouxinCommonProperties;
import com.youxin.common.constant.MsgTypeEnum;
import com.youxin.common.constant.RedisKey;
import com.youxin.chat.basic.dto.file.SysFileDto;
import com.youxin.chat.user.convert.GroupConvert;
import com.youxin.chat.user.convert.UserInfoConvert;
import com.youxin.chat.user.enums.GroupMemberTypeEnum;
import com.youxin.chat.user.manager.GroupManager;
import com.youxin.chat.user.manager.ImManager;
import com.youxin.chat.user.mapper.*;
import com.youxin.chat.user.model.*;
import com.youxin.chat.user.rpc.FileService;
import com.youxin.chat.user.service.GroupService;
import com.youxin.chat.user.vo.group.*;
import com.youxin.chat.user.vo.req.*;
import com.youxin.chat.user.vo.user.UserDetailVo;
import com.youxin.exception.SystemException;
import com.youxin.chat.user.context.ImageUtil;
import com.youxin.qiniu.QiniuUtil;
import com.youxin.utils.RandomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.youxin.common.constant.MqConstant.TOPIC_EXCHANGE;

/**
 * description: GroupServiceImpl <br>
 * date: 2020/2/27 14:20 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class GroupServiceImpl extends ServiceImpl<GroupMapper, Group> implements GroupService {

    private static final Logger logger = LoggerFactory.getLogger(GroupServiceImpl.class);

    @Resource
    private GroupMapper groupMapper;

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private GroupCodeMapper groupCodeMapper;

    @Resource
    private GroupMemberMapper groupMemberMapper;

    @Resource
    private GroupAddMapper groupAddMapper;

    @Resource
    private GroupManager groupManager;

    @Resource
    private ImManager imManager;

    @Resource
    private UserGroupMapper userGroupMapper;

    @Resource
    private GroupQuitMapper groupQuitMapper;

    @Resource
    private GroupBlacklistMapper groupBlacklistMapper;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private AmqpTemplate amqpTemplate;

    @Resource
    private FileService fileService;

    @Resource
    private UserManager userManager;

    @Resource
    private YouxinCommonProperties youxinCommonProperties;

    private ExecutorService executor = new ThreadPoolExecutor(10, 10,
            60L, TimeUnit.SECONDS,
            new ArrayBlockingQueue(10));

    @Override
    public GroupDetailVo addGroup(String userNo, GroupAddReq groupAddDto) throws SystemException {
        StringBuilder sb = new StringBuilder();
        String groupNo = generateGroupNo();
        UserInfo selfInfo = userManager.selectByUserNo(userNo);


        for (String item : groupAddDto.getUserNos()) {
            UserInfo userInfo = userManager.selectByUserNo(item);
            if (userInfo == null) {
                continue;
            }
            sb.append(userInfo.getNickName());
        }
        String groupName = sb.length() > 15 ? sb.substring(0, 15).toString() : sb.toString();
        String fileId = fileService.primaryId("F");
        Group group = new Group();
        group.setGroupName(groupName);
        group.setGroupNo(groupNo);
        group.setGroupOwner(selfInfo.getUserNo());
        group.setCreateUser(userNo);
        group.setCreateTime(LocalDateTime.now());
        group.setUpdateTime(LocalDateTime.now());
        group.setGroupAvatar(fileId);
        imManager.createGroup(groupNo, groupName, groupAddDto.getUserNos());
        this.baseMapper.insert(group);
        joinGroup(userNo, groupNo, groupAddDto.getUserNos(), 0);
        GroupDetailVo resp = new GroupDetailVo();
        resp.setGroupNo(groupNo);
        resp.setGroupName(group.getGroupName());
        resp.setGroupAvatar(youxinCommonProperties.getDownloadUrl()  + fileId);
        return resp;
    }

    @Override
    public void joinGroup(String userNo, String groupNo, List<String> userList, int type) throws SystemException {
        Group group = this.baseMapper.getByGroupNo(groupNo);
        if (group == null) {
            logger.error("待加入的群组不存在,groupNo={}", groupNo);
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群聊不存在");
        }
        joinGroupByInvite(group, userList, userNo);


    }

    @Override
    public int joinGroup(String userNo, GroupAddReq groupAddDto) throws SystemException {
        if (StringUtils.isEmpty(groupAddDto.getGroupCode()) && StringUtils.isEmpty(groupAddDto.getGroupNo())) {
            logger.error("传入参数不完整 ,groupAddDto={}", groupAddDto);
            throw new SystemException(BaseResultCode.COMMON_FAIL, "参数不完整");
        }

        int type = 0;
        String groupNo = groupAddDto.getGroupNo();
        if (!StringUtils.isEmpty(groupAddDto.getGroupCode())) {
            GroupCode groupCode = groupCodeMapper.getByGroupCode(groupAddDto.getGroupCode());
            if (groupCode == null) {
                logger.error("群聊不存在 ,groupAddDto={}", groupAddDto);
                throw new SystemException(BaseResultCode.COMMON_FAIL, "群聊不存在");
            }
            type = 1;
            groupNo = groupCode.getGroupNo();
        }
        Group group = this.baseMapper.getByGroupNo(groupNo);
        if (group == null) {
            logger.error("待加入的群组不存在,groupNo={}", groupNo);
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群聊不存在");
        }
        if (type != 0) {
            int result = joinGroupByScanCode(group, userNo, type);
            return result;
        } else {
            joinGroupByInvite(group, groupAddDto.getUserNos(), userNo);
            return 1;
        }

    }

    @Override
    public void quitGroup(QuitGroupReq quitGroupDto, String userNo) throws SystemException {
        Group group = this.baseMapper.getByGroupNo(quitGroupDto.getGroupNo());
        if (group == null) {
            logger.error("待加入的群组不存在,groupNo={}", quitGroupDto.getGroupNo());
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群聊不存在");
        }
        if (quitGroupDto.getType() == 1 && !groupManager.checkOperate(userNo, quitGroupDto.getGroupNo())) {
            logger.error("非管理员无法操作,groupNo={},userNo={}", quitGroupDto.getGroupNo(), userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "抱歉，您没有操作权限");
        }
        if (!CollectionUtils.isEmpty(quitGroupDto.getUserNos()) && quitGroupDto.getUserNos().contains(group.getGroupOwner())) {
            logger.error("群主只能解散群组，无法退出,groupNo={},userNo={}", quitGroupDto.getGroupNo(), userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "操作非法");
        }
        if (quitGroupDto.getType() == 0) {
            quitGroupDto.setUserNos(Arrays.asList(userNo));
        }
        imManager.quitGroup(quitGroupDto.getGroupNo(), quitGroupDto.getUserNos());
        groupManager.removeMember(quitGroupDto.getGroupNo(), quitGroupDto.getUserNos());
        addGroupQuit(quitGroupDto);
        Map<String, Object> msgMap = new HashMap<>();

        if (quitGroupDto.getType() == 1) {
            msgMap.put("groupNo", quitGroupDto.getGroupNo());
            msgMap.put("applyUserNo", userNo);
            msgMap.put("kickUserNo", quitGroupDto.getUserNos());
            amqpTemplate.convertAndSend(TOPIC_EXCHANGE, "msg.user", new MsgDto(MsgTypeEnum.KICK_GROUP.getType(), msgMap));
        } else {
            msgMap.put("groupNo", quitGroupDto.getGroupNo());
            msgMap.put("userNo", userNo);
            amqpTemplate.convertAndSend(TOPIC_EXCHANGE, "msg.user", new MsgDto(MsgTypeEnum.QUIT_GROUP.getType(), msgMap));
        }
        executor.execute(new CreateGroupAvatarRunner(quitGroupDto.getGroupNo(), youxinCommonProperties.getDownloadUrl()));

    }

    @Override
    public void dismissGroup(String groupNo, String userNo) throws SystemException {
        Group group = this.baseMapper.getByGroupNo(groupNo);
        if (group == null) {
            logger.error("待加入的群组不存在,groupNo={}", groupNo);
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群聊不存在");
        }
        if (!group.getGroupOwner().equalsIgnoreCase(userNo)) {
            logger.error("非管理员无法操作,groupNo={},userNo={}", groupNo, userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "非群主无法操作");
        }
        //chatThirdService.dismissGroup(groupNo, userNo);
        groupManager.removeGroup(groupNo);
        Map<String, Object> msgMap = new HashMap<>();
        msgMap.put("groupNo", groupNo);
        msgMap.put("userNo", userNo);
        amqpTemplate.convertAndSend(TOPIC_EXCHANGE, "msg.user", new MsgDto(MsgTypeEnum.MISS_GROUP.getType(), msgMap));
    }

    @Override
    public List<GroupMemberVo> listGroupMembers(String userNo, String groupNo) throws SystemException {
        List<GroupMemberVo> list = groupMemberMapper.selectByGroupNo(groupNo);
        list.stream().forEach(item -> {
            item.setAvatar(fileService.getImageUrl(FileTypeEnum.USER_AVATAR.getType(), item.getAvatar()));
        });
        Optional<GroupMemberVo> optional = list.stream().filter(item -> item.getGroupUser().equalsIgnoreCase(userNo)).findAny();
        if (!optional.isPresent()) {
            logger.error("当前用户不在群组成员中，无法查看群组信息,userNo={},groupNo={}", userNo, groupNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "您不是群级成员，无法查看群组信息");
        }

        Comparator<GroupMemberVo> c1 = new Comparator<GroupMemberVo>() {
            @Override
            public int compare(GroupMemberVo o1, GroupMemberVo o2) {
                Integer sort1 = GroupMemberTypeEnum.forType((int) o1.getUserType()).getOrdered();
                Integer sort2 = GroupMemberTypeEnum.forType((int) o2.getUserType()).getOrdered();

                return sort1 - sort2;
            }
        };
        Collections.sort(list, c1);

        return list;
    }

    @Override
    public GroupDetailVo acquireGroupInfo(String userNo, String groupNo) throws SystemException {
        Group group = this.baseMapper.getByGroupNo(groupNo);
        if (group == null) {
            logger.error("群组信息不存在,userNo={},groupNo={}", userNo, groupNo);
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群组不存在");
        }
        UserGroup userGroup = userGroupMapper.selectByGroupNo(groupNo, userNo);
        if (userGroup == null) {
            logger.error("用户不存群组中,userNo={},groupNo={}", userNo, groupNo);
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "用户未加入群组");
        }
        List<GroupMemberVo> list = groupMemberMapper.selectByGroupNo(groupNo);
        list.stream().forEach(item -> {
            item.setAvatar(fileService.getImageUrl(FileTypeEnum.USER_AVATAR.getType(), item.getAvatar()));
        });
        Optional<GroupMemberVo> optional = list.stream().filter(item -> item.getGroupUser().equalsIgnoreCase(userNo)).findAny();
        if (!optional.isPresent()) {
            logger.error("当前用户不在群组成员中，无法查看群组信息,userNo={},groupNo={}", userNo, groupNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "您不是群级成员，无法查看群组信息");
        }
        Comparator<GroupMemberVo> c1 = new Comparator<GroupMemberVo>() {
            @Override
            public int compare(GroupMemberVo o1, GroupMemberVo o2) {
                Integer sort1 = GroupMemberTypeEnum.forType((int) o1.getUserType()).getOrdered();
                Integer sort2 = GroupMemberTypeEnum.forType((int) o2.getUserType()).getOrdered();

                return sort1 - sort2;
            }
        };
        Collections.sort(list, c1);
        GroupCode groupCode = groupCodeMapper.selectOne(new LambdaQueryWrapper<GroupCode>().eq(GroupCode::getGroupNo, groupNo));
        Optional<GroupMemberVo> adminOptional = list.stream().filter(item -> item.getUserType() == (byte) 1).findAny();
        GroupDetailVo resp = GroupConvert.buildDetailResp(group, userGroup);
        resp.setAdminNickName(adminOptional.map(GroupMemberVo::getGroupNickName).orElse(""));
        resp.setAdminNo(adminOptional.map(GroupMemberVo::getGroupUser).orElse(""));
        resp.setNickName(optional.get().getGroupNickName());
        resp.setGroupMembers(list);
        String code = Optional.ofNullable(groupCode).map(GroupCode::getGroupCode).orElse(null);
        if (group.getQrStatus() == 0 && StringUtils.isEmpty(code)) {
            String str = group.getGroupNo() + System.currentTimeMillis() + RandomUtil.randomStr(5);
            code = Base64.getEncoder().encodeToString(str.getBytes());
            groupCode = new GroupCode();
            groupCode.setGroupCode(code);
            groupCode.setGroupNo(group.getGroupNo());
            groupCode.setCreateUser(userNo);
            groupCode.setCreateTime(LocalDateTime.now());
            groupCodeMapper.insert(groupCode);
        }
        resp.setGroupCode(code);
        resp.setGroupAvatar(fileService.getImageUrl(FileTypeEnum.GROUOP_AVATAR.getType(), group.getGroupAvatar()));
        resp.setMemberNum(list.size());
        return resp;
    }

    @Override
    public GroupBasicVo getBasicGroupInfo(String userNo, String groupNo) throws SystemException {
        Group group = this.baseMapper.getByGroupNo(groupNo);
        if (group == null) {
            logger.error("群组信息不存在,userNo={},groupNo={}", userNo, groupNo);
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群组不存在");
        }
        GroupBasicVo resp = GroupConvert.buildBasicResp(group);
        resp.setGroupAvatar(fileService.getImageUrl(FileTypeEnum.GROUOP_AVATAR.getType(), group.getGroupAvatar()));
        List<GroupMemberVo> list = groupMemberMapper.selectByGroupNo(groupNo);
        Optional<GroupMemberVo> optional = list.stream().filter(item -> item.getGroupUser().equalsIgnoreCase(userNo)).findAny();
        if (optional.isPresent()) {
            resp.setJoinInGroup(1);
        } else {
            resp.setJoinInGroup(0);
        }
        return resp;
    }

    @Override
    public GroupByCodeVo getGroupByGroupCode(String groupCode, String userNo) {
        GroupCode groupCodeInfo = groupCodeMapper.getByGroupCode(groupCode);
        if (groupCodeInfo == null) {
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群组不存在");
        }
        Group group = this.baseMapper.getByGroupNo(groupCodeInfo.getGroupNo());
        if (group == null) {
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "该群组不存在");
        }
        GroupByCodeVo groupView = new GroupByCodeVo();
        groupView.setGroupAvatar(fileService.getImageUrl(0, group.getGroupAvatar()));
        groupView.setGroupName(group.getGroupName());
        groupView.setGroupNo(group.getGroupNo());
        int num = groupMemberMapper.selectCount(new LambdaQueryWrapper<GroupMember>().eq(GroupMember::getGroupNo, group.getGroupNo()));
        GroupMember groupMember = groupMemberMapper.selectByGroupNoAndUserNo(group.getGroupNo(), userNo);
        if (groupMember == null) {
            groupView.setHasJoined(0);
        } else {
            groupView.setHasJoined(1);
        }
        groupView.setUserNums(num);
        return groupView;
    }

    @Override
    public void updateGroupNotice(String userNo, GroupNoticeReq groupNoticeDto) throws SystemException {
        boolean flag = groupManager.checkOperate(userNo, groupNoticeDto.getGroupNo());
        if (!flag) {
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "普通用户无法设置群公告");
        }
        Group group = new Group();
        group.setGroupNo(groupNoticeDto.getGroupNo());
        group.setGroupNotice(groupNoticeDto.getGroupNotice());
        this.baseMapper.updateGroupNotice(group);
    }

    @Override
    public void transferMaster(String userNo, String oldUserNo, String groupNo) throws SystemException {
        Group group = groupMapper.getByGroupNo(groupNo);
        if (group == null) {
            logger.error("群组信息不存在,userNo={},groupNo={}", userNo, groupNo);
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群组不存在");
        }
        if (!oldUserNo.equalsIgnoreCase(group.getGroupOwner())) {
            logger.error("该用户不是群主，无法进行转让操作");
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }
        GroupMember groupMember = groupMemberMapper.selectByGroupNoAndUserNo(groupNo, userNo);
        if (!Optional.ofNullable(groupMember).isPresent()) {
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "用户不在群组内");
        }
        groupManager.changeGroupMaster(groupNo, userNo);
        imManager.addBannedWhitelist(groupNo, Arrays.asList(userNo));
        imManager.removeBannedWhitelist(groupNo, Arrays.asList(oldUserNo));

    }

    @Override
    public void setGroupManager(String userNo, String masterNo, String groupNo) throws SystemException {
        Group group = this.baseMapper.getByGroupNo(groupNo);
        if (group == null) {
            logger.error("群组信息不存在,userNo={},groupNo={}", userNo, groupNo);
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群组不存在");
        }
        if (!masterNo.equalsIgnoreCase(group.getGroupOwner())) {
            logger.error("该用户不是群主，无法进行转让操作");
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }
        if (userNo.equalsIgnoreCase(masterNo)) {
            logger.error("用户是群组，无法设置管理员");
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "群主不能设置为管理员");
        }
        groupMemberMapper.updateGroupUserType(groupNo, Arrays.asList(userNo), GroupMemberTypeEnum.GROUP_MENAGER.getType());
        imManager.addBannedWhitelist(groupNo, Arrays.asList(userNo));
    }

    @Override
    public void removeGroupManager(String userNo, String masterNo, String groupNo) throws SystemException {
        Group group = this.baseMapper.getByGroupNo(groupNo);
        if (group == null) {
            logger.error("群组信息不存在,userNo={},groupNo={}", userNo, groupNo);
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群组不存在");
        }
        if (!masterNo.equalsIgnoreCase(group.getGroupOwner())) {
            logger.error("该用户不是群主，无法进行转让操作");
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }
        if (userNo.equalsIgnoreCase(masterNo)) {
            logger.error("用户是群组，无法设置管理员");
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "群主不能设置为管理员");
        }
        groupMemberMapper.updateGroupUserType(groupNo, Arrays.asList(userNo), GroupMemberTypeEnum.NORMAL.getType());
        imManager.removeBannedWhitelist(groupNo, Arrays.asList(userNo));
    }

    @Override
    public List<GroupMemberVo> listGroupManagers(String groupNo) throws SystemException {
        List<GroupMemberVo> list = groupMemberMapper.listGroupManagers(groupNo);
        return list;
    }

    @Override
    public void setBanned(GroupBannedReq bannedDto, String userNo) throws SystemException {
        if (!groupManager.checkOperate(userNo, bannedDto.getGroupId())) {
            logger.error("非管理员无法操作,groupNo={},userNo={}", bannedDto.getGroupId(), userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "抱歉，您没有操作权限");
        }
        Group group = this.baseMapper.getByGroupNo(bannedDto.getGroupId());
        group.setBanned(bannedDto.getBanned());
        this.baseMapper.updateById(group);
        String msg;
        if (bannedDto.getBanned() == 1) {
            List<String> managerList = groupMemberMapper.getManagerList(bannedDto.getGroupId());
            imManager.setBanned(bannedDto.getGroupId());
            imManager.addBannedWhitelist(bannedDto.getGroupId(), managerList);
            msg = "本群已开启禁言模式";
        } else {
            imManager.setBannedOpen(bannedDto.getGroupId());
            msg = "本群已关闭禁言模式";

        }
        imManager.sendGroupNoticeMsg(userNo, bannedDto.getGroupId(), userNo, msg, "banned", "");
    }

    @Override
    public void setGroupProtect(GroupProtectReq protectDto, String userNo) throws SystemException {
        if (!groupManager.checkOperate(userNo, protectDto.getGroupId())) {
            logger.error("普通成员无法操作，userNo={}", userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }
        Group group = this.baseMapper.getByGroupNo(protectDto.getGroupId());
        if (protectDto.getProtect() != null) {
            group.setProtectedMode(protectDto.getProtect());
        } else {
            throw new SystemException(BaseResultCode.COMMON_FAIL, "参数出错");
        }
        this.baseMapper.updateById(group);
    }

    @Override
    public void setQRStatus(GroupQRStatusReq qrStatusDto, String userNo) throws SystemException {
        if (!groupManager.checkOperate(userNo, qrStatusDto.getGroupId())) {
            logger.error("普通成员无法操作，userNo={}", userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }
        Group group = this.baseMapper.getByGroupNo(qrStatusDto.getGroupId());
        if (group.getQrStatus().equals(qrStatusDto.getQrStatus())) {
            return;
        }
        if (qrStatusDto.getQrStatus() != null) {
            group.setQrStatus(qrStatusDto.getQrStatus());
        } else {
            throw new SystemException(BaseResultCode.COMMON_FAIL, "参数出错");
        }
        //开启
        if (qrStatusDto.getQrStatus() == 0) {
            String str = group.getGroupNo() + System.currentTimeMillis() + RandomUtil.randomStr(5);
            String code = Base64.getEncoder().encodeToString(str.getBytes());
            GroupCode groupCode = new GroupCode();
            groupCode.setGroupCode(code);
            groupCode.setGroupNo(group.getGroupNo());
            groupCode.setCreateUser(userNo);
            groupCode.setCreateTime(LocalDateTime.now());
            groupCodeMapper.insert(groupCode);
        } else {
            groupCodeMapper.deleteByGroupNo(group.getGroupNo());
        }
        this.baseMapper.updateById(group);
    }

    @Override
    public void setNicknameProtect(GroupNicknameProtectReq nicknameProtectDto, String managerId) throws SystemException {
        if (!groupManager.checkOperate(managerId, nicknameProtectDto.getGroupId())) {
            logger.error("普通成员无法操作，userNo={}", managerId);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }
        Group group = this.baseMapper.getByGroupNo(nicknameProtectDto.getGroupId());
        if (nicknameProtectDto.getNicknameProtect() != null) {
            group.setNicknameProtect(nicknameProtectDto.getNicknameProtect());
        } else {
            throw new SystemException(BaseResultCode.COMMON_FAIL, "参数出错");
        }
        this.baseMapper.updateById(group);

        Map<String, Object> msgMap = new HashMap<>();
        msgMap.put("groupId", nicknameProtectDto.getGroupId());
        msgMap.put("nicknameProtect", String.valueOf(nicknameProtectDto.getNicknameProtect()));
        msgMap.put("userNo", managerId);

        amqpTemplate.convertAndSend(TOPIC_EXCHANGE, "msg.user", new MsgDto(MsgTypeEnum.NICKNAME_PROTECT.getType(), msgMap));

    }

    @Override
    public void setGroupAudit(GroupAuditReq groupAuditDto, String userNo) throws SystemException {
        if (!groupManager.checkOperate(userNo, groupAuditDto.getGroupId())) {
            logger.error("普通成员无法操作，userNo={}", userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }
        Group group = this.baseMapper.getByGroupNo(groupAuditDto.getGroupId());
        if (groupAuditDto.getAudit() != null) {
            group.setAudit(groupAuditDto.getAudit());
        } else {
            throw new SystemException(BaseResultCode.COMMON_FAIL, "参数出错");
        }
        this.baseMapper.updateById(group);
    }

    @Override
    public List<GroupAddListVo> listGroupAdd(String groupNo, String userNo) throws SystemException {
        if (!groupManager.checkOperate(userNo, groupNo)) {
            logger.error("普通成员无法操作，userNo={}", userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }
        List<GroupAddListVo> groupAddDtoList = groupAddMapper.listGroupAdd(groupNo);
        groupAddDtoList.stream().forEach(item -> {
            item.setAvatar(fileService.getImageUrl(0, item.getAvatar()));
            item.setAddAvatar(fileService.getImageUrl(0, item.getAddAvatar()));
        });
        this.baseMapper.updateAppendStatus(groupNo, 0);
        return groupAddDtoList;
    }

    @Override
    public void handleGroupAdd(String userNo, String auditUserNo, String auditGroupNo, int action) throws SystemException {
        if (!groupManager.checkOperate(userNo, auditGroupNo)) {
            logger.error("普通成员无法操作，userNo={}", userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }
        GroupAdd groupAdd = groupAddMapper.selectOne(new LambdaQueryWrapper<GroupAdd>().eq(GroupAdd::getAddGroupNo, auditGroupNo).eq(GroupAdd::getUserNo, auditUserNo).eq(GroupAdd::getAddStatus, 0));
        groupAdd.setAddStatus(action);
        groupAddMapper.updateById(groupAdd);

        if (action == 1) {
            Group group = this.baseMapper.getByGroupNo(auditGroupNo);
            int count = groupMemberMapper.selectCount(new LambdaQueryWrapper<GroupMember>().eq(GroupMember::getGroupNo, group.getGroupNo())
                    .eq(GroupMember::getGroupUser, auditUserNo));
            if (count > 0) {
                return;
            }
            List<String> auditList = new ArrayList<>();
            auditList.add(auditUserNo);
            joinGroupSuccess(auditList, userNo, group);
        } else if (action == -1) {
            String msg = String.format("%s拒绝了您的加入请求", groupAdd.getAddNickName());
            imManager.sendPrivateMsg(imManager.getSystemNo(), auditUserNo, msg);
        }
    }

    @Override
    public List<GroupBlacklistVo> blacklistGroup(String groupNo, String userNo) throws SystemException {
        if (!groupManager.checkOperate(userNo, groupNo)) {
            logger.error("普通成员无法操作，userNo={}", userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }

        List<GroupBlacklistVo> list = groupBlacklistMapper.blacklistGroup(groupNo, userNo);
        list.stream().forEach(item ->
                item.setAvatar(fileService.getImageUrl(FileTypeEnum.USER_AVATAR.getType(), item.getAvatar()))
        );
        return list;
    }

    @Override
    public void blacklistGroupAdd(GroupBlackAddReq groupBlackAdd, String userNo) throws SystemException {
        if (!groupManager.checkOperate(userNo, groupBlackAdd.getGroupNo())) {
            logger.error("普通成员无法操作，userNo={}", userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }
        List<GroupBlacklist> list = new ArrayList<>();
        for (String addUserNo : groupBlackAdd.getUserNos()) {
            int count = groupBlacklistMapper.countByGroupNoAndUserNo(groupBlackAdd.getGroupNo(), userNo);
            if (count > 0) {
                continue;
            }
            UserInfo userInfo = userManager.selectByUserNo(addUserNo);
            GroupBlacklist groupBlacklist = new GroupBlacklist();
            groupBlacklist.setUserNo(addUserNo);
            groupBlacklist.setId(IdWorker.get32UUID());
            groupBlacklist.setGroupNo(groupBlackAdd.getGroupNo());
            groupBlacklist.setAvatar(userInfo.getAvatar());
            groupBlacklist.setNickName(userInfo.getNickName());
            groupBlacklist.setCreateTime(LocalDateTime.now());
            list.add(groupBlacklist);
        }

        groupBlacklistMapper.insertBatch(list);
    }

    @Override
    public void blacklistGroupDel(String groupNo, String rmUserNo, String userNo) throws SystemException {
        if (!groupManager.checkOperate(userNo, groupNo)) {
            logger.error("普通成员无法操作，userNo={}", userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }

        groupBlacklistMapper.deleteByGroupNoAndUserNo(groupNo, rmUserNo);
    }

    @Override
    public List<GroupQuitVo> quitGroupList(String groupNo, String userNo) throws SystemException {
        if (!groupManager.checkOperate(userNo, groupNo)) {
            logger.error("普通成员无法操作，userNo={}", userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "没有操作权限");
        }
        List<GroupQuitVo> list = groupQuitMapper.quitGroupList(groupNo);
        list.stream().forEach(item ->
                item.setAvatar(fileService.getImageUrl(FileTypeEnum.USER_AVATAR.getType(), item.getAvatar()))
        );

        return list;
    }

    @Override
    public void renameGroup(String groupNo, String groupName, String userNo) throws SystemException {
        Group group = this.baseMapper.getByGroupNo(groupNo);
        if (group == null) {
            logger.error("待加入的群组不存在,groupNo={}", groupNo);
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群聊不存在");
        }
        Integer userType = groupMemberMapper.selectUserType(userNo, groupNo);
        if (userType.equals(GroupMemberTypeEnum.NORMAL.getType())) {
            logger.error("非管理员无法操作,groupNo={},userNo={}", groupNo, userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "非管理员无法操作");
        }
        group.setGroupName(groupName);
        imManager.updateGroup(groupNo, groupName);
        this.baseMapper.updateById(group);
    }

    @Override
    public void setGroupExt(GroupAttrReq groupExt, String userNo) throws SystemException {
        boolean flag = groupManager.checkOperate(userNo, groupExt.getGroupNo());
        if (!flag) {
            logger.error("非管理员无法操作,groupNo={},userNo={}", groupExt.getGroupNo(), userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "抱歉，您没有操作权限");
        }
        Group group = this.baseMapper.getByGroupNo(groupExt.getGroupNo());
        if (group == null) {
            logger.error("群组不存在,groupNo={},userNo={}", groupExt.getGroupNo(), userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "群组不存在");
        }

        int clearTime = Optional.ofNullable(group.getExt()).map(JSONObject::parseObject).map(item -> item.getInteger("clearTime")).orElse(-1);
        int printScreenNotify = Optional.ofNullable(group.getExt()).map(JSONObject::parseObject).map(item -> item.getInteger("printScreenNotify")).orElse(0);

        JSONObject ext = Optional.ofNullable(group.getExt()).map(JSONObject::parseObject).orElse(new JSONObject());
        Map<String, Object> msgMap = new HashMap<>();
        msgMap.put("groupNo", groupExt.getGroupNo());
        msgMap.put("userNo", userNo);
        msgMap.put(groupExt.getExtName(), groupExt.getExtValue());
        if ("clearTime".equals(groupExt.getExtName())) {
            Integer value = Integer.parseInt(groupExt.getExtValue());
            if (value > -1 && printScreenNotify != 1) {
                ext.put("printScreenNotify", "1");
                msgMap.put("printScreenNotify", "1");
            }
            ext.put("clearTimePoint", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        } else if ("printScreenNotify".equals(groupExt.getExtName())) {
            if (clearTime != -1) {
                logger.error("开启了群消息定时清理功能，无法设置截屏通知,groupNo={},userNo={}", groupExt.getGroupNo(), userNo);
                throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "开启了群消息定时清理功能，无法设置截屏通知");
            }
        } else {
            throw new SystemException(BaseResultCode.COMMON_FAIL, "接收参数有误");
        }
        ext.put(groupExt.getExtName(), groupExt.getExtValue());

        group.setExt(ext.toJSONString());
        groupMapper.update(group, new LambdaQueryWrapper<Group>().eq(Group::getGroupNo, group.getGroupNo()));

        amqpTemplate.convertAndSend(TOPIC_EXCHANGE, "msg.user", new MsgDto(MsgTypeEnum.GROUP_EXT_CHANGE.getType(), msgMap));

    }

    @Override
    public void printScreenMsg(GroupPrintScreenReq printScreenMsgDto, String userNo) throws SystemException {
        String key = printScreenMsgDto.getToChatNo() + userNo;
        if (!StringUtils.isEmpty(printScreenMsgDto.getUid())) {
            key = printScreenMsgDto.getUid();
        }
        key = RedisKey.PRINT_SCREEN + key;
        if (stringRedisTemplate.opsForValue().get(key) != null) {
            return;
        }
        Map<String, Object> msgMap = new HashMap<>();
        msgMap.put("type", printScreenMsgDto.getType());
        msgMap.put("toChatNo", printScreenMsgDto.getToChatNo());
        msgMap.put("userNo", userNo);
        stringRedisTemplate.opsForValue().set(key, "1");
        stringRedisTemplate.expire(key, 30, TimeUnit.SECONDS);
        amqpTemplate.convertAndSend(TOPIC_EXCHANGE, "msg.user", new MsgDto(MsgTypeEnum.PRINT_SCREEN.getType(), msgMap));
    }

    @Override
    public List<UserDetailVo> groupUserActive(String userNo, String groupNo, int type) throws SystemException {
        boolean flag = groupManager.checkOperate(userNo, groupNo);
        if (!flag) {
            logger.error("非管理员无法操作,groupNo={},userNo={}", groupNo, userNo);
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "抱歉，您没有操作权限");
        }
        List<String> userNos = groupMemberMapper.getGroupMember(groupNo);
        if (type == 0) {
            LambdaQueryWrapper<UserInfo> day = new LambdaQueryWrapper<UserInfo>();
            day.in(UserInfo::getUserNo, userNos);
            day.gt(UserInfo::getLastUseTime, LocalDateTime.now().minusDays(7));
            day.lt(UserInfo::getLastUseTime, LocalDateTime.now().minusDays(3));

            List<UserInfo> days = userInfoMapper.selectList(day);
            List<UserDetailVo> threeDay = days.stream().map(item -> UserInfoConvert.modelToDto(item, fileService.getFileUrl())).collect(Collectors.toList());
            return threeDay;
        } else if (type == 1) {
            LambdaQueryWrapper<UserInfo> week = new LambdaQueryWrapper<UserInfo>();
            week.in(UserInfo::getUserNo, userNos);
            week.gt(UserInfo::getLastUseTime, LocalDateTime.now().minusMonths(1));
            week.lt(UserInfo::getLastUseTime, LocalDateTime.now().minusDays(7));
            List<UserInfo> weeks = userInfoMapper.selectList(week);
            List<UserDetailVo> oneWeek = weeks.stream().map(item -> UserInfoConvert.modelToDto(item, fileService.getFileUrl())).collect(Collectors.toList());
            return oneWeek;
        } else if (type == 2) {
            LambdaQueryWrapper<UserInfo> month = new LambdaQueryWrapper<UserInfo>();
            month.in(UserInfo::getUserNo, userNos);
            month.lt(UserInfo::getLastUseTime, LocalDateTime.now().minusMonths(1));
            List<UserInfo> months = userInfoMapper.selectList(month);
            List<UserDetailVo> oneMonth = months.stream().map(item -> UserInfoConvert.modelToDto(item, fileService.getFileUrl())).collect(Collectors.toList());
            return oneMonth;
        }
        return new ArrayList<>();
    }

    @Override
    public void sendGroupWarning(String groupNo) {
        Group group = this.baseMapper.getByGroupNo(groupNo);
        if (group == null) {
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群组不存在");
        }
        String groupUser = group.getGroupOwner();
        String groupNoticeMsg = "警告：该群存在违反尤信使用规范行为，请规范使用";
        String errMsg = String.format("您的群聊%s，于%s，存在违反尤信使用规范行为，已被系统警告，请及时规范，如仍存在，此群将被冻结", group.getGroupName(), LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy年MM月dd日HH时mm分")));
        imManager.sendGroupNoticeMsg(imManager.getSystemNo(), groupNo, imManager.getSystemNo(), groupNoticeMsg, "groupWarning", "");
        imManager.sendUserCustomMsg(imManager.getSystemNo(), Arrays.asList(groupUser), errMsg, "群警告通知");
    }

    @Override
    public void lockGroup(String groupNo) {
        Group group = this.baseMapper.getByGroupNo(groupNo);
        if (group == null) {
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "群组不存在");
        }
        group.setGroupStatus(GroupStatusEnum.LOCK.getStatus());
        this.baseMapper.updateById(group);
        String errMsg = String.format("您的群聊%s多次出现违反尤信使用规范行为，已被系统冻结", group.getGroupName());
        String groupNoticeMsg = "警告：该群多次出现违反尤信使用规范行为，已被系统冻结";
        imManager.sendUserCustomMsg(imManager.getSystemNo(), Arrays.asList(group.getGroupOwner()), errMsg, "群冻结通知");
        imManager.sendGroupNoticeMsg(imManager.getSystemNo(), groupNo, imManager.getSystemNo(), groupNoticeMsg, "groupLock", "");
        imManager.dismissGroup(group.getGroupNo(), imManager.getSystemNo());
    }

    private String generateGroupNo() {
        RedisAtomicLong entityIdCounter = new RedisAtomicLong(RedisKey.GROUP_NO_GENERATOR, stringRedisTemplate.getConnectionFactory());
        Long increment = entityIdCounter.getAndIncrement();
        String seq = String.format("%04d", increment);
        System.out.println(seq);
        if ((null == increment || increment.longValue() == 0)) {//初始设置过期时间
            long liveTime = Duration.between(LocalDateTime.now(), LocalDate.now().plusMonths(1).atStartOfDay()).getSeconds();
            entityIdCounter.expire(liveTime, TimeUnit.SECONDS);
        }
        String userNo = "G" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) + seq;
        return userNo;
    }

    /*
     * description: 群主或管理员邀请进群 <br>
     * version: 1.0 <br>
     * date: 2020/1/14 14:37 <br>
     * author: llkj <br>
     *
     * @param group 群组信息
     * @param userList 用户列表
     * @param userNo  用户编码
     * @return void
     */
    private void joinGroupByInvite(Group group, List<String> userList, String userNo) {
        if (CollectionUtils.isEmpty(userList)) {
            logger.error("传入参数不完整,用户信息未传 ,groupAddDto={}", userList);
            throw new SystemException(BaseResultCode.COMMON_FAIL, "参数不完整");
        }
        boolean flag = groupManager.checkOperate(userNo, group.getGroupNo());
        if (!flag) {
            throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "非群主或管理员不能操作");
        }

        List<String> filterList = new ArrayList<>();
        for (String item : userList) {
            int count = groupMemberMapper.countMemberByGroupNoAndUserNo(group.getGroupNo(), item);
            if (count == 0) {
                filterList.add(item);
            }
        }
        if (CollectionUtils.isEmpty(filterList)) {
            throw new SystemException(BaseResultCode.RECORD_EXISTS, "没有新添用户");
        }
        joinGroupSuccess(filterList, userNo, group);
    }

    /*
     * description: 扫码进群 <br>
     * version: 1.0 <br>
     * date: 2020/1/14 14:38 <br>
     * author: llkj <br>
     *
     * @param group 群组信息
     * @param groupAddDto
     * @param type 类型
     * @return void
     */
    private int joinGroupByScanCode(Group group, String userNo, Integer type) {
        int count = groupMemberMapper.countMemberByGroupNoAndUserNo(group.getGroupNo(), userNo);
        if (count > 0) {
            return 1;
        }
        if (group.getAudit() == 1) {
            count = groupAddMapper.selectCount(new LambdaQueryWrapper<GroupAdd>().eq(GroupAdd::getUserNo, userNo).eq(GroupAdd::getAddGroupNo, group.getGroupNo()).eq(GroupAdd::getAddStatus, 0));
            if (count > 0) {
                throw new SystemException(BaseResultCode.HAS_NO_PERMISSION, "已添加申请，等管理员审核");
            }

            UserInfo userInfo = userManager.selectByUserNo(userNo);
            GroupAdd groupAdd = new GroupAdd();
            groupAdd.setUserNo(userNo);
            groupAdd.setAddGroupNo(group.getGroupNo());
            groupAdd.setNickName(userInfo.getNickName());
            groupAdd.setAvatar(userInfo.getAvatar());
            groupAdd.setAddNickName(group.getGroupName());
            groupAdd.setAddAvatar(group.getGroupAvatar());
            groupAdd.setAddTime(LocalDateTime.now());
            groupAdd.setAddStatus(0);
            groupAdd.setAddSource(type);
            groupAddMapper.insert(groupAdd);
            this.baseMapper.updateAppendStatus(group.getGroupNo(), 1);
            Map<String, Object> msgMap = new HashMap<>();
            msgMap.put("groupNo", group.getGroupNo());
            msgMap.put("applyUserNo", userNo);
            msgMap.put("nickName", userInfo.getNickName());
            amqpTemplate.convertAndSend(TOPIC_EXCHANGE, "msg.user", new MsgDto(MsgTypeEnum.GROUP_AUDIT.getType(), msgMap));
            return 0;
        } else {
            List<String> userNos = new ArrayList<>();
            userNos.add(userNo);
            joinGroupSuccess(userNos, userNo, group);
            return 1;
        }
    }


    private void joinGroupSuccess(List<String> filterList, String userNo, Group group) {
        List<GroupMember> groupMembers = new ArrayList<>();
        List<UserGroup> userGroups = new ArrayList<>();
        for (String item : filterList) {
            UserInfo userInfo = userManager.selectByUserNo(item);
            GroupMember groupMember = new GroupMember();
            groupMember.setAvatar(userInfo.getAvatar());
            groupMember.setGroupNickName(userInfo.getNickName());
            if (item.equalsIgnoreCase(group.getGroupOwner())) {
                groupMember.setUserType((byte) 1);
            } else {
                groupMember.setUserType((byte) 0);
            }
            groupMember.setGroupNo(group.getGroupNo());
            groupMember.setGroupUser(userInfo.getUserNo());
            groupMember.setCreateTime(new Date());
            groupMember.setUpdateTime(new Date());
            groupMembers.add(groupMember);
            UserGroup userGroup = new UserGroup();
            userGroup.setGroupNo(group.getGroupNo());
            userGroup.setIsTop(0);
            userGroup.setNotDisturb(0);
            userGroup.setUserNo(userInfo.getUserNo());
            userGroup.setCreateTime(LocalDateTime.now());
            userGroup.setUpdateTime(LocalDateTime.now());
            userGroups.add(userGroup);
        }
        imManager.joinGroup(group.getGroupNo(), group.getGroupName(), filterList);
        groupManager.addGroupMembers(groupMembers, userGroups);
        Map<String, Object> msgMap = new HashMap<>();
        msgMap.put("applyUserNo", userNo);
        msgMap.put("members", groupMembers);
        amqpTemplate.convertAndSend(TOPIC_EXCHANGE, "msg.user", new MsgDto(MsgTypeEnum.JOIN_GROUP.getType(), msgMap));
        executor.execute(new CreateGroupAvatarRunner(group.getGroupNo(), youxinCommonProperties.getDownloadUrl()));

    }

    private void addGroupQuit(QuitGroupReq quitGroupDto) {
        if (0 == quitGroupDto.getType()) {
            for (String userNo : quitGroupDto.getUserNos()) {
                GroupQuit groupQuit = new GroupQuit();
                UserInfo userInfo = userManager.selectByUserNo(userNo);
                groupQuit.setGroupNo(quitGroupDto.getGroupNo());
                groupQuit.setUserNo(userNo);
                groupQuit.setAvatar(fileService.getImageUrl(FileTypeEnum.USER_AVATAR.getType(), userInfo.getAvatar()));
                groupQuit.setNickName(userInfo.getNickName());
                groupQuit.setCreateTime(LocalDateTime.now());
                groupQuit.setRemark("主动退出");
                groupQuitMapper.insert(groupQuit);
            }
        } else if (1 == quitGroupDto.getType()) {
            List<GroupQuit> list = new ArrayList<>();
            for (String userNo : quitGroupDto.getUserNos()) {
                GroupQuit groupQuit = new GroupQuit();
                UserInfo userInfo = userManager.selectByUserNo(userNo);
                groupQuit.setUserNo(userNo);
                groupQuit.setId(IdUtil.getSnowflake(1L, 1L).nextIdStr());
                groupQuit.setGroupNo(quitGroupDto.getGroupNo());
                groupQuit.setNickName(userInfo.getNickName());
                groupQuit.setAvatar(fileService.getImageUrl(FileTypeEnum.USER_AVATAR.getType(), userInfo.getAvatar()));
                groupQuit.setCreateTime(LocalDateTime.now());
                groupQuit.setRemark("被踢出");
                list.add(groupQuit);
            }
            groupQuitMapper.insertBatch(list);
        }

    }

    class CreateGroupAvatarRunner implements Runnable {


        private String groupNo;

        private String path;

        public CreateGroupAvatarRunner(String groupNo, String path) {
            this.groupNo = groupNo;
            this.path = path;
        }

        @Override
        public void run() {
            logger.info("开始生成群头像，groupNo={}", groupNo);
            List<GroupMemberVo> list = groupMemberMapper.selectByGroupNo(groupNo);
            if (CollectionUtils.isEmpty(list)) {
                return;
            }
            if (list.size() > 9) {
                list = list.subList(0, 9);
            }
            List<String> userAvatars = list.stream().filter(item -> !StringUtils.isEmpty(item.getAvatar())).map(item -> path + item.getAvatar()).collect(Collectors.toList());

            Group group = groupMapper.getByGroupNo(groupNo);

//            String fileId = fileService.primaryId("F");
//            if (!StringUtils.isEmpty(group.getGroupAvatar())) {
//                fileId = group.getGroupAvatar();
//            }
            String fileId = group.getGroupAvatar();
            String relFilePath = FileTypeEnum.GROUOP_AVATAR.getPath();
            try {
                byte[] bytes = ImageUtil.getCombinationOfHead(userAvatars);
                QiniuUtil.upload(bytes, fileId);
                if (StringUtils.isEmpty(group.getGroupAvatar())) {
                    add(fileId, relFilePath);
                }
                group.setGroupAvatar(fileId);
                group.setGroupNo(groupNo);
                groupMapper.update(group, new LambdaQueryWrapper<Group>().eq(Group::getGroupNo, groupNo));
                logger.info("完成生成群头像，groupNo={},avatar={}", groupNo, fileId);

            } catch (Exception e) {
                logger.error("生成群组图片失败,groupNo={}", groupNo, e);
            }
        }

        public String add(String fileId, String relFilePath) {

            SysFileDto record = new SysFileDto();
            record.setFileType(1);
            record.setStatus(1);
            record.setSuffix(".jpg");
            record.setFilePath(relFilePath);
            record.setFileName(fileId + ".jpg");
            record.setFileSize(1024L);
            record.setId(fileId);
            record.setVirtualId(record.getId());
            record.setUpdateTime(new Date());
            record.setCreateTime(new Date());
            record.setUseCount(1);
            fileService.saveFile(record);
            return record.getId();
        }
    }
}
