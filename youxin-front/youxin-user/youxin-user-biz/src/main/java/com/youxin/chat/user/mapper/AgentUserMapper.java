package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.user.model.AgentUser;

public interface AgentUserMapper extends BaseMapper<AgentUser> {
}
