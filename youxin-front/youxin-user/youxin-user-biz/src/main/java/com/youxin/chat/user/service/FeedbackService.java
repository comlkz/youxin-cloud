package com.youxin.chat.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.user.model.ChatFeedback;
import com.youxin.chat.user.vo.req.FeedbackReq;

/**
 * description: FeedbackService <br>
 * date: 2020/3/10 10:15 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface FeedbackService extends IService<ChatFeedback> {

    void setFeedback(String userNo,FeedbackReq feedbackReq);
}
