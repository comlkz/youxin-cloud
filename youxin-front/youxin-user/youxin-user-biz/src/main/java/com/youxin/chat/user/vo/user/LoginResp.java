package com.youxin.chat.user.vo.user;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class LoginResp implements Serializable {
    private static final long serialVersionUID = -5210854148759383414L;

    @ApiModelProperty(value = "IM token")
    private String imToken;

    @ApiModelProperty(value = "用户编码")
    private String userNo;

    @ApiModelProperty(value = "系统token")
    private String token;

    @ApiModelProperty(value = "是否完善用户信息 0：否 1：是")
    private Integer hasPerfect;

    public String getImToken() {
        return imToken;
    }

    public void setImToken(String imToken) {
        this.imToken = imToken;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getHasPerfect() {
        return hasPerfect;
    }

    public void setHasPerfect(Integer hasPerfect) {
        this.hasPerfect = hasPerfect;
    }
}
