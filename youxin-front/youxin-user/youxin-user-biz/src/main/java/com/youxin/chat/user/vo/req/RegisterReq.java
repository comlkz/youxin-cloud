package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@ApiModel(description = "注册请求参数")
public class RegisterReq implements Serializable {

    @NotEmpty(message = "手机号不能为空")
    @ApiModelProperty(value = "手机号",required = true)
    private String mobile;

    @NotEmpty(message = "登录密码不能为空")
    @ApiModelProperty(value = "登录密码",required = true)
    private String password;

    @NotEmpty(message = "参数不正确")
    @ApiModelProperty(value = "上步返回的token",required = true)
    private String token;

    @ApiModelProperty(value = "设备类型 0：android 1:IOS",required = true)
    private Integer deviceType;

    @ApiModelProperty(value = "设备标识",required = true)
    private String deviceId;

    @ApiModelProperty(value = "代理商编码",required = false)
    private String agentNo;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(String agentNo) {
        this.agentNo = agentNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "RegisterReq{" +
                "mobile='" + mobile + '\'' +
                ", token='" + token + '\'' +
                ", deviceType=" + deviceType +
                ", deviceId='" + deviceId + '\'' +
                ", agentNo='" + agentNo + '\'' +
                '}';
    }
}
