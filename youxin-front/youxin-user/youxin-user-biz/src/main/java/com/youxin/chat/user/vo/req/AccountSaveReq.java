package com.youxin.chat.user.vo.req;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

public class AccountSaveReq implements Serializable {

    @Pattern(regexp = "^[a-zA-Z][a-zA-Z0-9_]{5,20}$",message = "尤信号不符合规范")
    private String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
