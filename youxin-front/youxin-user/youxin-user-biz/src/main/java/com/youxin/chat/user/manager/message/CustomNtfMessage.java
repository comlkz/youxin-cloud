package com.youxin.chat.user.manager.message;

import io.rong.messages.BaseMessage;
import io.rong.util.GsonUtil;

/**
 * description: CustomNftMessage <br>
 * date: 2020/3/10 10:34 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class CustomNtfMessage extends BaseMessage {

    private String content;

    private String title;

    private String time;

    private transient static final String TYPE = "HR:CustomNtf";

    public CustomNtfMessage(String content, String title, String time) {
        this.content = content;
        this.title = title;
        this.time = time;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return GsonUtil.toJson(this, CustomNtfMessage.class);    }
}
