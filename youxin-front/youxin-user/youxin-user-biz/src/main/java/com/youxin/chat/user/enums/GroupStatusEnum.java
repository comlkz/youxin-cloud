package com.youxin.chat.user.enums;



/**
 * description: GroupStatusEnum <br>
 * date: 2020/3/10 10:44 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public enum GroupStatusEnum {

    NORMAL(0,"正常"),LOCK(-1,"冻结");
    private Integer status;

    private String desc;

     GroupStatusEnum(Integer status,String desc){
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
