package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class GroupNickNameReq implements Serializable {

    @ApiModelProperty(value = "群聊ID",required = true)
    private String groupNo;
    @ApiModelProperty(value = "群成员昵称",required = true)
    private String groupNickName;

    @ApiModelProperty(value = "用户编码",required = true)
    private String userNo;

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupNickName() {
        return groupNickName;
    }

    public void setGroupNickName(String groupNickName) {
        this.groupNickName = groupNickName;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }
}
