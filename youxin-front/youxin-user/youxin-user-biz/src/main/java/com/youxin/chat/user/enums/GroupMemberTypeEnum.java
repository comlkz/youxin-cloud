package com.youxin.chat.user.enums;

public enum GroupMemberTypeEnum {

    NORMAL(0, "普通用户",2),
    GROUP_OWNER(1, "群主",0),
    GROUP_MENAGER(2, "管理员",1);

    private Integer type;

    private String desc;

    private Integer ordered;

    GroupMemberTypeEnum(Integer type, String desc, Integer ordered) {
        this.type = type;
        this.desc = desc;
        this.ordered = ordered;
    }

    public Integer getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getOrdered() {
        return ordered;
    }

    public static GroupMemberTypeEnum forType(Integer type){
        for(GroupMemberTypeEnum item : GroupMemberTypeEnum.values()){
            if(item.getType().equals(type)) {
                return item;
            }
        }
        return null;
    }
}
