package com.youxin.chat.user.context;

public class UserContext {

    public static String getLoginSalt(String mobile){

        int endIndex = mobile.length();
        int beginIndex = 0;

        if (endIndex > 8) {
            beginIndex = endIndex - 8;
        }
        String salt = mobile.substring(beginIndex, endIndex);
        return salt;
    }

}
