package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.chat.user.model.ChatFeedback;
import org.apache.ibatis.annotations.Param;

public interface ChatFeedbackMapper extends BaseMapper<ChatFeedback> {

}
