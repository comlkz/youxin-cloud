package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

public class QuitGroupReq implements Serializable {

    @ApiModelProperty(value = "用户编码",required = true)
    private List<String> userNos;

    private String groupNo;

    @ApiModelProperty(value = "操作类型 0：主动退出，1：踢除")
    private Integer type;

    public List<String> getUserNos() {
        return userNos;
    }

    public void setUserNos(List<String> userNos) {
        this.userNos = userNos;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }
}
