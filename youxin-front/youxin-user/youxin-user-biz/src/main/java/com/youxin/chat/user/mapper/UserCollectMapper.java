package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.youxin.chat.user.model.UserCollect;
import com.youxin.chat.user.vo.req.CollectSearchReq;
import com.youxin.chat.user.vo.user.UserCollectVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserCollectMapper extends BaseMapper<UserCollect> {

   Long getUserCollectSize(@Param("userNo") String userNo);

   List<UserCollectVo> collectList(Page<UserCollect> page, @Param("record") CollectSearchReq search);

}
