package com.youxin.chat.user.manager;

import com.alibaba.fastjson.JSONObject;
import com.youxin.base.BaseResultCode;
import com.youxin.chat.user.manager.message.*;
import com.youxin.exception.SystemException;
import io.rong.RongCloud;
import io.rong.messages.GroupNotificationMessage;
import io.rong.messages.InfoNtfMessage;
import io.rong.messages.TxtMessage;
import io.rong.models.Result;
import io.rong.models.conversation.ConversationModel;
import io.rong.models.group.GroupMember;
import io.rong.models.group.GroupModel;
import io.rong.models.message.*;
import io.rong.models.response.ResponseResult;
import io.rong.models.response.TokenResult;
import io.rong.models.user.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * description: ImManager <br>
 * date: 2020/2/26 16:49 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class ImManager {

    private static final Logger logger = LoggerFactory.getLogger(ImManager.class);

    @Resource
    private RongCloud rongCloud;

    public String getSystemNo() {
        String systemNo = "10000";
        return systemNo;
    }

    public String registerUser(String userNo, String nickName, String avatar) throws SystemException {
        UserModel userModel = new UserModel();
        String id = userNo;
        nickName = Optional.ofNullable(nickName).orElse("default");
        avatar = Optional.ofNullable(avatar).orElse("default");
        userModel.setId(id)
                .setName(nickName)
                .setPortrait(avatar);
        try {
            TokenResult tokenResult = rongCloud.user.register(userModel);
            return tokenResult.getToken();
        } catch (Exception e) {
            logger.error("获取融云token失败，data={},error=", JSONObject.toJSONString(userModel), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void addWhiteList(String userNo, List<String> whiteUserNos) {
        UserModel userModel = new UserModel();
        userModel.setId(userNo);
        UserModel[] whiteModels = new UserModel[whiteUserNos.size()];
        for (int i = 0; i < whiteUserNos.size(); i++) {
            UserModel whiteModel = new UserModel();
            whiteModel.setId(whiteUserNos.get(i));
            whiteModels[i] = whiteModel;
        }
        userModel.setWhitelist(whiteModels);
        try {

            rongCloud.user.whiteList.add(userModel);

        } catch (Exception e) {
            logger.error("添加融云白名单失败，userNo={},friendNo={},error=", userNo, JSONObject.toJSONString(whiteUserNos), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void removeWhiteList(String userNo, List<String> whiteUserNos) {
        UserModel userModel = new UserModel();
        userModel.setId(userNo);
        UserModel[] whiteModels = new UserModel[whiteUserNos.size()];
        for (int i = 0; i < whiteUserNos.size(); i++) {
            UserModel whiteModel = new UserModel();
            whiteModel.setId(whiteUserNos.get(i));
            whiteModels[i] = whiteModel;
        }
        userModel.setWhitelist(whiteModels);
        try {
            rongCloud.user.whiteList.remove(userModel);
        } catch (Exception e) {
            logger.error("删除融云白名单失败，userNo={},friendNo={},error=", userNo, JSONObject.toJSONString(whiteUserNos), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void updateUser(String userNo, String nickName, String avatar) throws SystemException {
        UserModel userModel = new UserModel();
        String id = userNo;
        nickName = Optional.ofNullable(nickName).orElse("default");
        avatar = Optional.ofNullable(avatar).orElse("default");
        userModel.setId(id)
                .setName(nickName)
                .setPortrait(avatar);
        try {
            Result result = rongCloud.user.update(userModel);
        } catch (Exception e) {
            logger.error("更新融云用户信息失败，data={},error=", JSONObject.toJSONString(userModel), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void createGroup(String groupId, String groupName, List<String> userNos) throws SystemException {
        GroupModel groupModel = new GroupModel();
        groupModel.setId(groupId);
        groupModel.setName(groupName);
        GroupMember[] groupMembers = new GroupMember[userNos.size()];
        for (int i = 0; i < userNos.size(); i++) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(userNos.get(i));
            groupMembers[i] = groupMember;
        }
        groupModel.setMembers(groupMembers);
        try {
            rongCloud.group.create(groupModel);
        } catch (Exception e) {
            logger.error("添加融云群组失败，groupId={},userNos={},error=", groupId, userNos, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void joinGroup(String groupId, String groupName, List<String> userNos) throws SystemException {
        GroupModel groupModel = new GroupModel();
        groupModel.setId(groupId);
        groupModel.setName(groupName);
        GroupMember[] groupMembers = new GroupMember[userNos.size()];
        for (int i = 0; i < userNos.size(); i++) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(userNos.get(i));
            groupMembers[i] = groupMember;
        }
        groupModel.setMembers(groupMembers);
        try {
            rongCloud.group.join(groupModel);
        } catch (Exception e) {
            logger.error("加入融云群组失败，groupId={},userNos={},error=", groupId, userNos, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void quitGroup(String groupId, List<String> userNos) throws SystemException {
        GroupModel groupModel = new GroupModel();
        groupModel.setId(groupId);
        GroupMember[] groupMembers = new GroupMember[userNos.size()];
        for (int i = 0; i < userNos.size(); i++) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(userNos.get(i));
            groupMembers[i] = groupMember;
        }
        groupModel.setMembers(groupMembers);
        try {
            rongCloud.group.quit(groupModel);
        } catch (Exception e) {
            logger.error("退出融云群组失败，groupId={},userNo={},error=", groupId, JSONObject.toJSONString(userNos), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void setFriendDisturb(String userNo, int friendId, int type) throws SystemException {
        ConversationModel conversationModel = new ConversationModel();
        conversationModel.setTargetId(String.valueOf(friendId));
        conversationModel.setType(String.valueOf(type));
        conversationModel.setUserId(userNo);
        try {
            if (type == 1) {
                rongCloud.conversation.mute(conversationModel);
            } else if (type == 0) {
                rongCloud.conversation.unMute(conversationModel);
            } else {
                throw new SystemException(BaseResultCode.COMMON_FAIL, "参数错误，请确认传输参数");
            }

        } catch (Exception e) {
            logger.error("设置好友消息免打扰，userNo={}，friendId={},type={}error=", userNo, friendId, type, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void sendShareMsg(String fromUserId, List<Map<Integer, String>> toUserIds, Map<String, Object> dataMap) {

        List<String> userNos = new ArrayList<>();
        List<String> groupNos = new ArrayList<>();
        for (Map<Integer, String> map : toUserIds) {
            if (map.get(0) != null) {
                userNos.add(map.get(0));
            } else if (map.get(1) != null) {
                groupNos.add(map.get(1));
            }
        }
        String content = String.valueOf(dataMap.get("content"));
        String title = String.valueOf(dataMap.get("title"));
        String imageUri = String.valueOf(dataMap.get("imageUri"));
        String url = String.valueOf(dataMap.get("url"));
        ShareMessage shareMessage = new ShareMessage(content, imageUri, title, url);
        try {
            if (!CollectionUtils.isEmpty(userNos)) {
                PrivateMessage privateMessage = new PrivateMessage();
                privateMessage.setSenderId(fromUserId);
                privateMessage.setTargetId(userNos.toArray(new String[userNos.size()]));
                privateMessage.setObjectName("RC:ImgTextMsg");
                privateMessage.setContent(shareMessage);
                rongCloud.message.msgPrivate.send(privateMessage);
            } else if (!CollectionUtils.isEmpty(groupNos)) {
                GroupMessage groupMessage = new GroupMessage();
                groupMessage.setSenderId(fromUserId);
                groupMessage.setTargetId(groupNos.toArray(new String[userNos.size()]));
                groupMessage.setObjectName("RC:ImgTextMsg");
                groupMessage.setContent(shareMessage);
                rongCloud.message.group.send(groupMessage);
            }
        } catch (Exception e) {
            logger.error("发送分享通知消息失败，message={}", JSONObject.toJSONString(shareMessage), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void setGroupDisturb(String userNo, String groupId, int type) throws SystemException {
        ConversationModel conversationModel = new ConversationModel();
        conversationModel.setTargetId(groupId);
        conversationModel.setType(String.valueOf(type));
        conversationModel.setUserId(userNo);
        try {
            if (type == 1) {
                rongCloud.conversation.mute(conversationModel);
            } else if (type == 0) {
                rongCloud.conversation.unMute(conversationModel);
            } else {
                throw new SystemException(BaseResultCode.COMMON_FAIL, "参数错误，请确认传输参数");
            }

        } catch (Exception e) {
            logger.error("设置群消息免打扰，userNo={}，friendId={},type={}error=", userNo, groupId, type, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void addBannedWhitelist(String groupId, List<String> managerId) throws SystemException {
        GroupModel group = new GroupModel();

        GroupMember[] groupMembers = new GroupMember[managerId.size()];
        for (int i = 0; i < managerId.size(); i++) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(managerId.get(i));
            groupMembers[i] = groupMember;
        }
        group.setId(groupId);
        group.setMembers(groupMembers);
        try {
            rongCloud.group.muteWhiteList.user.add(group);
        } catch (Exception e) {
            logger.error("设置禁言白名单，groupId={}", groupId, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void removeBannedWhitelist(String groupId, List<String> managerId) throws SystemException {
        GroupModel group = new GroupModel();

        GroupMember[] groupMembers = new GroupMember[managerId.size()];
        for (int i = 0; i < managerId.size(); i++) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(managerId.get(i));
            groupMembers[i] = groupMember;
        }
        group.setId(groupId);
        group.setMembers(groupMembers);
        try {
            rongCloud.group.muteWhiteList.user.remove(group);
        } catch (Exception e) {
            logger.error("设置禁言白名单，groupId={}", groupId, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void setBanned(String groupId) throws SystemException {
        String[] arr = {groupId};
        try {
            rongCloud.group.muteAllMembers.add(arr);
        } catch (Exception e) {
            logger.error("设置群全禁言，groupId={}", groupId, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void setBannedOpen(String groupId) throws SystemException {
        String[] arr = {groupId};
        try {
            rongCloud.group.muteAllMembers.remove(arr);
        } catch (Exception e) {
            logger.error("设置群开放禁言，groupId={}", groupId, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void sendGroupNoticeMsg(String fromUserId, String groupNo, String operatorUserId, String msg, String operation, String ext) throws SystemException {
        sendGroupNoticeMsg(fromUserId, groupNo, null, operatorUserId, msg, operation, ext);
    }

    public void sendGroupNoticeMsg(String fromUserId, String groupNo, List<String> userIds, String operatorUserId, String msg, String operation, String ext) throws SystemException {
        GroupNoticeMessage content = new GroupNoticeMessage(operatorUserId, msg, ext, operation);
        logger.info("content={}", JSONObject.toJSONString(content));

        if (!CollectionUtils.isEmpty(userIds)) {
            MentionMessage mentionMessage = new MentionMessage();
            mentionMessage.setSenderId(fromUserId);
            mentionMessage.setTargetId(new String[]{groupNo});
            mentionMessage.setObjectName("GM:GrpNtf");
            MentionedInfo mentionedInfo = new MentionedInfo(2, userIds.toArray(new String[userIds.size()]), msg);
            MentionMessageContent mentionMessageContent = new MentionMessageContent(content, mentionedInfo);
            mentionMessage.setContent(mentionMessageContent);
            try {

                ResponseResult result = rongCloud.message.group.sendMention(mentionMessage);
                logger.info("群组通知结果,result={}", JSONObject.toJSONString(result));
            } catch (Exception e) {
                logger.error("发送群组通知消息失败，operation={}，fromUserId={},toUserId={}error=", fromUserId, groupNo, e);
                throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
            }
        } else {
            GroupMessage messageModel = new GroupMessage();
            messageModel.setSenderId(fromUserId);
            messageModel.setTargetId(new String[]{groupNo});
            messageModel.setObjectName("GM:GrpNtf");
            messageModel.setContent(content);
            try {
                logger.info("群组通知结果,result={}", JSONObject.toJSONString(messageModel));

                ResponseResult result = rongCloud.message.group.send(messageModel);
                logger.info("群组通知结果,result={}", JSONObject.toJSONString(result));
            } catch (Exception e) {
                logger.error("发送群组通知消息失败，operation={}，fromUserId={},toUserId={}error=", fromUserId, groupNo, e);
                throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
            }
        }

    }

    public void sendPrivateMsg(String fromUserId, String toUserId, String msg) throws SystemException {
      /*  ContactNtfMessage message = new ContactNtfMessage(operation, "", fromUserId, toUserId, msg);
        SystemMessage messageModel = new SystemMessage();
        messageModel.setSenderId(toUserId);
        messageModel.setTargetId(new String[]{toUserId});
        messageModel.setObjectName("RC:ContactNtf");
        messageModel.setContent(message);
        try {
            logger.info("发送好友消息,data={}", JSONObject.toJSONString(messageModel));
            ResponseResult result = rongCloud.message.system.send(messageModel);
            logger.info("发送好友消息,result={}", JSONObject.toJSONString(result));
        } catch (Exception e) {
            logger.error("发送好友通知消息失败，operation={}，fromUserId={},toUserId={}error=", operation, fromUserId, toUserId, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }*/
        PrivateMessage privateMessage = new PrivateMessage();
        TxtMessage txtMessage = new TxtMessage(msg, "");
        privateMessage.setSenderId(fromUserId);
        privateMessage.setTargetId(new String[]{toUserId});
        privateMessage.setObjectName("RC:TxtMsg");
        //  messageModel.setObjectName("RC:TxtMsg");
        privateMessage.setContent(txtMessage);
        try {
            logger.info("发送好友消息,data={}", JSONObject.toJSONString(privateMessage));
            ResponseResult result = rongCloud.message.msgPrivate.send(privateMessage);
            logger.info("发送好友消息,result={}", JSONObject.toJSONString(result));
        } catch (Exception e) {
            logger.error("发送好友通知消息失败，operation={}，fromUserId={},toUserId={}error=", fromUserId, toUserId, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }
    public void updateGroup(String groupId, String groupName) throws SystemException {
        GroupModel groupModel = new GroupModel();
        groupModel.setId(groupId);
        groupModel.setName(groupName);
        try {
            rongCloud.group.update(groupModel);
        } catch (Exception e) {
            logger.error("修改融云群组失败，groupId={},groupName={},error=", groupId, groupName, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void sendGroupCustomNoticeMsg(String fromUserId, String groupNo, List<String> userIds, String operatorUserId, String msg, String operation, String ext) throws SystemException {
        GroupCutomStyleMessage content = new GroupCutomStyleMessage(operatorUserId, msg, ext, operation);
        logger.info("content={}", JSONObject.toJSONString(content));

        if (!CollectionUtils.isEmpty(userIds)) {
            MentionMessage mentionMessage = new MentionMessage();
            mentionMessage.setSenderId(fromUserId);
            mentionMessage.setTargetId(new String[]{groupNo});
            mentionMessage.setObjectName("GM:CustomNtf");
            MentionedInfo mentionedInfo = new MentionedInfo(2,userIds.toArray(new String[userIds.size()]),msg);
            MentionMessageContent mentionMessageContent = new MentionMessageContent(content,mentionedInfo);
            mentionMessage.setContent(mentionMessageContent);
            try {
                ResponseResult result = rongCloud.message.group.sendMention(mentionMessage);
                logger.info("群组通知结果,result={}", JSONObject.toJSONString(result));
            } catch (Exception e) {
                logger.error("发送群组通知消息失败，operation={}，fromUserId={},toUserId={}error=", fromUserId, groupNo, e);
                throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
            }
        } else {
            GroupMessage messageModel = new GroupMessage();
            messageModel.setSenderId(fromUserId);
            messageModel.setTargetId(new String[]{groupNo});
            messageModel.setObjectName("GM:CustomNtf");
            messageModel.setContent(content);
            try {
                logger.info("群组通知结果,result={}", JSONObject.toJSONString(messageModel));

                ResponseResult result = rongCloud.message.group.send(messageModel);
                logger.info("群组通知结果,result={}", JSONObject.toJSONString(result));
            } catch (Exception e) {
                logger.error("发送群组通知消息失败，operation={}，fromUserId={},toUserId={}error=", fromUserId, groupNo, e);
                throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
            }
        }
    }

    public void sendFriendNoticeMsg(String fromUserId, List<String> toUserIds, String msg,String extra) throws SystemException {
        logger.info("处理好友通知消息");
        InfoNtfMessage infoNtfMessage = new InfoNtfMessage(msg,extra);
        PrivateMessage messageModel = new PrivateMessage();
        messageModel.setSenderId(fromUserId);
        messageModel.setTargetId(toUserIds.toArray(new String[toUserIds.size()]));
        messageModel.setObjectName("RC:InfoNtf");
        messageModel.setContent(infoNtfMessage);
        try {
            logger.info("好友通知结果,result={}", JSONObject.toJSONString(messageModel));

            ResponseResult result = rongCloud.message.msgPrivate.send(messageModel);

            logger.info("好友通知结果,result={}", JSONObject.toJSONString(result));
        } catch (Exception e) {
            logger.error("发送好友小灰条消息失败，operation={}，fromUserId={},toUserId={}error=", fromUserId, JSONObject.toJSONString(toUserIds), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void sendUserCustomMsg(String fromUserId, List<String> toUserIds, String msg,String title) throws SystemException {
        logger.info("处理用户通知消息");
        String time =  LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        CustomNtfMessage infoNtfMessage = new CustomNtfMessage(msg,title,time);
        PrivateMessage messageModel = new PrivateMessage();
        messageModel.setSenderId(fromUserId);
        messageModel.setTargetId(toUserIds.toArray(new String[toUserIds.size()]));
        messageModel.setObjectName("HR:CustomNtf");
        messageModel.setContent(infoNtfMessage);
        try {
            logger.info("用户通知结果,result={}", JSONObject.toJSONString(messageModel));

            ResponseResult result = rongCloud.message.msgPrivate.send(messageModel);

            logger.info("用户通知结果,result={}", JSONObject.toJSONString(result));
        } catch (Exception e) {
            logger.error("发送用户通知消息失败，operation={}，fromUserId={},toUserId={}error=", fromUserId, JSONObject.toJSONString(toUserIds), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void sendGroupNickNameMsg(String userId, String groupId, Map<String, String> data) {
        GroupNickNameChangeMessage content = new GroupNickNameChangeMessage("update", JSONObject.toJSONString(data), "");
        logger.info("content={}", JSONObject.toJSONString(content));
        GroupMessage messageModel = new GroupMessage();
        messageModel.setSenderId(userId);
        messageModel.setTargetId(new String[]{groupId});
        messageModel.setObjectName("GR:ProfileNtf");
        messageModel.setContent(content);
        try {
            logger.info("群组修改昵称通知结果,result={}", JSONObject.toJSONString(messageModel));

            ResponseResult result = rongCloud.message.group.send(messageModel);
            logger.info("群组通知结果,result={}", JSONObject.toJSONString(result));
        } catch (Exception e) {
            logger.error("发送群组通知消息失败，operation={}，fromUserId={},toUserId={}error=", "update", userId, groupId, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void sendGroupMsg(String operation, String fromUserId, List<String> toUserIds, String operatorUserId, String data, String msg) throws SystemException {
        GroupNotificationMessage content = new GroupNotificationMessage(operatorUserId, operation, JSONObject.parseObject(data), msg, "");
        logger.info("content={}", JSONObject.toJSONString(content));
        GroupMessage messageModel = new GroupMessage();
        messageModel.setSenderId(fromUserId);
        messageModel.setTargetId(toUserIds.toArray(new String[toUserIds.size()]));
        messageModel.setObjectName("RC:GrpNtf");
        messageModel.setContent(content);
        try {
            logger.info("群组通知结果,result={}", JSONObject.toJSONString(messageModel));

            ResponseResult result = rongCloud.message.group.send(messageModel);
            logger.info("群组通知结果,result={}", JSONObject.toJSONString(result));
        } catch (Exception e) {
            logger.error("发送群组通知消息失败，operation={}，fromUserId={},toUserId={}error=", operation, fromUserId, JSONObject.toJSONString(toUserIds), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void dismissGroup(String groupId, String userNo) throws SystemException {
        GroupModel groupModel = new GroupModel();
        groupModel.setId(groupId);
        GroupMember[] groupMembers = new GroupMember[1];
        GroupMember groupMember = new GroupMember();
        groupMember.setId(userNo);
        groupMembers[0] = groupMember;
        groupModel.setMembers(groupMembers);
        try {
            rongCloud.group.dismiss(groupModel);
        } catch (Exception e) {
            logger.error("解散融云群组失败，groupId={},userNo={},error=", groupId, userNo, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void addBlockUser(String id) throws SystemException {
        UserModel userModel = new UserModel();
        userModel.setId(id);
        userModel.setMinute(30 * 24 * 60);
        try {
            rongCloud.user.block.add(userModel);
        } catch (Exception e) {
            logger.error("添加黑名单失败，userNo={}error=", id, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void removeBlockUser(String id) throws SystemException {
        try {
            rongCloud.user.block.remove(id);
        } catch (Exception e) {
            logger.error("删除黑名单失败，userNo={}error=", id, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }
}
