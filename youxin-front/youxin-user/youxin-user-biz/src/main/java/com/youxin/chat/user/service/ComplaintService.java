package com.youxin.chat.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.user.model.ChatComplaint;
import com.youxin.chat.user.vo.req.ComplaintReq;

/**
 * description: ComplaintService <br>
 * date: 2020/3/10 10:14 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface ComplaintService extends IService<ChatComplaint> {

    void setComplaint(String userNo, ComplaintReq complaint);
}

