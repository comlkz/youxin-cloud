package com.youxin.chat.user.convert;

import com.alibaba.fastjson.JSONObject;
import com.youxin.chat.user.model.Group;
import com.youxin.chat.user.model.UserGroup;
import com.youxin.chat.user.vo.group.GroupBasicVo;
import com.youxin.chat.user.vo.group.GroupDetailVo;

import java.util.Optional;

/**
 * description: GroupConvert <br>
 * date: 2020/1/8 11:10 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class GroupConvert {

    /*
     * description: 群组详情转换 <br>
     * version: 1.0 <br>
     * date: 2020/1/8 11:15 <br>
     * author: llkj <br>
     *
     * @param group
      * @param userGroup
     * @return com.guoliao.chat.user.dto.group.GroupDetailResp
     */
    public static GroupDetailVo buildDetailResp(Group group, UserGroup userGroup) {
        GroupDetailVo resp = new GroupDetailVo();
        resp.setGroupName(group.getGroupName());
        resp.setGroupNo(group.getGroupNo());
        resp.setGroupNotice(group.getGroupNotice());
        resp.setIsJoinAddress(userGroup.getIsAddressList());
        resp.setIsNotDisturb(userGroup.getNotDisturb());
        resp.setAudit(group.getAudit());
        resp.setIsTop(userGroup.getIsTop());
        resp.setProtectedMode(group.getProtectedMode());
        resp.setQrStatus(group.getQrStatus());
        resp.setBanned(group.getBanned());
        resp.setNicknameProtect(group.getNicknameProtect());
        resp.setClearTime(Optional.ofNullable(group.getExt()).map(JSONObject::parseObject).map(item->item.getInteger("clearTime")).orElse(-1));
        resp.setPrintScreenNotify(Optional.ofNullable(group.getExt()).map(JSONObject::parseObject).map(item->item.getInteger("printScreenNotify")).orElse(0));
        resp.setClearTimePoint(Optional.ofNullable(group.getExt()).map(JSONObject::parseObject).map(item->item.getString("clearTimePoint")).orElse(""));
        resp.setAppendStatus(group.getAppendStatus());

        return resp;
    }

    public static GroupBasicVo buildBasicResp(Group group) {
        GroupBasicVo resp = new GroupBasicVo();
        resp.setGroupName(group.getGroupName());
        resp.setGroupNo(group.getGroupNo());
        resp.setAudit(group.getAudit());

        resp.setProtectedMode(group.getProtectedMode());
        resp.setBanned(group.getBanned());
        resp.setQrStatus(group.getQrStatus());
        resp.setNicknameProtect(group.getNicknameProtect());
        resp.setClearTime(Optional.ofNullable(group.getExt()).map(JSONObject::parseObject).map(item->item.getInteger("clearTime")).orElse(-1));
        resp.setClearTimePoint(Optional.ofNullable(group.getExt()).map(JSONObject::parseObject).map(item->item.getString("clearTimePoint")).orElse(""));
        resp.setPrintScreenNotify(Optional.ofNullable(group.getExt()).map(JSONObject::parseObject).map(item->item.getInteger("printScreenNotify")).orElse(0));

        return resp;
    }

}
