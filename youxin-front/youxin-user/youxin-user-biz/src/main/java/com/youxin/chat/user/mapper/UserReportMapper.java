package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.youxin.chat.user.model.UserReport;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserReportMapper extends BaseMapper<UserReport> {


}
