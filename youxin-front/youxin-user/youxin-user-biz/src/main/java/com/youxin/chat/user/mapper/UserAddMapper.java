package com.youxin.chat.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.youxin.chat.user.model.UserAdd;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserAddMapper extends BaseMapper<UserAdd> {


     void emptyList(@Param("userNo") String userNo);
}
