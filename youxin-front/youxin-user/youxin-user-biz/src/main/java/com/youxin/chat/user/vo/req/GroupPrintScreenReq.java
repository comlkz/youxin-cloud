package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class GroupPrintScreenReq implements Serializable {
    @ApiModelProperty(value = "发送至群聊或私聊编码",required = true)
    private String toChatNo;
    @ApiModelProperty(value = "类型0：群里 1：私聊",required = true)
    private Integer type;

    @ApiModelProperty(value = "随机字符串，防止重复请求",required = true)
    private String uid;

    public String getToChatNo() {
        return toChatNo;
    }

    public void setToChatNo(String toChatNo) {
        this.toChatNo = toChatNo;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
