package com.youxin.chat.user.manager.message;

import io.rong.messages.BaseMessage;
import io.rong.util.GsonUtil;

public class ShareMessage extends BaseMessage {

    private transient static final String TYPE = "RC:ImgTextMsg";

    private String content;

    private String imageUri;

    private String title;

    private String url;

    public ShareMessage(String content, String imageUri, String title, String url) {
        this.content = content;
        this.imageUri = imageUri;
        this.title = title;
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public String toString() {
        return GsonUtil.toJson(this, ShareMessage.class);    }
}
