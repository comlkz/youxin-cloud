package com.youxin.chat.user.vo.req;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * description: GroupExtDto <br>
 * date: 2020/1/8 11:25 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class GroupAttrReq implements Serializable {

    @ApiModelProperty(value = "群编码")
    private String groupNo;

    @ApiModelProperty(value = "clearTime:禁群消息定时清理 printScreenNotify:截屏通知")
    private String extName;

    @ApiModelProperty(value = "clearTime： 关闭 -1，即刻清理 0，其余时间（单位秒） printScreenNotify：0:不开启，1：开启")
    private String extValue;

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getExtName() {
        return extName;
    }

    public void setExtName(String extName) {
        this.extName = extName;
    }

    public String getExtValue() {
        return extValue;
    }

    public void setExtValue(String extValue) {
        this.extValue = extValue;
    }
}
