package com.youxin.chat.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.user.model.ChatRole;

public interface ChatRoleMapper extends BaseMapper<ChatRole> {

}
