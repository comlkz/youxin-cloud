package com.youxin.chat.user.manager.message;

import io.rong.messages.BaseMessage;
import io.rong.util.GsonUtil;

import java.util.Map;

/**
 * description: GroupCutomStyleMessage <br>
 * date: 2020/1/14 18:19 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class GroupCutomStyleMessage extends BaseMessage {

    //操作人用户 Id
    private String operatorUserId;

    //操作名称
    private String operation;

    //群组中各种通知的操作数据
    private Map<String, Object> data;

    private String msg;

    private String extra;

    private transient static final String TYPE = "GM:CustomNtf";


    public GroupCutomStyleMessage(String operatorUserId, String msg, String ext, String operation) {
        this.operatorUserId = operatorUserId;
        this.msg = msg;
        this.extra = ext;
        this.operation = operation;
    }

    @Override
    public String getType() {
        return "GM:CustomNtf";
    }

    @Override
    public String toString() {
        return GsonUtil.toJson(this, GroupCutomStyleMessage.class);

    }

    public String getOperatorUserId() {
        return operatorUserId;
    }

    public void setOperatorUserId(String operatorUserId) {
        this.operatorUserId = operatorUserId;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

}
