package com.youxin;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;
import com.alicp.jetcache.anno.config.EnableMethodCache;
import com.youxin.auth.server.EnableAuthServer;
import com.youxin.log.EnableUserLog;
import com.youxin.user.annotation.EnableLoginArgResolver;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * description: UserBootStrap <br>
 * date: 2020/2/26 11:40 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@SpringBootApplication
@EnableDubbo
@EnableUserLog
@EnableAuthServer
@EnableDiscoveryClient
@EnableLoginArgResolver
@EnableMethodCache(basePackages = "com.youxin.chat.user")
@EnableCreateCacheAnnotation
public class UserBootStrap {

    public static void main(String[] args) {
        SpringApplication.run(UserBootStrap.class,args);
    }
}
