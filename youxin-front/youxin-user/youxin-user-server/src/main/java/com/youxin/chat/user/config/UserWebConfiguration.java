package com.youxin.chat.user.config;

import com.youxin.common.config.BaseConfig;
import com.youxin.log.event.SysLogListener;
import com.youxin.other.report.client.LogClient;
import io.rong.RongCloud;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author zuihou
 * @createTime 2017-12-15 14:42
 */
@Configuration
public class UserWebConfiguration extends BaseConfig {

    @Reference(retries = 0,timeout = 2000)
    private LogClient logClient;

    @Bean
    public SysLogListener sysLogListener() {
        return new SysLogListener( (log) -> logClient.save(log));
    }
}
