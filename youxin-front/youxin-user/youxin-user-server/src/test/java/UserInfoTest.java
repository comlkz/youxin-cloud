import com.youxin.UserBootStrap;
import com.youxin.chat.user.mapper.UserInfoMapper;
import com.youxin.chat.user.model.UserInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * description: UserInfoTest <br>
 * date: 2020/2/26 11:53 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@MybatisTest
@SpringBootTest(classes = UserBootStrap.class)
@EnableAutoConfiguration
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE )
@RunWith(SpringRunner.class)
public class UserInfoTest {

    @Resource
    private UserInfoMapper userInfoMapper;

    @Test
    public void test(){
       UserInfo userInfo =  userInfoMapper.selectByUserNo("111");
    }

}
