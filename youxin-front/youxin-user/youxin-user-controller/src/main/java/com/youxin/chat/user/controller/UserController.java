package com.youxin.chat.user.controller;

import com.youxin.base.TResult;
import com.youxin.chat.user.service.GroupService;
import com.youxin.chat.user.service.UserAddService;
import com.youxin.chat.user.service.UserFriendService;
import com.youxin.chat.user.service.UserService;
import com.youxin.chat.user.vo.group.GroupDetailVo;
import com.youxin.chat.user.vo.req.*;
import com.youxin.chat.user.vo.user.*;
import com.youxin.log.annotation.SysLog;
import com.youxin.user.annotation.LoginUser;
import com.youxin.user.model.SysUser;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * description: UserController <br>
 * date: 2020/2/25 11:13 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */


@RestController
@RequestMapping("user")
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private UserAddService userAddService;

    @Resource
    private UserFriendService userFriendService;

    @Resource
    private GroupService groupService;

    @PostMapping("pass/set")
    @ApiOperation("设置登录密码")
    @SysLog("设置登录密码")
    public TResult<String> setLoginPass(@ApiIgnore @LoginUser(isFull = false) SysUser user,
                                        @RequestBody @Valid LoginPassReq loginPassReq) {
        userService.setLoginPass(user.getUserNo(), loginPassReq);
        return TResult.success();
    }

    @GetMapping("apply/list")
    @ApiOperation("获取用户申请列表")
    public TResult<List<UserAddVo>> listAddUser(@ApiIgnore @LoginUser(isFull = false) SysUser user) {
        List<UserAddVo> userAddDtos = userAddService.listAddUsers(user.getUserNo());
        return TResult.success(userAddDtos);
    }

    @GetMapping("empty")
    @ApiOperation("清空用户申请列表")
    public TResult<String> emptyList(@ApiIgnore @LoginUser(isFull = false) SysUser user) {
        userAddService.emptyList(user.getUserNo());
        return TResult.success();
    }

    @PostMapping("apply")
    @ApiOperation("申请添加用户")
    public TResult<String> userApply(@ApiIgnore @LoginUser(isFull = false) SysUser user,
                                     @RequestBody @Valid UserApplyReq userApplyReq) {
        userFriendService.applyUser(user.getUserNo(), userApplyReq);
        return TResult.success();
    }

    @PostMapping("apply/handle/{userNo}/{action}")
    @ApiOperation("处理好友申请")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "userNo", value = "用户编号", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "path", name = "action", value = "操作类型  1：通过 -1：拒绝", required = true, dataType = "Integer"),
    })
    public TResult<String> handleApply(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String userNo, @PathVariable Integer action) {
        userFriendService.handleApply(user.getUserNo(), userNo, action);
        return TResult.success();
    }

    @PostMapping("set")
    @ApiOperation("设置用户信息")
    public TResult<String> setUserInfo(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody UserInfoSetReq userInfoSetReq) {
        userService.perfectUserInfo(user.getUserNo(),userInfoSetReq);
        return TResult.success();
    }

    @GetMapping("search/{searchKey}")
    @ApiOperation("搜索好友")
    public TResult<UserSearchVo> findUser(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String searchKey) {
        UserSearchVo userSearchResult = userFriendService.findUserByKey(user.getUserNo(), searchKey);
        return TResult.success(userSearchResult);
    }

    @GetMapping("friend/list")
    @ApiOperation("好友列表")
    public TResult<List<UserFriendVo>> listFriend(@ApiIgnore @LoginUser(isFull = true) SysUser user) {
        return TResult.success(userFriendService.listFriend(user.getUserNo()));
    }

    @GetMapping("getUser")
    @ApiOperation("查询当前用户信息")
    public TResult<UserDetailVo> getUserInfo(@ApiIgnore @LoginUser(isFull = false) SysUser user) {
        UserDetailVo resp = userService.getUserInfo(user.getUserNo());
        return TResult.success(resp);
    }

    @GetMapping("info/{userNo}")
    @ApiOperation("用户详情")
    public TResult<UserDetailVo> userDetails(@PathVariable String userNo) {
        UserDetailVo userInfo = userService.selectUserDetails(userNo);
        return TResult.success(userInfo);
    }

    @GetMapping("special/list")
    @ApiOperation("查询特殊用户信息")
    public TResult<List<UserDetailVo>> specialUserList() {
        List<UserDetailVo> list = userService.listSpecialUsers();
        return TResult.success(list);
    }

    @GetMapping("friendDetails/{friendNo}")
    @ApiOperation("好友详情")
    public TResult<UserFriendVo> friendDetails(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String friendNo) {
        UserFriendVo userFriendDto = userFriendService.selectFriendDetails(user.getUserNo(), friendNo);
        return TResult.success(userFriendDto);
    }

    @PostMapping("friend/black/{friendNo}/{status}")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "friendNo", value = "好友用户编码", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "path", name = "status", value = "状态 0：正常 1：拉黑", required = true, dataType = "Integer"),
    })
    @ApiOperation("拉黑或取消拉黑")
    public TResult<String> blackUser(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String friendNo, @PathVariable Integer status) {
        userFriendService.blackUser(user.getUserNo(), friendNo, status);
        return TResult.success();
    }


    @ApiOperation("删除好友")
    @PostMapping("del/{friendNo}")
    public TResult<String> deleteFriend(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String friendNo) {
        userFriendService.deleteFriend(user.getUserNo(), friendNo);
        return TResult.success();
    }

    @ApiOperation("星标好友")
    @PostMapping("star")
    public TResult<String> starFriend(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody FriendStarReq friendStarDto) {
        userFriendService.starFriend(user.getUserNo(), friendStarDto.getFriendNo(), friendStarDto.getStarFriend());
        return TResult.success();
    }

    @ApiOperation("备注好友")
    @PostMapping("remark")
    public TResult<String> remarkFriend(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody FriendRemarkReq friendRemarkDto) {
        userFriendService.remarkFriend(user.getUserNo(), friendRemarkDto.getFriendNo(), friendRemarkDto.getFriendNickName());
        return TResult.success();
    }

    @ApiOperation("通讯录匹配好友列表")
    @PostMapping("contact/match")
    public TResult<List<UserMatchVo>> matchContact(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody List<UserMatchReq> mobiles) {
        List<UserMatchVo> list = userService.matchConcatUser(user.getUserNo(), mobiles);
        return TResult.success(list);
    }

    @ApiOperation("对好友免打扰设置")
    @GetMapping("disturb/friend")
    public TResult<String> setFriendDisturb(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody FriendDisturbReq friendDisturbDto) {
        userFriendService.setFriendDisturb(user.getUserNo(), friendDisturbDto);
        return TResult.success();
    }

    @ApiOperation("小助手列表")
    @GetMapping("get/assistant")
    public TResult<Set<String>> selectAssistant() {
        Set set = userService.selectAssistant();
        set.remove(null);
        return TResult.success(set);
    }

    @ApiOperation("设置尤信账号")
    @PostMapping("save/account")
    public TResult<String> saveUserName(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody @Valid AccountSaveReq accountSaveDto){
        userService.saveUserName(user.getUserNo(), accountSaveDto.getAccount());
        return TResult.success();
    }

    @ApiOperation("分享接口")
    @PostMapping("share")
    public TResult<String> share(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody ShareMsgReq shareMsgDto){

        userService.shareMsg(shareMsgDto, user.getUserNo());
        return TResult.success();
    }

    @GetMapping("active/app")
    @ApiOperation("APP打开保存记录")
    public TResult<String> activeApp(@ApiIgnore @LoginUser(isFull = false) SysUser user){
        userService.saveActiveRecord(user.getUserNo());
        return TResult.success();
    }

    @ApiOperation("黑名单列表")
    @GetMapping("blackList")
    public TResult<List<UserFriendVo>> blackList(@ApiIgnore @LoginUser(isFull = false) SysUser user) {
        List<UserFriendVo> list = userFriendService.blackList(user.getUserNo());
        return TResult.success(list);
    }

    @ApiOperation("收藏列表")
    @GetMapping("collectList")
    public TResult<List<UserCollectVo>> collectList(@ApiIgnore @LoginUser(isFull = false) SysUser user,CollectSearchReq collectSearch) {
        List<UserCollectVo> list = userService.collectList(user.getUserNo(),collectSearch);
        return TResult.success(list);
    }

    @ApiOperation("添加收藏")
    @PostMapping("collect/add")
    public TResult<String> collectAdd(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody UserCollectReq userCollectAdd) {
        userService.collectAdd(user.getUserNo(), userCollectAdd);
        return TResult.success();
    }

    @ApiOperation("删除收藏")
    @PostMapping("collect/del/{id}")
    public TResult<String> colCollectSearchlectDel(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable Integer id) {
        userService.collectDel(user.getUserNo(), id);
        return TResult.success();
    }

    @ApiOperation("设置好友属性")
    @PostMapping("attr/set")
    @SysLog("设置好友属性")
    public TResult<String> setFriendAttr(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody FriendExtReq friendExtDto) {
        userService.setFriendAttr(friendExtDto, user.getUserNo());
        return TResult.success();
    }



}
