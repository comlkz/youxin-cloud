package com.youxin.chat.user.controller;

import com.youxin.base.TResult;
import com.youxin.chat.user.service.ComplaintService;
import com.youxin.chat.user.service.FeedbackService;
import com.youxin.chat.user.vo.req.ComplaintReq;
import com.youxin.chat.user.vo.req.FeedbackReq;
import com.youxin.user.annotation.LoginUser;
import com.youxin.user.model.SysUser;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

/**
 * description: UserAttrController <br>
 * date: 2020/3/10 10:19 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
public class UserAttrController {

    @Resource
    private FeedbackService feedbackService;

    @Resource
    private ComplaintService complaintService;

    @ApiOperation("意见反馈")
    @PostMapping("feedback/set")
    @ResponseBody
    public TResult<String> setFeedback(@LoginUser SysUser sysUser,@RequestBody FeedbackReq feedbackDto){
        feedbackService.setFeedback(sysUser.getUserNo(),feedbackDto);
        return TResult.success();
    }

    @ApiOperation("用户投诉")
    @PostMapping("complaint/set")
    @ResponseBody
    public TResult<String> setComplaint(@LoginUser SysUser sysUser, @RequestBody ComplaintReq complaintDto){

        complaintService.setComplaint(sysUser.getUserNo(),complaintDto);
        return TResult.success();
    }
}
