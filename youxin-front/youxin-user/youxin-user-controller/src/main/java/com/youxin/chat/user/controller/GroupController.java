package com.youxin.chat.user.controller;

import com.youxin.base.TResult;
import com.youxin.chat.user.service.GroupService;
import com.youxin.chat.user.vo.group.*;
import com.youxin.chat.user.vo.req.*;
import com.youxin.chat.user.vo.user.UserDetailVo;
import com.youxin.chat.user.vo.user.UserSearchVo;
import com.youxin.log.annotation.SysLog;
import com.youxin.user.annotation.LoginUser;
import com.youxin.user.model.SysUser;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;

/**
 * description: GroupController <br>
 * date: 2020/2/27 15:17 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
@RequestMapping("user/group/")
public class GroupController {

    @Resource
    private GroupService groupService;

    @ApiOperation("添加群组")
    @SysLog("添加群组")
    @PostMapping("add")
    public TResult<GroupDetailVo> addGroup(@ApiIgnore @LoginUser(isFull = false) SysUser user, @RequestBody GroupAddReq groupAddDto) {
        GroupDetailVo resp = groupService.addGroup(user.getUserNo(), groupAddDto);
        return TResult.success(resp);
    }

    @ApiOperation("加入群组")
    @PostMapping("join/{groupNo}")
    @SysLog("加入群组")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "groupNo", value = "群聊编码", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "type", value = "邀请方式 0：普通邀请 1：扫码申请", required = true, dataType = "Integer"),
            @ApiImplicitParam(paramType = "body", name = "userNos", value = "用户编码", required = true, dataType = "Array"),
    })
    public TResult<String> join(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String groupNo,
                                @RequestParam(value = "type",defaultValue = "0") Integer type,
                                @RequestBody List<String> userNos) {
        groupService.joinGroup(user.getUserNo(), groupNo, userNos, type);
        return TResult.success();
    }

    @ApiOperation("加入群组(改进版)")
    @SysLog("加入群组")
    @PostMapping("join")
    @ApiResponse(code = 200,message = "0:申请中 1：加入成功",response = String.class)
    public TResult<String> join(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupAddReq groupAddDto) {
        int result = groupService.joinGroup(user.getUserNo(), groupAddDto);
        return TResult.success(String.valueOf(result));
    }


    @ApiOperation("退出群组")
    @SysLog("退出群组")
    @PostMapping("quit")
    public TResult<String> quitGroup(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody QuitGroupReq quitGroupDto) {
        groupService.quitGroup(quitGroupDto, user.getUserNo());
        return TResult.success();
    }

    @ApiOperation("解散群组")
    @SysLog("解散群组")
    @PostMapping("dismiss/{groupNo}")
    public TResult<String> disMissGroup(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String groupNo) {
        groupService.dismissGroup(groupNo, user.getUserNo());
        return TResult.success();
    }

    @ApiOperation("获取群组成员")
    @GetMapping("members/{groupNo}")
    public TResult<List<GroupMemberVo>> listGroupMembers(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String groupNo) {
        List<GroupMemberVo> list = groupService.listGroupMembers(user.getUserNo(), groupNo);
        return TResult.success(list);
    }

    @ApiOperation("获取群详情")
    @GetMapping("detail/{groupNo}")
    public TResult<GroupDetailVo> groupInfo(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String groupNo) {
        GroupDetailVo resp = groupService.acquireGroupInfo(user.getUserNo(), groupNo);
        return TResult.success(resp);
    }

    @GetMapping("baisc/{groupNo}")
    @ApiOperation("获取群组基本信息列表")
    public TResult<GroupBasicVo> getBasicGroupInfo(@ApiIgnore @LoginUser(isFull = false) SysUser user, @PathVariable String groupNo) {
        GroupBasicVo resp = groupService.getBasicGroupInfo(user.getUserNo(), groupNo);
        return TResult.success(resp);
    }


    @GetMapping("code/view/{groupCode}")
    @ApiOperation("扫码获取群详情")
    public TResult<GroupByCodeVo> getGroupByGroupCode(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String groupCode) {
        GroupByCodeVo groupView = groupService.getGroupByGroupCode(groupCode,user.getUserNo());
        return TResult.success(groupView);
    }

    @PostMapping("set/notice")
    @SysLog("设置群公告")
    @ApiOperation("设置群公告")
    public TResult<String> setGroupNotice(@ApiIgnore @LoginUser(isFull = false) SysUser user, @RequestBody GroupNoticeReq groupNoticeDto) {
        groupService.updateGroupNotice(user.getUserNo(), groupNoticeDto);
        return TResult.success();
    }

    @PostMapping("master/transfer")
    @SysLog("群主转让")
    @ApiOperation("群主转让")
    public TResult<String> transferMaster(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupUserReq groupUserDto) {
        groupService.transferMaster(groupUserDto.getUserNo(), user.getUserNo(), groupUserDto.getGroupNo());
        return TResult.success();
    }

    @PostMapping("manager/add")
    @SysLog("添加群管理员")
    @ApiOperation("添加群管理员")
    public TResult<String> setGroupManager(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupUserReq groupTransferDto) {
        groupService.setGroupManager(groupTransferDto.getUserNo(), user.getUserNo(), groupTransferDto.getGroupNo());
        return TResult.success();
    }

    @PostMapping("manager/remove")
    @SysLog("删除群管理员")
    @ApiOperation("删除群管理员")
    public TResult<String> removeGroupManager(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupUserReq groupTransferDto) {
        groupService.removeGroupManager(groupTransferDto.getUserNo(), user.getUserNo(), groupTransferDto.getGroupNo());
        return TResult.success();
    }

    @GetMapping("manager/list/{groupNo}")
    @ApiOperation("群管理员列表")
    public TResult<List<GroupMemberVo>> listGroupManagers(@PathVariable String groupNo) {
        List<GroupMemberVo> list = groupService.listGroupManagers(groupNo);
        return TResult.success(list);
    }


    @PostMapping("set/banned")
    @SysLog("全员禁言")
    @ApiOperation("全员禁言")
    public TResult<String> setBanned(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupBannedReq bannedDto) {

        groupService.setBanned(bannedDto, user.getUserNo());
        return TResult.success();
    }

    @PostMapping("protect")
    @SysLog("群保护设置")
    @ApiOperation("群保护设置")
    public TResult<String> setGroupProtect(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupProtectReq protectDto) {
        groupService.setGroupProtect(protectDto, user.getUserNo());
        return TResult.success();
    }

    @PostMapping("set/qrStatus")
    @SysLog("群二维码状态设置")
    @ApiOperation("群二维码状态设置")
    public TResult<String> setQRStatus(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupQRStatusReq qrStatusDto) {
        groupService.setQRStatus(qrStatusDto, user.getUserNo());
        return TResult.success();
    }

    @PostMapping("set/nicknameProtect")
    @SysLog("群成员昵称保护设置")
    @ApiOperation("群成员昵称保护设置")
    public TResult<String> setNicknameProtect(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupNicknameProtectReq nicknameProtectDto) {
        groupService.setNicknameProtect(nicknameProtectDto, user.getUserNo());
        return TResult.success();
    }

    @PostMapping("set/audit")
    @SysLog("设置入群审核")
    @ApiOperation("设置入群审核")
    public TResult<String> setGroupAudit(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupAuditReq groupAuditDto){
        groupService.setGroupAudit(groupAuditDto, user.getUserNo());
        return TResult.success();
    }

    @GetMapping("groupAdd/list/{groupNo}")
    @ApiOperation("获取用户申请列表")
    public TResult<List<GroupAddListVo>> listGroupAdd(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String groupNo) {
        List<GroupAddListVo> listGroupAdd = groupService.listGroupAdd(groupNo, user.getUserNo());
        return TResult.success(listGroupAdd);
    }

    @PostMapping("groupAdd/handle/{userNo}/{groupNo}/{action}")
    @SysLog("处理入群申请")
    @ApiOperation("处理入群申请")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "userNo", value = "用户编号", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "path", name = "groupNo", value = "群编号", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "path", name = "action", value = "操作类型  1：通过 -1：拒绝", required = true, dataType = "Integer"),
    })
    public TResult<String> handleGroupAdd(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String userNo, @PathVariable String groupNo, @PathVariable Integer action) {
        groupService.handleGroupAdd(user.getUserNo(), userNo, groupNo, action);
        return TResult.success();
    }

    @GetMapping("group/blacklist/{groupNo}")
    @SysLog("群组黑名单列表")
    @ApiOperation("群组黑名单列表")
    public TResult<List<GroupBlacklistVo>> blacklistGroup(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String groupNo) {
        List<GroupBlacklistVo> blacklistList = groupService.blacklistGroup(groupNo, user.getUserNo());
        return TResult.success(blacklistList);
    }

    @PostMapping("group/blacklist/add")
    @SysLog("添加群组黑名单")
    @ApiOperation("添加群组黑名单")
    public TResult<String> blacklistGroupAdd(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupBlackAddReq groupBlackAdd) {
        groupService.blacklistGroupAdd(groupBlackAdd, user.getUserNo());
        return TResult.success();
    }

    @PostMapping("group/blacklist/del/{groupNo}/{userNo}")
    @SysLog("移除群组黑名单")
    @ApiOperation("移除群组黑名单")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "groupNo", value = "群编号", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "path", name = "userNo", value = "用户编号", required = true, dataType = "String"),

    })
    public TResult<String> blacklistGroupDel(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String groupNo,@PathVariable String userNo) {
        groupService.blacklistGroupDel(groupNo,userNo, user.getUserNo());
        return TResult.success();
    }

    @GetMapping("group/quit/list/{groupNo}")
    @SysLog("退出群成员列表")
    @ApiOperation("退出群成员列表")
    public TResult<List<GroupQuitVo>> quitGroupList(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String groupNo){
        List<GroupQuitVo> list = groupService.quitGroupList(groupNo, user.getUserNo());
        return TResult.success(list);
    }

    @ApiOperation("设置群组名称")
    @SysLog("设置群组名称")
    @PostMapping("rename")
    public TResult<String> renameGroup(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody RenameGroupReq renameGroupDto) {
        groupService.renameGroup(renameGroupDto.getGroupNo(), renameGroupDto.getGroupName(), user.getUserNo());
        return TResult.success();
    }

    @ApiOperation(value = "设置群属性",notes = "群消息定时清理，若为阅后即焚，APP端退出时即把消息清除，若为其他时间，若用户在群聊界面，则定时清理，否则获取群聊信息时清除掉待清理的信息。" +
            "截屏通知需在关闭消息清理的情况下才可设置，若开启消息清理，则默认开启截屏通知")
    @PostMapping("attr/set")
    public TResult<String> setGroupAttr(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupAttrReq groupExtDto) {
        groupService.setGroupExt(groupExtDto, user.getUserNo());
        return TResult.success();
    }

    @ApiOperation("截屏通知")
    @PostMapping("printScreen")
    public TResult<String> printScreenMsg(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupPrintScreenReq printScreenMsgDto) {
        groupService.printScreenMsg(printScreenMsgDto, user.getUserNo());
        return TResult.success();
    }

    @GetMapping("active/{groupNo}/{type}")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "groupNo", value = "群编号", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "path", name = "type", value = "0：三天，1：一周，2：一个月", required = true, dataType = "Integer"),

    })
    @ApiOperation("不活跃群成员")
    public TResult<List<UserDetailVo>> groupUserActive(@ApiIgnore @LoginUser(isFull = false) SysUser user, @PathVariable String groupNo, @PathVariable Integer type) {

        return TResult.success(groupService.groupUserActive(user.getUserNo(), groupNo, type));
    }
}
