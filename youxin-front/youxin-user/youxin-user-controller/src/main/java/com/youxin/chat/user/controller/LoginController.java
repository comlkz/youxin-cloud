package com.youxin.chat.user.controller;

import com.youxin.base.TResult;
import com.youxin.chat.user.service.UserService;
import com.youxin.chat.user.vo.req.*;
import com.youxin.chat.user.vo.user.LoginResp;
import com.youxin.user.annotation.LoginUser;
import com.youxin.user.model.SysUser;
import com.youxin.utils.IPUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * description: LoginController <br>
 * date: 2020/2/26 17:11 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
@RequestMapping("auth")
public class LoginController {

    @Resource
    private UserService userService;

    @PostMapping("mobile/login")
    @ApiOperation("手机号登录")
    public TResult<LoginResp> mobileLogin(@RequestBody @Valid LoginMobileReq loginMobileReq) {
       LoginResp resp = userService.mobileLogin(loginMobileReq);
        return TResult.success(resp);
    }

    @PostMapping("register")
    @ApiOperation("用户注册")
    public TResult<String> register(@RequestBody @Valid RegisterReq registerReq) {
        userService.register(registerReq);
        return TResult.success();
    }

    @PostMapping("register/code/validate")
    @ApiOperation("注册短信验证码验证")
    public TResult<String> register(@RequestBody @Valid RegisterCodeReq registerReq) {
        String token = userService.validateRegisterCode(registerReq);
        return TResult.success(token);
    }


    @PostMapping("sms/send")
    @ApiOperation("发送短信验证码")
    public TResult<String> sendSms(@RequestBody @Valid RegisterSmsReq smsReqDto) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        userService.sendSms(smsReqDto, IPUtil.ipToLong(IPUtil.getIpAddr(request)));
        return TResult.success();
    }

    @PostMapping("pass/forget")
    @ApiOperation("忘记密码")
    public TResult<String> forgetLoginPass(@RequestBody ForgetPassReq forgetPassReq) {
        userService.forgetLoginPass(forgetPassReq);
        return TResult.success();
    }

    @PostMapping("logout")
    @ApiOperation("退出登录")
    public TResult<String> logout(@ApiIgnore @LoginUser(isFull = false) SysUser user) {
        if (user== null || user.getUserNo() == null) {
            return TResult.success();
        }
        userService.logOut(user.getUserNo());
        return TResult.success();
    }
}
