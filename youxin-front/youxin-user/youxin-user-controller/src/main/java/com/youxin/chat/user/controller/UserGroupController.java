package com.youxin.chat.user.controller;

import com.youxin.base.TResult;
import com.youxin.chat.user.service.UserGroupService;
import com.youxin.chat.user.vo.group.GroupByCodeVo;
import com.youxin.chat.user.vo.group.GroupItemVo;
import com.youxin.chat.user.vo.req.GroupNickNameReq;
import com.youxin.chat.user.vo.req.GroupPreferenceReq;
import com.youxin.user.annotation.LoginUser;
import com.youxin.user.model.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;

/**
 * description: UserGroupController <br>
 * date: 2020/2/28 14:01 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */

@Api(value="用户群组相关", tags={"用户群组接口"})
@RestController
@RequestMapping("user/group")
public class UserGroupController {

    @Resource
    private UserGroupService userGroupService;

    @PostMapping("preference/set")
    @ApiOperation("群个性化设置")
    public TResult<String> setPreference(@ApiIgnore @LoginUser(isFull = false) SysUser user, @RequestBody GroupPreferenceReq groupPreferenceReq) {
        userGroupService.setPreference(user.getUserNo(), groupPreferenceReq);
        return TResult.success();
    }

    @GetMapping("get/group")
    @ApiOperation("获取通讯录群列表")
    public TResult<List<GroupItemVo>> getAddressGroup(@ApiIgnore @LoginUser(isFull = false) SysUser user) {
        List<GroupItemVo> list = userGroupService.getAddressGroup(user.getUserNo());
        return TResult.success(list);
    }

    @PostMapping("set/nickname")
    @ApiOperation("设置群内昵称")
    public TResult<String> setNickName(@ApiIgnore @LoginUser(isFull = false) SysUser user,@RequestBody GroupNickNameReq groupNickNameDto) {
        userGroupService.updateNickName(user.getUserNo(), groupNickNameDto);
        return TResult.success();
    }
}
