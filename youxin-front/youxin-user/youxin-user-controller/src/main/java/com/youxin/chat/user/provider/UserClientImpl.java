package com.youxin.chat.user.provider;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.youxin.chat.user.client.UserClient;
import com.youxin.chat.user.dto.UserAddDto;
import com.youxin.chat.user.dto.UserInfoDto;
import com.youxin.chat.user.mapper.UserInfoMapper;
import com.youxin.chat.user.model.UserInfo;
import com.youxin.chat.user.service.UserService;
import com.youxin.dozer.DozerUtils;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * description: UserClientImpl <br>
 * date: 2020/2/28 16:07 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class UserClientImpl implements UserClient {

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private DozerUtils dozerUtils;

    @Resource
    private UserService userService;

    @Override
    public UserInfoDto getByUserNo(String userNo) {
        UserInfo userInfo = userInfoMapper.selectByUserNo(userNo);
        return dozerUtils.map(userInfo,UserInfoDto.class);
    }

    @Override
    public List<UserInfoDto> getByUserNos(List<String> userNos) {
        List<UserInfo> list = userInfoMapper.selectList(new LambdaQueryWrapper<UserInfo>().in(UserInfo::getUserNo,userNos));
        List<UserInfoDto> users=  list.stream().map(item->{
          return  dozerUtils.map(item,UserInfoDto.class);
        }).collect(Collectors.toList());
        return users;
    }

    @Override
    public void updateUserStates(String id, Integer status) {
        userService.updateUserStates(id,status);
    }

    @Override
    public void saveUser(UserAddDto userAddDto) {
         userService.saveUser(userAddDto);
    }

    @Override
    public UserInfoDto getUserInfoByToken(String token) {
        return userService.getUserInfoByToken(token);
    }
}
