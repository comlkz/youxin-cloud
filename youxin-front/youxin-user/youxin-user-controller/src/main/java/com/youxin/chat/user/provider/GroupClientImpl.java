package com.youxin.chat.user.provider;

import com.youxin.chat.user.client.GroupClient;
import com.youxin.chat.user.enums.GroupMemberTypeEnum;
import com.youxin.chat.user.mapper.GroupBlacklistMapper;
import com.youxin.chat.user.mapper.GroupMapper;
import com.youxin.chat.user.mapper.GroupMemberMapper;
import com.youxin.chat.user.service.GroupService;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;

/**
 * description: GroupClientImpl <br>
 * date: 2020/2/29 15:40 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class GroupClientImpl implements GroupClient {

    @Resource
    private GroupMemberMapper groupMemberMapper;

    @Resource
    private GroupService groupService;

    @Resource
    private GroupBlacklistMapper groupBlacklistMapper;

    @Override
    public int countMemberByGroupNoAndUserNo(String groupNo, String userNo) {
        return groupMemberMapper.countMemberByGroupNoAndUserNo(groupNo,userNo);
    }

    @Override
    public boolean checkUserInBlackList(String userNo, String groupNo) {
        return groupBlacklistMapper.countByGroupNoAndUserNo(groupNo,userNo) > 0;
    }

    @Override
    public boolean checkOperate(String userNo, String groupNo) {
        Integer type = groupMemberMapper.selectUserType(userNo, groupNo);
        if (GroupMemberTypeEnum.NORMAL.getType().equals(type)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void sendGroupWarning(String groupNo) {
        groupService.sendGroupWarning(groupNo);
    }

    @Override
    public void lockGroup(String groupNo) {
        groupService.lockGroup(groupNo);
    }
}
