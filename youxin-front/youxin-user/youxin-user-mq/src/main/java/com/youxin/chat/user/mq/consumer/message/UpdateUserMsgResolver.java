package com.youxin.chat.user.mq.consumer.message;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.youxin.base.MsgDto;
import com.youxin.common.constant.MsgTypeEnum;
import com.youxin.chat.user.mapper.GroupMemberMapper;
import com.youxin.chat.user.mapper.UserFriendMapper;
import com.youxin.chat.user.model.GroupMember;
import com.youxin.chat.user.model.UserFriend;
import com.youxin.chat.user.model.UserInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UpdateUserMsgResolver implements MsgResolver {

    @Resource
    private GroupMemberMapper groupMemberMapper;

    @Resource
    private UserFriendMapper userFriendMapper;

    @Override
    public Boolean supportType(String type) {
        return MsgTypeEnum.MODIFY_USER_INFO.getType().equalsIgnoreCase(type);
    }

    @Override
    public void doResolve(MsgDto msgDto) {
        UserInfo userInfo = JSONObject.parseObject(msgDto.getData(), UserInfo.class);
        GroupMember groupMember = new GroupMember();
        UserFriend userFriend = new UserFriend();
        groupMember.setGroupUser(userInfo.getUserNo());
        groupMember.setAvatar(userInfo.getAvatar());
        groupMember.setGroupNickName(userInfo.getNickName());
        userFriend.setFriendAvatar(userInfo.getAvatar());
        userFriend.setFriendNickName(userInfo.getNickName());
        groupMemberMapper.update(groupMember, new LambdaQueryWrapper<GroupMember>().eq(GroupMember::getGroupUser, userInfo.getUserNo()));
        userFriendMapper.update(userFriend, new LambdaQueryWrapper<UserFriend>().eq(UserFriend::getFriendNo, userInfo.getUserNo()));

    }
}
