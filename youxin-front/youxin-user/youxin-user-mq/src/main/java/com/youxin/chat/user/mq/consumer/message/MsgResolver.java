package com.youxin.chat.user.mq.consumer.message;


import com.youxin.base.MsgDto;

public interface MsgResolver {

     Boolean supportType(String type);

    void doResolve(MsgDto msgDto);
}
