package com.youxin.chat.user.mq.consumer;

import com.alibaba.fastjson.JSONObject;
import com.youxin.base.MsgDto;
import com.youxin.chat.user.manager.ImManager;
import com.youxin.chat.user.mapper.UserDeviceMapper;
import com.youxin.chat.user.mapper.UserInfoMapper;
import com.youxin.chat.user.model.UserInfo;
import com.youxin.chat.user.mq.consumer.message.MsgResolver;
import com.youxin.common.constant.MqConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class CreateUserConsumer {

    private Logger logger = LoggerFactory.getLogger(CreateUserConsumer.class);

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private UserDeviceMapper userDeviceMapper;

    @Resource
    private ImManager imManager;


    @RabbitListener(queues = MqConstant.CREATE_USER_QUEUE)
    @RabbitHandler
    public void get(String userNo) {
        logger.info("收到创建用户通知,data={}", userNo);
        try {
            UserInfo userInfo = userInfoMapper.selectByUserNo(userNo);
            if(userInfo != null) {
               String imToken = imManager.registerUser(userInfo.getUserNo(), userInfo.getNickName(), userInfo.getAvatar());
                userDeviceMapper.updateImTokenByUserNo(userNo,imToken);
            }
        } catch (Exception e) {

        }

    }

}
