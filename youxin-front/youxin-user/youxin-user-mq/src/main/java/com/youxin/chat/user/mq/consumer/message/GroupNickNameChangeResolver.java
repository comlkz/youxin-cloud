package com.youxin.chat.user.mq.consumer.message;

import com.alibaba.fastjson.JSONObject;
import com.youxin.base.MsgDto;
import com.youxin.common.constant.MsgTypeEnum;
import com.youxin.chat.user.manager.ImManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Component
public class GroupNickNameChangeResolver implements MsgResolver {

    private Logger logger = LoggerFactory.getLogger(NicknameProtectResolver.class);

    @Resource
    private ImManager imManager;

    @Override
    public Boolean supportType(String type) {
        return MsgTypeEnum.NICKNAME_GROUP.getType().equalsIgnoreCase(type);
    }

    @Override
    public void doResolve(MsgDto msgDto) {
        logger.info("群成员昵称保护设置，data={}", JSONObject.toJSONString(msgDto));
        try {
            JSONObject jsonObject = JSONObject.parseObject(msgDto.getData());
            String groupNo = jsonObject.getString("groupNo");
            String nickName = jsonObject.getString("nickName");
            String userNo = jsonObject.getString("userNo");
            Map<String,String> map = new HashMap<>();
            map.put("nickName",nickName);
            map.put("groupNo",groupNo);
            map.put("userNo",userNo);

            imManager.sendGroupNickNameMsg(imManager.getSystemNo(),groupNo,map);

        } catch (Exception e) {
            logger.error("群成员昵称保护设置，error=", e);
        }
    }
}
