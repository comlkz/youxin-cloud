package com.youxin.chat.user.mq.consumer.message;

import com.alibaba.fastjson.JSONObject;

import com.youxin.base.MsgDto;
import com.youxin.common.constant.MsgTypeEnum;
import com.youxin.chat.user.manager.ImManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * description: GroupExtMsgResolver <br>
 * date: 2020/1/8 14:09 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Component
public class GroupExtMsgResolver implements MsgResolver{

    private Logger logger = LoggerFactory.getLogger(GroupExtMsgResolver.class);

    @Resource
    private ImManager imManager;


    @Override
    public Boolean supportType(String type) {
        return MsgTypeEnum.GROUP_EXT_CHANGE.getType().equalsIgnoreCase(type);
    }

    @Override
    public void doResolve(MsgDto msgDto) {
        logger.info("群扩展属性设置，data={}", JSONObject.toJSONString(msgDto));
        try {
            JSONObject jsonObject = JSONObject.parseObject(msgDto.getData());
            String groupNo = jsonObject.getString("groupNo");
            String userNo = jsonObject.getString("userNo");
            String msg = null;
            if (jsonObject.containsKey("clearTime")) {
                int value = jsonObject.getInteger("clearTime");
                if (value == -1) {
                    msg = "关闭了群消息自动清理";
                } else if (value == 0) {
                    msg = "设置了已读消息即刻清理，消息将会在退出会话时自动清理";
                } else if (value > 0 && value < 60) {
                    msg = String.format("设置了已读消息在%s秒后自动清理", value);
                } else if (value < 60 * 60) {
                    int miniuteValue = value / 60;
                    msg = String.format("设置了已读消息在%s分钟后自动清理", miniuteValue);
                } else if (value <= 24 * 60 * 60) {
                    int hourValue = value / 60 / 60;
                    msg = String.format("设置了已读消息在%s小时后自动清理", hourValue);
                } else {
                    int dayValue = value / 60 / 60 / 24;
                    msg = String.format("设置了已读消息在%s天后自动清理", dayValue);
                }
                imManager.sendGroupNoticeMsg(imManager.getSystemNo(), groupNo, userNo, msg, "autClear", String.valueOf(value));

            }
            if (jsonObject.containsKey("printScreenNotify")) {
                msg = "开启了\"截屏通知\"";
                int value = jsonObject.getInteger("printScreenNotify");
                if (value == 0) {
                    msg = "关闭了\"截屏通知\"";
                } else if (value == 1) {
                    msg = "开启了\"截屏通知\"";
                }
                imManager.sendGroupNoticeMsg(imManager.getSystemNo(), groupNo, userNo, msg, "printScreenNotify", String.valueOf(value));
            }

        }catch (Exception e){
            logger.error("处理消息扩展属性失败，data={},error={}",msgDto.getData(),e);
        }

    }
}
