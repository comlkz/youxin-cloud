package com.youxin.chat.user.mq.consumer.message;

import com.alibaba.fastjson.JSONObject;
import com.youxin.base.MsgDto;
import com.youxin.common.constant.MsgTypeEnum;
import com.youxin.chat.user.manager.ImManager;
import com.youxin.chat.user.model.UserAdd;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

@Component
public class UserAuditMsgResolver implements MsgResolver {

    private Logger logger = LoggerFactory.getLogger(UserAuditMsgResolver.class);

    @Resource
    private ImManager imManager;


    @Override
    public Boolean supportType(String type) {
        return MsgTypeEnum.USER_AUDIT.getType().equalsIgnoreCase(type);
    }

    @Override
    public void doResolve(MsgDto msgDto) {
        logger.info("处理用户申请通知，data={}", JSONObject.toJSONString(msgDto));
        try {
            UserAdd userAdd = JSONObject.parseObject(msgDto.getData(), UserAdd.class);
            String operation = "Request";
            String msg = StringUtils.isEmpty(userAdd.getRemark()) ? String.format("%s申请添加为好友",userAdd.getAddNickName()) : userAdd.getRemark();
            imManager.sendPrivateMsg(imManager.getSystemNo(),userAdd.getAddUserNo(),msg);
            logger.info("处理用户申请通知完成，data={}", JSONObject.toJSONString(msgDto));

        }catch (Exception e){
            logger.error("处理用户申请通知失败，error=",e);
        }
    }
}
