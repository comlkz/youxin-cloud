package com.youxin.chat.user.mq.consumer.message;

import com.alibaba.fastjson.JSONObject;
import com.youxin.base.MsgDto;
import com.youxin.common.constant.MsgTypeEnum;
import com.youxin.chat.user.manager.ImManager;
import com.youxin.chat.user.mapper.UserInfoMapper;
import com.youxin.chat.user.model.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class MissGroupMsgResolver implements MsgResolver {

    private Logger logger = LoggerFactory.getLogger(MissGroupMsgResolver.class);

    @Resource
    private ImManager imManager;

    @Resource
    private UserInfoMapper userInfoMapper;

    @Override
    public Boolean supportType(String type) {
        return MsgTypeEnum.MISS_GROUP.getType().equalsIgnoreCase(type);
    }

    @Override
    public void doResolve(MsgDto msgDto) {
        logger.info("处理解散群组通知，data={}", JSONObject.toJSONString(msgDto));
        try {
            JSONObject jsonObject = JSONObject.parseObject(msgDto.getData());
            String userNo = jsonObject.getString("userNo");
            String groupNo = jsonObject.getString("groupNo");
            UserInfo userInfo = userInfoMapper.selectByUserNo(userNo);
            Map<String, Object> dataMap = new HashMap<>();
            dataMap.put("operatorNickname", Optional.ofNullable(userInfo.getNickName()).orElse(""));
            String msg = "解散群组";
            imManager.sendGroupMsg("Dismiss",imManager.getSystemNo(), Arrays.asList(groupNo),userNo, JSONObject.toJSONString(dataMap),msg);
          //  TimeUnit.SECONDS.sleep(30);
            imManager.dismissGroup(groupNo, userNo);

            logger.info("处理解散群组通知完成，data={}", JSONObject.toJSONString(msgDto));

        }catch (Exception e){
            logger.info("处理解散群组通知异常，error={}", e);

        }
    }
}
