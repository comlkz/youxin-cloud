package com.youxin.chat.user.mq.config;


import com.youxin.common.constant.MqConstant;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MqConfig {



    /**
     * 默认及时消息交换机
     * @return
     */
    @Bean("defaultTopicExchange")
    public TopicExchange defaultTopicExchange() {
        return new TopicExchange(MqConstant.TOPIC_EXCHANGE, true, false);
    }




    @Bean
    public Queue createMsgSendQueue() {
        return new Queue("msg.user");
    }

    @Bean
    public Queue createUserQueue() {
        return new Queue(MqConstant.CREATE_USER_QUEUE);
    }

    @Bean
    public Queue updateUserQueue() {
        return new Queue("updateUserQueue");
    }

    @Bean
    public Binding msgSendBinding() {
        return BindingBuilder.bind(createMsgSendQueue()).to(defaultTopicExchange()).with("msg.*");
    }


    @Bean
    public Binding creteUserBinding() {
        return BindingBuilder.bind(createMsgSendQueue()).to(defaultTopicExchange()).with("user.create");
    }


    @Bean
    public Binding updateUserBinding() {
        return BindingBuilder.bind(updateUserQueue()).to(defaultTopicExchange()).with("user.update");
    }



}
