package com.youxin.chat.user.mq.consumer.message;

import com.alibaba.fastjson.JSONObject;
import com.youxin.base.MsgDto;
import com.youxin.common.constant.MsgTypeEnum;
import com.youxin.chat.user.manager.ImManager;
import com.youxin.chat.user.mapper.UserInfoMapper;
import com.youxin.chat.user.model.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;

@Component
public class PrintScrennMsgResolver implements MsgResolver {
    private Logger logger = LoggerFactory.getLogger(PrintScrennMsgResolver.class);
    @Resource
    private ImManager imManager;

    @Resource
    private UserInfoMapper userInfoMapper;

    @Override
    public Boolean supportType(String type) {
        return MsgTypeEnum.PRINT_SCREEN.getType().equalsIgnoreCase(type);
    }

    @Override
    public void doResolve(MsgDto msgDto) {
        logger.info("处理截屏通知，data={}", JSONObject.toJSONString(msgDto));
        try{
            JSONObject jsonObject = JSONObject.parseObject(msgDto.getData());
            String userNo = jsonObject.getString("userNo");
            String toChatNo = jsonObject.getString("toChatNo");
            Integer type = Integer.valueOf(jsonObject.getString("type"));
            String msg = null;
            UserInfo userInfo = userInfoMapper.selectByUserNo(userNo);
            if (0 == type) {
                msg = String.format("%s截取了聊天记录", userInfo.getNickName());
                imManager.sendGroupNoticeMsg(imManager.getSystemNo(), toChatNo, userNo, msg, "printScreen", "");
            }else if (1 == type) {
                msg = String.format("%s截取了聊天记录", userInfo.getNickName());
                imManager.sendFriendNoticeMsg(userNo, Arrays.asList(toChatNo), msg,String.valueOf(type));
                imManager.sendFriendNoticeMsg(toChatNo, Arrays.asList(userNo), msg,String.valueOf(type));

            }
        }catch (Exception e){
            logger.info("处理截屏通知失败，error={}", e);
        }
    }
}
