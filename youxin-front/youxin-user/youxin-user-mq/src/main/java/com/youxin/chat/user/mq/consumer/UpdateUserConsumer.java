package com.youxin.chat.user.mq.consumer;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.youxin.chat.user.manager.ImManager;
import com.youxin.chat.user.mapper.GroupMemberMapper;
import com.youxin.chat.user.mapper.UserFriendMapper;
import com.youxin.chat.user.model.GroupMember;
import com.youxin.chat.user.model.UserFriend;
import com.youxin.chat.user.model.UserInfo;
import com.youxin.common.config.YouxinCommonProperties;
import com.youxin.common.constant.MqConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class UpdateUserConsumer {

    private Logger logger = LoggerFactory.getLogger(UpdateUserConsumer.class);

    @Resource
    private GroupMemberMapper groupMemberMapper;

    @Resource
    private UserFriendMapper userFriendMapper;

    @Resource
    private ImManager imManager;

    @Resource
    private YouxinCommonProperties youxinCommonProperties;

    @RabbitListener(queues = "updateUserQueue")
    @RabbitHandler
    public void get(String data) {
        logger.info("收到修改用户通知,data={}", data);
        try {
            UserInfo userInfo = JSONObject.parseObject(data, UserInfo.class);
            GroupMember groupMember = new GroupMember();
            UserFriend userFriend = new UserFriend();
            groupMember.setGroupUser(userInfo.getUserNo());
            groupMember.setAvatar(userInfo.getAvatar());
            groupMember.setGroupNickName(userInfo.getNickName());
            userFriend.setFriendAvatar(userInfo.getAvatar());
            userFriend.setFriendNickName(userInfo.getNickName());
            imManager.updateUser(userInfo.getUserNo(), userInfo.getNickName(), youxinCommonProperties.getDownloadUrl()+ userInfo.getAvatar());
            groupMemberMapper.update(groupMember, new LambdaQueryWrapper<GroupMember>().eq(GroupMember::getGroupUser, userInfo.getUserNo()));
            userFriendMapper.update(userFriend, new LambdaQueryWrapper<UserFriend>().eq(UserFriend::getFriendNo, userInfo.getUserNo()));
        } catch (Exception e) {

        }

    }


}
