package com.youxin.chat.user.mq.consumer.message;

import com.alibaba.fastjson.JSONObject;

import com.youxin.base.MsgDto;
import com.youxin.common.constant.MsgTypeEnum;
import com.youxin.chat.user.manager.ImManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;

@Component
public class FriendExtMsgResolver implements MsgResolver {
    private Logger logger = LoggerFactory.getLogger(FriendExtMsgResolver.class);
    @Resource
    private ImManager imManager;

    @Override
    public Boolean supportType(String type) {
        return MsgTypeEnum.FRIEND_EXT_CHANGE.getType().equalsIgnoreCase(type);
    }

    @Override
    public void doResolve(MsgDto msgDto) {
        logger.info("好友扩展属性设置，data={}", JSONObject.toJSONString(msgDto));
        try {
            JSONObject jsonObject = JSONObject.parseObject(msgDto.getData());
            String friendNo = jsonObject.getString("friendNo");
            String userNo = jsonObject.getString("userNo");
            String msg = null;
            if (jsonObject.containsKey("clearTime")) {
                int value = jsonObject.getInteger("clearTime");
                if (value == -1) {
                    msg = "关闭了自动清理";
                } else if (value == 0) {
                    msg = "对方设置了已读消息即刻清理，消息将会在退出会话时自动清理";
                } else if (value > 0 && value < 60) {
                    msg = String.format("对方设置了已读消息在%s秒后自动清理", value);
                } else if (value < 60 * 60) {
                    int miniuteValue = value / 60;
                    msg = String.format("对方设置了已读消息在%s分钟后自动清理", miniuteValue);
                } else if (value <= 24 * 60 * 60) {
                    int hourValue = value / 60 / 60;
                    msg = String.format("对方设置了已读消息在%s小时后自动清理", hourValue);
                } else {
                    int dayValue = value / 60 / 60 / 24;
                    msg = String.format("对方设置了已读消息在%s天后自动清理", dayValue);
                }
                imManager.sendFriendNoticeMsg(userNo, Arrays.asList(friendNo), msg,String.valueOf(value));
                msg = msg.replace("对方","您");
                imManager.sendFriendNoticeMsg(friendNo, Arrays.asList(userNo), msg,String.valueOf(value));


            }
            if (jsonObject.containsKey("printScreenNotify")) {
                msg = "对方开启了\"截屏通知\"";
                int value = jsonObject.getInteger("printScreenNotify");
                if (value == 0) {
                    msg = "对方关闭了\"截屏通知\"";
                } else if (value == 1) {
                    msg = "对方开启了\"截屏通知\"";
                }
                imManager.sendFriendNoticeMsg(userNo, Arrays.asList(friendNo), msg,String.valueOf(value));
                msg = msg.replace("对方","您");
                imManager.sendFriendNoticeMsg(friendNo, Arrays.asList(userNo), msg,String.valueOf(value));

            }

        }catch (Exception e){
            logger.error("处理消息扩展属性失败，data={},error={}",msgDto.getData(),e);
        }
    }
}
