package com.youxin.chat.user.mq.consumer;

import com.alibaba.fastjson.JSONObject;

import com.youxin.base.MsgDto;
import com.youxin.common.constant.MqConstant;
import com.youxin.chat.user.mq.consumer.message.MsgResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class MsgSendConsumer {

    private Logger logger = LoggerFactory.getLogger(MsgSendConsumer.class);

    @Resource
    private List<MsgResolver> msgResolvers;


    @RabbitListener(queues = "msg.user")
    @RabbitHandler
    public void get(MsgDto msgDto) {
        logger.info("收到发送消息通知,data={}", JSONObject.toJSONString(msgDto));
        try {
            for (MsgResolver msgResolver : msgResolvers) {
                if (msgResolver.supportType(msgDto.getType())) {
                    msgResolver.doResolve(msgDto);

                }
            }
        } catch (Exception e) {

        }

    }

}
