package com.youxin.chat.user.mq.consumer.message;

/*@Component

public class GroupAuditMsgResolver implements MsgResolver {
    private Logger logger = LoggerFactory.getLogger(GroupAuditMsgResolver.class);

    @Reference
    private IChatThirdService chatThirdService;

    @Override
    public Boolean supportType(String type) {
        return MsgTypeEnum.GROUP_AUDIT.getType().equalsIgnoreCase(type);
    }

    @Override
    public void doResolve(MsgDto msgDto) {
        logger.info("处理用户审核结果通知，data={}", JSONObject.toJSONString(msgDto));
        try {
            GroupAdd groupAdd = JSONObject.parseObject(msgDto.getData(), GroupAdd.class);
            String operation;
            String msg;
            if (groupAdd.getAddStatus() == 1) {
                operation = "AcceptResponse";
                msg = String.format("%s通过了您加入群的请求", groupAdd.getAddNickName());
            } else {
                operation = "RejectResponse";
                msg = String.format("%s拒绝了您加入群的请求", groupAdd.getAddNickName());

            }
            chatThirdService.sendPrivateMsg(chatThirdService.getHelperNo(), groupAdd.getUserNo(), msg);
            logger.info("处理用户审核结果通知，data={}", JSONObject.toJSONString(msgDto));

        } catch (Exception e) {
            logger.error("处理用户审核结果通知，error=", e);
        }
    }
}
*/
