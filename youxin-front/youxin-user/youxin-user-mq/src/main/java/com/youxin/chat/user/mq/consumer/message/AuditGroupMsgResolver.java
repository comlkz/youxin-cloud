package com.youxin.chat.user.mq.consumer.message;

import com.alibaba.fastjson.JSONObject;
import com.youxin.base.MsgDto;
import com.youxin.common.constant.MsgTypeEnum;
import com.youxin.chat.user.manager.ImManager;
import com.youxin.chat.user.mapper.GroupMemberMapper;
import com.youxin.chat.user.vo.group.GroupMemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * description: AuditGroupMsgResolver <br>
 * date: 2020/1/14 14:53 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Component
public class AuditGroupMsgResolver implements MsgResolver {

    private static final Logger logger = LoggerFactory.getLogger(AuditGroupMsgResolver.class);

    @Resource
    private ImManager imManager;

    @Resource
    private GroupMemberMapper groupMemberMapper;

    @Override
    public Boolean supportType(String type) {
        return MsgTypeEnum.GROUP_AUDIT.getType().equalsIgnoreCase(type);    }

    @Override
    public void doResolve(MsgDto msgDto) {
        logger.info("群成员邀请通知，data={}", JSONObject.toJSONString(msgDto));
        try {
            JSONObject jsonObject = JSONObject.parseObject(msgDto.getData());
            String groupNo = jsonObject.getString("groupNo");
            String nickName = jsonObject.getString("nickName");
            String userNo = jsonObject.getString("applyUserNo");
            List<GroupMemberVo> groupMemberResps =  groupMemberMapper.listGroupManagers(groupNo);
            List<String> userNos = groupMemberResps.stream().map(GroupMemberVo::getGroupUser).collect(Collectors.toList());
            String msg = String.format("%s申请扫码入群，去确认",nickName);
            Map<String,String> extMap  = new HashMap<>();
            extMap.put("nickName",nickName);
            extMap.put("userNo",userNo);
            extMap.put("groupNo",groupNo);

            imManager.sendGroupCustomNoticeMsg(userNo, groupNo,userNos, userNo,  msg, "groupAudit", JSONObject.toJSONString(extMap));

        } catch (Exception e) {
            logger.error("群成员昵称保护设置，error=", e);
        }
    }
}
