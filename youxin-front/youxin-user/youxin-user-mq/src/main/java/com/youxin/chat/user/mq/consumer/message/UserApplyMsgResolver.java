package com.youxin.chat.user.mq.consumer.message;

import com.alibaba.fastjson.JSONObject;
import com.youxin.base.MsgDto;
import com.youxin.common.constant.MsgTypeEnum;
import com.youxin.chat.user.manager.ImManager;
import com.youxin.chat.user.model.UserAdd;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class UserApplyMsgResolver implements MsgResolver {

    private Logger logger = LoggerFactory.getLogger(UserApplyMsgResolver.class);


    @Resource
    private ImManager imManager;

    @Override
    public Boolean supportType(String type) {
        return MsgTypeEnum.USER_APPLY.getType().equalsIgnoreCase(type);
    }

    @Override
    public void doResolve(MsgDto msgDto) {
        logger.info("处理用户审核结果通知，data={}", JSONObject.toJSONString(msgDto));
        try {
            UserAdd userAdd = JSONObject.parseObject(msgDto.getData(), UserAdd.class);
            String operation  ;
            String msg;
            if(userAdd.getAddStatus() == 1){
                operation = "AcceptResponse";
                msg = String.format("%s通过了您加好友的请求",userAdd.getAddNickName());
            }else{
                operation = "RejectResponse";
                msg = String.format("%s拒绝了您加好友的请求",userAdd.getAddNickName());

            }
            imManager.sendPrivateMsg(userAdd.getAddUserNo(),userAdd.getUserNo(),msg);
            logger.info("处理用户审核结果通知，data={}", JSONObject.toJSONString(msgDto));

        }catch (Exception e){
            logger.error("处理用户审核结果通知，error=",e);
        }
    }
}
