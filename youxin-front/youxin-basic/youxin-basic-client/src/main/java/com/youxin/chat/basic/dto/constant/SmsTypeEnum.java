package com.youxin.chat.basic.dto.constant;

public enum SmsTypeEnum {

    REGISTER(0, 2000, "用户注册"),
    LOGIN(1, 2000, "用户登录"),
    FORGET_LOGIN_PASS(2, 2000, "忘记登录密码"),
    FORGET_PAY_PASS(3, 200, "忘记支付密码"),
    BIND_CARD(4,5,"绑卡短信");
    private String desc;

    private Integer type;

    private Integer maxCount;

    SmsTypeEnum(Integer type, Integer maxCount, String desc) {
        this.type = type;
        this.maxCount = maxCount;
        this.desc = desc;
    }

    public static SmsTypeEnum forType(Integer type) {
        for (SmsTypeEnum smsTypeEnum : SmsTypeEnum.values()) {
            if (Integer.compare(smsTypeEnum.getType(), type) == 0) {
                return smsTypeEnum;
            }
        }
        return null;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(Integer maxCount) {
        this.maxCount = maxCount;
    }
}
