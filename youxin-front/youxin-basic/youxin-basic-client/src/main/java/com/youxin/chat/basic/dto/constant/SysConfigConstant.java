package com.youxin.chat.basic.dto.constant;

public interface SysConfigConstant {

    String SMS_CONFIG_INFO = "sms.config.info";

    String SMS_TYPE_INFO = "sms.type.info";

    String FILTER_MOBILE = "sms.user.filter";

    String MOBILE_ACCOUNT = "mobile.charge.account";

    String BIND_CARD = "pay.bind.card";

    String PAY_ACCOUNT = "pay.account";

    String NOTIFY_URL = "pay.notify.url";

    String DISCOVER_URL = "sys.config.info";

    String CHANNEL_RATE = "channel.rate";
}
