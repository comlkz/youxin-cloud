package com.youxin.chat.basic.dto.file;

import java.io.File;
import java.io.Serializable;

/**
 * Class Describe
 * <p>
 * User: yangguang
 * Date: 17/7/19
 * Time: 下午4:12
 */
public class FileDto implements Serializable {
    private static final long serialVersionUID = 2834651570866680355L;
    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件流
     */
    private String   suffix;

    private String filePath;

    private String fileId;

    /**
     * 那个用户上传的
     */
    private String account;
    /**
     * 文件大小
     */
    private Long fileSize;
    /**
     * 文件
     */
    private File file;

    private Integer type;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
