package com.youxin.chat.basic.dto.constant;

public class CommonConstant {

     public static class UserStatus{

         public static final int ON_LINE = 0;

         public static final int LOCK = 1;
     }
}
