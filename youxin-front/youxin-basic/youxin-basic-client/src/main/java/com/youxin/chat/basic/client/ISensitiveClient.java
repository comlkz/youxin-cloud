package com.youxin.chat.basic.client;


import com.youxin.base.TPage;
import com.youxin.chat.basic.dto.SensitiveDto;
import com.youxin.chat.basic.dto.SensitiveSearch;
import com.youxin.exception.SystemException;

public interface ISensitiveClient {

    void addSensitive(SensitiveDto sensitiveDto, String userName) throws SystemException;

    void removeSensitive(Long id) throws SystemException;

    TPage<SensitiveDto> list(SensitiveSearch sensitiveSearch) throws SystemException;
}
