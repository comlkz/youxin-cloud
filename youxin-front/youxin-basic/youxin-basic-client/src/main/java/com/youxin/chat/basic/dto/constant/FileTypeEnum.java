package com.youxin.chat.basic.dto.constant;

public enum FileTypeEnum {

    USER_AVATAR(0, "/image/user/", "用户头像"),
    GROUOP_AVATAR(1, "/image/group/", "群组头像"),
    APP_PACKAGE(2, "/app/package/", "应用包"),
    COLLECT(3, "/collect/", "收藏图片");
    private Integer type;

    private String path;

    private String desc;

    FileTypeEnum(Integer type, String path, String desc) {
        this.type = type;
        this.path = path;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public String getPath() {
        return path;
    }

    public String getDesc() {
        return desc;
    }

    public static FileTypeEnum forType(Integer type) {
        for (FileTypeEnum fileTypeEnum : FileTypeEnum.values()) {
            if (fileTypeEnum.getType().equals(type)) {
                return fileTypeEnum;
            }
        }
        return null;
    }
}
