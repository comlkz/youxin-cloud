package com.youxin.chat.basic.dto.constant;

public interface RiskKey {

    String PER_CASH_AMOUNT = "perCashAmount";

    String MAX_DAY_CASH_AMOUNT = "maxDayCashAmount";

    String MAX_DAY_COUNT = "maxDayCount";

    String RECHARGE_ONCE_LIMIT = "rechargeOnceLimit";

    String RECHARGE_DAY_LIMIT = "rechargeDayLimit";
}
