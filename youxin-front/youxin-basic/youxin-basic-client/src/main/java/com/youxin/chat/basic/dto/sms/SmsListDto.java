package com.youxin.chat.basic.dto.sms;

import com.youxin.base.TQuery;
import io.swagger.annotations.ApiModelProperty;

public class SmsListDto extends TQuery {
    @ApiModelProperty(value = "手机号",required = false)
    private String mobile;

    @ApiModelProperty(value = "类型，0：注册；1：登录，2：忘记登录密码",required = false)
    private Integer smsType;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getSmsType() {
        return smsType;
    }

    public void setSmsType(Integer smsType) {
        this.smsType = smsType;
    }
}
