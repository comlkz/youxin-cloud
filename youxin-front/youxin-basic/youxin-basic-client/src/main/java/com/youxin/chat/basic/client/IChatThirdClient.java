package com.youxin.chat.basic.client;


import com.youxin.exception.SystemException;

import java.util.List;
import java.util.Map;

public interface IChatThirdClient {

    String getSystemNo();
    /**
     * 注册用户
     *
     * @param userNo
     * @param nickName
     * @param avatar
     * @return token信息
     */
    String registerUser(String userNo, String nickName, String avatar) throws SystemException;

    /**
     * 修改用户
     *
     * @param userNo
     * @param nickName
     * @param avatar
     */
    void updateUser(String userNo, String nickName, String avatar) throws SystemException;

    /**
     * 黑名单设置
     *
     * @param userNo
     * @param friendNo
     * @param status
     */
    void blackUser(String userNo, String friendNo, Integer status) throws SystemException;

    /**
     * 创建群组
     *
     * @param groupId
     * @param groupName
     * @param userNos
     */
    void createGroup(String groupId, String groupName, List<String> userNos) throws SystemException;

    /**
     * 加入群组
     *
     * @param groupId
     * @param groupName
     * @param userNos
     */
    void joinGroup(String groupId, String groupName, List<String> userNos) throws SystemException;

    /**
     * 退出群组
     *
     * @param groupId
     * @param
     */
    void quitGroup(String groupId, List<String> userNos) throws SystemException;

    void dismissGroup(String groupId, String userNo) throws SystemException;

    /**
     * 添加白名单功能
     * @param userNo
     * @param whiteUserNos
     */
    void addWhiteList(String userNo, List<String> whiteUserNos);


    /**
     * 删除白名单功能
     * @param userNo
     * @param whiteUserNos
     */
    void removeWhiteList(String userNo, List<String> whiteUserNos);

    /**
     * 修改群组
     *
     * @param groupId
     * @param groupName
     */
    void updateGroup(String groupId, String groupName) throws SystemException;

    /**
     * 添加敏感词
     *
     * @param keyWord
     * @param type
     * @param replace
     */
    void addSensitive(String keyWord, Integer type, String replace) throws SystemException;

    /**
     * 删除敏感词
     *
     * @param keyWords
     */
    void removeSensitive(String keyWords) throws SystemException;

    /**
     * 锁定用户
     *
     * @param id
     */
    void addBlockUser(String id) throws SystemException;

    /***
     * 解禁用户
     * @param id
     */
    void removeBlockUser(String id) throws SystemException;

    /**
     * 执行用户添加申请操作
     *
     * @param
     * @param fromUserId
     * @param toUserId
     * @param msg
     */
    void sendPrivateMsg(String fromUserId, String toUserId, String msg) throws SystemException;

    void sendGroupMsg(String operation, String fromUserId, List<String> toUserIds, String operatorUserId, String data, String msg) throws SystemException;


    void sendGroupNoticeMsg(String fromUserId, List<String> toUserIds, String operatorUserId, String msg, String operation, String ext) throws SystemException;

    void sendFriendNoticeMsg(String fromUserId, List<String> toUserIds, String msg) throws SystemException;

    void setFriendDisturb(String userNo, int friendId, int type) throws SystemException;

    void setGroupDisturb(String userNo, String groupId, int type) throws SystemException;

    void pushMsg(String message, List<Integer> type, List<String> userIds, Integer isToAll) throws SystemException;

    void setBanned(String groupId) throws SystemException;

    void addBannedWhitelist(String groupId, List<String> managerId) throws SystemException;

    void removeBannedWhitelist(String groupId, List<String> managerId) throws SystemException;


    void setBannedOpen(String groupId) throws SystemException;

    /**
     * @param fromUserId
     * @param toUserId
     * @param map
     */
    void sendPrivateRedReceiveMsg(String fromUserId, String toUserId, Map<String, Object> map) throws SystemException;

    /**
     * @param fromUserId
     * @param toUserId
     * @param map
     */
    void sendGroupRedReceiveMsg(String fromUserId, String toUserId, Map<String, Object> map);

    void sendPayMsg(String toUserId, Map<String, Object> map) throws SystemException;

    void sendShareMsg(String fromUserId, List<Map<Integer, String>> toUserIds, Map<String, Object> dataMap);

    void sendGroupNoticeMsg(String userId, String groupId, String msg);

    void sendGroupNickNameMsg(String userId, String groupId, Map<String, String> data);

    String getHelperNo();

}
