package com.youxin.chat.basic.dto.sms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel(description = "短信请求参数")
public class SmsReqDto implements Serializable {

    @ApiModelProperty(value = "手机号",required = true)
    @NotNull(message = "手机号不能为空")
    private String phone;

    @ApiModelProperty(value = "类型，0：注册；1：登录，2：忘记登录密码",required = true)
    @NotNull(message = "类型不参为空")
    private Integer type;



    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }




}
