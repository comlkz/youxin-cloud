package com.youxin.chat.basic.client;


import com.youxin.base.TPage;
import com.youxin.chat.basic.dto.SysConfigDto;
import com.youxin.exception.SystemException;

public interface ISysConfigClient {

    void saveConfig(SysConfigDto sysConfigDto) throws SystemException;

    TPage<SysConfigDto> page(String key, Integer currentPage, Integer showCount) throws SystemException;

    String getConfigValue(String key) throws SystemException;

    void saveConfigBySysKey(SysConfigDto sysConfigDto) throws SystemException;
}
