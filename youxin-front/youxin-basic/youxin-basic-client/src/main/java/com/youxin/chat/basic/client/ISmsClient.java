package com.youxin.chat.basic.client;


import com.youxin.base.TPage;
import com.youxin.chat.basic.dto.sms.SmsDto;
import com.youxin.chat.basic.dto.sms.SmsListDto;
import com.youxin.exception.SystemException;

public interface ISmsClient {

    void sendSms(Integer type, String phone, String content, Long ip) throws SystemException;

    TPage<SmsDto> getSmsList(SmsListDto smsListDto) throws SystemException;

}
