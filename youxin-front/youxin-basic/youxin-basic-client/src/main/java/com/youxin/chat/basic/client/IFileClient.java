package com.youxin.chat.basic.client;

import com.youxin.chat.basic.dto.file.FileDto;
import com.youxin.chat.basic.dto.file.SysFileDto;
import com.youxin.exception.SystemException;

import java.util.List;

public interface IFileClient {

    /**
     * 新增文件
     *
     * @param fileDto
     * @return
     */
    String add(FileDto fileDto, Integer type) throws SystemException;

    /**
     * 下载文件
     *
     * @param fileId
     * @return
     */
    FileDto download(String fileId) throws SystemException;



    String getFileUrl() throws SystemException;


    List<SysFileDto> getFilesByIds(List<String> fileIds) throws SystemException;

    String saveFile(SysFileDto sysFile) throws SystemException;

    String getImageUrl(Integer type, String fileId) throws SystemException;

    String primaryId(String prefix) throws SystemException;

    String getRealPath() throws SystemException;

    void removeFile(String fileId) throws SystemException;
}
