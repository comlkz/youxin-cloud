package com.youxin.chat.basic.dto;


import com.youxin.base.TQuery;

public class SensitiveSearch extends TQuery {

    private String keyWord;

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }
}
