package com.youxin.chat.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.basic.model.Helper;

/**
 * description: HelperMapper <br>
 * date: 2020/3/8 17:36 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface HelperMapper extends BaseMapper<Helper> {
}
