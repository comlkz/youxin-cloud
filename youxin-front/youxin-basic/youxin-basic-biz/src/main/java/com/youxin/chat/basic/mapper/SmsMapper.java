package com.youxin.chat.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.youxin.chat.basic.dto.sms.SmsDto;
import com.youxin.chat.basic.dto.sms.SmsListDto;
import com.youxin.chat.basic.model.Sms;
import org.apache.ibatis.annotations.Param;

public interface SmsMapper extends BaseMapper<Sms> {
    IPage<SmsDto> getSmsList(Page page, @Param("smsListDto") SmsListDto smsListDto);
}
