package com.youxin.chat.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.basic.model.Version;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * description: VersionMapper <br>
 * date: 2020/3/9 11:52 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface VersionMapper extends BaseMapper<Version> {

    @Select("select * from t_version where status = #{status} order by crt_time desc limit 1")
    Version getVersion(@Param("status") int status);

}
