package com.youxin.chat.basic.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.base.TPage;
import com.youxin.chat.basic.dto.SensitiveDto;
import com.youxin.chat.basic.dto.SensitiveSearch;
import com.youxin.chat.basic.model.Sensitive;
import com.youxin.exception.SystemException;

public interface SensitiveService extends IService<Sensitive> {

    void addSensitive(SensitiveDto sensitiveDto, String userName) throws SystemException;

    void removeSensitive(Long id) throws SystemException;

    TPage<SensitiveDto> list(SensitiveSearch sensitiveSearch) throws SystemException;
}
