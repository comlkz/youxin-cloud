package com.youxin.chat.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.basic.model.SysConfig;

public interface SysConfigMapper extends BaseMapper<SysConfig> {
}
