package com.youxin.chat.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.basic.model.SysFile;

public interface SysFileMapper extends BaseMapper<SysFile> {
}
