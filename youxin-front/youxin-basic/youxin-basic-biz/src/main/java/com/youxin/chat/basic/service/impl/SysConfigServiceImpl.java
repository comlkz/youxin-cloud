package com.youxin.chat.basic.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.base.TPage;
import com.youxin.chat.basic.convert.SysConfigConvert;
import com.youxin.chat.basic.dto.SysConfigDto;
import com.youxin.chat.basic.mapper.SysConfigMapper;
import com.youxin.chat.basic.model.SysConfig;
import com.youxin.chat.basic.service.SysConfigService;
import com.youxin.common.constant.RedisKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements SysConfigService {

     private static final Logger logger = LoggerFactory.getLogger(SysConfigServiceImpl.class);

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void saveConfig(SysConfigDto sysConfigDto) {
        SysConfig sysConfig = SysConfigConvert.dtoToModel(sysConfigDto);
       saveOrUpdate(sysConfig);
        stringRedisTemplate.delete(RedisKey.SYS_CONFIG_PREFIX + sysConfig.getSysKey());
        stringRedisTemplate.opsForValue().set(RedisKey.SYS_CONFIG_PREFIX + sysConfig.getSysKey(), sysConfig.getSysValue(),24 * 60 * 60, TimeUnit.SECONDS);
    }

    @Override
    public TPage<SysConfigDto> page(String key, Integer currentPage, Integer showCount) {
        LambdaQueryWrapper<SysConfig> queryWrapper = new LambdaQueryWrapper<SysConfig>();
        if (!StringUtils.isEmpty(key)) {
            queryWrapper.like(SysConfig::getSysKey, key);
        }
        IPage<SysConfig> page = page(new Page<>(currentPage, showCount), queryWrapper);
        List<SysConfigDto> list = page.getRecords().stream().map(SysConfigConvert::modelToDto).collect(Collectors.toList());
        return TPage.page(list, page.getTotal());
    }

    @Override
    public String getConfigValue(String key) {
        String value = (String) stringRedisTemplate.opsForValue().get(RedisKey.SYS_CONFIG_PREFIX + key);
        logger.info("value={}",value);

        if (!StringUtils.isEmpty(value)) {
            return value;
        }
        SysConfig sysConfig = getOne(new LambdaQueryWrapper<SysConfig>().eq(SysConfig::getSysKey, key));
        if (sysConfig != null && !StringUtils.isEmpty(sysConfig.getSysValue())) {
            stringRedisTemplate.opsForValue().set(RedisKey.SYS_CONFIG_PREFIX + sysConfig.getSysKey(), sysConfig.getSysValue(), 24 * 60 * 60, TimeUnit.SECONDS);
            return sysConfig.getSysValue();
        }
        return null;
    }

    @Override
    public void saveConfigBySysKey(SysConfigDto sysConfigDto)  {
        LambdaQueryWrapper<SysConfig> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysConfig::getSysKey,sysConfigDto.getSysKey());
        SysConfig sysConfig = getOne(queryWrapper);
        if(sysConfig == null){
            sysConfig = SysConfigConvert.dtoToModel(sysConfigDto);
        }
        sysConfig.setSysValue(sysConfigDto.getSysValue());
        saveOrUpdate(sysConfig);
        stringRedisTemplate.delete(RedisKey.SYS_CONFIG_PREFIX + sysConfig.getSysKey());
        stringRedisTemplate.opsForValue().set(RedisKey.SYS_CONFIG_PREFIX + sysConfig.getSysKey(), sysConfig.getSysValue(), 24 * 60 * 60, TimeUnit.SECONDS);
    }
}
