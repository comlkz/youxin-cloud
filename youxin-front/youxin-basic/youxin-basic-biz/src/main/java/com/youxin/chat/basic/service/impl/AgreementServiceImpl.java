package com.youxin.chat.basic.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.chat.basic.mapper.ChatAgreementMapper;
import com.youxin.chat.basic.model.ChatAgreement;
import com.youxin.chat.basic.service.AgreementService;
import org.springframework.stereotype.Service;

/**
 * description: AgreementServiceImpl <br>
 * date: 2020/3/8 16:52 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class AgreementServiceImpl extends ServiceImpl<ChatAgreementMapper, ChatAgreement> implements AgreementService {
    @Override
    public ChatAgreement getByType(Integer type) {
        LambdaQueryWrapper<ChatAgreement> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ChatAgreement::getAgKey, type);
        ChatAgreement agreement = this.baseMapper.selectOne(queryWrapper);
        return agreement;
    }
}
