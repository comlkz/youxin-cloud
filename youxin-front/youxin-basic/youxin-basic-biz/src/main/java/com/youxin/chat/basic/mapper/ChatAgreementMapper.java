package com.youxin.chat.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.basic.model.ChatAgreement;

/**
 * description: ChatAgreementMapper <br>
 * date: 2020/3/8 16:50 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface ChatAgreementMapper extends BaseMapper<ChatAgreement> {
}
