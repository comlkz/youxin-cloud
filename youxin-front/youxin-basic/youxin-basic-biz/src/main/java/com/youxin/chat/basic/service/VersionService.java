package com.youxin.chat.basic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.basic.entity.vo.VersionVo;
import com.youxin.chat.basic.model.Version;

/**
 * description: VersionService <br>
 * date: 2020/3/9 11:52 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface VersionService extends IService<Version> {

    VersionVo getVersion(Integer status);
}
