package com.youxin.chat.basic.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.base.BaseResultCode;
import com.youxin.base.TPage;
import com.youxin.chat.basic.dto.SensitiveDto;
import com.youxin.chat.basic.dto.SensitiveSearch;
import com.youxin.chat.basic.mapper.SensitiveMapper;
import com.youxin.chat.basic.model.Sensitive;
import com.youxin.chat.basic.service.ChatThirdService;
import com.youxin.chat.basic.service.SensitiveService;
import com.youxin.exception.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class SensitiveServiceImpl extends ServiceImpl<SensitiveMapper, Sensitive> implements SensitiveService {

    private Logger logger = LoggerFactory.getLogger(SensitiveServiceImpl.class);


    @Resource
    private ChatThirdService chatThirdService;

    @Override
    public void addSensitive(SensitiveDto sensitiveDto, String userName)  {
        if (sensitiveDto.getKeyWord().contains(",")) {
            String str[] = sensitiveDto.getKeyWord().split(",");
            List<Sensitive> list = new ArrayList<>();
            for (int i = 0; i < str.length; i++) {
                int count = count(
                        new LambdaQueryWrapper<Sensitive>().eq(Sensitive::getKeyWord, str[i]));
                if (count > 0) {
                    throw new SystemException(BaseResultCode.RECORD_EXISTS, "敏感词不能重复");
                }
                chatThirdService.addSensitive(str[i], sensitiveDto.getWordType(), sensitiveDto.getReplaceWord());
                list.add(buildModel(sensitiveDto, userName, str[i]));
            }
            saveBatch(list, str.length);
        } else {
            int count = count(
                    new LambdaQueryWrapper<Sensitive>().eq(Sensitive::getKeyWord, sensitiveDto.getKeyWord()));
            if (count > 0) {
                throw new SystemException(BaseResultCode.RECORD_EXISTS, "敏感词不能重复");
            }
            chatThirdService.addSensitive(sensitiveDto.getKeyWord(), sensitiveDto.getWordType(), sensitiveDto.getReplaceWord());
            Sensitive sensitive = buildModel(sensitiveDto, userName, sensitiveDto.getKeyWord());
            save(sensitive);
        }
    }

    @Override
    public void removeSensitive(Long id) throws SystemException {
        Sensitive sensitive = getById(id);
        if (sensitive == null) {
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "敏感词不存在");
        }
        chatThirdService.removeSensitive(sensitive.getKeyWord());
        removeById(id);
    }

    @Override
    public TPage<SensitiveDto> list(SensitiveSearch sensitiveSearch) throws SystemException {
        LambdaQueryWrapper<Sensitive> queryWrapper = new LambdaQueryWrapper<Sensitive>();
        if (!StringUtils.isEmpty(sensitiveSearch.getKeyWord())) {
            queryWrapper.like(Sensitive::getKeyWord, sensitiveSearch.getKeyWord());
        }
        IPage page = page(new Page<>(sensitiveSearch.getCurrentPage(), sensitiveSearch.getShowCount()), queryWrapper);
        List<SensitiveDto> list = JSONObject.parseArray(JSONObject.toJSONString(page.getRecords()), SensitiveDto.class);
        return TPage.page(list, page.getTotal());
    }

    private Sensitive buildModel(SensitiveDto sensitiveDto, String userName, String keyWord) {
        Sensitive sensitive = new Sensitive();
        sensitive.setKeyWord(keyWord);
        sensitive.setReplaceWord(sensitiveDto.getReplaceWord());
        sensitive.setWordType(sensitiveDto.getWordType());
        sensitive.setCreateTime(LocalDateTime.now());
        sensitive.setUpdateTime(LocalDateTime.now());
        sensitive.setCreateUser(userName);
        return sensitive;
    }

}
