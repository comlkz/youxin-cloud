package com.youxin.chat.basic.message;

import io.rong.messages.BaseMessage;
import io.rong.util.GsonUtil;

import java.util.Map;

public class PayNotifyMessage extends BaseMessage {
    //操作人用户 Id
    private String operatorUserId;

    //操作名称
    private String operation;

    //群组中各种通知的操作数据
    private Map<String, Object> data;


    private transient static final String TYPE = "GR:PayNotifyMsg";


    public PayNotifyMessage(String operatorUserId, String operation, Map<String, Object> data) {
        this.operatorUserId = operatorUserId;
        this.operation = operation;
        this.data = data;
    }


    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public String toString() {

        return GsonUtil.toJson(this, PayNotifyMessage.class);
    }

    public String getOperatorUserId() {
        return operatorUserId;
    }

    public void setOperatorUserId(String operatorUserId) {
        this.operatorUserId = operatorUserId;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
