package com.youxin.chat.basic.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.base.TPage;
import com.youxin.chat.basic.dto.sms.SmsDto;
import com.youxin.chat.basic.dto.sms.SmsListDto;
import com.youxin.chat.basic.model.Sms;


public interface SmsService extends IService<Sms> {

    void sendSms(Integer type, String phone, String content, Long ip) ;

    TPage<SmsDto> getSmsList(SmsListDto smsListDto);
}
