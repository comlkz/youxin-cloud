package com.youxin.chat.basic.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.base.TPage;
import com.youxin.chat.basic.dto.SysConfigDto;
import com.youxin.chat.basic.model.SysConfig;

public interface SysConfigService extends IService<SysConfig> {

    void saveConfig(SysConfigDto sysConfigDto);

    TPage<SysConfigDto> page(String key, Integer currentPage, Integer showCount) ;

    String getConfigValue(String key);

    void saveConfigBySysKey(SysConfigDto sysConfigDto) ;
}
