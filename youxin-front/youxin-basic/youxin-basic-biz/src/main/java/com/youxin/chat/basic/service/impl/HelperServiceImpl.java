package com.youxin.chat.basic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.chat.basic.entity.vo.HelperVo;
import com.youxin.chat.basic.mapper.HelperMapper;
import com.youxin.chat.basic.model.Helper;
import com.youxin.chat.basic.service.HelperService;
import com.youxin.dozer.DozerUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * description: HelperService <br>
 * date: 2020/3/8 16:43 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class HelperServiceImpl extends ServiceImpl<HelperMapper, Helper> implements HelperService {

    @Resource
    private DozerUtils dozerUtils;

    @Override
    public HelperVo getHelper(int id) {
        Helper helper = getById(id);
        return dozerUtils.map(helper,HelperVo.class);
    }

    @Override
    public List<HelperVo> listAll() {
        List<Helper> list = list();
        List<HelperVo> helperVos = list.stream().map(item-> dozerUtils.map(item,HelperVo.class)).collect(Collectors.toList());
        return helperVos;
    }


}
