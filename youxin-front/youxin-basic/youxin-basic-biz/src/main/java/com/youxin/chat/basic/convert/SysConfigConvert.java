package com.youxin.chat.basic.convert;



import com.youxin.chat.basic.dto.SysConfigDto;
import com.youxin.chat.basic.model.SysConfig;

import java.util.Optional;

public class SysConfigConvert {

    public static SysConfig dtoToModel(SysConfigDto sysConfigDto){
        SysConfig sysConfig = new SysConfig();
        if(!Optional.ofNullable(sysConfigDto).isPresent()){
            return sysConfig;
        }
        sysConfig.setId(sysConfigDto.getId());
        sysConfig.setSysKey(sysConfigDto.getSysKey());
        sysConfig.setSysValue(sysConfigDto.getSysValue());
        sysConfig.setPid(0);
        sysConfig.setDescription(sysConfigDto.getDescription());
        return sysConfig;
    }

    public static SysConfigDto modelToDto(SysConfig sysConfig){
        SysConfigDto sysConfigDto = new SysConfigDto();
        if(!Optional.ofNullable(sysConfig).isPresent()){
            return sysConfigDto;
        }
        sysConfigDto.setId(sysConfig.getId());
        sysConfigDto.setSysKey(sysConfig.getSysKey());
        sysConfigDto.setSysValue(sysConfig.getSysValue());
        sysConfigDto.setDescription(sysConfig.getDescription());
        return sysConfigDto;
    }
}
