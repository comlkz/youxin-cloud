package com.youxin.chat.basic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.basic.entity.vo.HelperVo;
import com.youxin.chat.basic.model.Helper;

import java.util.List;

/**
 * description: HelperService <br>
 * date: 2020/3/8 16:43 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface HelperService extends IService<Helper> {

    HelperVo getHelper(int id);

    List<HelperVo> listAll();
}
