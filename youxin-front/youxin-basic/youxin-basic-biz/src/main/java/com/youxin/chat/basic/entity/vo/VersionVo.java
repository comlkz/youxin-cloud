package com.youxin.chat.basic.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel(description = "系统版本表")
public class VersionVo  {

    private Integer id;
    @ApiModelProperty(value = "发布人", required = false)
    private String issuer;
    @ApiModelProperty(value = "版本号", required = false)
    private String versions;
    @ApiModelProperty(value = "描述", required = false)
    private String description;
    @ApiModelProperty(value = "是否强制更新 0：不需要 1：需要", required = false)
    private Integer updating;
    @ApiModelProperty(value = "0：安卓 1：IOS", required = false)
    private Integer status;
    @ApiModelProperty(value = "0：失效 1：有效", required = false)
    private Integer valid;
    @ApiModelProperty(value = "创建时间", required = false)
    private Date crtTime;
    @ApiModelProperty(value = "更新包地址", required = false)
    private String url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getVersions() {
        return versions;
    }

    public void setVersions(String versions) {
        this.versions = versions;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getUpdating() {
        return updating;
    }

    public void setUpdating(Integer updating) {
        this.updating = updating;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getValid() {
        return valid;
    }

    public void setValid(Integer valid) {
        this.valid = valid;
    }

    public Date getCrtTime() {
        return crtTime;
    }

    public void setCrtTime(Date crtTime) {
        this.crtTime = crtTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
