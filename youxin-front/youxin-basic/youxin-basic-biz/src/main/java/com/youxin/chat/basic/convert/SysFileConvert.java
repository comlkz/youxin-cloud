package com.youxin.chat.basic.convert;

import com.youxin.chat.basic.dto.file.FileDto;
import com.youxin.chat.basic.dto.file.SysFileDto;
import com.youxin.chat.basic.model.SysFile;

import java.util.Date;

/**
 * description: SysFileConvert <br>
 * date: 2020/1/13 14:58 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class SysFileConvert {

    public static SysFile dtoToModel(FileDto fileDto, Integer type){
        SysFile record = new SysFile();
        record.setFileType(type);
        record.setStatus(1);
        record.setSuffix(fileDto.getSuffix());
        record.setFilePath(fileDto.getFilePath());
        record.setFileName(fileDto.getFileName());
        record.setFileSize(fileDto.getFileSize());
        record.setId(fileDto.getFileId());
        record.setVirtualId(record.getId());
        // record.setSvrAddr(guoliaoConfig.getFile().getAddr());
        record.setUpdateBy(fileDto.getAccount());
        record.setUpdateTime(new Date());
        record.setCreateBy(fileDto.getAccount());
        record.setCreateTime(new Date());
        record.setUseCount(1);
        return record;
    }

    public static SysFile dtoToModel(SysFileDto sysFileDto) {
        SysFile sysFile = new SysFile();
        sysFile.setId(sysFileDto.getId());
        sysFile.setVirtualId(sysFileDto.getVirtualId());
        sysFile.setFileName(sysFileDto.getFileName());
        sysFile.setFileMd5(sysFileDto.getFileMd5());
        sysFile.setSuffix(sysFileDto.getSuffix());
        sysFile.setFileType(sysFileDto.getFileType());
        sysFile.setFileSize(sysFileDto.getFileSize());
        sysFile.setSourceType(sysFileDto.getSourceType());
        sysFile.setFilePath(sysFileDto.getFilePath());
        sysFile.setSvrAddr(sysFileDto.getSvrAddr());
        sysFile.setStatus(sysFileDto.getStatus());
        sysFile.setUseCount(sysFileDto.getUseCount());
        sysFile.setUpdateBy(sysFileDto.getUpdateBy());
        sysFile.setUpdateTime(sysFileDto.getUpdateTime());
        sysFile.setCreateBy(sysFileDto.getCreateBy());
        sysFile.setCreateTime(sysFileDto.getCreateTime());
        return sysFile;
    }


    public static SysFileDto modelToDto(SysFile sysFile) {
        SysFileDto sysFileDto = new SysFileDto();
        sysFileDto.setId(sysFile.getId());
        sysFileDto.setVirtualId(sysFile.getVirtualId());
        sysFileDto.setFileName(sysFile.getFileName());
        sysFileDto.setFileMd5(sysFile.getFileMd5());
        sysFileDto.setSuffix(sysFile.getSuffix());
        sysFileDto.setFileType(sysFile.getFileType());
        sysFileDto.setFileSize(sysFile.getFileSize());
        sysFileDto.setSourceType(sysFile.getSourceType());
        sysFileDto.setFilePath(sysFile.getFilePath());
        sysFileDto.setSvrAddr(sysFile.getSvrAddr());
        sysFileDto.setStatus(sysFile.getStatus());
        sysFileDto.setUseCount(sysFile.getUseCount());
        sysFileDto.setUpdateBy(sysFile.getUpdateBy());
        sysFileDto.setUpdateTime(sysFile.getUpdateTime());
        sysFileDto.setCreateBy(sysFile.getCreateBy());
        sysFileDto.setCreateTime(sysFile.getCreateTime());
        return sysFileDto;
    }
}
