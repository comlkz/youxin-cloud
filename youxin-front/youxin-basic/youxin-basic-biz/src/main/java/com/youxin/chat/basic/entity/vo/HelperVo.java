package com.youxin.chat.basic.entity.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class HelperVo implements Serializable {
    private Integer id;
    @ApiModelProperty(value = "问题",required = false)
    private String question;
    @ApiModelProperty(value = "答案",required = false)
    private String answer;
    @ApiModelProperty(value = "格式",required = false)
    private String format;
    @ApiModelProperty(value = "摘要",required = false)
    private String breviary;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getBreviary() {
        return breviary;
    }

    public void setBreviary(String breviary) {
        this.breviary = breviary;
    }
}
