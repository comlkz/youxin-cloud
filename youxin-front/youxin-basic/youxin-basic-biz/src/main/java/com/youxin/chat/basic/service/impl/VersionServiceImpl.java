package com.youxin.chat.basic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.chat.basic.entity.vo.VersionVo;
import com.youxin.chat.basic.mapper.VersionMapper;
import com.youxin.chat.basic.model.Version;
import com.youxin.chat.basic.service.VersionService;
import com.youxin.common.config.YouxinCommonProperties;
import com.youxin.dozer.DozerUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * description: VersionServiceImpl <br>
 * date: 2020/3/9 11:53 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class VersionServiceImpl extends ServiceImpl<VersionMapper, Version> implements VersionService {

    @Resource
    private DozerUtils dozerUtils;

    @Resource
    private YouxinCommonProperties youxinCommonProperties;

    @Override
    public VersionVo getVersion(Integer status) {
        Version version = this.getBaseMapper().getVersion(status);
        VersionVo versionVo =  dozerUtils.map(version,VersionVo.class);
        versionVo.setUrl(youxinCommonProperties.getDownloadUrl()+ versionVo.getUrl());
        return versionVo;
    }
}
