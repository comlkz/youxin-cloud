package com.youxin.chat.basic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.basic.model.ChatAgreement;

/**
 * description: AgreementService <br>
 * date: 2020/3/8 16:51 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface AgreementService extends IService<ChatAgreement> {

    ChatAgreement getByType(Integer type);
}
