package com.youxin.chat.basic.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.chat.basic.dto.file.FileDto;
import com.youxin.chat.basic.dto.file.SysFileDto;
import com.youxin.chat.basic.model.SysFile;
import com.youxin.exception.SystemException;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

public interface SysFileService extends IService<SysFile> {

    public String add(FileDto fileDto, Integer type);

    String saveFile(MultipartFile file,Integer type,String userNo);


    String saveFile(String parentPath, String fileName, InputStream inputStream);

    String getFileUrl();


    List<SysFileDto> getFilesByIds(List<String> fileIds);

    String saveFile(SysFileDto sysFileDto);

    String getImageUrl(Integer type, String fileId);

    String primaryId(String prefix);

    FileDto download(String fileId) throws SystemException;
}
