package com.youxin.chat.basic.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * description: YouxinConfig <br>
 * date: 2020/1/13 14:37 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Configuration
@ConfigurationProperties(prefix = "youxin.basic")
public class YouxinProperties {


    private RongProperties rong = new RongProperties();

    private FileProperties file = new FileProperties();

    private SmsProperties sms = new SmsProperties();

    public RongProperties getRong() {
        return rong;
    }

    public FileProperties getFile() {
        return file;
    }

    public SmsProperties getSms() {
        return sms;
    }

    public static class RongProperties {

        /**
         * 单位 秒
         */
        private String appKey = "";

        private String appSecret = "";

        private String systemNo = "10001";

        public String getAppKey() {
            return appKey;
        }

        public void setAppKey(String appKey) {
            this.appKey = appKey;
        }

        public String getAppSecret() {
            return appSecret;
        }

        public void setAppSecret(String appSecret) {
            this.appSecret = appSecret;
        }

        public String getSystemNo() {
            return systemNo;
        }

        public void setSystemNo(String systemNo) {
            this.systemNo = systemNo;
        }
    }

    public static class SmsProperties {

        /**
         * 单位 秒
         */

        private String entNo = "21522029798003";

        private String account = "13067219554";

        private String password = "1q2w3e4r";

        private String url = "http://interface.i314.net:6666/interface/SendMsg";

        public String getEntNo() {
            return entNo;
        }

        public void setEntNo(String entNo) {
            this.entNo = entNo;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class FileProperties {

        /**
         * 单位 秒
         */

        private String path = "/data/web/upload";

        private String versionPath = "/data/chat/file/upload/app/package";

        private String url = "http://154.220.2.52/chat/file/download/";

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getVersionPath() {
            return versionPath;
        }

        public void setVersionPath(String versionPath) {
            this.versionPath = versionPath;
        }
    }
}
