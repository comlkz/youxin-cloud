package com.youxin.chat.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.chat.basic.model.Sensitive;

public interface SensitiveMapper extends BaseMapper<Sensitive> {
}
