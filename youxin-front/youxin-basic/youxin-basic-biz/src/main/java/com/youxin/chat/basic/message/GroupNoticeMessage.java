package com.youxin.chat.basic.message;

import io.rong.messages.BaseMessage;
import io.rong.util.GsonUtil;

import java.util.Map;

public class GroupNoticeMessage extends BaseMessage {

    //操作人用户 Id
    private String operatorUserId;

    //操作名称
    private String operation;

    //群组中各种通知的操作数据
    private Map<String, Object> data;

    private String msg;

    private String extra;

    private transient static final String TYPE = "GM:GrpNtf";


    public GroupNoticeMessage(String operatorUserId, String msg, String ext, String operation) {
        this.operatorUserId = operatorUserId;
        this.msg = msg;
        this.extra = ext;
        this.operation = operation;
    }

    @Override
    public String getType() {
        return "GM:GrpNtf";
    }

    @Override
    public String toString() {
        return GsonUtil.toJson(this, GroupNoticeMessage.class);

    }

    public String getOperatorUserId() {
        return operatorUserId;
    }

    public void setOperatorUserId(String operatorUserId) {
        this.operatorUserId = operatorUserId;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
