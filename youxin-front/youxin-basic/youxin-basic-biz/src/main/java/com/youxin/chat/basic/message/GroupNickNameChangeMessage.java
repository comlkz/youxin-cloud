package com.youxin.chat.basic.message;

import io.rong.messages.BaseMessage;
import io.rong.util.GsonUtil;

public class GroupNickNameChangeMessage  extends BaseMessage {

    private String operation = "";
    private String data = "";
    private String extra = "";


    private transient static final String TYPE = "GR:ProfileNtf";


    public GroupNickNameChangeMessage(String operation, String data, String extra) {
        this.operation = operation;
        this.data = data;
        this.extra = extra;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    @Override
    public String getType() {
        return TYPE;    }

    @Override
    public String toString() {
        return GsonUtil.toJson(this, GroupNickNameChangeMessage.class);
    }
}
