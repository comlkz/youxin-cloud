package com.youxin.chat.basic.client.impl;

import com.youxin.chat.basic.service.ChatThirdService;
import com.youxin.chat.basic.client.IChatThirdClient;
import com.youxin.exception.SystemException;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class ChatThirdClient implements IChatThirdClient {

    @Resource
    private ChatThirdService chatThirdService;

    @Override
    public String getSystemNo() {
        return chatThirdService.getSystemNo();
    }

    @Override
    public String registerUser(String userNo, String nickName, String avatar) throws SystemException {
        return chatThirdService.registerUser(userNo, nickName, avatar);
    }

    @Override
    public void updateUser(String userNo, String nickName, String avatar) throws SystemException {
        chatThirdService.updateUser(userNo, nickName, avatar);
    }

    @Override
    public void blackUser(String userNo, String friendNo, Integer status) throws SystemException {
        chatThirdService.blackUser(userNo, friendNo, status);
    }

    @Override
    public void createGroup(String groupId, String groupName, List<String> userNos) throws SystemException {
        chatThirdService.createGroup(groupId, groupName, userNos);
    }

    @Override
    public void joinGroup(String groupId, String groupName, List<String> userNos) throws SystemException {
        chatThirdService.joinGroup(groupId, groupName, userNos);
    }

    @Override
    public void quitGroup(String groupId, List<String> userNos) throws SystemException {
        chatThirdService.quitGroup(groupId, userNos);
    }

    @Override
    public void dismissGroup(String groupId, String userNo) throws SystemException {
        chatThirdService.dismissGroup(groupId, userNo);
    }

    @Override
    public void addWhiteList(String userNo, List<String> whiteUserNos) {
        chatThirdService.addWhiteList(userNo, whiteUserNos);
    }

    @Override
    public void removeWhiteList(String userNo, List<String> whiteUserNos) {
        chatThirdService.removeWhiteList(userNo, whiteUserNos);
    }

    @Override
    public void updateGroup(String groupId, String groupName) throws SystemException {
        chatThirdService.updateGroup(groupId, groupName);
    }

    @Override
    public void addSensitive(String keyWord, Integer type, String replace) throws SystemException {
        chatThirdService.addSensitive(keyWord, type, replace);
    }

    @Override
    public void removeSensitive(String keyWords) throws SystemException {
        chatThirdService.removeSensitive(keyWords);
    }

    @Override
    public void addBlockUser(String id) throws SystemException {
        chatThirdService.addBlockUser(id);
    }

    @Override
    public void removeBlockUser(String id) throws SystemException {
        chatThirdService.removeBlockUser(id);
    }

    @Override
    public void sendPrivateMsg(String fromUserId, String toUserId, String msg) throws SystemException {
        chatThirdService.sendPrivateMsg(fromUserId, toUserId, msg);
    }

    @Override
    public void sendGroupMsg(String operation, String fromUserId, List<String> toUserIds, String operatorUserId, String data, String msg) throws SystemException {
        chatThirdService.sendGroupMsg(operation, fromUserId, toUserIds, operatorUserId, data, msg);
    }

    @Override
    public void sendGroupNoticeMsg(String fromUserId, List<String> toUserIds, String operatorUserId, String msg, String operation, String ext) throws SystemException {
        chatThirdService.sendGroupNoticeMsg(fromUserId, toUserIds, operatorUserId, msg, operation, ext);
    }

    @Override
    public void sendFriendNoticeMsg(String fromUserId, List<String> toUserIds, String msg) throws SystemException {
        chatThirdService.sendFriendNoticeMsg(fromUserId, toUserIds, msg);
    }

    @Override
    public void setFriendDisturb(String userNo, int friendId, int type) throws SystemException {
        chatThirdService.setFriendDisturb(userNo, friendId, type);
    }

    @Override
    public void setGroupDisturb(String userNo, String groupId, int type) throws SystemException {
        chatThirdService.setGroupDisturb(userNo, groupId, type);
    }

    @Override
    public void pushMsg(String message, List<Integer> type, List<String> userIds, Integer isToAll) throws SystemException {
        chatThirdService.pushMsg(message, type, userIds, isToAll);
    }

    @Override
    public void setBanned(String groupId) throws SystemException {
        chatThirdService.setBanned(groupId);
    }

    @Override
    public void addBannedWhitelist(String groupId, List<String> managerId) throws SystemException {
        chatThirdService.addBannedWhitelist(groupId, managerId);
    }

    @Override
    public void removeBannedWhitelist(String groupId, List<String> managerId) throws SystemException {
        chatThirdService.removeBannedWhitelist(groupId, managerId);
    }

    @Override
    public void setBannedOpen(String groupId) throws SystemException {
        chatThirdService.setBannedOpen(groupId);
    }

    @Override
    public void sendPrivateRedReceiveMsg(String fromUserId, String toUserId, Map<String, Object> map) throws SystemException {
        chatThirdService.sendPrivateRedReceiveMsg(fromUserId, toUserId, map);
    }

    @Override
    public void sendGroupRedReceiveMsg(String fromUserId, String toUserId, Map<String, Object> map) {
        chatThirdService.sendGroupRedReceiveMsg(fromUserId, toUserId, map);
    }

    @Override
    public void sendPayMsg(String toUserId, Map<String, Object> map) throws SystemException {
        chatThirdService.sendPayMsg(toUserId, map);
    }

    @Override
    public void sendShareMsg(String fromUserId, List<Map<Integer, String>> toUserIds, Map<String, Object> dataMap) {
        chatThirdService.sendShareMsg(fromUserId, toUserIds, dataMap);
    }

    @Override
    public void sendGroupNoticeMsg(String userId, String groupId, String msg) {
        chatThirdService.sendGroupNoticeMsg(userId, groupId, msg);
    }

    @Override
    public void sendGroupNickNameMsg(String userId, String groupId, Map<String, String> data) {
        chatThirdService.sendGroupNickNameMsg(userId, groupId, data);
    }

    @Override
    public String getHelperNo() {
        return chatThirdService.getHelperNo();
    }
}
