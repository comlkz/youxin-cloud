package com.youxin.chat.basic.client.impl;

import com.youxin.base.TPage;
import com.youxin.chat.basic.dto.SensitiveDto;
import com.youxin.chat.basic.dto.SensitiveSearch;
import com.youxin.chat.basic.client.ISensitiveClient;
import com.youxin.chat.basic.service.SensitiveService;
import com.youxin.exception.SystemException;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;

@Service
public class SensitiveClient implements ISensitiveClient {

    @Resource
    private SensitiveService sensitiveService;

    @Override
    public void addSensitive(SensitiveDto sensitiveDto, String userName) throws SystemException {
        sensitiveService.addSensitive(sensitiveDto, userName);
    }

    @Override
    public void removeSensitive(Long id) throws SystemException {
        sensitiveService.removeSensitive(id);
    }

    @Override
    public TPage<SensitiveDto> list(SensitiveSearch sensitiveSearch) throws SystemException {
        return sensitiveService.list(sensitiveSearch);
    }
}
