package com.youxin.chat.basic.client.impl;

import com.youxin.base.TPage;
import com.youxin.chat.basic.dto.SysConfigDto;
import com.youxin.chat.basic.client.ISysConfigClient;
import com.youxin.chat.basic.service.SysConfigService;
import com.youxin.exception.SystemException;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

@Service
public class SysConfigClient implements ISysConfigClient {

    private static final Logger logger = LoggerFactory.getLogger(SysConfigClient.class);

    @Resource
    private SysConfigService sysConfigService;

    @Override
    public void saveConfig(SysConfigDto sysConfigDto) throws SystemException {
        sysConfigService.saveConfig(sysConfigDto);
    }

    @Override
    public TPage<SysConfigDto> page(String key, Integer currentPage, Integer showCount) throws SystemException {
        return sysConfigService.page(key,currentPage,showCount);
    }

    @Override
    public String getConfigValue(String key) throws SystemException {
        return sysConfigService.getConfigValue(key);
    }

    @Override
    public void saveConfigBySysKey(SysConfigDto sysConfigDto) throws SystemException {
        sysConfigService.saveConfigBySysKey(sysConfigDto);
    }
}
