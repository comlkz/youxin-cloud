package com.youxin.chat.basic.client.impl;


import com.youxin.base.BaseResultCode;
import com.youxin.chat.basic.dto.file.FileDto;
import com.youxin.chat.basic.dto.file.SysFileDto;
import com.youxin.chat.basic.model.SysFile;
import com.youxin.chat.basic.client.IFileClient;
import com.youxin.chat.basic.service.SysFileService;
import com.youxin.common.config.YouxinCommonProperties;
import com.youxin.exception.SystemException;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;

@Service
public class FileClientImpl implements IFileClient {

    private static Logger logger = LoggerFactory.getLogger(FileClientImpl.class);

    @Resource
    private SysFileService sysFileService;

    @Resource
    private YouxinCommonProperties youxinCommonProperties;



    @Override
    public String getFileUrl() throws SystemException {
        return sysFileService.getFileUrl();
    }


    @Override
    public List<SysFileDto> getFilesByIds(List<String> fileIds) throws SystemException {
        return sysFileService.getFilesByIds(fileIds);
    }

    @Override
    public String saveFile(SysFileDto sysFileDto) throws SystemException {
        return sysFileService.saveFile(sysFileDto);
    }

    @Override
    public String getImageUrl(Integer type, String fileId) throws SystemException {
        return sysFileService.getImageUrl(type, fileId);
    }

    @Override
    public String primaryId(String prefix) throws SystemException {
        return sysFileService.primaryId(prefix);
    }

    @Override
    public String getRealPath() throws SystemException {
        return youxinCommonProperties.getFilePath();
    }

    @Override
    public String add(FileDto fileDto, Integer type) throws SystemException {
        return sysFileService.add(fileDto, type);
    }

    @Override
    public FileDto download(String fileId) throws SystemException {
        return sysFileService.download(fileId);
    }

    @Override
    public void removeFile(String fileId) {
        SysFile resFileVo = sysFileService.getById(fileId);
        if (resFileVo == null) {
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "文件不存在");
        }
        String filePath = youxinCommonProperties.getFilePath();
        filePath += resFileVo.getFilePath() + resFileVo.getId() + resFileVo.getSuffix();
        File file = new File(filePath);
        file.delete();
    }

}
