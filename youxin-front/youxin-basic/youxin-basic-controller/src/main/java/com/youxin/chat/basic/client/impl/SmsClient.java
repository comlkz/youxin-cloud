package com.youxin.chat.basic.client.impl;


import com.youxin.base.TPage;
import com.youxin.chat.basic.dto.sms.SmsDto;
import com.youxin.chat.basic.dto.sms.SmsListDto;
import com.youxin.chat.basic.client.ISmsClient;
import com.youxin.chat.basic.service.SmsService;
import com.youxin.exception.SystemException;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;

@Service
public class SmsClient implements ISmsClient {


    @Resource
    private SmsService smsService;


    @Override
    public void sendSms(Integer type, String phone, String content, Long ip) throws SystemException {
        smsService.sendSms(type, phone, content, ip);
    }

    @Override
    public TPage<SmsDto> getSmsList(SmsListDto smsListDto) throws SystemException {
        return smsService.getSmsList(smsListDto);
    }
}
