package com.youxin.chat.basic.controller;

import com.youxin.base.TResult;
import com.youxin.chat.basic.entity.vo.HelperVo;
import com.youxin.chat.basic.entity.vo.VersionVo;
import com.youxin.chat.basic.model.ChatAgreement;
import com.youxin.chat.basic.service.AgreementService;
import com.youxin.chat.basic.service.HelperService;
import com.youxin.chat.basic.service.SysConfigService;
import com.youxin.chat.basic.dto.constant.SysConfigConstant;
import com.youxin.chat.basic.service.VersionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.List;


@Api(value="版本信息", tags={"属性接口"})
@Controller
public class AttrController {

    @Resource
    private SysConfigService sysConfigService;

    @Resource
    private HelperService helperService;

    @Resource
    private AgreementService agreementService;

    @Resource
    private VersionService versionService;

    @ApiOperation("获取发现页URL")
    @GetMapping("discover/get")
    @ResponseBody
    public TResult<String> getUrl(){

        return TResult.success(sysConfigService.getConfigValue(SysConfigConstant.DISCOVER_URL));
    }


    @ApiOperation("查询详细内容")
    @GetMapping("helper/view/{id}")
    public void get(@PathVariable int id, HttpServletResponse response)throws Exception{
        HelperVo helperDto = helperService.getHelper(id);
        response.setDateHeader("expries", -1);
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-typeR", "text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
        out.println("<HTML>");
        out.println(" <HEAD><TITLE>sender</TITLE><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></HEAD>");
        out.println(" <BODY>");
        if(helperDto != null){
            out.println(helperDto.getAnswer());
        }else{
            out.println("暂无内容，敬请期待");

        }
        out.println(" </BODY>");
        out.println("</HTML>");
        out.flush();
        out.close();
    }

    @ApiOperation("查询问题列表")
    @GetMapping("helper/list")
    @ResponseBody
    public TResult<List<HelperVo>> list(){
        List<HelperVo> list = helperService.listAll();

        return TResult.success(list);
    }

    @ApiOperation("查询协议信息")
    @GetMapping("agreement/view/{type}")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "type", value = "1:尤信用户协议 2:举报投诉须知 3:充值提醒 4:提现提醒", required = true, dataType = "Integer"),
    })
    public void  agreementView(@PathVariable Integer type, HttpServletResponse response)throws Exception{
        ChatAgreement agreementDto = agreementService.getByType(type);
        response.setDateHeader("expries", -1);
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-typeR", "text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
        out.println("<HTML>");
        out.println("<HEAD>");
        out.println("<TITLE>" + agreementDto.getDescription() +"</TITLE>");
        out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>");
        out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></HEAD>");
        out.println(" <BODY>");
        if(agreementDto != null){
            out.println(agreementDto.getAgValue());
        }else{
            out.println("暂无内容，敬请期待");

        }
        out.println(" </BODY>");
        out.println("</HTML>");
        out.flush();
        out.close();
    }

    @GetMapping("version/get/{status}")
    @ApiOperation("获取最新版本信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "status", value = "终端类型0：安卓 1：IOS", required = true, dataType = "Integer"),
    })
    @ResponseBody
    public TResult<VersionVo> getVersion(@PathVariable Integer status){
        VersionVo versionDto = versionService.getVersion(status);

        return TResult.success(versionDto);
    }
}

