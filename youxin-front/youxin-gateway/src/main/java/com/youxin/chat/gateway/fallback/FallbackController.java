package com.youxin.chat.gateway.fallback;


import com.youxin.base.TResult;
import com.youxin.exception.code.ExceptionCode;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * 响应超时熔断处理器
 *
 * @author zuihou
 */
@RestController
public class FallbackController {

    @RequestMapping("/fallback")
    public Mono<TResult> fallback() {
        return Mono.just(TResult.build(ExceptionCode.SYSTEM_TIMEOUT.getCode(),"系统繁忙，请稍候重试"));
    }
}
