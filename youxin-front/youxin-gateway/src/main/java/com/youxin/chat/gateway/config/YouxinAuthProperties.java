package com.youxin.chat.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

/**
 * description: YouxinAuthProperties <br>
 * date: 2020/3/9 16:14 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@ConfigurationProperties(prefix = "youxin.auth")
@Configuration
public class YouxinAuthProperties {

    private List<String> ignoreUrls = Arrays.asList("/error",
            "/actuator/**",
            "/gate/**",
            "/auth/login",
            "/auth/captcha",
            "/static/**",
            "/anno/**",
            "/**/anno/**",
            "/**/swagger-ui.html",
            "/**/doc.html",
            "/**/webjars/**",
            "/**/v2/api-docs/**",
            "/**/v2/api-docs-ext/**",
            "/**/swagger-resources/**",
            "/file/download",
            "/discover/**",
            "/helper/**",
            "/version/**",
            "/agreement/**");


    public List<String> getIgnoreUrls() {
        return ignoreUrls;
    }

    public void setIgnoreUrls(List<String> ignoreUrls) {
        this.ignoreUrls = ignoreUrls;
    }
}
