package com.youxin;

import com.youxin.auth.client.EnableAuthClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * description: GatewayApplicatioin <br>
 * date: 2020/2/25 11:25 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableAuthClient
public class GatewayApplication {



    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class,args);
    }
}
