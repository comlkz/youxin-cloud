package com.youxin.task.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.base.TPage;
import com.youxin.task.entity.req.JobSearch;
import com.youxin.task.model.JobLog;

/**
 * description: JobLogService <br>
 * date: 2020/3/6 15:51 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface JobLogService extends IService<JobLog> {

    TPage<JobLog> findJobLogs(JobSearch jobLogSearch);

    void saveJobLog(JobLog log);

    void deleteJobLogs(String[] jobLogIds);
}
