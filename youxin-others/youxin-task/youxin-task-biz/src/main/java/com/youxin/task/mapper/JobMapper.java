package com.youxin.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.task.entity.req.JobSearch;
import com.youxin.task.entity.vo.JobVo;
import com.youxin.task.model.Job;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JobMapper extends BaseMapper<Job> {

    List<JobVo> queryList();

    IPage<JobVo> findJob(Page<JobVo> page, @Param("record") JobSearch jobSearch);
}
