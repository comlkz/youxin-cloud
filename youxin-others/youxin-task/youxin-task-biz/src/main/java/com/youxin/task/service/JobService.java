package com.youxin.task.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import com.youxin.task.entity.req.JobSearch;
import com.youxin.task.entity.vo.JobVo;
import com.youxin.task.model.Job;

import java.util.List;

/**
 * description: JobService <br>
 * date: 2020/3/6 15:50 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface JobService extends IService<Job> {


    TPage<JobVo> findJob(JobSearch jobSearch);

    int updateBatch(List<String> jobIds, String status);

    TResult<Boolean> checkCron(String cron);

    TResult<String> createJob(JobVo jobDto);

    TResult<String> updateJob(JobVo jobDto);

    TResult<String> runJob(String jobIds);

    TResult<String> pauseJob(String jobIds);

    TResult<String> resumeJob(String jobIds);

    TResult<String> deleteJob(String jobIds);
}
