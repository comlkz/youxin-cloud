package com.youxin.task.entity.enums;

public class JobConstant {

    public interface JobLogConstant{
       String JOB_SUCCESS = "0";

        String JOB_FAIL = "1";
    }

    /**
     * 任务调度参数 key
     */
    public static final String JOB_PARAM_KEY = "JOB_PARAM_KEY";

}
