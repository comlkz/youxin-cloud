package com.youxin.task.job;

import com.youxin.other.report.client.ReportClient;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * description: SystemSettlementTask <br>
 * date: 2020/2/9 14:31 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Component
public class SystemSettlementTask {

    private static final Logger log = LoggerFactory.getLogger(SystemSettlementTask.class);

    @Reference
    private ReportClient reportClient;

    public void generate(){
        log.info("开始生成系统结算数据");
        LocalDateTime startTime = LocalDate.now().minusDays(1).atStartOfDay();
        LocalDateTime endTime = LocalDate.now().atStartOfDay();
        reportClient.addSystemSettlement(startTime,endTime);
        log.info("结束生成系统结算数据");

    }

}
