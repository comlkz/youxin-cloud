package com.youxin.task.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * description: TestTask <br>
 * date: 2020/3/11 15:48 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Component
public class TestTask {

    private static final Logger log = LoggerFactory.getLogger(TestTask.class);

    public void test(){
        log.info("开始执行测试");
    }
}
