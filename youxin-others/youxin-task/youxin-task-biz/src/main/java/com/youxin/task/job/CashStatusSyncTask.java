package com.youxin.task.job;

import com.youxin.chat.pay.client.OrderClient;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

/**
 * description: CashStatusSyncTask <br>
 * date: 2020/3/7 15:46 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Component
public class CashStatusSyncTask {

    @Reference
    private OrderClient orderClient;

    public void sync(String minute){
        orderClient.syncCashStatus(Integer.parseInt(minute));
    }
}
