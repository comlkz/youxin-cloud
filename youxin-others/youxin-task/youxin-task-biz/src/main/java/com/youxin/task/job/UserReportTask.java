package com.youxin.task.job;

import com.youxin.other.report.client.ReportClient;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

@Component
public class UserReportTask {

    @Reference
    private ReportClient reportClient;

    public void generate(){
        reportClient.generateUserReport();
    }


}
