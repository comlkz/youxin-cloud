package com.youxin.task.controller;



import com.youxin.base.TPage;
import com.youxin.base.TResult;
import com.youxin.task.entity.req.JobSearch;
import com.youxin.task.entity.vo.JobVo;
import com.youxin.task.model.JobLog;
import com.youxin.task.service.JobLogService;
import com.youxin.task.service.JobService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("job")
public class JobController {

    @Resource
    private JobService jobService;

    @Resource
    private JobLogService jobLogService;


    @GetMapping("page")
    public TResult<TPage<JobVo>> jobList(JobSearch search) {
        return TResult.success(jobService.findJob(search));
    }

    @GetMapping("cron/check")
    public TResult<Boolean> checkCron(String cron) {
        return jobService.checkCron(cron);
    }

    @PostMapping("save")
    public TResult<String> saveJob(@RequestBody JobVo jobDto) {
        if (StringUtils.isEmpty(jobDto.getJobId())) {
            return jobService.createJob(jobDto);
        } else {
            return jobService.updateJob(jobDto);
        }
    }

    @GetMapping("run/{jobId}")
    public TResult<String> runJob(@PathVariable String jobId) {
        return jobService.runJob(jobId);
    }

    @GetMapping("pause/{jobId}")
    public TResult<String> pauseJob(@PathVariable String jobId) {
        return jobService.pauseJob(jobId);
    }

    @GetMapping("resume/{jobId}")
    public TResult<String> resumeJob(@PathVariable String jobId) {
        return jobService.resumeJob(jobId);
    }

    @PostMapping("delete/{jobIds}")
    public TResult<String> deleteJob(@PathVariable String jobIds) {
        return jobService.deleteJob(jobIds);
    }

    @GetMapping("log/page")
    public TResult<TPage<JobLog>> jobLogList(JobSearch search) {
        return TResult.success(jobLogService.findJobLogs(search));
    }
}
