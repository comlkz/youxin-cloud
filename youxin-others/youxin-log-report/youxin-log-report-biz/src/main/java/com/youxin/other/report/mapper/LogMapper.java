package com.youxin.other.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.other.report.model.Log;

/**
 * description: LogMapper <br>
 * date: 2020/3/12 10:20 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface LogMapper extends BaseMapper<Log> {
}
