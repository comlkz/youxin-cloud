package com.youxin.other.report.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.util.DateUtils;
import com.youxin.other.report.entity.vo.UserReportVo;

public class UserReportExcelItem {

    @ExcelProperty(value = "时间", index = 0)
    private String staticTime;



    @ExcelProperty(value = "净增用户数", index = 1)
    private Integer addNum;

    @ExcelProperty(value = "累计用户数", index = 2)
    private Integer totalNum;

    @ExcelProperty(value = "活跃用户数", index = 3)
    private Integer activeNum;

    public UserReportExcelItem(){

    }

    public UserReportExcelItem(UserReportVo userReportDto){
         this.staticTime = DateUtils.format(userReportDto.getStaticTime(),"yyyy-MM-dd");
         this.addNum = userReportDto.getAddNum();
         this.totalNum = userReportDto.getTotalNum();
         this.activeNum = userReportDto.getActiveNum();
    }

    public String getStaticTime() {
        return staticTime;
    }

    public void setStaticTime(String staticTime) {
        this.staticTime = staticTime;
    }

    public Integer getAddNum() {
        return addNum;
    }

    public void setAddNum(Integer addNum) {
        this.addNum = addNum;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getActiveNum() {
        return activeNum;
    }

    public void setActiveNum(Integer activeNum) {
        this.activeNum = activeNum;
    }
}
