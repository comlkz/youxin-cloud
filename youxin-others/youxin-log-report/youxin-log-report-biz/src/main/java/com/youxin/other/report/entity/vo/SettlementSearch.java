package com.youxin.other.report.entity.vo;


import com.youxin.base.TQuery;

/**
 * description: SettlementSearch <br>
 * date: 2020/2/9 14:15 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class SettlementSearch extends TQuery {

    private String startTime;

    private String endTime;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
