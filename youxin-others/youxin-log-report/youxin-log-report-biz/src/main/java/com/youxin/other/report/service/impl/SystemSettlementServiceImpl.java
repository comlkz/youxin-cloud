package com.youxin.other.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.base.BaseResultCode;
import com.youxin.base.TPage;
import com.youxin.exception.SystemException;
import com.youxin.other.report.entity.vo.SettlementSearch;
import com.youxin.other.report.mapper.SystemSettlementMapper;
import com.youxin.other.report.model.SystemSettlement;
import com.youxin.other.report.service.SystemSettlementService;
import com.youxin.other.report.service.rpc.RpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * description: SystemSettlementServiceImpl <br>
 * date: 2020/3/6 12:16 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class SystemSettlementServiceImpl extends ServiceImpl<SystemSettlementMapper, SystemSettlement> implements SystemSettlementService {

    private static Logger logger = LoggerFactory.getLogger(SystemSettlementServiceImpl.class);

    private ExecutorService executor = Executors.newSingleThreadExecutor();

    @Resource
    private RpcService rpcService;

    @Override
    public void addSystemSettlement(LocalDateTime startTime, LocalDateTime endTime) throws SystemException {
        if (startTime == null) {
            throw new SystemException(BaseResultCode.COMMON_FAIL, "用户开始时间未传");
        }
        if (endTime == null) {
            endTime = LocalDateTime.now();
        }
        final LocalDateTime finalEndTime = endTime;
        executor.execute(() -> {
            LambdaQueryWrapper<SystemSettlement> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.ge(SystemSettlement::getStaticTime, startTime);
            queryWrapper.lt(SystemSettlement::getStaticTime, finalEndTime);
            remove(queryWrapper);
            List<SystemSettlement> list = new ArrayList<>();
            LocalDateTime date = startTime;
            while (date.isBefore(finalEndTime)){
                LocalDateTime nextDate = date.plusDays(1);
                SystemSettlement systemSettlement = rpcService.staticChannelOrderData(date,nextDate);
                if(systemSettlement == null){
                    systemSettlement = new SystemSettlement();
                }
                systemSettlement.setRechargeServiceFee(systemSettlement.getUserRechargeAmount().subtract(systemSettlement.getChannelRechargeAmount()));
                systemSettlement.setStaticTime(date);
                SystemSettlement channelSettlement = rpcService.staticChannelCashData(date,nextDate);
                if(systemSettlement != null){
                    SystemSettlement userSettlement =   rpcService.staticUserCashData(date,nextDate);
                    systemSettlement.setUserDefrayAmount(userSettlement.getUserDefrayAmount());
                    systemSettlement.setChannelDefrayAmount(channelSettlement.getChannelDefrayAmount());
                    systemSettlement.setDefrayServiceFee(userSettlement.getUserDefrayAmount().subtract(channelSettlement.getChannelDefrayAmount()));
                    systemSettlement.setChannelDefrayFee(channelSettlement.getChannelDefrayFee());
                    systemSettlement.setDefrayCount(channelSettlement.getDefrayCount());
                }
                list.add(systemSettlement);
                date = date.plusDays(1);
            }
            saveBatch(list);

        });
    }

    @Override
    public TPage<SystemSettlement> pageByParam(SettlementSearch search) {
        LambdaQueryWrapper<SystemSettlement> queryWrapper = new LambdaQueryWrapper<>();
        if(!StringUtils.isEmpty(search.getStartTime())){
            queryWrapper.ge(SystemSettlement::getStaticTime,search.getStartTime());
        }
        if(!StringUtils.isEmpty(search.getEndTime())){
            queryWrapper.le(SystemSettlement::getStaticTime,search.getEndTime());
        }
        queryWrapper.orderByDesc(SystemSettlement::getStaticTime);
        IPage<SystemSettlement> page = page(new Page<>(search.getCurrentPage(),search.getShowCount()),queryWrapper);
        return TPage.page(page.getRecords(),page.getTotal());
    }
}
