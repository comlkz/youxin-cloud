package com.youxin.other.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.base.TPage;
import com.youxin.other.report.entity.vo.SettlementSearch;
import com.youxin.other.report.model.SystemSettlement;

import java.time.LocalDateTime;

/**
 * description: SystemSettlementService <br>
 * date: 2020/3/6 12:16 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface SystemSettlementService extends IService<SystemSettlement> {
    void addSystemSettlement(LocalDateTime startTime, LocalDateTime endTime);

    TPage<SystemSettlement> pageByParam(SettlementSearch search);
}
