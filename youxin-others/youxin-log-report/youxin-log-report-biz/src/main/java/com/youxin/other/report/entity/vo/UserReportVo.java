package com.youxin.other.report.entity.vo;

import java.io.Serializable;
import java.util.Date;

public class UserReportVo implements Serializable {

    private Integer id;

    private Integer activeNum;

    private Integer totalNum;

    private Integer addNum;

    private Date staticTime;

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getActiveNum() {
        return activeNum;
    }

    public void setActiveNum(Integer activeNum) {
        this.activeNum = activeNum;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getAddNum() {
        return addNum;
    }

    public void setAddNum(Integer addNum) {
        this.addNum = addNum;
    }

    public Date getStaticTime() {
        return staticTime;
    }

    public void setStaticTime(Date staticTime) {
        this.staticTime = staticTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
