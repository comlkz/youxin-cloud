package com.youxin.other.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.pay.client.req.AgentStaticSearch;
import com.youxin.base.TPage;
import com.youxin.other.report.model.AgentStatic;
import com.youxin.other.report.model.UserReport;

/**
 * description: UserReportService <br>
 * date: 2020/3/6 11:26 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface AgentStaticService extends IService<AgentStatic> {

    void staticAgentData(AgentStaticSearch agentStaticSearch);

    TPage<AgentStatic> listAgentStatics(AgentStaticSearch search);

}
