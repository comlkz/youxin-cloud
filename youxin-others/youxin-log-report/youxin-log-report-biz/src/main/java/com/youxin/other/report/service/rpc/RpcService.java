package com.youxin.other.report.service.rpc;

import com.youxin.admin.pay.client.PayClient;
import com.youxin.admin.pay.client.dto.AgentStaticDto;
import com.youxin.admin.pay.client.dto.SystemSettlementDto;
import com.youxin.admin.pay.client.req.AgentStaticSearch;
import com.youxin.admin.user.client.UserClient;
import com.youxin.admin.user.client.dto.UserReportItem;
import com.youxin.admin.user.client.req.ReportSearch;
import com.youxin.dozer.DozerUtils;
import com.youxin.other.report.entity.req.UserReportSearch;
import com.youxin.other.report.model.AgentStatic;
import com.youxin.other.report.model.SystemSettlement;
import com.youxin.other.report.model.UserReport;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * description: RpcService <br>
 * date: 2020/3/6 12:18 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class RpcService {

    @Reference
    private UserClient userClient;

    @Reference
    private PayClient payClient;

    @Resource
    private DozerUtils dozerUtils;

    public List<UserReportItem> staticActiveReport(UserReportSearch search){
        ReportSearch reportSearch = dozerUtils.map(search,ReportSearch.class);
        List<UserReportItem> list = userClient.staticActiveReport(reportSearch);
        return list;
    }

    public int staticUserReport(UserReportSearch search){
        ReportSearch reportSearch = dozerUtils.map(search,ReportSearch.class);
       return userClient.staticUserReport(reportSearch);
    }

    public SystemSettlement staticChannelOrderData(LocalDateTime startTime, LocalDateTime endTime){
        SystemSettlementDto info = payClient.staticChannelOrderData(startTime,endTime);
        SystemSettlement systemSettlement = dozerUtils.map(info,SystemSettlement.class);
        return systemSettlement;
    }

    public SystemSettlement staticChannelCashData(LocalDateTime startTime, LocalDateTime endTime){
        SystemSettlementDto info = payClient.staticChannelCashData(startTime,endTime);
        SystemSettlement systemSettlement = dozerUtils.map(info,SystemSettlement.class);
        return systemSettlement;
    }

    public SystemSettlement staticUserCashData(LocalDateTime startTime, LocalDateTime endTime){
        SystemSettlementDto info = payClient.staticUserCashData(startTime,endTime);
        SystemSettlement systemSettlement = dozerUtils.map(info,SystemSettlement.class);
        return systemSettlement;
    }

    public List<AgentStatic> staticAgentTransaction(AgentStaticSearch search){
        List<AgentStaticDto> list = payClient.staticAgentTransaction(search);
        List<AgentStatic> agentStatics = list.stream().map(item ->dozerUtils.map(item,AgentStatic.class)).collect(Collectors.toList());
        return agentStatics;
    }
}
