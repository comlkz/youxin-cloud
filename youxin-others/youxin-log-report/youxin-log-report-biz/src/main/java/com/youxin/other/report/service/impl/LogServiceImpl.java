package com.youxin.other.report.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.other.report.mapper.LogMapper;
import com.youxin.other.report.model.Log;
import com.youxin.other.report.service.LogService;
import org.springframework.stereotype.Service;

/**
 * description: LogServiceImpl <br>
 * date: 2020/3/12 10:20 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements LogService {
}
