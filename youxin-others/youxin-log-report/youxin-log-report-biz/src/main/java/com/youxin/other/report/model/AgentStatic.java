package com.youxin.other.report.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * description: 代理统计 <br>
 * date: 2020/1/6 14:23 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class AgentStatic implements Serializable {

    private Integer id;

    /**
     * 充值总金额
     */
    private BigDecimal chargeAmount;

    /**
     * 充值总数
     */
    private Integer chargeRecord;

    /**
     * 提现总数
     */
    private Integer defrayRecord;

    /**
     * 提现总金额
     */
    private BigDecimal defrayAmount;

    /**
     * 代理号
     */
    private String agentNo;

    /**
     * 返点金额
     */
    private BigDecimal returnMoney;

    /**
     * 统计时间
     */
    private LocalDateTime staticTime;

    private LocalDateTime createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(BigDecimal chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public Integer getChargeRecord() {
        return chargeRecord;
    }

    public void setChargeRecord(Integer chargeRecord) {
        this.chargeRecord = chargeRecord;
    }

    public Integer getDefrayRecord() {
        return defrayRecord;
    }

    public void setDefrayRecord(Integer defrayRecord) {
        this.defrayRecord = defrayRecord;
    }

    public BigDecimal getDefrayAmount() {
        return defrayAmount;
    }

    public void setDefrayAmount(BigDecimal defrayAmount) {
        this.defrayAmount = defrayAmount;
    }

    public String getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(String agentNo) {
        this.agentNo = agentNo;
    }

    public BigDecimal getReturnMoney() {
        return returnMoney;
    }

    public void setReturnMoney(BigDecimal returnMoney) {
        this.returnMoney = returnMoney;
    }

    public LocalDateTime getStaticTime() {
        return staticTime;
    }

    public void setStaticTime(LocalDateTime staticTime) {
        this.staticTime = staticTime;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
}
