package com.youxin.other.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.other.report.model.Log;

/**
 * description: LogService <br>
 * date: 2020/3/12 10:20 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface LogService extends IService<Log> {
}
