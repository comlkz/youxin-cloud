package com.youxin.other.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.other.report.entity.vo.UserReportVo;
import com.youxin.other.report.entity.req.UserReportSearch;
import com.youxin.other.report.model.UserReport;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserReportMapper extends BaseMapper<UserReport> {

    IPage<UserReportVo> selectByParam(Page<UserReportVo> page, @Param("record") UserReportSearch search);

    List<UserReportVo> selectList(@Param("record") UserReportSearch search);
}
