package com.youxin.other.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.other.report.model.SystemSettlement;

/**
 * description: SystemSettlementMapper <br>
 * date: 2020/2/9 13:00 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface SystemSettlementMapper extends BaseMapper<SystemSettlement> {
}
