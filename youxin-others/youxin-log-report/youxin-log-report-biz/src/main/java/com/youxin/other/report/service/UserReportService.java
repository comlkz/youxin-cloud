package com.youxin.other.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.base.TPage;
import com.youxin.other.report.entity.req.UserReportSearch;
import com.youxin.other.report.entity.vo.UserReportVo;
import com.youxin.other.report.model.UserReport;

import java.util.List;

/**
 * description: UserReportService <br>
 * date: 2020/3/6 12:14 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface UserReportService extends IService<UserReport> {

    void generateReport();

    void generateReport(String startTime,String endTime);

    TPage<UserReportVo> pageReport(UserReportSearch search);

    List<UserReportVo> listReport(UserReportSearch search);
}
