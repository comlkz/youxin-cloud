package com.youxin.other.report.provider;

import com.youxin.other.report.client.ReportClient;
import com.youxin.other.report.service.SystemSettlementService;
import com.youxin.other.report.service.UserReportService;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * description: ReportClientImpl <br>
 * date: 2020/3/7 15:55 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class ReportClientImpl implements ReportClient {

    @Resource
    private SystemSettlementService systemSettlementService;

    @Resource
    private UserReportService userReportService;

    @Override
    public void addSystemSettlement(LocalDateTime startTime, LocalDateTime endTime) {
        systemSettlementService.addSystemSettlement(startTime, endTime);
    }

    @Override
    public void generateUserReport() {
         userReportService.generateReport();
    }
}
