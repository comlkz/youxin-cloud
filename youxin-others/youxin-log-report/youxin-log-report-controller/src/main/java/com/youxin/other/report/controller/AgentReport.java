package com.youxin.other.report.controller;


import com.youxin.admin.pay.client.req.AgentStaticSearch;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import com.youxin.other.report.model.AgentStatic;
import com.youxin.other.report.service.AgentStaticService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * description: AgentReport <br>
 * date: 2020/1/6 15:25 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RequestMapping("/agent")
@RestController
public class AgentReport {

    @Resource
    private AgentStaticService agentStaticService;

    @PostMapping("generate")
    public TResult<String> generatePay(@RequestBody AgentStaticSearch search) {
        agentStaticService.staticAgentData(search);
        return TResult.success();
    }

    @GetMapping("page")
    public TResult<TPage<AgentStatic>> page(AgentStaticSearch search) {
        TPage<AgentStatic> page = agentStaticService.listAgentStatics(search);
        return TResult.success(page);
    }
}
