package com.youxin.other.report.controller;

import com.alibaba.excel.EasyExcel;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import com.youxin.other.report.entity.excel.UserReportExcelItem;
import com.youxin.other.report.entity.req.UserReportSearch;
import com.youxin.other.report.entity.vo.UserReportVo;
import com.youxin.other.report.service.UserReportService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("user")
public class UserReportController {

    @Resource
    private UserReportService userReportService;

    @GetMapping("page")
    public TResult<TPage<UserReportVo>> pageUserReport(UserReportSearch search){
        TPage<UserReportVo> page = userReportService.pageReport(search);
        return TResult.success(page);
    }

    @GetMapping("generate")
    public TResult<String> generateUserReport(UserReportSearch search){
        userReportService.generateReport(search.getStartTime(),search.getEndTime());
        return TResult.success();
    }

    @GetMapping("list")
    public TResult<List<UserReportVo>> listUserReport(UserReportSearch search){
        List<UserReportVo> list = userReportService.listReport(search);
        return TResult.success(list);
    }

    @PostMapping("export")
    public void exportTransaction(UserReportSearch search, HttpServletResponse response) throws Exception {
        search.setShowCount(50000);
        TPage<UserReportVo> page = userReportService.pageReport(search);
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename=用户统计报表.xlsx");
        List<UserReportExcelItem> list = page.getRecords().stream().map(item -> new UserReportExcelItem((item))).collect(Collectors.toList());
        EasyExcel.write(response.getOutputStream(), UserReportExcelItem.class).sheet("sheet1").doWrite(list);
    }
}
