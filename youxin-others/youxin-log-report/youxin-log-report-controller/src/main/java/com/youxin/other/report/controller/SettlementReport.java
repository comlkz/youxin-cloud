package com.youxin.other.report.controller;


import com.youxin.base.BaseResultCode;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import com.youxin.other.report.entity.vo.SettlementSearch;
import com.youxin.other.report.model.SystemSettlement;
import com.youxin.other.report.service.SystemSettlementService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * description: SettlementReport <br>
 * date: 2020/2/9 14:25 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
@RequestMapping("/settlement")
public class SettlementReport {

    @Resource
    private SystemSettlementService systemSettlementService;

    @PostMapping("generate")
    public TResult<String> generateData(@RequestBody SettlementSearch search){
        if(search.getStartTime() == null || search.getEndTime() == null){
            return TResult.build(BaseResultCode.COMMON_FAIL,"请选择开始时间和结束时间");
        }
        LocalDateTime startTime = LocalDate.parse(search.getStartTime(),DateTimeFormatter.ofPattern("yyyy-MM-dd")).atStartOfDay();
        LocalDateTime endTime = LocalDate.parse(search.getEndTime(),DateTimeFormatter.ofPattern("yyyy-MM-dd")).plusDays(1).atStartOfDay();
        systemSettlementService.addSystemSettlement(startTime,endTime);
        return TResult.success();
    }

    @GetMapping("page")
    public TResult<TPage<SystemSettlement>> page(SettlementSearch search){
        TPage<SystemSettlement> page = systemSettlementService.pageByParam(search);
        return TResult.success(page);
    }
}
