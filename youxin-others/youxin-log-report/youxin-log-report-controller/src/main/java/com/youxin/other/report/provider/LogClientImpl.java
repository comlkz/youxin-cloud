package com.youxin.other.report.provider;

import com.youxin.dozer.DozerUtils;
import com.youxin.log.entity.SysLogDTO;
import com.youxin.other.report.client.LogClient;
import com.youxin.other.report.model.Log;
import com.youxin.other.report.service.LogService;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;

/**
 * description: LogClientImpl <br>
 * date: 2020/3/12 10:28 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service(executes = 100)
public class LogClientImpl implements LogClient {

    @Resource
    private LogService logService;

    @Resource
    private DozerUtils dozerUtils;

    @Override
    public void save(SysLogDTO sysLogDTO) {
        Log log = dozerUtils.map(sysLogDTO,Log.class);
        logService.save(log);
    }
}
