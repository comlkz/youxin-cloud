package com.youxin.other.report.client;

import java.time.LocalDateTime;

/**
 * description: ReportClient <br>
 * date: 2020/3/7 15:54 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface ReportClient {

    void addSystemSettlement(LocalDateTime startTime, LocalDateTime endTime);

    void generateUserReport();

}
