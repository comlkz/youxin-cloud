package com.youxin.other.report.client;

import com.youxin.log.entity.SysLogDTO;

/**
 * description: LogClient <br>
 * date: 2020/3/12 10:26 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface LogClient {

    void save(SysLogDTO sysLogDTO);
}
