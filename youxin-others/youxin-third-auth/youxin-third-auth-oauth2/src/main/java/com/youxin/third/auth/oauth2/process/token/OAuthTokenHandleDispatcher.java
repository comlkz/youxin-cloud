/*
 * Copyright (c) 2013 Andaily Information Technology Co. Ltd
 * www.andaily.com
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Andaily Information Technology Co. Ltd ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with Andaily Information Technology Co. Ltd.
 */
package com.youxin.third.auth.oauth2.process.token;


import com.youxin.third.auth.oauth2.process.OAuthTokenxRequest;
import com.youxin.third.auth.oauth2.service.OauthService;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 2015/7/3
 *
 * @author Shengzhao Li
 */
public class OAuthTokenHandleDispatcher {

    private static final Logger LOG = LoggerFactory.getLogger(OAuthTokenHandleDispatcher.class);

    @Resource
    private List<OAuthTokenHandler> handlers = new ArrayList<>();

    private OAuthTokenxRequest tokenRequest;
    private HttpServletResponse response;

    private OauthService oauthService;

    public OAuthTokenHandleDispatcher(OAuthTokenxRequest tokenRequest, HttpServletResponse response, OauthService oauthService) {
        this.tokenRequest = tokenRequest;
        this.response = response;
        this.oauthService = oauthService;
        initialHandlers();
    }

    private void initialHandlers() {
        handlers.add(new AuthorizationCodeTokenHandler(oauthService));
        handlers.add(new PasswordTokenHandler(oauthService));
        handlers.add(new RefreshTokenHandler(oauthService));

        handlers.add(new ClientCredentialsTokenHandler(oauthService));

        LOG.debug("Initialed '{}' OAuthTokenHandler(s): {}", handlers.size(), handlers);
    }


    public void dispatch() throws OAuthProblemException, OAuthSystemException {
        for (OAuthTokenHandler handler : handlers) {
            if (handler.support(tokenRequest)) {
                LOG.debug("Found '{}' handle OAuthTokenxRequest: {}", handler, tokenRequest);
                handler.handle(tokenRequest, response);
                return;
            }
        }
        throw new IllegalStateException("Not found 'OAuthTokenHandler' to handle OAuthTokenxRequest: " + tokenRequest);
    }
}
