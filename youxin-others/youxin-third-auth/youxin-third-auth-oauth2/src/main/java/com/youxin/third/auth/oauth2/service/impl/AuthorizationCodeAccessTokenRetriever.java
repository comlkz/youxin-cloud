/*
 * Copyright (c) 2015 MONKEYK Information Technology Co. Ltd
 * www.monkeyk.com
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * MONKEYK Information Technology Co. Ltd ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with MONKEYK Information Technology Co. Ltd.
 */
package com.youxin.third.auth.oauth2.service.impl;


import com.youxin.third.auth.model.AccessToken;
import com.youxin.third.auth.model.ClientDetails;
import com.youxin.third.auth.model.OauthCode;
import com.youxin.third.auth.oauth2.service.AbstractAccessTokenHandler;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 2015/10/26
 *
 * @author Shengzhao Li
 */
public class AuthorizationCodeAccessTokenRetriever extends AbstractAccessTokenHandler {


    private static final Logger LOG = LoggerFactory.getLogger(AuthorizationCodeAccessTokenRetriever.class);


    private ClientDetails clientDetails;
    private String code;

    public AuthorizationCodeAccessTokenRetriever(ClientDetails clientDetails, String code) {
        this.clientDetails = clientDetails;
        this.code = code;
    }


    //Always return new AccessToken
    public AccessToken retrieve() throws OAuthSystemException {

        final OauthCode oauthCode = loadOauthCode();
        final String username = oauthCode.getUsername();

        final String clientId = clientDetails.getClientId();
        final String authenticationId = authenticationIdGenerator.generate(clientId, username, null);

        AccessToken accessToken = oAuthManager.findAccessToken(clientId, username, authenticationId);
        if (accessToken != null) {
            LOG.debug("Delete existed AccessToken: {}", accessToken);
            oAuthManager.deleteAccessToken(accessToken);
        }
        accessToken = createAndSaveAccessToken(clientDetails, clientDetails.supportRefreshToken(), username, authenticationId);
        LOG.debug("Create a new AccessToken: {}", accessToken);

        return accessToken;
    }

    private OauthCode loadOauthCode() {
        final String clientId = clientDetails.getClientId();
        return oAuthManager.findOauthCode(code, clientId);
    }
}
