/*
 * Copyright (c) 2015 MONKEYK Information Technology Co. Ltd
 * www.monkeyk.com
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * MONKEYK Information Technology Co. Ltd ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with MONKEYK Information Technology Co. Ltd.
 */
package com.youxin.third.auth.oauth2.service.impl;


import com.youxin.third.auth.model.AccessToken;
import com.youxin.third.auth.model.ClientDetails;
import com.youxin.third.auth.oauth2.service.AbstractAccessTokenHandler;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * 2015/10/26
 *
 * @author Shengzhao Li
 */
public class AccessTokenByRefreshTokenChanger extends AbstractAccessTokenHandler {


    private static final Logger LOG = LoggerFactory.getLogger(AccessTokenByRefreshTokenChanger.class);


    private final String refreshToken;
    private final String clientId;

    public AccessTokenByRefreshTokenChanger(String refreshToken, String clientId) {
        this.refreshToken = refreshToken;
        this.clientId = clientId;
    }

    /**
     * Get AccessToken
     * Generate a new AccessToken from existed(exclude token,refresh_token)
     * Update access_token,refresh_token, expired.
     * Save and remove old
     */
    public AccessToken change() throws OAuthSystemException {

        final AccessToken oldToken = oAuthManager.findAccessTokenByRefreshToken(refreshToken, clientId);

        AccessToken newAccessToken = oldToken.cloneMe();
        LOG.debug("Create new AccessToken: {} from old AccessToken: {}", newAccessToken, oldToken);

        ClientDetails details = oAuthManager.findClientDetails(clientId);
        newAccessToken.updateByClientDetails(details);

        final String authId = authenticationIdGenerator.generate(clientId, oldToken.getUsername(), null);
        newAccessToken.setAuthenticationId(authId);
        newAccessToken.setTokenId(oAuthIssuer.accessToken());
        newAccessToken.setRefreshToken(oAuthIssuer.refreshToken());
        newAccessToken.setCreateTime(new Date());
        oAuthManager.deleteAccessToken(oldToken);
        LOG.debug("Delete old AccessToken: {}", oldToken);

        oAuthManager.saveAccessToken(newAccessToken);
        LOG.debug("Save new AccessToken: {}", newAccessToken);

        return newAccessToken;
    }
}
