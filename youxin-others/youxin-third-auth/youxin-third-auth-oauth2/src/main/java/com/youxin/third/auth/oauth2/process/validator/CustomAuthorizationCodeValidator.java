package com.youxin.third.auth.oauth2.process.validator;

import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.validators.AbstractValidator;

import javax.servlet.http.HttpServletRequest;

public class CustomAuthorizationCodeValidator  extends AbstractValidator<HttpServletRequest> {

    public CustomAuthorizationCodeValidator() {
        this.requiredParams.add("grant_type");
        this.requiredParams.add("code");
        this.enforceClientAuthentication = true;
    }

    @Override
    public void validateContentType(HttpServletRequest request) throws OAuthProblemException {
        String contentType = request.getContentType();

    }
}
