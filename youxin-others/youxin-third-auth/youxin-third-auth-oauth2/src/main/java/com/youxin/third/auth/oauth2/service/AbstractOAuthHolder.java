/*
 * Copyright (c) 2015 MONKEYK Information Technology Co. Ltd
 * www.monkeyk.com
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * MONKEYK Information Technology Co. Ltd ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with MONKEYK Information Technology Co. Ltd.
 */
package com.youxin.third.auth.oauth2.service;

import com.youxin.chat.user.dto.UserInfoDto;
import com.youxin.context.BaseContextHandler;

/**
 * 2015/10/22
 *
 * @author Shengzhao Li
 */
public abstract class AbstractOAuthHolder {


//    protected transient OauthRepository oauthRepository = BeanProvider.getBean(OauthRepository.class);


    /**
     * Return current login username
     *
     * @return Username
     */
    protected String currentUsername() {

        return BaseContextHandler.getUserNo();
    }

}
