package com.youxin.third.auth.oauth2.process.authorize;


import com.youxin.third.auth.model.ClientDetails;
import com.youxin.third.auth.oauth2.process.OAuthAuthxRequest;
import com.youxin.third.auth.oauth2.process.validator.AbstractClientDetailsValidator;
import com.youxin.third.auth.oauth2.process.validator.CodeClientDetailsValidator;
import com.youxin.third.auth.oauth2.service.OauthService;
import com.youxin.third.auth.resource.context.WebUtils;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 2015/6/25
 * <p/>
 * Handle response_type = 'code'
 *
 * @author Shengzhao Li
 */
public class CodeAuthorizeHandler extends AbstractAuthorizeHandler {

    private static final Logger LOG = LoggerFactory.getLogger(CodeAuthorizeHandler.class);




    public CodeAuthorizeHandler(OAuthAuthxRequest oauthRequest, HttpServletResponse response, OauthService oauthService) {
        super(oauthRequest, response,oauthService);
    }

    @Override
    protected AbstractClientDetailsValidator getValidator() {
        return new CodeClientDetailsValidator(oauthRequest,oauthService);
    }


    //response code
    @Override
    protected void handleResponse() throws OAuthSystemException, IOException {
        final ClientDetails clientDetails = clientDetails();
        final String authCode = oauthService.retrieveAuthCode(clientDetails);

        final OAuthResponse oAuthResponse = OAuthASResponse
                .authorizationResponse(oauthRequest.request(), HttpServletResponse.SC_OK)
                .location(clientDetails.getRedirectUri())
                .setCode(authCode)
                .buildQueryMessage();
        LOG.debug(" 'code' response: {}", oAuthResponse);

        //WebUtils.writeOAuthJsonResponse(response,oAuthResponse);
        WebUtils.writeOAuthQueryResponse(response, oAuthResponse);
    }


}
