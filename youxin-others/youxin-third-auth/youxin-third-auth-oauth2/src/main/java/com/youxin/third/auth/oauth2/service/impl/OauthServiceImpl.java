package com.youxin.third.auth.oauth2.service.impl;


import com.youxin.third.auth.model.AccessToken;
import com.youxin.third.auth.model.ClientDetails;
import com.youxin.third.auth.model.OauthCode;
import com.youxin.third.auth.oauth2.service.AuthenticationIdGenerator;
import com.youxin.third.auth.oauth2.service.OauthService;
import com.youxin.third.auth.service.manager.OAuthManager;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Set;

/**
 * 15-6-10
 *
 * @author Shengzhao Li
 */
@Service("oauthService")
public class OauthServiceImpl implements OauthService {

    private static final Logger LOG = LoggerFactory.getLogger(OauthServiceImpl.class);


    @Autowired
    private OAuthManager oAuthManager;

    @Resource
    private AuthenticationIdGenerator authenticationIdGenerator;

    private OAuthIssuer oAuthIssuer = new OAuthIssuerImpl(new MD5Generator());


    /**
     * Load  ClientDetails instance by clientId
     *
     * @param clientId clientId
     * @return ClientDetails
     */
    @Override
    public ClientDetails loadClientDetails(String clientId) {
        LOG.debug("Load ClientDetails by clientId: {}", clientId);
        return oAuthManager.findClientDetails(clientId);
    }


    /**
     * Retrieve an existed code, if it is existed , remove it and create a new one,
     * otherwise, create  a new one and return
     *
     * @param clientDetails ClientDetails
     * @return code
     * @throws OAuthSystemException
     */
    @Override
    public String retrieveAuthCode(ClientDetails clientDetails) throws OAuthSystemException {
        AuthCodeRetriever authCodeRetriever = new AuthCodeRetriever(clientDetails);
        authCodeRetriever.setoAuthManager(oAuthManager);
        authCodeRetriever.setoAuthIssuer(oAuthIssuer);
        return authCodeRetriever.retrieve();
    }


    @Override
    public AccessToken retrieveAccessToken(ClientDetails clientDetails, Set<String> scopes, boolean includeRefreshToken) throws OAuthSystemException {
        AccessTokenRetriever retriever = new AccessTokenRetriever(clientDetails, scopes, includeRefreshToken);
        retriever.setAuthenticationIdGenerator(authenticationIdGenerator);
        retriever.setoAuthManager(oAuthManager);
        retriever.setoAuthIssuer(oAuthIssuer);
        return retriever.retrieve();
    }

    //Always return new AccessToken, exclude refreshToken
    @Override
    public AccessToken retrieveNewAccessToken(ClientDetails clientDetails, Set<String> scopes) throws OAuthSystemException {
        NewAccessTokenRetriever tokenRetriever = new NewAccessTokenRetriever(clientDetails, scopes);
        tokenRetriever.setAuthenticationIdGenerator(authenticationIdGenerator);
        tokenRetriever.setoAuthManager(oAuthManager);
        tokenRetriever.setoAuthIssuer(oAuthIssuer);
        return tokenRetriever.retrieve();
    }

    @Override
    public OauthCode loadOauthCode(String code, ClientDetails clientDetails) {
        final String clientId = clientDetails.getClientId();
        return oAuthManager.findOauthCode(code, clientId);
    }

    @Override
    public boolean removeOauthCode(String code, ClientDetails clientDetails) {
        final OauthCode oauthCode = loadOauthCode(code, clientDetails);
        final int rows = oAuthManager.deleteOauthCode(oauthCode);
        return rows > 0;
    }

    //Always return new AccessToken
    @Override
    public AccessToken retrieveAuthorizationCodeAccessToken(ClientDetails clientDetails, String code) throws OAuthSystemException {
        AuthorizationCodeAccessTokenRetriever codeAccessTokenRetriever = new AuthorizationCodeAccessTokenRetriever(clientDetails, code);
        codeAccessTokenRetriever.setAuthenticationIdGenerator(authenticationIdGenerator);
        codeAccessTokenRetriever.setoAuthManager(oAuthManager);
        codeAccessTokenRetriever.setoAuthIssuer(oAuthIssuer);
        return codeAccessTokenRetriever.retrieve();
    }

    //grant_type=password AccessToken
    @Override
    public AccessToken retrievePasswordAccessToken(ClientDetails clientDetails, Set<String> scopes, String username) throws OAuthSystemException {
        PasswordAccessTokenRetriever tokenRetriever = new PasswordAccessTokenRetriever(clientDetails, scopes, username);
        tokenRetriever.setAuthenticationIdGenerator(authenticationIdGenerator);
        tokenRetriever.setoAuthManager(oAuthManager);
        tokenRetriever.setoAuthIssuer(oAuthIssuer);
        return tokenRetriever.retrieve();
    }


    //grant_type=client_credentials
    @Override
    public AccessToken retrieveClientCredentialsAccessToken(ClientDetails clientDetails, Set<String> scopes) throws OAuthSystemException {
        ClientCredentialsAccessTokenRetriever tokenRetriever = new ClientCredentialsAccessTokenRetriever(clientDetails, scopes);
        tokenRetriever.setAuthenticationIdGenerator(authenticationIdGenerator);
        tokenRetriever.setoAuthManager(oAuthManager);
        tokenRetriever.setoAuthIssuer(oAuthIssuer);
        return tokenRetriever.retrieve();
    }

    @Override
    public AccessToken loadAccessTokenByRefreshToken(String refreshToken, String clientId) {
        LOG.debug("Load ClientDetails by refreshToken: {} and clientId: {}", refreshToken, clientId);
        return oAuthManager.findAccessTokenByRefreshToken(refreshToken, clientId);
    }

    /*
    * Get AccessToken
    * Generate a new AccessToken from existed(exclude token,refresh_token)
    * Update access_token,refresh_token, expired.
    * Save and remove old
    * */
    @Override
    public AccessToken changeAccessTokenByRefreshToken(String refreshToken, String clientId) throws OAuthSystemException {
        AccessTokenByRefreshTokenChanger refreshTokenChanger = new AccessTokenByRefreshTokenChanger(refreshToken, clientId);
        refreshTokenChanger.setoAuthManager(oAuthManager);
        refreshTokenChanger.setoAuthIssuer(oAuthIssuer);
        refreshTokenChanger.setAuthenticationIdGenerator(authenticationIdGenerator);

        return refreshTokenChanger.change();
    }


}
