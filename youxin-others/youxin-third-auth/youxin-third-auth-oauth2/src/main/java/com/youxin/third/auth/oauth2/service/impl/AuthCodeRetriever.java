/*
 * Copyright (c) 2015 MONKEYK Information Technology Co. Ltd
 * www.monkeyk.com
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * MONKEYK Information Technology Co. Ltd ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with MONKEYK Information Technology Co. Ltd.
 */
package com.youxin.third.auth.oauth2.service.impl;

import com.youxin.third.auth.model.ClientDetails;
import com.youxin.third.auth.model.OauthCode;
import com.youxin.third.auth.oauth2.service.AbstractOAuthHolder;
import com.youxin.third.auth.service.manager.OAuthManager;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 2015/10/22
 *
 * @author Shengzhao Li
 */
public class AuthCodeRetriever extends AbstractOAuthHolder {

    private static final Logger LOG = LoggerFactory.getLogger(AuthCodeRetriever.class);

    private OAuthManager oAuthManager;


    private ClientDetails clientDetails;

    protected transient OAuthIssuer oAuthIssuer;

    public OAuthManager getoAuthManager() {
        return oAuthManager;
    }

    public void setoAuthManager(OAuthManager oAuthManager) {
        this.oAuthManager = oAuthManager;
    }

    public OAuthIssuer getoAuthIssuer() {
        return oAuthIssuer;
    }

    public void setoAuthIssuer(OAuthIssuer oAuthIssuer) {
        this.oAuthIssuer = oAuthIssuer;
    }

    public AuthCodeRetriever(ClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }

    public String retrieve() throws OAuthSystemException {

        final String clientId = clientDetails.getClientId();
        final String username = currentUsername();

        OauthCode oauthCode = oAuthManager.findOauthCodeByUsernameClientId(username, clientId);
        if (oauthCode != null) {
            //Always delete exist
            LOG.debug("OauthCode ({}) is existed, remove it and create a new one", oauthCode);
            oAuthManager.deleteOauthCode(oauthCode);
        }
        //create a new one
        oauthCode = createOauthCode();

        return oauthCode.getCode();
    }


    private OauthCode createOauthCode() throws OAuthSystemException {
        final String authCode = oAuthIssuer.authorizationCode();

        LOG.debug("Save OauthCode authorizationCode '{}' of ClientDetails '{}'", authCode, clientDetails);
        final String username = currentUsername();
        OauthCode oauthCode = new OauthCode();
        oauthCode.setCode(authCode);
        oauthCode.setUsername(username);
        oauthCode.setClientId(clientDetails.getClientId());

        oAuthManager.saveOauthCode(oauthCode);
        return oauthCode;
    }


}
