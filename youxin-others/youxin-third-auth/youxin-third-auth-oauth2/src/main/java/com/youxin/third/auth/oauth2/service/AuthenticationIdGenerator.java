package com.youxin.third.auth.oauth2.service;

public interface AuthenticationIdGenerator {

    public String generate(String clientId, String username, String scope);



}
