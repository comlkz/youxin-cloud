package com.youxin.third.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.third.auth.model.AccessToken;
import org.apache.ibatis.annotations.Param;

public interface AccessTokenMapper extends BaseMapper<AccessToken> {

    AccessToken findAccessToken(@Param("clientId") String clientId, @Param("username") String username, @Param("authenticationId") String authenticationId);

    int deleteAccessToken(@Param("record") AccessToken accessToken);

    AccessToken findAccessTokenByRefreshToken(@Param("refreshToken") String refreshToken, @Param("clientId") String clientId);


}
