package com.youxin.third.auth.service;


import com.youxin.exception.SystemException;
import com.youxin.third.auth.entity.dto.ThridAuthDto;

public interface OauthCodeService {
    void setThirdAuth(ThridAuthDto thirdAuth) throws SystemException;

    ThridAuthDto selectThirdAuth(String userNo, String clientId);

    void updateThirdAuthByUserNo(ThridAuthDto thridAuthDto, String userNo);

}
