package com.youxin.third.auth.service;


import com.youxin.base.TPage;
import com.youxin.third.auth.entity.dto.ClientDetailDto;
import com.youxin.third.auth.entity.dto.ClientSearch;

/**
 * description: ClientDetailService <br>
 * date: 2020/2/7 15:22 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface ClientDetailService {

    ClientDetailDto getClientDetail(String clientId);

    void saveClientDetail(ClientDetailDto clientDetailDto);

    TPage<ClientDetailDto> page(ClientSearch clientSearch);
}
