package com.youxin.third.auth.convert;


import com.youxin.third.auth.entity.dto.ClientDetailDto;
import com.youxin.third.auth.model.ClientDetails;

/**
 * description: ClientDetailConvert <br>
 * date: 2020/2/10 11:38 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class ClientDetailConvert {

    public static ClientDetailDto modelToDto(ClientDetails clientDetails){
        if(clientDetails == null){
             return null;
        }
        ClientDetailDto clientDetailDto = new ClientDetailDto();

        clientDetailDto.setClientId(clientDetails.getClientId());
        clientDetailDto.setClientSecret(clientDetails.getClientSecret());
        clientDetailDto.setClientName(clientDetails.getClientName());
        clientDetailDto.setClientUri(clientDetails.getClientUri());
        clientDetailDto.setClientIconUri(clientDetails.getClientIconUri());
        clientDetailDto.setResourceIds(clientDetails.getResourceIds());
        clientDetailDto.setScope(clientDetails.getScope());
        clientDetailDto.setGrantTypes(clientDetails.getGrantTypes());
        clientDetailDto.setRedirectUri(clientDetails.getRedirectUri());
        clientDetailDto.setRoles(clientDetails.getRoles());
        clientDetailDto.setAccessTokenValidity(clientDetails.getAccessTokenValidity());
        clientDetailDto.setRefreshTokenValidity(clientDetails.getRefreshTokenValidity());
        clientDetailDto.setDescription(clientDetails.getDescription());
        clientDetailDto.setCreateTime(clientDetails.getCreateTime());
        clientDetailDto.setArchived(clientDetails.getArchived());
        clientDetailDto.setTrusted(clientDetails.getTrusted());
        return clientDetailDto;
    }

    public static ClientDetails dtoToModel(ClientDetailDto clientDetailDto){
        if(clientDetailDto == null){
            return null;
        }
        ClientDetails clientDetails = new ClientDetails();
        clientDetails.setClientId(clientDetailDto.getClientId());
        clientDetails.setClientSecret(clientDetailDto.getClientSecret());
        clientDetails.setClientName(clientDetailDto.getClientName());
        clientDetails.setClientUri(clientDetailDto.getClientUri());
        clientDetails.setClientIconUri(clientDetailDto.getClientIconUri());
        clientDetails.setResourceIds(clientDetailDto.getResourceIds());
        clientDetails.setScope(clientDetailDto.getScope());
        clientDetails.setGrantTypes(clientDetailDto.getGrantTypes());
        clientDetails.setRedirectUri(clientDetailDto.getRedirectUri());
        clientDetails.setRoles(clientDetailDto.getRoles());
        clientDetails.setAccessTokenValidity(clientDetailDto.getAccessTokenValidity());
        clientDetails.setRefreshTokenValidity(clientDetailDto.getRefreshTokenValidity());
        clientDetails.setDescription(clientDetailDto.getDescription());
        clientDetails.setCreateTime(clientDetailDto.getCreateTime());
        clientDetails.setArchived(clientDetailDto.getArchived());
        clientDetails.setTrusted(clientDetailDto.getTrusted());
        return clientDetails;
    }
}
