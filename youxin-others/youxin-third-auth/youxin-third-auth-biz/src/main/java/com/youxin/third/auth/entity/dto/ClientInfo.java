package com.youxin.third.auth.entity.dto;

import java.io.Serializable;

/**
 * description: ClientInfo <br>
 * date: 2020/2/10 12:18 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class ClientInfo implements Serializable {

    private String clientId;

    private String clientIconUri;

    private String clientName;

    private String scopes;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientIconUri() {
        return clientIconUri;
    }

    public void setClientIconUri(String clientIconUri) {
        this.clientIconUri = clientIconUri;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getScopes() {
        return scopes;
    }

    public void setScopes(String scopes) {
        this.scopes = scopes;
    }
}
