package com.youxin.third.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.youxin.third.auth.entity.dto.ThridAuthDto;
import com.youxin.third.auth.model.ThridAuth;
import org.apache.ibatis.annotations.Param;

public interface ThridAuthMapper extends BaseMapper<ThridAuth> {
    void saveThridAuth(ThridAuth thirdAuth);

    ThridAuthDto selectThirdAuth(@Param("userNo") String userNo, @Param("clientId") String clientId);
}
