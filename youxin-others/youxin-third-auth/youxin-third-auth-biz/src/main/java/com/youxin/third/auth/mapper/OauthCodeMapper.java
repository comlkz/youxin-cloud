package com.youxin.third.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.third.auth.model.OauthCode;
import org.apache.ibatis.annotations.Param;

public interface OauthCodeMapper extends BaseMapper<OauthCode> {

    OauthCode findOauthCode(@Param("code") String code, @Param("clientId") String clientId);

    OauthCode findOauthCodeByUsernameClientId(@Param("username") String username, @Param("clientId") String clientId);

    int deleteOauthCode(@Param("record") OauthCode oauthCode);
}
