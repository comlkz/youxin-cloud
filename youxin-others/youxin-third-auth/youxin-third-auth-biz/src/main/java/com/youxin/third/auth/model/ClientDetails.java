package com.youxin.third.auth.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.util.Date;

public class ClientDetails implements Serializable {
    /**
     * `client_id` varchar(255) NOT NULL,
     *   `client_secret` varchar(255) DEFAULT NULL,
     *   `client_name` varchar(255) DEFAULT NULL,
     *   `client_uri` varchar(255) DEFAULT NULL,
     *   `client_icon_uri` varchar(255) DEFAULT NULL,
     *   `resource_ids` varchar(255) DEFAULT NULL,
     *   `scope` varchar(255) DEFAULT NULL,
     *   `grant_types` varchar(255) DEFAULT NULL,
     *   `redirect_uri` varchar(255) DEFAULT NULL,
     *   `roles` varchar(255) DEFAULT NULL,
     *   `access_token_validity` int(11) DEFAULT '-1',
     *   `refresh_token_validity` int(11) DEFAULT '-1',
     *   `description` varchar(4096) DEFAULT NULL,
     *   `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
     *   `archived` tinyint(1) DEFAULT '0',
     *   `trusted` tinyint(1) DEFAULT '0',
     */
    @TableId(type = IdType.INPUT)
    private String clientId;

    private String clientSecret;

    private String clientName;

    private String clientUri;

    private String clientIconUri;

    private String resourceIds;

    private String scope;

    private String grantTypes;

    private String redirectUri;

    private String roles;

    private Integer accessTokenValidity;

    private Integer refreshTokenValidity;

    private String description;

    private Date createTime;

    private Integer archived;

    private Integer trusted;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientUri() {
        return clientUri;
    }

    public void setClientUri(String clientUri) {
        this.clientUri = clientUri;
    }

    public String getClientIconUri() {
        return clientIconUri;
    }

    public void setClientIconUri(String clientIconUri) {
        this.clientIconUri = clientIconUri;
    }

    public String getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(String resourceIds) {
        this.resourceIds = resourceIds;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getGrantTypes() {
        return grantTypes;
    }

    public void setGrantTypes(String grantTypes) {
        this.grantTypes = grantTypes;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public Integer getAccessTokenValidity() {
        return accessTokenValidity;
    }

    public void setAccessTokenValidity(Integer accessTokenValidity) {
        this.accessTokenValidity = accessTokenValidity;
    }

    public Integer getRefreshTokenValidity() {
        return refreshTokenValidity;
    }

    public void setRefreshTokenValidity(Integer refreshTokenValidity) {
        this.refreshTokenValidity = refreshTokenValidity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getArchived() {
        return archived;
    }

    public void setArchived(Integer archived) {
        this.archived = archived;
    }

    public Integer getTrusted() {
        return trusted;
    }

    public void setTrusted(Integer trusted) {
        this.trusted = trusted;
    }

    public boolean supportRefreshToken() {
        return this.grantTypes != null && this.grantTypes.contains("refresh_token");
    }
}
