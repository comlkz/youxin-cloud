package com.youxin.third.auth.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.youxin.dozer.DozerUtils;
import com.youxin.exception.SystemException;
import com.youxin.third.auth.entity.dto.ThridAuthDto;
import com.youxin.third.auth.mapper.ThridAuthMapper;
import com.youxin.third.auth.model.ThridAuth;
import com.youxin.third.auth.service.OauthCodeService;
import com.youxin.third.auth.service.manager.OAuthManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class OauthCodeServiceImpl implements OauthCodeService {

    private static final Logger log = LoggerFactory.getLogger(OauthCodeService.class);

    @Resource
    private OAuthManager oAuthManager;

    @Resource
    private ThridAuthMapper thridAuthMapper;

    @Resource
    private DozerUtils dozerUtils;

    @Override
    public void setThirdAuth(ThridAuthDto thirdAuthDto) throws SystemException {
        ThridAuth thirdAuth =dozerUtils.map(thirdAuthDto,ThridAuth.class);
        log.info("thirdAuth=" + JSONObject.toJSONString(thirdAuth));
        if (thirdAuth.getId() == null) {
            thridAuthMapper.insert(thirdAuth);
        }else{
            thridAuthMapper.updateById(thirdAuth);
        }

    }

    @Override
    public ThridAuthDto selectThirdAuth(String userNo, String clientId) {
        return thridAuthMapper.selectThirdAuth(userNo, clientId);
    }

    @Override
    public void updateThirdAuthByUserNo(ThridAuthDto thridAuthDto, String userNo) {

        ThridAuth thirdAuth = JSONObject.parseObject(JSONObject.toJSONString(thridAuthDto),ThridAuth.class);
        log.info("thirdAuth=" + JSONObject.toJSONString(thirdAuth));

        LambdaQueryWrapper<ThridAuth> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ThridAuth::getUserNo,userNo);
        thridAuthMapper.update(thirdAuth,queryWrapper);
    }

}
