package com.youxin.third.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.third.auth.model.ClientDetails;

public interface ClientDetailsMapper extends BaseMapper<ClientDetails> {
}
