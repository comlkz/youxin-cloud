package com.youxin.third.auth.controller.platform;


import com.youxin.base.TResult;
import com.youxin.chat.user.client.UserClient;
import com.youxin.chat.user.dto.UserInfoDto;
import com.youxin.common.config.YouxinCommonProperties;
import com.youxin.dozer.DozerUtils;
import com.youxin.third.auth.context.PlatformContextHandler;
import com.youxin.third.auth.entity.dto.ThridAuthDto;
import com.youxin.third.auth.service.OauthCodeService;
import com.youxin.utils.MD5Util;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@RestController
@RequestMapping("platform")
public class PlatformController {

    @Resource
    private OauthCodeService oauthCodeService;

    @Reference
    private UserClient userClient;

    @Resource
    private DozerUtils dozerUtils;

    @Resource
    private YouxinCommonProperties youxinCommonProperties;

    @ApiOperation("查询三方信息")
    @PostMapping("userinfo")
    public TResult<ThridAuthDto> setThirdAuth(HttpServletRequest request) {

        ThridAuthDto thridAuthDto = oauthCodeService.selectThirdAuth(PlatformContextHandler.getUserName(), PlatformContextHandler.getClientId());
        if (thridAuthDto == null) {
            UserInfoDto userInfoDto = userClient.getByUserNo(PlatformContextHandler.getUserName());
            ThridAuthDto thirdAuth = new ThridAuthDto();
            thirdAuth.setClientId(PlatformContextHandler.getClientId());
            String openId = MD5Util.MD5(userInfoDto.getUserNo()+ PlatformContextHandler.getClientId());
            thirdAuth.setOpenId(openId);
            thirdAuth.setUserNo(userInfoDto.getUserNo());
            thirdAuth.setNickName(userInfoDto.getNickName());
            thirdAuth.setSex(userInfoDto.getSex());
            thirdAuth.setArea(userInfoDto.getArea());
            thirdAuth.setAvatar(youxinCommonProperties.getDownloadUrl()+userInfoDto.getAvatar());
            thirdAuth.setUserStatus(userInfoDto.getUserStatus());
            thirdAuth.setCreateTime(LocalDateTime.now());
            oauthCodeService.setThirdAuth(thirdAuth);

            thridAuthDto = oauthCodeService.selectThirdAuth(PlatformContextHandler.getUserName(), PlatformContextHandler.getClientId());
            return TResult.success(thridAuthDto);
        } else {
            UserInfoDto userInfoDto = userClient.getByUserNo(PlatformContextHandler.getUserName());
            ThridAuthDto thirdAuth = new ThridAuthDto();
            thirdAuth.setId(thridAuthDto.getId());
            thirdAuth.setNickName(userInfoDto.getNickName());
            thirdAuth.setSex(userInfoDto.getSex());
            thirdAuth.setArea(userInfoDto.getArea());
            thirdAuth.setAvatar(youxinCommonProperties.getDownloadUrl()+userInfoDto.getAvatar());
            oauthCodeService.setThirdAuth(thirdAuth);
            thridAuthDto.setNickName(userInfoDto.getNickName());
            thridAuthDto.setSex(userInfoDto.getSex());
            thridAuthDto.setArea(userInfoDto.getArea());
            thridAuthDto.setAvatar(youxinCommonProperties.getDownloadUrl()+userInfoDto.getAvatar());
            return TResult.success(thridAuthDto);
        }
    }
}
