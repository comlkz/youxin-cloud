package com.youxin.third.auth.context;



import com.youxin.context.BaseContextConstants;
import com.youxin.utils.NumberHelper;
import com.youxin.utils.StrHelper;

import java.util.HashMap;
import java.util.Map;


/**
 * 获取当前域中的 用户id appid 用户昵称
 * 注意： appid 通过token解析，  用户id 和 用户昵称必须在前端 通过请求头的方法传入。 否则这里无法获取
 *
 * @author zuihou
 * @createTime 2017-12-13 16:52
 */
public class PlatformContextHandler {
    private static final ThreadLocal<Map<String, String>> THREAD_LOCAL = new ThreadLocal<>();

    private static final String ACCESS_TOKEN="ACCESS_TOKEN";

    private static final String CLIENT_ID = "CLIENT_ID";

    private static final String USER_NAME = "USER_NAME";

    public static void set(String key, Long value) {
        Map<String, String> map = getLocalMap();
        map.put(key, value == null ? "0" : String.valueOf(value));
    }

    public static void set(String key, String value) {
        Map<String, String> map = getLocalMap();
        map.put(key, value == null ? "" : value);
    }

    public static void set(String key, Boolean value) {
        Map<String, String> map = getLocalMap();
        map.put(key, value == null ? "false" : value.toString());
    }


    public static Map<String, String> getLocalMap() {
        Map<String, String> map = THREAD_LOCAL.get();
        if (map == null) {
            map = new HashMap<>(10);
            THREAD_LOCAL.set(map);
        }
        return map;
    }

    public static void setLocalMap(Map<String, String> threadLocalMap) {
        THREAD_LOCAL.set(threadLocalMap);
    }


    public static String get(String key) {
        Map<String, String> map = getLocalMap();
        return map.getOrDefault(key, "");
    }

    public static Boolean isBoot() {
        Object value = get(BaseContextConstants.IS_BOOT);
        return NumberHelper.boolValueOf0(value);
    }

    /**
     * 账号id
     *
     * @param val
     */
    public static void setBoot(Boolean val) {
        set(BaseContextConstants.IS_BOOT, val);
    }

    /**
     * accessToken
     *
     * @return
     */
    public static String getAccessToken() {
        Object value = get(ACCESS_TOKEN);
        return returnObjectValue(value);
    }

    /**
     * 账号id
     *
     * @param accessToken
     */
    public static void setAccessToken(String accessToken) {
        set(ACCESS_TOKEN, accessToken);
    }

    /**
     * accessToken
     *
     * @return
     */
    public static String getUserName() {
        Object value = get(USER_NAME);
        return returnObjectValue(value);
    }

    /**
     * 账号id
     *
     * @param userName
     */
    public static void setUserName(String userName) {
        set(USER_NAME, userName);
    }

    /**
     * accessToken
     *
     * @return
     */
    public static String getClientId() {
        Object value = get(CLIENT_ID);
        return returnObjectValue(value);
    }

    /**
     * 账号id
     *
     * @param clientId
     */
    public static void setClientId(String clientId) {
        set(CLIENT_ID, clientId);
    }

    public static void remove() {
        if (THREAD_LOCAL != null) {
            THREAD_LOCAL.remove();
        }
    }

    private static String returnObjectValue(Object value) {
        return value == null ? "" : value.toString();
    }


}
