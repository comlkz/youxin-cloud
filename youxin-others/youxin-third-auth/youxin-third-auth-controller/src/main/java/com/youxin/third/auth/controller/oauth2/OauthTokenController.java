package com.youxin.third.auth.controller.oauth2;

import com.youxin.third.auth.oauth2.process.OAuthTokenxRequest;
import com.youxin.third.auth.oauth2.process.token.OAuthTokenHandleDispatcher;
import com.youxin.third.auth.oauth2.service.OauthService;
import com.youxin.third.auth.resource.context.WebUtils;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 2015/7/3
 * <p/>
 * URL: oauth/token
 *
 * @author Shengzhao Li
 */
@Controller
@RequestMapping("authz/oauth/")
public class OauthTokenController {



    @Resource
    private OauthService oauthService;

    /**
     * Handle grant_types as follows:
     * <p/>
     * grant_type=authorization_code
     * grant_type=password
     * grant_type=refresh_token
     * grant_type=client_credentials
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws Exception
     */
    @RequestMapping("token")
    public void authorize(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            OAuthTokenxRequest tokenRequest = new OAuthTokenxRequest(request);
            OAuthTokenHandleDispatcher tokenHandleDispatcher = new OAuthTokenHandleDispatcher(tokenRequest, response, oauthService);
            tokenHandleDispatcher.dispatch();

        } catch (OAuthProblemException e) {
            //exception
            OAuthResponse oAuthResponse = OAuthASResponse
                    .errorResponse(HttpServletResponse.SC_OK)
                    .location(e.getRedirectUri())
                    .error(e)
                    .buildJSONMessage();
            WebUtils.writeOAuthJsonResponse(response, oAuthResponse);
        }

    }
}
