package com.youxin.third.auth.config;

import com.youxin.third.auth.config.interceptors.AdminInterceptor;
import com.youxin.third.auth.config.interceptors.OAuthInterceptor;
import com.youxin.third.auth.config.interceptors.PlatformInterceptor;
import javafx.print.PageLayout;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * description: ThirdWebConfig <br>
 * date: 2020/3/8 11:26 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Configuration
public class ThirdWebConfig implements WebMvcConfigurer {

    @Resource
    private OAuthInterceptor oAuthInterceptor;

    @Resource
    private PlatformInterceptor platformInterceptor;

    @Resource
    private AdminInterceptor adminInterceptor;

    /**
     * 注册 拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(oAuthInterceptor)
                .addPathPatterns("/authz/**")
                .order(10)
           .excludePathPatterns("/authz/oauth/token","/authz/scope/**");
        registry.addInterceptor(platformInterceptor)
                .addPathPatterns("/platform/**")
                .order(10);
        registry.addInterceptor(adminInterceptor)
                .addPathPatterns("/admin/**")
                .order(10);
      /*  registry.addInterceptor(permissionHandlerInterceptor)
                .addPathPatterns("/**")
                .order(11)
                .excludePathPatterns(commonPathPatterns);*/
        WebMvcConfigurer.super.addInterceptors(registry);
    }

}
