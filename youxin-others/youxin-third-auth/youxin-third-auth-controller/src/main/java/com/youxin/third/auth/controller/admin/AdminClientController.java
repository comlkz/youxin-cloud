package com.youxin.third.auth.controller.admin;

import com.youxin.base.TPage;
import com.youxin.base.TResult;
import com.youxin.third.auth.entity.dto.ClientDetailDto;
import com.youxin.third.auth.entity.dto.ClientSearch;
import com.youxin.third.auth.service.ClientDetailService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * description: AdminClientController <br>
 * date: 2020/3/8 12:15 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
@RequestMapping("admin")
public class AdminClientController {

    @Resource
    private ClientDetailService clientDetailService;

    @PostMapping("client/save")
    public TResult<String> saveClient(@RequestBody ClientDetailDto clientDetailDto){
        clientDetailService.saveClientDetail(clientDetailDto);
        return TResult.success();
    }

    @GetMapping("client/info/{clientId}")
    public TResult<ClientDetailDto> clientInfo(@PathVariable String clientId){
        ClientDetailDto clientDetailDto =  clientDetailService.getClientDetail(clientId);
        return TResult.success(clientDetailDto);
    }

    @GetMapping("client/page")
    public TResult<TPage<ClientDetailDto>> page(ClientSearch search){
        TPage<ClientDetailDto> page =  clientDetailService.page(search);
        return TResult.success(page);
    }
}
