package com.youxin.third.auth.controller.oauth2;

import com.youxin.base.BaseResultCode;
import com.youxin.base.TResult;
import com.youxin.chat.user.client.UserClient;
import com.youxin.chat.user.dto.UserInfoDto;
import com.youxin.common.config.YouxinCommonProperties;
import com.youxin.context.BaseContextHandler;
import com.youxin.third.auth.domain.AuthRequest;
import com.youxin.third.auth.entity.dto.ClientDetailDto;
import com.youxin.third.auth.entity.dto.ClientInfo;
import com.youxin.third.auth.service.ClientDetailService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping("/authz")
public class LoginController {

    @Resource
    private ClientDetailService clientDetailService;

    @Reference
    private UserClient userClient;

    @Resource
    private YouxinCommonProperties youxinCommonProperties;

    @GetMapping("scope/{clientId}")
    @ResponseBody
    public TResult<ClientInfo> acquireScopes(@PathVariable String clientId){
        ClientDetailDto clientDetailDto = clientDetailService.getClientDetail(clientId);
        if(clientDetailDto == null){
            return TResult.build(BaseResultCode.RECORD_NOT_EXISTS,"帐号不存在");
        }
        if(clientDetailDto.getArchived() != 0){
            return TResult.build(BaseResultCode.HAS_NO_PERMISSION,"帐号已被禁用");
        }
        ClientInfo clientInfo = new ClientInfo();
        clientInfo.setClientId(clientId);
        clientInfo.setClientIconUri(clientDetailDto.getClientIconUri());
        clientInfo.setClientName(clientDetailDto.getClientName());
        String[] scopes = clientDetailDto.getScope().split(" ");
        clientInfo.setScopes(Stream.of(scopes).collect(Collectors.joining(",")));
        return TResult.success(clientInfo);
    }

    @RequestMapping("oauth/login")
    @ResponseBody
    public ModelAndView login(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("oauth_approval");
        Map<String,Object> param = new HashMap<>();

        if(!StringUtils.isEmpty(request.getParameter("token"))) {
            response.addCookie(new Cookie("token", request.getParameter("token")));
        }
        String clientId = request.getParameter("client_id");
        ClientDetailDto clientDetailDto = clientDetailService.getClientDetail(clientId);
        String userNo = BaseContextHandler.getUserNo();
        UserInfoDto userInfoDto = userClient.getByUserNo(userNo);
        modelAndView.addObject("clientName", clientDetailDto.getClientName());
        modelAndView.addObject("clientIcon", clientDetailDto.getClientIconUri());
        modelAndView.addObject("nickName", userInfoDto.getNickName());
        modelAndView.addObject("avatar", youxinCommonProperties.getDownloadUrl() +userInfoDto.getAvatar());

        modelAndView.addObject("response_type", "code");
        modelAndView.addObject("redirect_uri", "https://localhost/authorization_code_callback");
        return modelAndView;
    }
}
