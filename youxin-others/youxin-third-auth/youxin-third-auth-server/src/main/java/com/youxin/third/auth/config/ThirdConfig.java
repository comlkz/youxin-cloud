package com.youxin.third.auth.config;

import com.youxin.common.config.BaseConfig;
import org.springframework.context.annotation.Configuration;

/**
 * description: ThirdConfig <br>
 * date: 2020/3/8 13:35 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Configuration
public class ThirdConfig extends BaseConfig {
}
