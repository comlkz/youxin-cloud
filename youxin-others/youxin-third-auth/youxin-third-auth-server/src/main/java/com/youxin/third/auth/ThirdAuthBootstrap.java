package com.youxin.third.auth;

import com.youxin.auth.client.EnableAuthClient;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * description: AdminUserBootstrap <br>
 * date: 2020/3/4 17:10 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.youxin")
@EnableAuthClient
public class ThirdAuthBootstrap {

    public static void main(String[] args) {

        SpringApplication.run(ThirdAuthBootstrap.class,args);
    }
}
