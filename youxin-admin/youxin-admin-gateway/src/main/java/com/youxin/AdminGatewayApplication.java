package com.youxin;

import com.youxin.auth.client.EnableAuthClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * description: GatewayApplicatioin <br>
 * date: 2020/2/25 11:25 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableAuthClient
public class AdminGatewayApplication {



    public static void main(String[] args) {
        SpringApplication.run(AdminGatewayApplication.class,args);
    }
}
