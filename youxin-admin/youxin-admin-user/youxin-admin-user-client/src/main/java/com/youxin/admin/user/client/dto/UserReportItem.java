package com.youxin.admin.user.client.dto;

import java.io.Serializable;
import java.util.Date;

public class UserReportItem implements Serializable {

    private Integer num;

    private Date staticDate;

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Date getStaticDate() {
        return staticDate;
    }

    public void setStaticDate(Date staticDate) {
        this.staticDate = staticDate;
    }
}
