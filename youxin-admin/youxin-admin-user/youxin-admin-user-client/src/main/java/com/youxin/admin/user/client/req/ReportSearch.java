package com.youxin.admin.user.client.req;


import com.youxin.base.TQuery;

public class ReportSearch extends TQuery {

    private String startTime;

    private String endTime;

    private Boolean groupDate  = false;


    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Boolean getGroupDate() {
        return groupDate;
    }

    public void setGroupDate(Boolean groupDate) {
        this.groupDate = groupDate;
    }
}
