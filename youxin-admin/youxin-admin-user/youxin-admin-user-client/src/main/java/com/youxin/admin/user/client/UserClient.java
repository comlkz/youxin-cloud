package com.youxin.admin.user.client;

import com.youxin.admin.user.client.dto.UserReportItem;
import com.youxin.admin.user.client.req.ReportSearch;

import java.util.List;

/**
 * description: UserReportClient <br>
 * date: 2020/3/6 11:31 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface UserClient {

    List<UserReportItem> staticActiveReport(ReportSearch search);

   int staticUserReport(ReportSearch search);
}
