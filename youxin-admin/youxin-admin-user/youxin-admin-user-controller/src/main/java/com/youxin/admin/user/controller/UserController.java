package com.youxin.admin.user.controller;

import com.youxin.admin.user.entity.req.CreateSpecialUserReq;
import com.youxin.admin.user.entity.req.UserSearchReq;
import com.youxin.admin.user.entity.vo.UserInfoVo;
import com.youxin.admin.user.model.UserGroup;
import com.youxin.admin.user.service.GroupService;
import com.youxin.admin.user.service.UserService;
import com.youxin.admin.user.service.rpc.RemoteUserService;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * description: UserController <br>
 * date: 2020/3/4 16:11 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private RemoteUserService remoteUserService;

    @Resource
    private GroupService groupService;

    /**
     * APP用户列表
     * @return
     */
    @ApiOperation("APP用户列表")
    @GetMapping(value = "/user/list")
    //@RequiresPermissions("app:userList")
    public TResult<TPage<UserInfoVo>> getUserList(UserSearchReq userSearchDto) {
        return TResult.success(userService.selectAppUserList(userSearchDto));
    }

    /**
     * APP用户锁定
     * @return
     */
    @ApiOperation("APP用户锁定")
    @PostMapping(value = "/user/status/{id}/{status}")
    //@RequiresPermissions("app:lockUser")
    public TResult<String> lockUser(@PathVariable String id, @PathVariable Integer status) {
        remoteUserService.updateUserStates(id,status);
        return TResult.success();
    }

    @ApiOperation("特殊用户创建")
    @PostMapping(value = "/user/create")
    public TResult<String> addSpecialUser(@RequestBody CreateSpecialUserReq createSpecialUserReq){
        userService.createSpecialUserReq(createSpecialUserReq);
        return TResult.success();
    }

    @GetMapping(value = "/user/group/page/{userNo}")
    public TResult<TPage<UserGroup>> getUserGroupList(@PathVariable String userNo,
                                                      @RequestParam(name = "currentPage",defaultValue = "1")Integer currentPage,
                                                      @RequestParam(name = "showCount",defaultValue = "10")Integer showCount) {
        return TResult.success(groupService.pageUserGroup(userNo,currentPage,showCount));
    }
}
