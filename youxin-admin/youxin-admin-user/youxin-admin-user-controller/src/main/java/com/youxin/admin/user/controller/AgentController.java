package com.youxin.admin.user.controller;


import com.youxin.admin.user.entity.req.AgentSearchReq;
import com.youxin.admin.user.model.AgentInfo;
import com.youxin.admin.user.service.AgentService;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("agent")
public class AgentController {

    @Resource
    private AgentService agentService;

    @PostMapping("save")
    public TResult<String> saveAgent(@RequestBody AgentInfo agentInfoDto){
        agentService.saveAgent(agentInfoDto);
        return TResult.success();
    }

    @GetMapping("page")
    public TResult<TPage<AgentInfo>> page(AgentSearchReq agentSearch){
        TPage<AgentInfo> page = agentService.pageAgents(agentSearch);
        return TResult.success(page);
    }

    @GetMapping("info/{id}")
    public TResult<AgentInfo> info(@PathVariable String id){
        AgentInfo agentInfo = agentService.getById(id);
        return TResult.success(agentInfo);
    }
}
