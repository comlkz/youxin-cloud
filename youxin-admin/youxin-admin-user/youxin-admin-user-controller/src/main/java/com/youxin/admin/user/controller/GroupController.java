package com.youxin.admin.user.controller;

import com.youxin.admin.user.entity.req.GroupSearchReq;
import com.youxin.admin.user.entity.vo.GroupVo;
import com.youxin.admin.user.model.GroupMember;
import com.youxin.admin.user.service.GroupService;
import com.youxin.admin.user.service.rpc.RemoteUserService;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * description: GroupController <br>
 * date: 2020/3/10 11:18 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
@RequestMapping("group")
public class GroupController {

    @Resource
    private GroupService groupService;

    @Resource
    private RemoteUserService remoteUserService;

    @GetMapping(value = "/page")
    public TResult<TPage<GroupVo>> getGroupList(GroupSearchReq groupSearchReq) {
        return TResult.success(groupService.pageGroupsByParam(groupSearchReq));
    }

    @GetMapping(value = "member/page/{groupNo}")
    public TResult<TPage<GroupMember>> getGroupMemberList(@PathVariable String groupNo, @RequestParam(name = "currentPage",defaultValue = "1")Integer currentPage,
                                                          @RequestParam(name = "showCount",defaultValue = "10")Integer showCount) {
        return TResult.success(groupService.pageGroupMember(groupNo,currentPage,showCount));
    }

    @PostMapping("warning/{groupNo}")
    public TResult<String> sendGroupWarning(@PathVariable String groupNo){
        remoteUserService.sendGroupWarning(groupNo);
        return TResult.success();
    }

    @PostMapping("lock/{groupNo}")
    public TResult<String> lockGroup(@PathVariable String groupNo){
        remoteUserService.lockGroup(groupNo);
        return TResult.success();
    }
}
