package com.youxin.admin.user.controller;


import com.youxin.admin.user.entity.req.ChatComplaintReq;
import com.youxin.admin.user.model.ChatComplaint;
import com.youxin.admin.user.service.ComplaintService;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import com.youxin.common.config.YouxinCommonProperties;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 投诉表
 */
@RestController
@RequestMapping("chatComplaint")
public class ChatComplaintController {
    @Resource
    private ComplaintService complaintService;
    @Resource
    private YouxinCommonProperties youxinCommonProperties;

    @PostMapping("list")
    public TResult<TPage<ChatComplaint>> list(@RequestBody ChatComplaintReq chatComplaintReq) {
        TPage<ChatComplaint> list = complaintService.getComplaintList(chatComplaintReq);
        return TResult.success(list);
    }

    @GetMapping("get/{id}")
    public TResult<ChatComplaint> get(@PathVariable Integer id) {
        ChatComplaint chatComplaint = complaintService.getById(id);
        if (!StringUtils.isEmpty(chatComplaint.getPictureId())) {
            String str[] = chatComplaint.getPictureId().split(",");
            String pictures = "";
            for (int i = 0; i < str.length; i++) {
                pictures += youxinCommonProperties.getDownloadUrl() + str[i] + ",";
            }
            chatComplaint.setPictureId(pictures.substring(0, pictures.length() - 1));
        }
        return TResult.success(chatComplaint);
    }
}
