package com.youxin.admin.user.controller;


import com.youxin.admin.user.entity.req.ChatFeedbackReq;
import com.youxin.admin.user.model.ChatFeedback;
import com.youxin.admin.user.service.FeedbackService;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 意见反馈表
 */
@RestController
@RequestMapping("chatFeedback")
public class ChatFeedbackController {
    @Resource
    private FeedbackService feedbackService;

    @PostMapping("list")
    public TResult<TPage<ChatFeedback>> list(@RequestBody ChatFeedbackReq chatFeedbackReq){

        TPage<ChatFeedback> list = feedbackService.getFeedbackList(chatFeedbackReq);
        return TResult.success(list);
    }

    @GetMapping("get/{id}")
    public TResult<ChatFeedback> get(@PathVariable Integer id){
        ChatFeedback chatFeedback = feedbackService.getById(id);
        return TResult.success(chatFeedback);
    }
}
