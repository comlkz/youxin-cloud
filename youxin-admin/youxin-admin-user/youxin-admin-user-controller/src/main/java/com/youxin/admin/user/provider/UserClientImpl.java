package com.youxin.admin.user.provider;

import com.youxin.admin.user.client.UserClient;
import com.youxin.admin.user.client.dto.UserReportItem;
import com.youxin.admin.user.client.req.ReportSearch;
import com.youxin.admin.user.service.UserService;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * description: UserClientImpl <br>
 * date: 2020/3/6 11:38 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class UserClientImpl implements UserClient {

    @Resource
    private UserService userService;

    @Override
    public List<UserReportItem> staticActiveReport(ReportSearch search) {
        return userService.staticActiveReport(search);
    }

    @Override
    public int staticUserReport(ReportSearch search) {
        return userService.staticUserReport(search);
    }
}
