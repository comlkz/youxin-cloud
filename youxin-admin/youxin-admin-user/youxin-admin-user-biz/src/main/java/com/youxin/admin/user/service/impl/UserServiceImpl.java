package com.youxin.admin.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.user.client.dto.UserReportItem;
import com.youxin.admin.user.client.req.ReportSearch;
import com.youxin.admin.user.entity.req.CreateSpecialUserReq;
import com.youxin.admin.user.entity.req.UserSearchReq;
import com.youxin.admin.user.entity.vo.UserInfoVo;
import com.youxin.admin.user.mapper.UserInfoMapper;
import com.youxin.admin.user.model.UserInfo;
import com.youxin.admin.user.service.UserService;
import com.youxin.admin.user.service.rpc.RemoteUserService;
import com.youxin.base.BaseResultCode;
import com.youxin.base.TPage;
import com.youxin.chat.user.dto.UserAddDto;
import com.youxin.common.config.YouxinCommonProperties;
import com.youxin.dozer.DozerUtils;
import com.youxin.exception.SystemException;
import com.youxin.utils.MD5Util;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * description: UserServiceImpl <br>
 * date: 2020/3/4 15:42 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private YouxinCommonProperties youxinCommonProperties;

    @Resource
    private RemoteUserService remoteUserService;

    @Resource
    private DozerUtils dozerUtils;

    @Override
    public TPage<UserInfoVo> selectAppUserList(UserSearchReq userSearchDto) {

        IPage<UserInfoVo> page = userInfoMapper.selectAppUserList(new Page(userSearchDto.getCurrentPage(), userSearchDto.getShowCount()), userSearchDto);
        page.getRecords().stream().forEach(item -> {
            item.setAvatar(youxinCommonProperties.getDownloadUrl() + item.getAvatar());
        });
        return TPage.page(page.getRecords(), page.getTotal());
    }

    @Override
    public void createSpecialUserReq(CreateSpecialUserReq createSpecialUserReq) {
        if (createSpecialUserReq.getUserType() == 2 && !StringUtils.isEmpty(createSpecialUserReq.getMobile())) {
            int count = userInfoMapper.selectCount(new LambdaQueryWrapper<UserInfo>().eq(UserInfo::getMobile, createSpecialUserReq.getMobile()));
            if (count > 0) {
                throw new SystemException(BaseResultCode.RECORD_EXISTS, "手机号已存在");
            }
        }
        UserAddDto userAddDto = dozerUtils.map(createSpecialUserReq, UserAddDto.class);
        if (createSpecialUserReq.getUserType() != 1 && !StringUtils.isEmpty(createSpecialUserReq.getPassword())) {
            String salt = getLoginSalt(createSpecialUserReq.getMobile());
            String password = MD5Util.md5Salt(createSpecialUserReq.getPassword().toUpperCase(), salt, 1);
            userAddDto.setPassword(password);
        }
        if (!StringUtils.isEmpty(createSpecialUserReq.getAvatar())) {
            int index = createSpecialUserReq.getAvatar().lastIndexOf("/");
            if (index > 0) {
                userAddDto.setAvatar(createSpecialUserReq.getAvatar().substring(index + 1));
            } else {
                userAddDto.setAvatar(createSpecialUserReq.getAvatar());
            }
        }
        remoteUserService.saveUser(userAddDto);
    }

    @Override
    public List<UserReportItem> staticActiveReport(ReportSearch search) {
        return userInfoMapper.staticActiveReport(search);
    }

    @Override
    public int staticUserReport(ReportSearch search) {
        return userInfoMapper.staticReport(search);
    }

    private  String getLoginSalt(String mobile){

        int endIndex = mobile.length();
        int beginIndex = 0;

        if (endIndex > 8) {
            beginIndex = endIndex - 8;
        }
        String salt = mobile.substring(beginIndex, endIndex);
        return salt;
    }
}
