package com.youxin.admin.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.user.entity.req.GroupSearchReq;
import com.youxin.admin.user.entity.vo.GroupVo;
import com.youxin.admin.user.mapper.GroupMapper;
import com.youxin.admin.user.mapper.GroupMembermapper;
import com.youxin.admin.user.mapper.UserGroupMapper;
import com.youxin.admin.user.model.GroupMember;
import com.youxin.admin.user.model.UserGroup;
import com.youxin.admin.user.service.GroupService;
import com.youxin.base.TPage;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

/**
 * description: GroupServiceImpl <br>
 * date: 2020/3/10 11:10 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class GroupServiceImpl implements GroupService {

    @Resource
    private GroupMapper groupMapper;

    @Resource
    private GroupMembermapper groupMembermapper;

    @Resource
    private UserGroupMapper userGroupMapper;

    @Override
    public TPage<GroupVo> pageGroupsByParam(GroupSearchReq req) {
        IPage<GroupVo> page = groupMapper.selectByParam(new Page<>(req.getCurrentPage(),req.getShowCount()),req);
        return TPage.page(page.getRecords(),page.getTotal());
    }

    @Override
    public TPage<GroupMember> pageGroupMember(String groupNo, Integer currentPage, Integer showCount) {
        LambdaQueryWrapper<GroupMember> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(GroupMember::getGroupNo,groupNo);
        IPage<GroupMember> page = groupMembermapper.selectPage(new Page<>(currentPage,showCount),queryWrapper);
        return TPage.page(page.getRecords(),page.getTotal());
    }

    @Override
    public TPage<UserGroup> pageUserGroup(String userNo, Integer currentPage, Integer showCount) {
        LambdaQueryWrapper<UserGroup> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserGroup::getUserNo,userNo);
        IPage<UserGroup> page = userGroupMapper.selectPage(new Page<>(currentPage,showCount),queryWrapper);
        return TPage.page(page.getRecords(),page.getTotal());    }
}
