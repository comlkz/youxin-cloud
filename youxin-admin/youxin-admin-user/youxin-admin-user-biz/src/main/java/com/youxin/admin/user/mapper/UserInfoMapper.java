package com.youxin.admin.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.user.client.dto.UserReportItem;
import com.youxin.admin.user.client.req.ReportSearch;
import com.youxin.admin.user.entity.req.UserSearchReq;
import com.youxin.admin.user.entity.vo.UserInfoVo;
import com.youxin.admin.user.model.UserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserInfoMapper extends BaseMapper<UserInfo> {

    IPage<UserInfoVo> selectAppUserList(Page page, @Param("userSearchDto") UserSearchReq userSearchDto);


    int staticReport(@Param("record")ReportSearch search);

    List<UserReportItem> staticActiveReport(@Param("record")ReportSearch search);
}
