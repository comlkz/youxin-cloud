package com.youxin.admin.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.user.model.ChatComplaint;
import org.apache.ibatis.annotations.Param;

public interface ChatComplaintMapper extends BaseMapper<ChatComplaint> {
}
