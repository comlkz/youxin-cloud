package com.youxin.admin.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.user.entity.req.AgentSearchReq;
import com.youxin.admin.user.model.AgentInfo;
import com.youxin.base.TPage;

/**
 * description: AgentService <br>
 * date: 2020/3/4 16:37 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface AgentService extends IService<AgentInfo> {

    TPage<AgentInfo> pageAgents(AgentSearchReq agentSearch);

    void saveAgent(AgentInfo agentInfo);
}
