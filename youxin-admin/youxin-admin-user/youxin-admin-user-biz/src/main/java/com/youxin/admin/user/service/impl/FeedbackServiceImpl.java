package com.youxin.admin.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.admin.user.entity.req.ChatFeedbackReq;
import com.youxin.admin.user.mapper.ChatFeedbackMapper;
import com.youxin.admin.user.model.ChatFeedback;
import com.youxin.admin.user.service.FeedbackService;
import com.youxin.base.TPage;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * description: FeedbackServiceImpl <br>
 * date: 2020/3/4 16:46 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class FeedbackServiceImpl extends ServiceImpl<ChatFeedbackMapper, ChatFeedback> implements FeedbackService {
    @Override
    public TPage<ChatFeedback> getFeedbackList(ChatFeedbackReq feedbackReq) {
        LambdaQueryWrapper<ChatFeedback> queryWrapper = new LambdaQueryWrapper<>();
        if(!StringUtils.isEmpty(feedbackReq.getFeedback())){
            queryWrapper.like(ChatFeedback::getFeedback,feedbackReq.getFeedback());
        }
        queryWrapper.orderByDesc(ChatFeedback::getCrtTime);
        IPage<ChatFeedback> page = this.baseMapper.selectPage(new Page<>(feedbackReq.getCurrentPage(),feedbackReq.getShowCount()),queryWrapper);
        return TPage.page(page.getRecords(),page.getTotal());
    }
}
