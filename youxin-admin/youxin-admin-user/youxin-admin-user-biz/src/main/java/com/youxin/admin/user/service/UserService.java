package com.youxin.admin.user.service;

import com.youxin.admin.user.client.dto.UserReportItem;
import com.youxin.admin.user.client.req.ReportSearch;
import com.youxin.admin.user.entity.req.CreateSpecialUserReq;
import com.youxin.admin.user.entity.req.UserSearchReq;
import com.youxin.admin.user.entity.vo.UserInfoVo;
import com.youxin.base.TPage;

import java.util.List;

/**
 * description: UserService <br>
 * date: 2020/3/4 15:42 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface UserService {

    TPage<UserInfoVo> selectAppUserList(UserSearchReq userSearchDto);

    void createSpecialUserReq(CreateSpecialUserReq createSpecialUserReq);

    List<UserReportItem> staticActiveReport(ReportSearch search);

    int staticUserReport(ReportSearch search);
}
