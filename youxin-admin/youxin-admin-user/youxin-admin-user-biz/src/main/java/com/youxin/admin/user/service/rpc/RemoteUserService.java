package com.youxin.admin.user.service.rpc;

import com.youxin.chat.user.client.GroupClient;
import com.youxin.chat.user.client.UserClient;
import com.youxin.chat.user.dto.UserAddDto;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

/**
 * description: RemoteUserService <br>
 * date: 2020/3/4 16:17 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class RemoteUserService {

    @Reference(retries = 1)
    private UserClient userClient;

    @Reference(retries = 1)
    private GroupClient groupClient;

    public void updateUserStates(String id, Integer status){
        userClient.updateUserStates(id, status);
    }

    public void saveUser(UserAddDto userAddDto){
        userClient.saveUser(userAddDto);
    }

    /**
     * 发送群警告
     * @param groupNo
     */
    public void sendGroupWarning(String groupNo){
        groupClient.sendGroupWarning(groupNo);
    }

    /**
     * 冻结群组
     * @param groupNo
     */
    public void lockGroup(String groupNo){
        groupClient.lockGroup(groupNo);
    }
}
