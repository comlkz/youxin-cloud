package com.youxin.admin.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.user.entity.vo.AgentInfoVo;
import com.youxin.admin.user.model.AgentInfo;
import org.apache.ibatis.annotations.Param;

public interface AgentInfoMapper extends BaseMapper<AgentInfo> {

    void updateStatusByAgentNo(@Param("status") Integer status, @Param("agentNo") String agentNo);

    AgentInfoVo selectByAgentNo(@Param("agentNo") String agentNo);
}
