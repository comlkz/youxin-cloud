package com.youxin.admin.user.entity.req;


import com.youxin.base.TQuery;

public class AgentSearchReq extends TQuery {

    private String agentNo;

    public String getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(String agentNo) {
        this.agentNo = agentNo;
    }
}
