package com.youxin.admin.user.entity.req;


import com.youxin.base.TQuery;

public class ChatFeedbackReq extends TQuery {
    private String feedback;

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
