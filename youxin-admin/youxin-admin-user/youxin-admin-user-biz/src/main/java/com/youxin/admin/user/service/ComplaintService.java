package com.youxin.admin.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.user.entity.req.ChatComplaintReq;
import com.youxin.admin.user.model.ChatComplaint;
import com.youxin.base.TPage;

/**
 * description: ComplaintService <br>
 * date: 2020/3/4 16:45 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface ComplaintService extends IService<ChatComplaint> {

    TPage<ChatComplaint> getComplaintList(ChatComplaintReq chatComplaintReq);
}
