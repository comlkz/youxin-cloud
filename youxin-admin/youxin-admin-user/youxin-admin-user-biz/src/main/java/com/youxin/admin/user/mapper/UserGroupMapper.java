package com.youxin.admin.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.user.model.UserGroup;

/**
 * description: UserGroupMapper <br>
 * date: 2020/3/10 11:07 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface UserGroupMapper extends BaseMapper<UserGroup> {
}
