package com.youxin.admin.user.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

@TableName("t_group_member")
public class GroupMember {

    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    private String groupNo;//群聊ID

    private String groupUser;//群成员编码

    private String groupNickName;//群成员昵称

    private String groupNote;//群成员备注

    private String avatar;//群成员头像

    //成员类型 0：普通成员 1：群主 2：管理员
    private Byte userType;

    private Date createTime;

    private Date updateTime;//修改时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id  ;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupUser() {
        return groupUser;
    }

    public void setGroupUser(String groupUser) {
        this.groupUser = groupUser == null ? null : groupUser.trim();
    }

    public String getGroupNickName() {
        return groupNickName;
    }

    public void setGroupNickName(String groupNickName) {
        this.groupNickName = groupNickName == null ? null : groupNickName.trim();
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar == null ? null : avatar.trim();
    }



    public Byte getUserType() {
        return userType;
    }

    public void setUserType(Byte userType) {
        this.userType = userType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getGroupNote() {
        return groupNote;
    }

    public void setGroupNote(String groupNote) {
        this.groupNote = groupNote;
    }
}
