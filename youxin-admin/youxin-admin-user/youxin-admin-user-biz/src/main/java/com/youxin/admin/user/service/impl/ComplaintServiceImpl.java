package com.youxin.admin.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.admin.user.entity.req.ChatComplaintReq;
import com.youxin.admin.user.mapper.ChatComplaintMapper;
import com.youxin.admin.user.model.ChatComplaint;
import com.youxin.admin.user.model.ChatFeedback;
import com.youxin.admin.user.service.ComplaintService;
import com.youxin.base.TPage;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * description: ComplaintServiceImpl <br>
 * date: 2020/3/4 16:46 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class ComplaintServiceImpl extends ServiceImpl<ChatComplaintMapper, ChatComplaint> implements ComplaintService {
    @Override
    public TPage<ChatComplaint> getComplaintList(ChatComplaintReq chatComplaintReq) {
        LambdaQueryWrapper<ChatComplaint> queryWrapper = new LambdaQueryWrapper<>();
        if(!StringUtils.isEmpty(chatComplaintReq.getComplaint())){
            queryWrapper.like(ChatComplaint::getComplaint,chatComplaintReq.getComplaint());
        }
        queryWrapper.orderByDesc(ChatComplaint::getCrtTime);
        IPage<ChatComplaint> page = this.baseMapper.selectPage(new Page<>(chatComplaintReq.getCurrentPage(),chatComplaintReq.getShowCount()),queryWrapper);
        return TPage.page(page.getRecords(),page.getTotal());
    }
}
