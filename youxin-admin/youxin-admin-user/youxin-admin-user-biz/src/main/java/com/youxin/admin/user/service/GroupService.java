package com.youxin.admin.user.service;

import com.youxin.admin.user.entity.req.GroupSearchReq;
import com.youxin.admin.user.entity.vo.GroupVo;
import com.youxin.admin.user.model.GroupMember;
import com.youxin.admin.user.model.UserGroup;
import com.youxin.base.TPage;

/**
 * description: GroupService <br>
 * date: 2020/3/10 11:08 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface GroupService {

    TPage<GroupVo> pageGroupsByParam(GroupSearchReq req);

    TPage<GroupMember> pageGroupMember(String groupNo,Integer currentPage,Integer showCount);

    TPage<UserGroup> pageUserGroup(String userNo, Integer currentPage, Integer showCount);

}
