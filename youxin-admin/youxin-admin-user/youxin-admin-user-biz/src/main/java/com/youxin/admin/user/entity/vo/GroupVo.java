package com.youxin.admin.user.entity.vo;

import java.util.Date;

/**
 * description: GroupVo <br>
 * date: 2020/3/10 10:54 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class GroupVo {

    private String groupNo;

    private String groupName;//群聊名称


    private String groupOwner;//群主编号

    private Integer groupStatus;

    private Date createTime;

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public Integer getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(Integer groupStatus) {
        this.groupStatus = groupStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
