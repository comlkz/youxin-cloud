package com.youxin.admin.user.entity.req;


import com.youxin.base.TQuery;

public class ChatComplaintReq extends TQuery {
    private String complaint;

    public String getComplaint() {
        return complaint;
    }

    public void setComplaint(String complaint) {
        this.complaint = complaint;
    }
}
