package com.youxin.admin.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.user.entity.req.GroupSearchReq;
import com.youxin.admin.user.entity.vo.GroupVo;
import com.youxin.admin.user.model.Group;
import org.apache.ibatis.annotations.Param;

/**
 * description: GroupMapper <br>
 * date: 2020/3/10 10:59 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface GroupMapper extends BaseMapper<Group> {

    IPage<GroupVo> selectByParam(Page<GroupVo> page,@Param("record") GroupSearchReq groupSearchReq);
}
