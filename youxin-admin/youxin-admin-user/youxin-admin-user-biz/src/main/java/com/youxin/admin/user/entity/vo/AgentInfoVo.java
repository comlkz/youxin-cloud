package com.youxin.admin.user.entity.vo;

import java.io.Serializable;
import java.time.LocalDateTime;

public class AgentInfoVo implements Serializable {

    private static final long serialVersionUID = 6710056120879156687L;

    private String id;

    /**
     * 代理商编号
     */
    private String agentNo;

    /**
     * 代理商昵称
     */
    private String nickName;

    /**
     * 代理手机号
     */
    private String mobile;

    /**
     * 游戏描述
     */
    private String gameDesc;

    /**
     * 推广URL
     */
    private String promotionUrl;

    /**
     * 所属尤信号
     */
    private String userNo;

    /**
     * 状态 0：禁用 1：正常
     */
    private Integer agentStatus;


    private LocalDateTime createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(String agentNo) {
        this.agentNo = agentNo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGameDesc() {
        return gameDesc;
    }

    public void setGameDesc(String gameDesc) {
        this.gameDesc = gameDesc;
    }

    public String getPromotionUrl() {
        return promotionUrl;
    }

    public void setPromotionUrl(String promotionUrl) {
        this.promotionUrl = promotionUrl;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public Integer getAgentStatus() {
        return agentStatus;
    }

    public void setAgentStatus(Integer agentStatus) {
        this.agentStatus = agentStatus;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
}
