package com.youxin.admin.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.user.model.GroupMember;

/**
 * description: GroupMembermapper <br>
 * date: 2020/3/10 11:06 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface GroupMembermapper extends BaseMapper<GroupMember> {
}
