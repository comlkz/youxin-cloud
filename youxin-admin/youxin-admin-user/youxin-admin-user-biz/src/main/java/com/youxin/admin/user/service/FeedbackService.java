package com.youxin.admin.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.user.entity.req.ChatFeedbackReq;
import com.youxin.admin.user.model.ChatFeedback;
import com.youxin.base.TPage;

/**
 * description: FeedbackService <br>
 * date: 2020/3/4 16:45 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface FeedbackService extends IService<ChatFeedback> {

    TPage<ChatFeedback> getFeedbackList(ChatFeedbackReq feedbackReq);

}
