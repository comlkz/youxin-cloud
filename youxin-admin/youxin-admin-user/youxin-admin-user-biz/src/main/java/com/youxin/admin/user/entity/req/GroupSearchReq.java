package com.youxin.admin.user.entity.req;

import com.youxin.base.TQuery;

/**
 * description: GroupSearchReq <br>
 * date: 2020/3/10 10:53 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class GroupSearchReq extends TQuery {

    private String groupNo;

    private String groupName;

    private String startTime;

    private String endTime;

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
