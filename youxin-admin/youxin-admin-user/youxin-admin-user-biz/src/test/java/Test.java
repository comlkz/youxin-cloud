import com.alibaba.fastjson.JSONObject;
import com.youxin.admin.user.model.UserGroup;
import com.youxin.base.Builder;
import io.rong.RongCloud;
import io.rong.models.Result;
import io.rong.models.user.UserModel;

import javax.annotation.Resource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * description: Test <br>
 * date: 2020/3/14 14:58 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class Test {

//    public static void main(String[] args) {
//        UserGroup userGroup = Builder.of(UserGroup::new)
//                .with(UserGroup::setId,"1")
//                .with(UserGroup::setCreateTime, LocalDateTime.now())
//                .build();
//        System.out.println(userGroup.getId());
//    }

    public static final String DBDRIVER = "com.mysql.cj.jdbc.Driver";
    public static final String DBURL = "jdbc:mysql://47.111.115.236:3608/chat";
    public static final String DBUSER = "root";
    public static final String DBPASS = "WdeG9>5CEBDLIM(Y<c3v";


    public static void main(String[] args) throws Exception {
        RongCloud rongCloud = RongCloud.getInstance("82hegw5u8xcwx", "IqWy0sUceIket");
        Connection conn = null;// 数据库连接
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM t_user_info";
        Class.forName(DBDRIVER);
        conn = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
        stmt = conn.createStatement();
        rs = stmt.executeQuery(sql);
        UserModel userModel = new UserModel();
        String id;
        String nickName;
        String avatar;
        try {
            while (rs.next()) {

                id = rs.getString("USER_NO");
                nickName = Optional.ofNullable(rs.getString("NICK_NAME")).orElse("default");
                avatar = Optional.ofNullable(rs.getString("AVATAR")).orElse("default");
                avatar = "https://file.fy-pay.com/file/download/" + avatar;

                userModel = new UserModel();
                userModel.setId(id)
                        .setName(nickName)
                        .setPortrait(avatar);
                Result result = rongCloud.user.update(userModel);

                System.out.print("用户编码: " + id + ";");
                System.out.print("昵称: " + nickName + ";");
                System.out.println("头像: " + avatar + ";");
                System.out.println("---------------------");

            }
        } catch (Exception e) {
            System.out.println("更新融云用户信息失败" + JSONObject.toJSONString(userModel) + e);
        }
        rs.close();
        stmt.close();
        conn.close();
    }
}
