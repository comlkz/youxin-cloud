package com.youxin.admin.user;

import com.youxin.admin.annotation.EnableLoginArgResolver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * description: AdminUserBootstrap <br>
 * date: 2020/3/4 17:10 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@SpringBootApplication
@EnableLoginArgResolver
@ComponentScan(basePackages = "com.youxin")
public class AdminUserBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(AdminUserBootstrap.class,args);
    }
}
