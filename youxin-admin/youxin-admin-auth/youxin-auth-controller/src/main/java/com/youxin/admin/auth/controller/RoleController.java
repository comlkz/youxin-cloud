package com.youxin.admin.auth.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.annotation.LoginUser;
import com.youxin.admin.auth.domain.AuthorityElementDto;
import com.youxin.admin.auth.domain.AuthorityMenuTree;
import com.youxin.admin.auth.domain.RoleDto;
import com.youxin.admin.auth.domain.RoleUser;
import com.youxin.admin.auth.model.Role;
import com.youxin.admin.auth.model.User;
import com.youxin.admin.auth.service.ChatRoleService;
import com.youxin.admin.auth.service.ChatUserService;
import com.youxin.admin.model.SysUser;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;

/**
 * @create 2019/5/23
 */
@RestController
@RequestMapping("auth/role")
public class RoleController {


    @Resource
    private ChatRoleService chatRoleService;
    @Resource
    private ChatUserService chatUserService;

    /**
     * 后台角色列表
     *
     * @param name
     * @param currentPage
     * @param showCount
     * @return
     */
    @GetMapping(value = "/list")
    public TResult<TPage<List<Role>>> list(@RequestParam(value = "name", required = false) String name, Integer currentPage, Integer showCount) {

        QueryWrapper example = new QueryWrapper();
        if (StringUtils.isNotBlank(name)) {
            example.like("name", name);
        }
        IPage page = chatRoleService.selectPage(new Page<>(currentPage, showCount), example);
        return TResult.success(TPage.page(page.getRecords(), page.getTotal()));
    }


    @PostMapping(value = "/editRoleUser")
//    @RequiresPermissions("role:bindUser")
    public TResult<String> modifiyUsers(int roleId, int userId, int action) {
        chatRoleService.modifyRoleUsers(roleId, userId, action);
        return TResult.success();
    }

    @GetMapping(value = "/{id}/user")
    //@RequiresPermissions("role:listUser")
    public TResult<TPage<List<RoleUser>>> getUsers(@PathVariable int id,
                                                   @RequestParam(value = "username", required = false) String username,
                                                   Integer currentPage, Integer showCount) {
        return TResult.success(chatRoleService.getRoleUsers(id, username, currentPage, showCount));
    }

    @PostMapping(value = "/{id}/authority/menu")
//    @RequiresPermissions("role:bindResource")
    public TResult<String> modifyMenuAuthority(@PathVariable int id, String menuTrees) {
        String[] menus = menuTrees.split(",");
        chatRoleService.modifyAuthorityMenu(id, menus);
        return TResult.success();
    }

    @GetMapping(value = "/{id}/authority/menu")
    //@RequiresPermissions("role:listResource")
    public TResult<List<AuthorityMenuTree>> getMenuAuthority(@PathVariable int id) {
        return TResult.success(chatRoleService.getAuthorityMenu(id));
    }

    @PostMapping(value = "/{id}/authority/element/add")
    @ResponseBody
    public TResult<String> addElementAuthority(@PathVariable int id, int menuId, int elementId) {
        chatRoleService.modifyAuthorityElement(id, menuId, elementId);
        return TResult.success();
    }

    @PostMapping(value = "/{id}/authority/element/batchAdd")
    @ResponseBody
    public TResult<String> batchAddElementAuthority(@PathVariable int id, @RequestBody AuthorityElementDto authorityElementDto) {
        chatRoleService.batchModifyAuthorityElement(id, authorityElementDto.getMenuId(), authorityElementDto.getElementIds());
        return TResult.success();
    }

    @PostMapping(value = "/{id}/authority/element/batchRemove")
    @ResponseBody
    public TResult<String> batchRemoveElementAuthority(@PathVariable int id, @RequestBody AuthorityElementDto authorityElementDto) {
        chatRoleService.batchRemoveElementAuthority(id, authorityElementDto.getMenuId(), authorityElementDto.getElementIds());
        return TResult.success();
    }

    @PostMapping(value = "/{id}/authority/element/remove")
    @ResponseBody
    public TResult<String> removeElementAuthority(@PathVariable int id, int menuId, int elementId) {
        chatRoleService.removeAuthorityElement(id, menuId, elementId);
        return TResult.success();
    }

    @GetMapping(value = "/{id}/authority/element")
    @ResponseBody
    public TResult<List<Integer>> getElementAuthority(@PathVariable int id) {
        return TResult.success(chatRoleService.getAuthorityElement(id));
    }

    @PostMapping(value = "save")
    //@RequiresPermissions("role:add")
    public TResult<String> addRole(@ApiIgnore @LoginUser(isFull = false) SysUser user, @RequestBody Role role) {
        role.setCreateBy(user.getUserName());
        boolean flag = chatRoleService.insert(role);
        return TResult.success();
    }

    @GetMapping(value = "/{id}")
    public TResult<Role> get(@PathVariable int id) {
        Role role = chatRoleService.selectById(id);
        return TResult.success(role);
    }

    @PutMapping(value = "/{id}")
    //@RequiresPermissions("role:del")
    public TResult<String> del(@PathVariable int id) {
        chatRoleService.deleteById(id);
        return TResult.success();
    }

    @PutMapping(value = "/update")
    //@RequiresPermissions("role:edit")
    public TResult<String> update(@ApiIgnore @LoginUser(isFull = false) SysUser sysUser,@RequestBody RoleDto roleDto) {
        User user = chatUserService.getUserByUsername(sysUser.getUserName());
        roleDto.setUpdateBy(user.getName());
        chatRoleService.updateById(roleDto);
        return TResult.success();
    }

}
