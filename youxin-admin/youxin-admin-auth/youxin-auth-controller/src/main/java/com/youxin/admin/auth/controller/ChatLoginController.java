package com.youxin.admin.auth.controller;


import com.youxin.admin.annotation.LoginUser;
import com.youxin.admin.auth.model.UserDto;
import com.youxin.admin.auth.service.ChatUserService;
import com.youxin.admin.auth.service.ValidateCodeService;
import com.youxin.admin.model.SysUser;
import com.youxin.base.TResult;
import com.youxin.common.constant.RedisKey;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.io.ByteArrayOutputStream;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("auth")
public class ChatLoginController {
    /**
     * 1、验证码工具
     */
    @Resource
    private ValidateCodeService validateCodeService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private ChatUserService chatUserService;



    @PostMapping("/login")
    public TResult<UserDto> login(
            @NotBlank(message = "{required}") String username,
            @NotBlank(message = "{required}") String password,
            @NotBlank(message = "{required}") String merchantNo,
            @NotBlank(message = "{required}") String code,
            @NotBlank(message = "{required}") String key,

            HttpServletRequest request) throws Exception {

        return chatUserService.login(username, password, merchantNo, code, key, request);
        /*// 保存登录记录
        LoginLog loginLog = new LoginLog();
        loginLog.setUsername(username);
        this.loginLogService.saveLoginLog(loginLog);*/


    }


    /**
     * 2、生成验证码
     *
     * @param
     * @param
     * @throws Exception
     */
    @GetMapping("/captcha")
    public void defaultKaptcha(@RequestParam(value = "key") String key, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        this.validateCodeService.create(key, response);



    }

    @PostMapping("/logout")
    @ResponseBody
    public TResult<String> logout() {

        return TResult.success();
    }

}
