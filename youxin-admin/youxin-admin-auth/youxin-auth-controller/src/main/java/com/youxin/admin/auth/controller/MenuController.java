package com.youxin.admin.auth.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;

import com.youxin.admin.annotation.LoginUser;
import com.youxin.admin.auth.domain.ManageMenu;
import com.youxin.admin.auth.domain.MenuDto;
import com.youxin.admin.auth.domain.MenuSearch;
import com.youxin.admin.auth.model.Menu;
import com.youxin.admin.auth.service.ChatMenuService;
import com.youxin.admin.auth.service.ChatUserService;
import com.youxin.admin.auth.service.PermissionService;
import com.youxin.admin.model.SysUser;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @create 2019/5/23
 */
@Validated
@RestController
@RequestMapping("auth/menu")
public class MenuController {
    @Autowired
    private ChatUserService chatUserService;

    @Autowired
    private ChatMenuService chatMenuService;

    @Resource
    private PermissionService permissionService;


    @PostMapping(value = "/addMenu")
    //@RequiresPermissions("menu:add")
    public TResult<String> addMenu(@RequestBody Menu menu) {
        menu.setType("0");
        boolean flag = chatMenuService.insert(menu);
        return TResult.success();
    }

    /**
     * 菜单详情
     * @param id
     * @return
     */
    @GetMapping(value = "getMenu/{id}")
    public TResult<Menu> getMenu(@PathVariable int id) {
        Menu menu = chatMenuService.selectById(id);
        return TResult.success(menu);
    }

    @PutMapping(value = "delMenu/{id}")
    //@RequiresPermissions("menu:del")
    public TResult<String> delMenu(@PathVariable int id) {
        chatMenuService.removeById(id);
        return TResult.success();
    }

    @PutMapping(value = "/updateMenu")
    //@RequiresPermissions("menu:edit")
    public TResult<String> updateMenu(@RequestBody Menu menu) {
        chatMenuService.updateById(menu);
        return TResult.success();
    }

    @PostMapping(value = "/addButton")
    //@RequiresPermissions("menu:addButton")
    public TResult<String> addButton(@RequestBody MenuDto menuDto) {
        Menu menu = new Menu();
        menu.setCode(menuDto.getCode());
        menu.setName(menuDto.getName());
        menu.setParentId(Integer.valueOf(menuDto.getMenuId()));
        menu.setDescription(menuDto.getDescription());
        menu.setCrtTime(menuDto.getCrtTime());
        menu.setCrtHost(menuDto.getCrtHost());
        menu.setCrtName(menuDto.getCrtName());
        menu.setCrtUser(menuDto.getCrtUser());
        menu.setType("1");
        boolean flag = chatMenuService.insert(menu);
        return TResult.success();
    }

    /**
     * 功能详情
     * @param id
     * @return
     */
    @GetMapping(value = "getButton/{id}")
    public TResult<MenuDto> getButton(@PathVariable int id) {
        Menu menu = chatMenuService.selectById(id);
        MenuDto menuDto = new MenuDto();
        menuDto.setCode(menu.getCode());
        menuDto.setName(menu.getName());
        menuDto.setMenuId(String.valueOf(menu.getParentId()));
        menuDto.setDescription(menu.getDescription());
        menuDto.setCrtTime(menu.getCrtTime());
        menuDto.setCrtName(menu.getCrtName());
        menuDto.setCrtHost(menu.getCrtHost());
        menuDto.setCrtUser(menu.getCrtUser());
        return TResult.success(menuDto);
    }

    @PutMapping(value = "delButton/{id}")
    //@RequiresPermissions("menu:delButton")
    public TResult<String> delButton(@PathVariable int id) {
        chatMenuService.removeById(id);
        return TResult.success();
    }

    @PutMapping(value = "/updateButton")
    //@RequiresPermissions("menu:updateButton")
    public TResult<String> updateButton(@RequestBody MenuDto menuDto) {
        Menu menu = new Menu();
        menu.setId(menuDto.getId());
        menu.setName(menuDto.getName());
        menu.setCode(menuDto.getCode());
        menu.setDescription(menuDto.getDescription());
        menu.setParentId(Integer.valueOf(menuDto.getMenuId()));
        menu.setCrtTime(menuDto.getCrtTime());
        menu.setCrtHost(menuDto.getCrtHost());
        menu.setCrtName(menuDto.getCrtName());
        menu.setCrtUser(menuDto.getCrtUser());
        chatMenuService.updateById(menu);
        return TResult.success();
    }
/*
    @GetMapping(value = "/all")
    public TResult<List<Menu>> all(){
        return TResult.success(chatMenuService.selectListAll());
    }*/


    /**
     * 分页查询全部菜单
     * @param limit
     * @param offset
     * @param name
     * @param menuId
     * @return
     */
    @GetMapping(value = "/menuList")
    //@RequiresPermissions("menuManager")
    public TResult<TPage<Menu>> menu(@RequestParam(defaultValue = "50") int limit,
                                     @RequestParam(defaultValue = "1") int offset, String name, @RequestParam(defaultValue = "0") int menuId) {

        IPage<Menu> page = chatMenuService.selectByPage(name, menuId, offset, limit, "0");
        return TResult.success(TPage.page(page.getRecords(), page.getTotal()));
    }

    /**
     * 分页查询全部功能
     * @param limit
     * @param offset
     * @param name
     * @param menuId
     * @return
     */
    @GetMapping(value = "/buttonList")
    public TResult<TPage<Menu>> button(@RequestParam(defaultValue = "50") int limit,
                                       @RequestParam(defaultValue = "1") int offset, String name, @RequestParam(defaultValue = "0") int menuId) {

        IPage<Menu> page = chatMenuService.selectByPage(name, menuId, offset, limit, "1");
        return TResult.success(TPage.page(page.getRecords(), page.getTotal()));
    }

    /**
     * 当前用户菜单功能
     * @param menuId
     * @return
     */
    @GetMapping(value = "/getAuthorityMenu/{menuId}")
    public TResult<List<Menu>> getAuthorityMenu(@ApiIgnore @LoginUser(isFull = false) SysUser user,@PathVariable String menuId) {
        int userId = user.getId();
        List<Menu> menus = chatMenuService.getAuthorityMenuByMenuId(userId + "", menuId);
        return TResult.success(menus);
    }

    /**
     * 查询用户菜单功能
     */
    @GetMapping(value = "/getUserMenu/{menuId}/{userId}")
    public TResult<List<Menu>> getUserMenu(@PathVariable String menuId, @PathVariable String userId) {
        List<Menu> menus = chatMenuService.getUserMenuByUserId(userId + "", menuId);
        return TResult.success(menus);
    }

    /**
     * 查询用户权限
     * @return
     */
    @GetMapping(value = "/user/menuList")
    public TResult<List<Menu>> getAuthorityMenuList(@ApiIgnore @LoginUser(isFull = false) SysUser user) {
        int userId = user.getId();
        List<Menu> elements = chatMenuService.getAuthorityMenuListByUserId(userId + "");
        return TResult.success(elements);
    }

    @GetMapping("/tree")
    public TResult<List<ManageMenu>> listMenus() {
        MenuSearch search = new MenuSearch();
        search.setBusinessType(1);
        TResult<List<ManageMenu>> result = chatMenuService.listTreeMenu(search);
        return result;
    }

    @GetMapping("permissions")
    public TResult<Map<String, Object>> info(@ApiIgnore @LoginUser(isFull = false) SysUser user) {

        return chatUserService.info(user.getUserName());
    }

}
