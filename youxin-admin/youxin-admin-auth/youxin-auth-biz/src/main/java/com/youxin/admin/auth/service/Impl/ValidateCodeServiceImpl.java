package com.youxin.admin.auth.service.Impl;


import com.wf.captcha.ArithmeticCaptcha;
import com.wf.captcha.ChineseCaptcha;
import com.wf.captcha.GifCaptcha;
import com.wf.captcha.SpecCaptcha;
import com.wf.captcha.base.Captcha;
import com.youxin.admin.auth.service.ValidateCodeService;
import com.youxin.base.BaseResultCode;
import com.youxin.common.constant.RedisKey;
import com.youxin.exception.SystemException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * 验证码服务
 *
 * @author zuihou
 */
@Service
public class ValidateCodeServiceImpl implements ValidateCodeService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void create(String key, HttpServletResponse response) throws IOException {
        if (StringUtils.isBlank(key)) {
            throw new SystemException(BaseResultCode.COMMON_FAIL,"验证码key不能为空");
        }
        setHeader(response, "arithmetic");

        Captcha captcha = createCaptcha("arithmetic");
        stringRedisTemplate.opsForValue().set(RedisKey.CAPTCHA_CODE+key, StringUtils.lowerCase(captcha.text()),2, TimeUnit.MINUTES);
        captcha.out(response.getOutputStream());
    }

    @Override
    public boolean check(String key, String value) {
        if (StringUtils.isBlank(value)) {
            throw new SystemException(BaseResultCode.COMMON_FAIL,"请输入验证码");

        }
       String codeValue =  stringRedisTemplate.opsForValue().get(RedisKey.CAPTCHA_CODE+key);
        if (StringUtils.isEmpty(codeValue)) {
            throw new SystemException(BaseResultCode.COMMON_FAIL,"验证码已过期");

        }
        if (!StringUtils.equalsIgnoreCase(value, String.valueOf(codeValue))) {
            throw new SystemException(BaseResultCode.COMMON_FAIL,"验证码不正确");

        }
        stringRedisTemplate.delete(RedisKey.CAPTCHA_CODE+key);
        return true;
    }

    private Captcha createCaptcha(String type) {
        Captcha captcha = null;
        if (StringUtils.equalsIgnoreCase(type, "gif")) {
            captcha = new GifCaptcha(115, 42, 4);
        } else if (StringUtils.equalsIgnoreCase(type, "png")) {
            captcha = new SpecCaptcha(115, 42, 4);
        } else if (StringUtils.equalsIgnoreCase(type, "arithmetic")) {
            captcha = new ArithmeticCaptcha(115, 42);
        } else if (StringUtils.equalsIgnoreCase(type, "chinese")) {
            captcha = new ChineseCaptcha(115, 42);
        }
        captcha.setCharType(2);
        return captcha;
    }

    private void setHeader(HttpServletResponse response, String type) {
        if (StringUtils.equalsIgnoreCase(type, "gif")) {
            response.setContentType(MediaType.IMAGE_GIF_VALUE);
        } else {
            response.setContentType(MediaType.IMAGE_PNG_VALUE);
        }
        response.setHeader(HttpHeaders.PRAGMA, "No-cache");
        response.setHeader(HttpHeaders.CACHE_CONTROL, "No-cache");
        response.setDateHeader(HttpHeaders.EXPIRES, 0L);
    }
}
