package com.youxin.admin.auth.domain;


import com.youxin.base.TQuery;

public class UserSearchParam extends TQuery {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
