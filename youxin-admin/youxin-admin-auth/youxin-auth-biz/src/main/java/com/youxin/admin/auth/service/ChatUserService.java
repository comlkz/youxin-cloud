package com.youxin.admin.auth.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.auth.domain.ChatUserDto;
import com.youxin.admin.auth.model.User;
import com.youxin.admin.auth.model.UserDto;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import com.youxin.exception.SystemException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface ChatUserService extends IService<User> {

    User getUserByUsername(String username) throws SystemException;

    boolean insert(User user) throws SystemException;

    User selectById(Integer id) throws SystemException;

    boolean deleteById(Integer id) throws SystemException;

    boolean updateById(User user) throws SystemException;

    IPage<User> selectByPage(IPage<User> page, QueryWrapper<User> entityWrapper) throws SystemException;

    List<User> selectList(QueryWrapper<User> entityWrapper) throws SystemException;

    void updateLoginTime(String username) throws SystemException;

    TPage<List<ChatUserDto>> selectChatUserList(ChatUserDto chatUserDto, int currentPage, int showCount) throws SystemException;

    TResult<UserDto> login(String username, String password, String merchantNo, String code, String uuid, HttpServletRequest request) throws SystemException;

    TResult<Map<String, Object>> info(String userName) throws SystemException;

}
