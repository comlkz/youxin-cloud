package com.youxin.admin.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.auth.model.RoleMember;

public interface RoleMemberMapper extends BaseMapper<RoleMember> {
}
