package com.youxin.admin.auth.domain.router;



import java.io.Serializable;

/**
 * Vue路由 Meta
 */

public class RouterMeta implements Serializable {

    private static final long serialVersionUID = 5499925008927195914L;

    private String title;


    public RouterMeta(String title){
        this.title= title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
