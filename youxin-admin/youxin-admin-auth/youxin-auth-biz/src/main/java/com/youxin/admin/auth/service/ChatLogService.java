package com.youxin.admin.auth.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.auth.model.ChatLog;
import com.youxin.exception.SystemException;

public interface ChatLogService extends IService<ChatLog> {
    void saveLog(ChatLog chatLog) throws SystemException;
}
