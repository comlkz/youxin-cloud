package com.youxin.admin.auth.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.admin.auth.mapper.ChatLoginLogMapper;
import com.youxin.admin.auth.model.LoginLog;
import com.youxin.admin.auth.service.LoginLogService;
import com.youxin.exception.SystemException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LoginLogServiceImpl extends ServiceImpl<ChatLoginLogMapper, LoginLog> implements LoginLogService {



    @Override
    public void saveLoginLog(LoginLog loginLog) throws SystemException {
        save(loginLog);
    }
}
