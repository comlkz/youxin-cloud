package com.youxin.admin.auth.model;


import com.youxin.base.TQuery;

public class PayListDto extends TQuery {
    private String cashNo;

    public String getCashNo() {
        return cashNo;
    }

    public void setCashNo(String cashNo) {
        this.cashNo = cashNo;
    }
}
