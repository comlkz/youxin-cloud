package com.youxin.admin.auth.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.auth.model.LoginLog;
import com.youxin.exception.SystemException;

public interface LoginLogService extends IService<LoginLog> {
    void saveLoginLog(LoginLog loginLog) throws SystemException;
}
