package com.youxin.admin.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.auth.model.ChatLog;

public interface ChatLogMapper extends BaseMapper<ChatLog> {
}
