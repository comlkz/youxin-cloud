package com.youxin.admin.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.auth.model.LoginLog;

public interface ChatLoginLogMapper extends BaseMapper<LoginLog> {
}
