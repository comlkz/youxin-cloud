package com.youxin.admin.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.auth.domain.ChatUserDto;
import com.youxin.admin.auth.domain.RoleUser;
import com.youxin.admin.auth.model.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper extends BaseMapper<User> {
    public IPage<RoleUser> selectMemberByRoleId(Page page, @Param("roleId") int roleId, @Param("username") String username);

    IPage<ChatUserDto> selectChatUserList(@Param("chatUserDto") ChatUserDto chatUserDto, Page<User> page);

    void updateLoginTime(@Param("userName") String userName);
}
