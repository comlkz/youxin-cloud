package com.youxin.admin.auth.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.auth.model.ResourceAuthority;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ResourceAuthorityMapper extends BaseMapper<ResourceAuthority> {
    int deleteRoleMemberByRoleId(@Param("roleId") Integer roleId);

    int insertRoleMember(@Param("list") List<Integer> menuIds, @Param("roleId") Integer roleId, @Param("userpermission") String user);

    void insertBatch(@Param("list") List<ResourceAuthority> list);
}
