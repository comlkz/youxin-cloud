package com.youxin.admin.auth.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.admin.auth.mapper.ChatLogMapper;
import com.youxin.admin.auth.model.ChatLog;
import com.youxin.admin.auth.service.ChatLogService;
import com.youxin.exception.SystemException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ChatLogServiceImpl extends ServiceImpl<ChatLogMapper, ChatLog> implements ChatLogService {

    @Override
    public void saveLog(ChatLog chatLog) throws SystemException {
        save(chatLog);
    }
}
