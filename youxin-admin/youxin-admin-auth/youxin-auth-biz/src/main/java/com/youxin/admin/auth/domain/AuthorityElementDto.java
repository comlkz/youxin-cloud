package com.youxin.admin.auth.domain;

import java.util.List;

public class AuthorityElementDto {

    private int menuId;

    private List<Integer> elementIds;

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public List<Integer> getElementIds() {
        return elementIds;
    }

    public void setElementIds(List<Integer> elementIds) {
        this.elementIds = elementIds;
    }
}
