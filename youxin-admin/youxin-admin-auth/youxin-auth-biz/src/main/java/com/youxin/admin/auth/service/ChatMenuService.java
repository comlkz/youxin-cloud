package com.youxin.admin.auth.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.auth.domain.ManageMenu;
import com.youxin.admin.auth.domain.MenuSearch;
import com.youxin.admin.auth.domain.router.VueRouter;
import com.youxin.admin.auth.model.Menu;
import com.youxin.base.TResult;
import com.youxin.exception.SystemException;


import java.util.List;
import java.util.Set;

public interface ChatMenuService extends IService<Menu> {

    List<Menu> selectList(QueryWrapper<Menu> wrapper) throws SystemException;

    Menu selectById(Integer id) throws SystemException;


    List<Menu> getUserAuthorityMenuByUserId(int id) throws SystemException;

    List<Menu> getUserAuthoritySystemByUserId(int id) throws SystemException;

    boolean insert(Menu menu) throws SystemException;


    List<Menu> getAllAuthorityElement();

    List<Menu> getAuthorityMenuByUserId(Integer userId);

    IPage<Menu> selectByPage(String name, Integer menuId, Integer pageNo, Integer pageSize, String type) throws SystemException;

    List<Menu> getAuthorityMenuByMenuId(String userId, String menuId) throws SystemException;

    boolean insertMenu(Menu menu) throws SystemException;

    List<Menu> getAuthorityMenuListByUserId(String userId) throws SystemException;

    TResult<List<ManageMenu>> listTreeMenu(MenuSearch menuSearch) throws SystemException;

    TResult<Set<String>> getPermissionsByUserName(String userName) throws SystemException;

    TResult<List<VueRouter>> getRouterByUser(String userName) throws SystemException;

    List<Menu> getUserMenuByUserId(String userId, String menuId) throws SystemException;

}
