package com.youxin.admin.auth.service;



import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.auth.domain.RoleDto;
import com.youxin.admin.auth.model.ResourceAuthority;
import com.youxin.exception.SystemException;

import java.util.List;

public interface ResourceAuthorityService extends IService<ResourceAuthority> {

    void batchModifyAuthorityElements(int groupId, int menuId, List<Integer> elementIds) throws SystemException;

    void batchRemoveElementAuthority(int groupId, int menuId, List<Integer> elementIds) throws SystemException;

    void updateRoleMember(RoleDto roleDto) throws SystemException;

}
