package com.youxin.admin.auth.service;


import com.youxin.admin.auth.model.User;
import com.youxin.exception.SystemException;

import java.util.Map;
import java.util.Set;

public interface PermissionService {

    Set<String> getPermissionByUsername(String username) throws SystemException;


    Map<String, Object> generateUserInfo(String userName) throws SystemException;

}
