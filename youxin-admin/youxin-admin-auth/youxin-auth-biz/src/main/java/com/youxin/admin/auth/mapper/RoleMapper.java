package com.youxin.admin.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.auth.model.Role;
import org.apache.ibatis.annotations.Param;

public interface RoleMapper extends BaseMapper<Role> {

    void deleteRoleMembersById(@Param("roleId") int roleId, @Param("userId") int userId);

    void deleteRoleMembersByUserId(@Param("userId") int userId);

    void deleteRoleMembersByRoleId(@Param("roleId") int roleId);

    int countRoleMembersByRoleId(@Param("roleId") int roleId);

    void insertRoleMembersById(@Param("roleId") int roleId, @Param("userId") int userId);

}
