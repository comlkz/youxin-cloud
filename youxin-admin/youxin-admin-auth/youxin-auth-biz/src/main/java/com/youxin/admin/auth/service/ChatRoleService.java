package com.youxin.admin.auth.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.auth.domain.AuthorityMenuTree;
import com.youxin.admin.auth.domain.RoleDto;
import com.youxin.admin.auth.domain.RoleUser;
import com.youxin.admin.auth.model.Role;
import com.youxin.base.TPage;
import com.youxin.exception.SystemException;

import java.util.List;

public interface ChatRoleService extends IService<Role> {

    IPage<Role> selectPage(IPage<Role> page, QueryWrapper<Role> wrapper) throws SystemException;

    List<Role> selectList(QueryWrapper<Role> wrapper) throws SystemException;

    void modifyRoleUsers(int roleId, Integer userId, Integer action) throws SystemException;

    TPage<List<RoleUser>> getRoleUsers(int roleId, String username, int currentPage, int showCount) throws SystemException;

    void modifyAuthorityMenu(int roleId, String[] menus) throws SystemException;

    List<AuthorityMenuTree> getAuthorityMenu(int groupId) throws SystemException;

    void modifyAuthorityElement(int roleId, int menuId, int elementId) throws SystemException;

    void removeAuthorityElement(int roleId, int menuId, int elementId) throws SystemException;

    List<Integer> getAuthorityElement(int roleId) throws SystemException;

    void batchModifyAuthorityElement(int roleId, int menuId, List<Integer> elementIds) throws SystemException;

    void batchRemoveElementAuthority(int groupId, int menuId, List<Integer> elementIds) throws SystemException;

    boolean insert(Role role) throws SystemException;

    Role selectById(Integer id) throws SystemException;

    int deleteById(Integer id) throws SystemException;

    void updateById(RoleDto group) throws SystemException;


}
