package concurrent;

/**
 * description: Run <br>
 * date: 2020/3/4 0:10 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class Run {
    public static void main(String[] args) {
        HasSelfPrivateNum numRef = new HasSelfPrivateNum();

        ThreadA threadA = new ThreadA(numRef);
        threadA.start();
        ThreadB threadb = new ThreadB(numRef);
        threadb.start();
    }
}
