package concurrent;

/**
 * description: ThreadA <br>
 * date: 2020/3/4 0:08 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class ThreadA extends Thread {

    private HasSelfPrivateNum numRef;

    public ThreadA(HasSelfPrivateNum numRef){
        super();
        this.numRef = numRef;
    }

    @Override
    public void run() {
        super.run();
        numRef.addI("a");
    }
}
