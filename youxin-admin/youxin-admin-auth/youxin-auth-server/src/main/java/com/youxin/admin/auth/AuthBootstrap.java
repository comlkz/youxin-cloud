package com.youxin.admin.auth;

import com.youxin.admin.annotation.EnableLoginArgResolver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * description: AuthBootstrap <br>
 * date: 2020/3/3 12:50 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@SpringBootApplication
@EnableLoginArgResolver
@ComponentScan(basePackages = "com.youxin")
public class AuthBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(AuthBootstrap.class,args);
    }
}
