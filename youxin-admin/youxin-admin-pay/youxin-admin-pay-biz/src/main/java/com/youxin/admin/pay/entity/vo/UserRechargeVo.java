package com.youxin.admin.pay.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@ApiModel(description = "用户充值表")
public class UserRechargeVo implements Serializable {

    @ApiModelProperty(value = "充值编号", required = false)
    private String rechargeNo;//充值编号
    @ApiModelProperty(value = "用户编码", required = false)
    private String userNo;//用户编码
    @ApiModelProperty(value = "充值金额", required = false)
    private BigDecimal rechargeAmount;//充值金额
    @ApiModelProperty(value = "支付方式", required = false)
    private String payType;//支付方式
    @ApiModelProperty(value = "银行卡号", required = false)
    private String cardNo;//银行卡号
    @ApiModelProperty(value = "银行卡姓名", required = false)
    private String accountName;//银行卡姓名
    @ApiModelProperty(value = "手机号", required = false)
    private String accountMobile;//手机号
    @ApiModelProperty(value = "身分证号", required = false)
    private String idNo;//身分证号
    @ApiModelProperty(value = "用户IP", required = false)
    private String clientIp;//用户IP
    @ApiModelProperty(value = "充值状态", required = false)
    private Integer rechargeStatus;//充值状态 0：待充值  1：充值完成
    @ApiModelProperty(value = "成功时间", required = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime succTime;//成功时间
    @ApiModelProperty(value = "更新时间", required = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;

    public String getRechargeNo() {
        return rechargeNo;
    }

    public void setRechargeNo(String rechargeNo) {
        this.rechargeNo = rechargeNo;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public BigDecimal getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(BigDecimal rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountMobile() {
        return accountMobile;
    }

    public void setAccountMobile(String accountMobile) {
        this.accountMobile = accountMobile;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public Integer getRechargeStatus() {
        return rechargeStatus;
    }

    public void setRechargeStatus(Integer rechargeStatus) {
        this.rechargeStatus = rechargeStatus;
    }

    public LocalDateTime getSuccTime() {
        return succTime;
    }

    public void setSuccTime(LocalDateTime succTime) {
        this.succTime = succTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}
