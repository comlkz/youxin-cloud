package com.youxin.admin.pay.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.youxin.admin.pay.mapper.PayRulesMapper;
import com.youxin.admin.pay.model.PayRules;
import com.youxin.admin.pay.service.PayRulesService;
import com.youxin.chat.pay.dto.PayRulesDto;
import com.youxin.dozer.DozerUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * description: PayRuleServiceImpl <br>
 * date: 2020/3/5 15:31 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class PayRuleServiceImpl implements PayRulesService {

    @Resource
    private PayRulesMapper payRulesMapper;

    @Resource
    private DozerUtils dozerUtils;

    @Override
    public PayRulesDto info() {
        List<PayRules> list = payRulesMapper.selectList(new LambdaQueryWrapper<>());
        Map<String, Object> map = new HashMap<>();
        for (PayRules item : list) {
            map.put(item.getRuleKey(), item.getRuleValue());
        }
        PayRulesDto payRulesDto = dozerUtils.map(map,PayRulesDto.class);
        return payRulesDto;
    }
}
