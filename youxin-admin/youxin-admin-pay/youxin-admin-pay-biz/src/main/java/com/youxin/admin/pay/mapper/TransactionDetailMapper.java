package com.youxin.admin.pay.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.pay.client.dto.AgentStaticDto;
import com.youxin.admin.pay.client.req.AgentStaticSearch;
import com.youxin.admin.pay.entity.req.TransactionSearchReq;
import com.youxin.admin.pay.entity.vo.TransactionDetailVo;
import com.youxin.admin.pay.model.TransactionDetail;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface TransactionDetailMapper extends BaseMapper<TransactionDetail> {

    IPage<TransactionDetailVo> selectListByParam(Page page, @Param("record") TransactionSearchReq search);

    BigDecimal sumUserTransAmountByParam( @Param("record") TransactionSearchReq search);

    List<AgentStaticDto> staticAgentTransaction(@Param("record") AgentStaticSearch search);

}
