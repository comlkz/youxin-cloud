package com.youxin.admin.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.pay.model.BankInfo;

/**
 * description: BankInfoMapper <br>
 * date: 2020/3/5 12:07 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface BankInfoMapper extends BaseMapper<BankInfo> {
}
