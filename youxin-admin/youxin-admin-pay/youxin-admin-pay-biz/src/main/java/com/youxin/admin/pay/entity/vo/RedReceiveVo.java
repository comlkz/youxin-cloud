package com.youxin.admin.pay.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class RedReceiveVo implements Serializable {

    @ApiModelProperty(value = "红包ID",required = true)
    private Long redId;//红包ID
    @ApiModelProperty(value = "领取用户编码",required = false)
    private String userNo;//领取用户编码
    @ApiModelProperty(value = "昵称",required = false)
    private String nickName;//昵称
    @ApiModelProperty(value = "头像",required = false)
    private String avatar;//头像
    @ApiModelProperty(value = "领取金额",required = false)
    private BigDecimal amount;//领取金额
    @ApiModelProperty(value = "领取时间",required = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime receiveTime;//领取时间
    @ApiModelProperty(value = "修改时间",required = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;//修改时间

    public Long getRedId() {
        return redId;
    }

    public void setRedId(Long redId) {
        this.redId = redId;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(LocalDateTime receiveTime) {
        this.receiveTime = receiveTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}
