package com.youxin.admin.pay.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.pay.entity.vo.RedReceiveVo;
import com.youxin.admin.pay.model.RedReceive;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RedReceiveMapper extends BaseMapper<RedReceive> {

    List<RedReceiveVo> selectRedReceive(@Param("redId") String redId);

}
