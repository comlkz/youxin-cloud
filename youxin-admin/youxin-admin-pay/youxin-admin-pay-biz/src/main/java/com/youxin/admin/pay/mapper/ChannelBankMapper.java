package com.youxin.admin.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.pay.model.ChannelBank;

public interface ChannelBankMapper extends BaseMapper<ChannelBank> {
}
