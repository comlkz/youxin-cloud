package com.youxin.admin.pay.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class TransactionDetail {
    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    private String userNo;//用户编号

    private String orderNo;//用户订单号

    private String channelOrderNo;//渠道订单号

    private BigDecimal userTransAmount;//用户交易金额（单位分）

    private BigDecimal channelTransAmount;//渠道交易金额（单位分）

    private BigDecimal userServiceFee;//用户手续费（单位分）

    private BigDecimal channelServiceFee;//渠道手续费（单位分）

    private Integer opType;//交易类型  0：支付 1：代付 2：红包 3：红包退款

    private Integer feeType;  //费用类型  0：入金 1：出金

    private String channelAccount;//渠道帐号

    private String channelAlias;//渠道别名

    private String agentNo;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;//修改时间

    private String states;//标识状态：0-不显示 1-显示

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id ;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo == null ? null : userNo.trim();
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    public String getChannelOrderNo() {
        return channelOrderNo;
    }

    public void setChannelOrderNo(String channelOrderNo) {
        this.channelOrderNo = channelOrderNo == null ? null : channelOrderNo.trim();
    }

    public BigDecimal getUserTransAmount() {
        return userTransAmount;
    }

    public void setUserTransAmount(BigDecimal userTransAmount) {
        this.userTransAmount = userTransAmount;
    }

    public BigDecimal getChannelTransAmount() {
        return channelTransAmount;
    }

    public void setChannelTransAmount(BigDecimal channelTransAmount) {
        this.channelTransAmount = channelTransAmount;
    }

    public BigDecimal getUserServiceFee() {
        return userServiceFee;
    }

    public void setUserServiceFee(BigDecimal userServiceFee) {
        this.userServiceFee = userServiceFee;
    }

    public BigDecimal getChannelServiceFee() {
        return channelServiceFee;
    }

    public void setChannelServiceFee(BigDecimal channelServiceFee) {
        this.channelServiceFee = channelServiceFee;
    }

    public Integer getOpType() {
        return opType;
    }

    public void setOpType(Integer opType) {
        this.opType = opType;
    }

    public String getChannelAccount() {
        return channelAccount;
    }

    public void setChannelAccount(String channelAccount) {
        this.channelAccount = channelAccount == null ? null : channelAccount.trim();
    }

    public String getChannelAlias() {
        return channelAlias;
    }

    public void setChannelAlias(String channelAlias) {
        this.channelAlias = channelAlias == null ? null : channelAlias.trim();
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public Integer getFeeType() {
        return feeType;
    }

    public void setFeeType(Integer feeType) {
        this.feeType = feeType;
    }

    public String getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(String agentNo) {
        this.agentNo = agentNo;
    }
}
