package com.youxin.admin.pay.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.admin.pay.entity.req.ChannelAccountSearchReq;
import com.youxin.admin.pay.entity.vo.PayChannelVo;
import com.youxin.admin.pay.mapper.ChannelAccountMapper;
import com.youxin.admin.pay.mapper.ChannelBankMapper;
import com.youxin.admin.pay.model.ChannelAccount;
import com.youxin.admin.pay.model.ChannelBank;
import com.youxin.admin.pay.service.ChannelAccountService;
import com.youxin.base.TPage;
import com.youxin.chat.pay.enums.PayChannelEnum;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * description: ChannelAccountServiceImpl <br>
 * date: 2020/3/5 14:47 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class ChannelAccountServiceImpl extends ServiceImpl<ChannelAccountMapper, ChannelAccount> implements ChannelAccountService {

    @Resource
    private ChannelBankMapper channelBankMapper;

    @Override
    public List<ChannelAccount> listChannels() {
        return this.baseMapper.selectChannels();
    }

    @Override
    public TPage<ChannelAccount> pageAccount(ChannelAccountSearchReq search) {
        LambdaQueryWrapper<ChannelAccount> queryWrapper = new LambdaQueryWrapper<>();
        if (!StringUtils.isEmpty(search.getChannelCode())) {
            queryWrapper.eq(ChannelAccount::getChannelCode, search.getChannelCode());
        }
        if (!StringUtils.isEmpty(search.getChannelAccount())) {
            queryWrapper.eq(ChannelAccount::getChannelAccount, search.getChannelAccount());
        }
        if (search.getStatus() != null) {
            queryWrapper.eq(ChannelAccount::getAccountStatus, search.getStatus());
        }
        IPage<ChannelAccount> page = this.baseMapper.selectPage(new Page<>(search.getCurrentPage(), search.getShowCount()), queryWrapper);
        if(!CollectionUtils.isEmpty(page.getRecords())){
            page.getRecords().stream().forEach(item->{
                item.setRsaKeyPri(null);
                item.setRsaKeyPub(null);
            });
        }
        return TPage.page(page.getRecords(), page.getTotal());
    }

    @Override
    public List<ChannelBank> listChannelBank(String channelCode) {
        LambdaQueryWrapper<ChannelBank> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ChannelBank::getChannelCode,channelCode);
        List<ChannelBank> channelBanks = channelBankMapper.selectList(queryWrapper);
        return channelBanks;
    }

    @Override
    public List<PayChannelVo> listPayChannels() {
        List<PayChannelVo> list = new ArrayList<>();
        for(PayChannelEnum payChannelEnum: PayChannelEnum.values()){
            PayChannelVo dto = new PayChannelVo();
            dto.setChannelCode(payChannelEnum.getChannelCode());
            dto.setChannelName(payChannelEnum.getChannelName());
            list.add(dto);
        }
        return list;
    }
}
