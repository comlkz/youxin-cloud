package com.youxin.admin.pay.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.pay.entity.req.UserRechargeSearchReq;
import com.youxin.admin.pay.entity.vo.UserRechargeVo;
import com.youxin.admin.pay.model.UserRecharge;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface UserRechargeMapper extends BaseMapper<UserRecharge> {


    IPage<UserRechargeVo> selectRechargeList(Page page, @Param("userRechargeDto") UserRechargeSearchReq userRechargeDto);


}
