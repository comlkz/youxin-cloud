package com.youxin.admin.pay.entity.req;


import com.youxin.base.TQuery;

public class ChannelAccountSearchReq extends TQuery {

    private String channelCode;

    private String channelAccount;

    private Integer status;


    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelAccount() {
        return channelAccount;
    }

    public void setChannelAccount(String channelAccount) {
        this.channelAccount = channelAccount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
