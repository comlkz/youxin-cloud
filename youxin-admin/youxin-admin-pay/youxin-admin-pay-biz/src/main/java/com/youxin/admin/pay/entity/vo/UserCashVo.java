package com.youxin.admin.pay.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@ApiModel(description = "用户提现表")
public class UserCashVo implements Serializable {

    @ApiModelProperty(value = "提现单号", required = false)
    private String cashNo;//提现单号
    @ApiModelProperty(value = "用户编号", required = false)
    private String userNo;//用户编号
    @ApiModelProperty(value = "提现金额", required = false)
    private BigDecimal cashAmount;//提现金额（单位：分）
    @ApiModelProperty(value = "提现费率", required = false)
    private BigDecimal cashRate;//提现费率（含%）
    @ApiModelProperty(value = "银行卡号", required = false)
    private String cardNo;//银行卡号
    @ApiModelProperty(value = "银行卡姓名", required = false)
    private String accountName;//银行卡姓名
    @ApiModelProperty(value = "手机号", required = false)
    private String accountMobile;//手机号
    @ApiModelProperty(value = "提现手续费", required = false)
    private Date finishTime;//提现手续费
    @ApiModelProperty(value = "用户IP", required = false)
    private Byte clientIp;//用户IP
    @ApiModelProperty(value = "提现状态", required = false)
    private String cashStatus;//提现状态 0：申请中 1：提现成功 2：提现失败
    @ApiModelProperty(value = "结果描述", required = false)
    private String reason;//结果描述
    @ApiModelProperty(value = "修改时间", required = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;//修改时间

    public String getCashNo() {
        return cashNo;
    }

    public void setCashNo(String cashNo) {
        this.cashNo = cashNo;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public BigDecimal getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(BigDecimal cashAmount) {
        this.cashAmount = cashAmount;
    }

    public BigDecimal getCashRate() {
        return cashRate;
    }

    public void setCashRate(BigDecimal cashRate) {
        this.cashRate = cashRate;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountMobile() {
        return accountMobile;
    }

    public void setAccountMobile(String accountMobile) {
        this.accountMobile = accountMobile;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public Byte getClientIp() {
        return clientIp;
    }

    public void setClientIp(Byte clientIp) {
        this.clientIp = clientIp;
    }

    public String getCashStatus() {
        return cashStatus;
    }

    public void setCashStatus(String cashStatus) {
        this.cashStatus = cashStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
