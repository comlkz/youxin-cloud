package com.youxin.admin.pay.service.rpc;

import com.youxin.chat.pay.client.ChannelClient;
import com.youxin.chat.pay.client.OrderClient;
import com.youxin.chat.pay.client.WalletClient;
import com.youxin.chat.pay.dto.*;
import com.youxin.chat.user.client.UserClient;
import com.youxin.chat.user.dto.UserInfoDto;
import com.youxin.exception.SystemException;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

/**
 * description: RemoteWalletService <br>
 * date: 2020/3/5 11:06 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class RemotePayService {

    @Reference(retries = 1)
    private WalletClient walletClient;

    @Reference(retries = 1)
    private OrderClient orderClient;

    @Reference(retries = 1)
    private ChannelClient channelClient;

    @Reference(retries = 1)
    private UserClient userClient;

    public void updateUserWalletStates(String userNo, Integer status){
        walletClient.updateUserWalletStates(userNo, status);
    }

    public void changeCashStatus(CashStatusChange cashStatusChange){
        orderClient.changeCashStatus(cashStatusChange);
    }

    public void saveChannel(ChannelAccountDto channelAccountDto) {
        channelClient.saveChannel(channelAccountDto);
    }


    public void saveChannelBank(ChannelBankDto channelBankDto) {
        channelClient.saveChannelBank(channelBankDto);
    }

    public void removeChannelBank(Integer id) {
        channelClient.removeChannelBank(id);
    }

    public void saveRisk(PayRulesDto riskDto){
        walletClient.saveRisk(riskDto);
    }

    public ChannelBalanceDto balanceQuery(String channelAccount){
        return channelClient.balanceQuery(channelAccount);
    }

    public void syncOrderStatus(String orderNo) throws SystemException {
        orderClient.syncOrderStatus(orderNo);
    }

    public void syncCashStatus(String orderNo) throws SystemException {
        orderClient.syncCashStatus(orderNo);
    }

    public UserInfoDto selectUserInfo(String userNo) throws SystemException {
        return userClient.getByUserNo(userNo);
    }
}
