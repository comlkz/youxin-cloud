package com.youxin.admin.pay.service;

import com.youxin.admin.pay.client.dto.AgentStaticDto;
import com.youxin.admin.pay.client.dto.SystemSettlementDto;
import com.youxin.admin.pay.client.req.AgentStaticSearch;
import com.youxin.admin.pay.entity.req.*;
import com.youxin.admin.pay.entity.vo.*;
import com.youxin.base.PageDto;
import com.youxin.base.TPage;

import java.time.LocalDateTime;
import java.util.List;

/**
 * description: OrderService <br>
 * date: 2020/3/5 10:57 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface OrderService {

   TPage<UserCashVo> getCashList(UserCashSearchReq req);

   PageDto<TransactionDetailVo> pageTransactions(TransactionSearchReq req);

   PageDto<CommonOrderVo> listCommonOrder(CommonOrderSearchReq search);

   List<RedReceiveVo> getRedReceive(String redId);

   Object getOrderDetail(String orderNo);

   TPage<UserRechargeVo> getRechargeList(UserRechargeSearchReq req);

   PageDto<ChannelCashVo> pageChannelCash(ChannelCashSearch search);

   PageDto<ChannelOrderVo> pageChannelOrder(ChannelOrderSearch search);

   SystemSettlementDto staticChannelOrderData(LocalDateTime startTime, LocalDateTime endTime);

   SystemSettlementDto staticChannelCashData(LocalDateTime startTime, LocalDateTime endTime);

   SystemSettlementDto staticUserCashData(LocalDateTime startTime, LocalDateTime endTime);

   List<AgentStaticDto> staticAgentTransaction(AgentStaticSearch search);


}
