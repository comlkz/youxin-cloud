package com.youxin.admin.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.pay.client.dto.SystemSettlementDto;
import com.youxin.admin.pay.entity.req.ChannelOrderSearch;
import com.youxin.admin.pay.entity.vo.ChannelOrderVo;
import com.youxin.admin.pay.model.ChannelOrder;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * description: ChannelOrderMapper <br>
 * date: 2020/3/5 15:57 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface ChannelOrderMapper extends BaseMapper<ChannelOrder> {

    IPage<ChannelOrderVo> selectOrderByParam(Page page, @Param("record") ChannelOrderSearch search);

    BigDecimal sumCashAmountByParam(@Param("record") ChannelOrderSearch search);

    SystemSettlementDto staticOrderData(@Param("startTime") LocalDateTime startTime, @Param("endTime")LocalDateTime endTime);

}
