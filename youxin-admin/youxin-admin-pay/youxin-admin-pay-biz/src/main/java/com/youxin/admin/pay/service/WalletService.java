package com.youxin.admin.pay.service;

import com.youxin.admin.pay.model.UserWallet;

/**
 * description: WalletService <br>
 * date: 2020/3/5 10:47 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface WalletService {

    UserWallet selectUserWallet(String userNo);
}
