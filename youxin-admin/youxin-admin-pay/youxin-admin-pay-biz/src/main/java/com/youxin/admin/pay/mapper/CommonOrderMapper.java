package com.youxin.admin.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.pay.entity.req.CommonOrderSearchReq;
import com.youxin.admin.pay.entity.vo.CommonOrderVo;
import com.youxin.admin.pay.model.CommonOrder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;

public interface CommonOrderMapper extends BaseMapper<CommonOrder> {




    BigDecimal sumOrderAmountByParam(@Param("record") CommonOrderSearchReq search);

    IPage<CommonOrderVo> selectOrderByParam(Page<CommonOrderVo> page, @Param("record") CommonOrderSearchReq search);

    @Select("select * from t_common_order where order_no=#{orderNo}")
    CommonOrder selectByOrderNo(@Param("orderNo") String orderNo);

}
