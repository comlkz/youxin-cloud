package com.youxin.admin.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.pay.model.BankInfo;
import com.youxin.base.TPage;

/**
 * description: BankInfoService <br>
 * date: 2020/3/5 12:08 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface BankInfoService extends IService<BankInfo> {

    TPage<BankInfo> pageByParam(String key, Integer currentPage, Integer showCount);
}
