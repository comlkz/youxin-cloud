package com.youxin.admin.pay.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.pay.model.UserWallet;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;

public interface UserWalletMapper extends BaseMapper<UserWallet> {

    @Select("select * from t_user_wallet where user_no=#{userNo}")
    UserWallet selectUserWalleByUserNo(@Param("userNo")String userNo);
}
