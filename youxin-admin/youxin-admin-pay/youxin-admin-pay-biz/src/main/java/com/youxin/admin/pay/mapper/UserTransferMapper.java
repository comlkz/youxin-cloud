package com.youxin.admin.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.pay.model.UserTransfer;

public interface UserTransferMapper extends BaseMapper<UserTransfer> {
}
