package com.youxin.admin.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.pay.model.PayRules;

/**
 * description: PayRulesMapper <br>
 * date: 2020/3/5 15:33 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface PayRulesMapper extends BaseMapper<PayRules> {
}
