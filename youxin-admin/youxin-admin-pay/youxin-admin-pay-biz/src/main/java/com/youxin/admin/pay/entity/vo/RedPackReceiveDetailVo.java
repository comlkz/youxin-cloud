package com.youxin.admin.pay.entity.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RedPackReceiveDetailVo implements Serializable {

    private List<RedReceiveVo> user;

    private String time;

    private Integer num;

    private BigDecimal amount;

    private BigDecimal receiveAmount;

    @ApiModelProperty(value = "是否过期 0：否 1：是")
    private Integer expired;

    @ApiModelProperty(value = "红包类型 0：普通红包 1：拼手气红包 2:单个红包 3:定向红包")
    private Integer redType;

    public List<RedReceiveVo> getUser() {
        return user;
    }

    public void setUser(List<RedReceiveVo> user) {
        this.user = user;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getReceiveAmount() {
        return receiveAmount;
    }

    public void setReceiveAmount(BigDecimal receiveAmount) {
        this.receiveAmount = receiveAmount;
    }

    public Integer getExpired() {
        return expired;
    }

    public void setExpired(Integer expired) {
        this.expired = expired;
    }

    public Integer getRedType() {
        return redType;
    }

    public void setRedType(Integer redType) {
        this.redType = redType;
    }
}
