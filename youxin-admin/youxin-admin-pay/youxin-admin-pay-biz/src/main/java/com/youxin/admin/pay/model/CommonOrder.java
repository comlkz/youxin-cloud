package com.youxin.admin.pay.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class CommonOrder  {

    @TableId(type = IdType.ID_WORKER_STR)
    private String id;
    @ApiModelProperty(value = "订单号")
    private String orderNo;
    @ApiModelProperty(value = "订单来源 0:用户充值,1:用户提现,2:红包发送,3:红包接收,4:红包退款,5:手机充值")
    private Integer orderSource;
    @ApiModelProperty(value = "订单金额（单位：分）")
    private BigDecimal orderAmount;
    @ApiModelProperty(value = "订单备注")
    private String orderRemark;
    @ApiModelProperty(value = "完成时间")
    private LocalDateTime opTime;
    @ApiModelProperty(value = "订单状态 0:处理中 1：成功 2：失败")
    private Integer orderStatus;
    @ApiModelProperty(value = "用户编码")
    private String userNo;
    @ApiModelProperty(value = "客户IP")
    private String clientIp;

    @ApiModelProperty(value = "是否展示 0：不展示 1：展示")
    @TableField(value = "IS_DISPLAY")
    private Integer disPlay;

    @ApiModelProperty(value = "代理商编码")
    private String agentNo;

    @ApiModelProperty(value = "费用类型 0：入金 1：出金")
    private Integer feeType;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(Integer orderSource) {
        this.orderSource = orderSource;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderRemark() {
        return orderRemark;
    }

    public void setOrderRemark(String orderRemark) {
        this.orderRemark = orderRemark;
    }

    public LocalDateTime getOpTime() {
        return opTime;
    }

    public void setOpTime(LocalDateTime opTime) {
        this.opTime = opTime;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public Integer getDisPlay() {
        return disPlay;
    }

    public void setDisPlay(Integer disPlay) {
        this.disPlay = disPlay;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public Integer getFeeType() {
        return feeType;
    }

    public void setFeeType(Integer feeType) {
        this.feeType = feeType;
    }

    public String getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(String agentNo) {
        this.agentNo = agentNo;
    }
}
