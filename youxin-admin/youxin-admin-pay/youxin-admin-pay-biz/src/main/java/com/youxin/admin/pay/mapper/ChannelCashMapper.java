package com.youxin.admin.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.pay.client.dto.SystemSettlementDto;
import com.youxin.admin.pay.entity.req.ChannelCashSearch;
import com.youxin.admin.pay.entity.vo.ChannelCashVo;
import com.youxin.admin.pay.model.ChannelCash;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * description: ChannelCashMapper <br>
 * date: 2020/3/5 15:57 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface ChannelCashMapper extends BaseMapper<ChannelCash> {

    IPage<ChannelCashVo> selectCashByParam(Page<ChannelCashVo> page, @Param("record")ChannelCashSearch search);

    BigDecimal sumCashAmountByParam(@Param("record")ChannelCashSearch search);

    SystemSettlementDto staticCashData(@Param("startTime") LocalDateTime startTime, @Param("endTime")LocalDateTime endTime);

}
