package com.youxin.admin.pay.entity.req;

import com.youxin.base.TQuery;

/**
 * description: UserRechargeSearchReq <br>
 * date: 2020/3/5 15:47 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class UserRechargeSearchReq extends TQuery {

    private String rechargeNo;

    private String userNo;

    public String getRechargeNo() {
        return rechargeNo;
    }

    public void setRechargeNo(String rechargeNo) {
        this.rechargeNo = rechargeNo;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }
}
