package com.youxin.admin.pay.entity.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.youxin.base.TQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@ApiModel(description = "用户提现表")
public class UserCashSearchReq extends TQuery {

    @ApiModelProperty(value = "提现单号", required = false)
    private String cashNo;//提现单号
    @ApiModelProperty(value = "用户编号", required = false)
    private String userNo;//用户编号

    public String getCashNo() {
        return cashNo;
    }

    public void setCashNo(String cashNo) {
        this.cashNo = cashNo;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

}
