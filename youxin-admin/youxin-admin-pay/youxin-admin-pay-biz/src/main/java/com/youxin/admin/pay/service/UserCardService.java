package com.youxin.admin.pay.service;

import com.youxin.admin.pay.model.UserCard;

import java.util.List;

public interface UserCardService {
    List<UserCard> cardList(String userNo);
}
