package com.youxin.admin.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.youxin.admin.pay.client.dto.SystemSettlementDto;
import com.youxin.admin.pay.entity.req.UserCashSearchReq;
import com.youxin.admin.pay.entity.vo.UserCashVo;
import com.youxin.admin.pay.model.UserCash;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface UserCashMapper extends BaseMapper<UserCash> {
    IPage<UserCashVo> selectCashList(Page page,@Param("userCashDto") UserCashSearchReq userCashDto);

    SystemSettlementDto staticCashData(@Param("startTime")LocalDateTime startTime, @Param("endTime")LocalDateTime endTime);
}
