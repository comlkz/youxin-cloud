package com.youxin.admin.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.pay.model.UserCard;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserCardMapper extends BaseMapper<UserCard> {

    List<UserCard> selectUserCardByUserNo(@Param("userNo")String userNo);
}
