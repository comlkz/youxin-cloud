package com.youxin.admin.pay.service.impl;

import com.youxin.admin.pay.mapper.UserWalletMapper;
import com.youxin.admin.pay.model.UserWallet;
import com.youxin.admin.pay.service.WalletService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * description: WalletServiceImpl <br>
 * date: 2020/3/5 10:48 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class WalletServiceImpl implements WalletService {

    @Resource
    private UserWalletMapper userWalletMapper;


    @Override
    public UserWallet selectUserWallet(String userNo) {
        UserWallet userWallet = userWalletMapper.selectUserWalleByUserNo(userNo);
        if(userWallet != null){
            userWallet.setPayPwd(null);
        }
        return userWallet;
    }
}
