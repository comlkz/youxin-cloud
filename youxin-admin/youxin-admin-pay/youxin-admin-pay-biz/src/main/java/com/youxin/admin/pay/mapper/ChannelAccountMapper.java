package com.youxin.admin.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.pay.model.ChannelAccount;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * description: ChannelAccountMapper <br>
 * date: 2020/3/5 14:43 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface ChannelAccountMapper extends BaseMapper<ChannelAccount> {

    @Select("select ID,CHANNEL_ACCOUNT,CHANNEL_CODE,CHANNEL_ALIAS from t_channel_account where ACCOUNT_STATUS=1")
    List<ChannelAccount> selectChannels();
}
