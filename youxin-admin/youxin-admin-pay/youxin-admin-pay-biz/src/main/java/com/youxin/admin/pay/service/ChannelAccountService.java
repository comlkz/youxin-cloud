package com.youxin.admin.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.pay.entity.req.ChannelAccountSearchReq;
import com.youxin.admin.pay.entity.vo.PayChannelVo;
import com.youxin.admin.pay.model.ChannelAccount;
import com.youxin.admin.pay.model.ChannelBank;
import com.youxin.base.TPage;

import java.util.List;

/**
 * description: ChannelAccountService <br>
 * date: 2020/3/5 14:47 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface ChannelAccountService extends IService<ChannelAccount> {

    List<ChannelAccount> listChannels();

    TPage<ChannelAccount> pageAccount(ChannelAccountSearchReq search);

    List<ChannelBank> listChannelBank(String channelCode);

    List<PayChannelVo> listPayChannels();
}
