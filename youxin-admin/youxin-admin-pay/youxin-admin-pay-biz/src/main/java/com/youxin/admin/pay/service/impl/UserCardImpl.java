package com.youxin.admin.pay.service.impl;

import com.youxin.admin.pay.mapper.UserCardMapper;
import com.youxin.admin.pay.model.UserCard;
import com.youxin.admin.pay.service.UserCardService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserCardImpl implements UserCardService {

    @Resource
    private UserCardMapper userCardMapper;

    @Override
    public List<UserCard> cardList(String userNo) {
        List<UserCard> userCard = userCardMapper.selectUserCardByUserNo(userNo);
        if (userCard != null) {
            userCard.stream().forEach((item) -> {
                        String start;
                        String end;

                        String cardNo = item.getCardNo();
                        start = cardNo.substring(0, 4);
                        end = cardNo.substring(cardNo.length() - 4, cardNo.length());
                        item.setCardNo(start + " **** " + end);

                        String cardId = item.getCardId();
                        start = cardId.substring(0, 6);
                        end = cardId.substring(cardId.length() - 4, cardId.length());
                        item.setCardId(start + " **** " + end);

                        String cardMobile = item.getCardMobile();
                        start = cardMobile.substring(0, 4);
                        end = cardMobile.substring(cardMobile.length() - 4, cardMobile.length());
                        item.setCardMobile(start + " **** " + end);

                        String ownerName = item.getOwnerName();
                        start = ownerName.substring(0, 1);
                        item.setOwnerName(start + " **");
                    }
            );
        }
        return userCard;
    }
}
