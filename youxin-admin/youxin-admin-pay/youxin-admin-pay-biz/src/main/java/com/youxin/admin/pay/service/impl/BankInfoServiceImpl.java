package com.youxin.admin.pay.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.admin.pay.mapper.BankInfoMapper;
import com.youxin.admin.pay.model.BankInfo;
import com.youxin.admin.pay.service.BankInfoService;
import com.youxin.base.TPage;
import com.youxin.common.config.YouxinCommonProperties;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

/**
 * description: BankInfoServiceImpl <br>
 * date: 2020/3/5 12:08 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class BankInfoServiceImpl extends ServiceImpl<BankInfoMapper, BankInfo> implements BankInfoService {

    @Resource
    private YouxinCommonProperties youxinCommonProperties;

    @Override
    public TPage<BankInfo> pageByParam(String key, Integer currentPage, Integer showCount) {
        LambdaQueryWrapper<BankInfo> queryWrapper = new LambdaQueryWrapper<>();
        if (!StringUtils.isEmpty(key)) {
            queryWrapper.like(BankInfo::getBankCode, key).or().like(BankInfo::getBankName, key);
        }
        IPage<BankInfo> page = this.baseMapper.selectPage(new Page<>(currentPage, showCount), queryWrapper);
        if(!CollectionUtils.isEmpty(page.getRecords())){
            page.getRecords().stream().forEach(
                    item->{
                        item.setBankCover(youxinCommonProperties.getDownloadUrl()+ item.getBankCover());
                        item.setBankIcon(youxinCommonProperties.getDownloadUrl()+ item.getBankIcon());
                    }
            );
        }
        return TPage.page(page.getRecords(),page.getTotal());
    }
}
