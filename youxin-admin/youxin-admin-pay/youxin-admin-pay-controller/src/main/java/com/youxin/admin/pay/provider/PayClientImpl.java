package com.youxin.admin.pay.provider;

import com.youxin.admin.pay.client.PayClient;
import com.youxin.admin.pay.client.dto.AgentStaticDto;
import com.youxin.admin.pay.client.dto.SystemSettlementDto;
import com.youxin.admin.pay.client.req.AgentStaticSearch;
import com.youxin.admin.pay.service.OrderService;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * description: PayClientImpl <br>
 * date: 2020/3/6 12:01 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class PayClientImpl implements PayClient {

    @Resource
    private OrderService orderService;

    @Override
    public SystemSettlementDto staticChannelOrderData(LocalDateTime startTime, LocalDateTime endTime) {
        return orderService.staticChannelOrderData(startTime, endTime);
    }

    @Override
    public SystemSettlementDto staticChannelCashData(LocalDateTime startTime, LocalDateTime endTime) {
        return orderService.staticChannelCashData(startTime, endTime);
    }

    @Override
    public SystemSettlementDto staticUserCashData(LocalDateTime startTime, LocalDateTime endTime) {
        return orderService.staticUserCashData(startTime, endTime);
    }

    @Override
    public List<AgentStaticDto> staticAgentTransaction(AgentStaticSearch search) {
        return orderService.staticAgentTransaction(search);
    }
}
