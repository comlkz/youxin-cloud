package com.youxin.admin.pay.controller;

import com.youxin.admin.pay.service.PayRulesService;
import com.youxin.admin.pay.service.rpc.RemotePayService;
import com.youxin.base.TResult;
import com.youxin.chat.pay.dto.PayRulesDto;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * description: PayConfigController <br>
 * date: 2020/3/5 15:23 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
@RequestMapping("rules")
public class PayRulesController {

    @Resource
    private PayRulesService payRulesService;

    @Resource
    private RemotePayService remotePayService;

    @GetMapping("info")
    public TResult<PayRulesDto> info() {

        PayRulesDto riskDto = payRulesService.info();
        return TResult.success(riskDto);
    }

    @PostMapping("save")
    public TResult<String> saveBatch(@RequestBody PayRulesDto riskDto){
        remotePayService.saveRisk(riskDto);
        return TResult.success();
    }
}
