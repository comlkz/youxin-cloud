package com.youxin.admin.pay.controller;

import com.youxin.admin.pay.entity.req.ChannelAccountSearchReq;
import com.youxin.admin.pay.entity.vo.PayChannelVo;
import com.youxin.admin.pay.model.ChannelAccount;
import com.youxin.admin.pay.model.ChannelBank;
import com.youxin.admin.pay.service.ChannelAccountService;
import com.youxin.admin.pay.service.rpc.RemotePayService;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import com.youxin.chat.pay.dto.ChannelAccountDto;
import com.youxin.chat.pay.dto.ChannelBalanceDto;
import com.youxin.chat.pay.dto.ChannelBankDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * description: ChannelAccountController <br>
 * date: 2020/3/5 14:44 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
@RequestMapping("channel")
public class ChannelAccountController {

    @Resource
    private ChannelAccountService channelAccountService;

    @Resource
    private RemotePayService remotePayService;

    @GetMapping("account/info/{id}")
    public TResult<ChannelAccount> info(@PathVariable Integer id){
        ChannelAccount accountDto =  channelAccountService.getById(id);
        accountDto.setSecretKey(null);
        accountDto.setRsaKeyPri(null);
        accountDto.setRsaKeyPub(null);
        return TResult.success(accountDto);
    }

    @GetMapping("account/list")
    public TResult<List<ChannelAccount>> list(ChannelAccountSearchReq req){

        List<ChannelAccount> list =  channelAccountService.listChannels();
        return TResult.success(list);
    }

    @GetMapping("account/page")
    public TResult<TPage<ChannelAccount>> pageChannels(ChannelAccountSearchReq search){
        TPage<ChannelAccount> page = channelAccountService.pageAccount(search);
        return TResult.success(page);
    }

    @GetMapping("bank/list/{channelCode}")
    public TResult<List<ChannelBank>> listChannelBanks(@PathVariable String channelCode){

        List<ChannelBank> list = channelAccountService.listChannelBank(channelCode);
        return TResult.success(list);
    }

    @GetMapping("list")
    public TResult<List<PayChannelVo> > listPayChannels(){
        List<PayChannelVo> list = channelAccountService.listPayChannels();
        return TResult.success(list);
    }

    @PostMapping("account/save")
    public TResult<String> saveChannel(@RequestBody ChannelAccountDto channelAccountDto){
        remotePayService.saveChannel(channelAccountDto);
        return TResult.success();
    }

    @PostMapping("bank/save")
    public TResult<String> saveChannelBank(@RequestBody ChannelBankDto channelBankDto){
        remotePayService.saveChannelBank(channelBankDto);
        return TResult.success();
    }

    @GetMapping("bank/del/{id}")
    public TResult<String> delChannelBank(@PathVariable Integer id){
        remotePayService.removeChannelBank(id);
        return TResult.success();
    }

    @ApiOperation("余额查询")
    @PostMapping("account/balance/{accountNo}")
    public TResult<ChannelBalanceDto> balanceQuery(@PathVariable String accountNo){
        return TResult.success(remotePayService.balanceQuery(accountNo));
    }
}
