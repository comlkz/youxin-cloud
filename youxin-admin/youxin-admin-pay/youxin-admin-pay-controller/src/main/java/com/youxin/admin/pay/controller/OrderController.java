package com.youxin.admin.pay.controller;

import com.youxin.admin.pay.entity.req.*;
import com.youxin.admin.pay.entity.vo.*;
import com.youxin.admin.pay.service.OrderService;
import com.youxin.admin.pay.service.rpc.RemotePayService;
import com.youxin.base.PageDto;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import com.youxin.chat.pay.dto.CashStatusChange;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * description: OrderController <br>
 * date: 2020/3/5 10:52 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private OrderService orderService;

    @Resource
    private RemotePayService remotePayService;

    @ApiOperation("用户提现列表")
    @GetMapping(value = "/cash/list")
    public TResult<TPage<UserCashVo>> getCashList(UserCashSearchReq userCashDto) {
        return TResult.success(orderService.getCashList(userCashDto));
    }

    @GetMapping("transaction/page")
    public TResult<PageDto<TransactionDetailVo>> listTransactions(TransactionSearchReq search) {

        return TResult.success(orderService.pageTransactions(search));
    }

    @GetMapping("common/page")
    public TResult<PageDto<CommonOrderVo>> listCommonOrder(CommonOrderSearchReq search){
         return TResult.success(orderService.listCommonOrder(search));
    }

    @GetMapping("red/receive/{redId}")
    public TResult<List<RedReceiveVo>> acquireRedReceive(@PathVariable String redId){
        List<RedReceiveVo> list = orderService.getRedReceive(redId);
        return TResult.success(list);
    }

    @GetMapping("detail/{orderNo}")
    public TResult<Object> getOrderDetail(@PathVariable String orderNo){
        Object object = orderService.getOrderDetail(orderNo);
        return TResult.success(object);
    }

    @PostMapping("cash/status/change")
    public TResult<String> changeCashStatus(@RequestBody CashStatusChange cashStatusChange) {
         remotePayService.changeCashStatus(cashStatusChange);
        return TResult.success();
    }

    @ApiOperation("系统用户支付列表-充值表")
    @GetMapping(value = "/recharge/list")
    //@RequiresPermissions("app:getRechargeList")
    public TResult<TPage<UserRechargeVo>> getRechargeList( UserRechargeSearchReq userRechargeDto) {
        return TResult.success(orderService.getRechargeList(userRechargeDto));
    }

    @GetMapping("channel/cash/page")
    public TResult<PageDto<ChannelCashVo>> pageChannelCash(ChannelCashSearch search){
        PageDto<ChannelCashVo> pageDto = orderService.pageChannelCash(search);
        return TResult.success(pageDto);
    }

    @GetMapping("channel/order/page")
    public TResult<PageDto<ChannelOrderVo>> pageChannelOrder(ChannelOrderSearch search){
        PageDto<ChannelOrderVo> pageDto = orderService.pageChannelOrder(search);
        return TResult.success(pageDto);
    }

    @PostMapping("sync/order/{orderNo}")
    public TResult<String> syncOrderNo(@PathVariable String orderNo){
        remotePayService.syncOrderStatus(orderNo);
        return TResult.success();
    }

    @PostMapping("sync/cash/{orderNo}")
    public TResult<String> syncCashNo(@PathVariable String orderNo){
        remotePayService.syncCashStatus(orderNo);
        return TResult.success();
    }
}
