package com.youxin.admin.pay.controller;

import com.youxin.admin.pay.model.BankInfo;
import com.youxin.admin.pay.service.BankInfoService;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("bank")
public class BankInfoController {

    @Resource
    private BankInfoService bankInfoService;

    /**
     * 银行管理添加
     * @param bankInfoDto
     * @return
     */
    @PostMapping("save")
    public TResult<String> saveBankInfo(@RequestBody BankInfo bankInfoDto) {
        bankInfoService.save(bankInfoDto);
        return TResult.success();
    }

    @GetMapping("page")
    public TResult<TPage<BankInfo>> info(String key,
                                         @RequestParam(value = "currentPage", defaultValue = "1") Integer currentPage,
                                         @RequestParam(value = "showCount", defaultValue = "10") Integer showCount) {
        TPage<BankInfo> page = bankInfoService.pageByParam(key, currentPage, showCount);
        return TResult.success(page);
    }


    @GetMapping("list")
    public TResult<List<BankInfo>> list() {
        List<BankInfo> list= bankInfoService.list();
        return TResult.success(list);
    }

}
