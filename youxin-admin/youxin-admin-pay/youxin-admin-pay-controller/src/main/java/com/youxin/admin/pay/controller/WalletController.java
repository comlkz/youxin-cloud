package com.youxin.admin.pay.controller;

import com.youxin.admin.pay.model.UserCard;
import com.youxin.admin.pay.model.UserWallet;
import com.youxin.admin.pay.service.UserCardService;
import com.youxin.admin.pay.service.WalletService;
import com.youxin.admin.pay.service.rpc.RemotePayService;
import com.youxin.base.TResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * description: WalletController <br>
 * date: 2020/3/5 10:51 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
public class WalletController {

    @Resource
    private WalletService walletService;

    @Resource
    private RemotePayService remoteWalletService;

    @Resource
    private UserCardService userCardService;

    @ApiOperation("APP用户钱包")
    @PostMapping(value = "/wallet/{userNo}")
    //@RequiresPermissions("app:Wallet")
    public TResult<UserWallet> getUserWallet(@PathVariable String userNo) {
        return TResult.success(walletService.selectUserWallet(userNo));
    }

    @ApiOperation("APP用户钱包锁定")
    @PostMapping(value = "/wallet/status/{userNo}/{status}")
    //@RequiresPermissions("app:lockUser")
    public TResult<String> lockUserWallet(@PathVariable String userNo,@PathVariable Integer status) {
        remoteWalletService.updateUserWalletStates(userNo,status);
        return TResult.success();
    }

    @ApiOperation("APP用户绑卡列表")
    @PostMapping(value = "/wallet/card/list/{userNo}")
    //@RequiresPermissions("user:bankCard")
    public TResult<List<UserCard>> cardList(@PathVariable String userNo) {

        return TResult.success(userCardService.cardList(userNo));
    }
}
