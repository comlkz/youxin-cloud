package com.youxin.admin.pay.client;

import java.time.LocalDateTime;
import java.util.List;

import com.youxin.admin.pay.client.dto.AgentStaticDto;
import com.youxin.admin.pay.client.dto.SystemSettlementDto;
import com.youxin.admin.pay.client.req.AgentStaticSearch;

/**
 * description: PayClient <br>
 * date: 2020/3/6 11:44 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface PayClient {

    SystemSettlementDto staticChannelOrderData(LocalDateTime startTime, LocalDateTime endTime);

    SystemSettlementDto staticChannelCashData(LocalDateTime startTime, LocalDateTime endTime);

    SystemSettlementDto staticUserCashData(LocalDateTime startTime, LocalDateTime endTime);

    List<AgentStaticDto> staticAgentTransaction(AgentStaticSearch search);
}
