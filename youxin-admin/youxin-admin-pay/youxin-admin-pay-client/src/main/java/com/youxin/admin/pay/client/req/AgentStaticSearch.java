package com.youxin.admin.pay.client.req;


import com.youxin.base.TQuery;

/**
 * description: 推广统计条件 <br>
 * date: 2020/1/6 14:34 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class AgentStaticSearch extends TQuery {

    private String agentNo;

    private String startTime;

    private String endTime;

    public String getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(String agentNo) {
        this.agentNo = agentNo;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
