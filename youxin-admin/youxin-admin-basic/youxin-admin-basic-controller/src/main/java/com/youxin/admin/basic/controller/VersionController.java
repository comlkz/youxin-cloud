package com.youxin.admin.basic.controller;

import com.youxin.admin.basic.entity.vo.VersionReq;
import com.youxin.admin.basic.model.Version;
import com.youxin.admin.basic.service.VersionService;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import com.youxin.common.config.YouxinCommonProperties;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

@Validated
@RestController
@RequestMapping("version")
public class VersionController {
    private static final Logger logger = LoggerFactory.getLogger(VersionController.class);

    @Resource
    private VersionService versionService;

    @Resource
    private YouxinCommonProperties youxinCommonProperties;

    /**
     * 添加新版本
     *
     * @return
     */
    @PostMapping(value = "add")
    public TResult<String> add(@RequestBody Version version) {
        versionService.save(version);
        return TResult.success();
    }

    /**
     * 修改版本内容
     *
     * @return
     */
    @PutMapping(value = "update")
    public TResult<String> update(@RequestBody Version version) {
        versionService.updateById(version);
        return TResult.success();
    }

    /**
     * 查看版本内容
     *
     * @return
     */
    @GetMapping(value = "get/{id}")
    public TResult<Version> get(@PathVariable Integer id) {
        Version version = versionService.getById(id);
        return TResult.success(version);
    }

    @ApiOperation("查询版本列表")
    @GetMapping("list")
    public TResult<TPage<Version>> list(VersionReq versionReq) {

        TPage<Version> page = versionService.searchByParam(versionReq);
        return TResult.success(page);
    }


}
