package com.youxin.admin.basic.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youxin.admin.basic.model.Helper;
import com.youxin.admin.basic.service.HelperService;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 问题帮助表
 */
@RestController
@RequestMapping("helper")
public class HelperController {

    @Resource
    private HelperService helperService;

    @ApiOperation("添加内容")
    @PostMapping("add")
    public TResult<String> add(@RequestBody Helper helper) {
        //helperDto.setBreviary(helperDto.getAnswer().substring(0, 10));
        helperService.save(helper);
        return TResult.success();
    }

    @ApiOperation("删除内容")
    @PostMapping("del/{id}")
    public TResult<String> del(@PathVariable int id) {
        helperService.removeById(id);

        return TResult.success();
    }

    @ApiOperation("查询详细内容")
    @GetMapping("get/{id}")
    public TResult<Helper> get(@PathVariable int id) {
        Helper helperDto = helperService.getById(id);

        return TResult.success(helperDto);
    }

    @ApiOperation("查询问题列表")
    @GetMapping("list")
    public TResult<TPage<Helper>> list(String question, Integer currentPage, Integer showCount) {
        LambdaQueryWrapper<Helper> queryWrapper = new LambdaQueryWrapper<>();
        if (!StringUtils.isEmpty(question)) {
            queryWrapper.like(Helper::getQuestion, question);
        }
        IPage<Helper> page = helperService.page(new Page<Helper>(currentPage,showCount),queryWrapper);

        return TResult.success(TPage.page(page.getRecords(),page.getTotal()));
    }

    @ApiOperation("修改内容")
    @PostMapping("update")
    public TResult<String> update(@RequestBody Helper helperDto) {
        helperService.updateById(helperDto);
        return TResult.success();
    }

    private boolean regular(String key) {
        // 验证规则
        String regEx = "/<\\/?[^>]+>/g";
        // 编译正则表达式
        Pattern pattern = Pattern.compile(regEx);
        // 忽略大小写的写法
        // Pattern pat = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(key);
        // 字符串是否与正则表达式相匹配
        return matcher.matches();

    }
}
