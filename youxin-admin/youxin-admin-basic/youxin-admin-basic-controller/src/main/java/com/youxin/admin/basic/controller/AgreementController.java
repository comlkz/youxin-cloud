package com.youxin.admin.basic.controller;

import com.youxin.admin.basic.entity.search.AgreementSearch;
import com.youxin.admin.basic.entity.vo.AgreementVo;
import com.youxin.admin.basic.model.ChatAgreement;
import com.youxin.admin.basic.service.AgreementService;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 协议表
 */
@RestController
@RequestMapping("agreement")
public class AgreementController {

    @Resource
    private AgreementService agreementService;

    @ApiOperation("添加内容")
    @PostMapping("add")
    public TResult<String> add(@RequestBody ChatAgreement agreementDto){
        agreementService.save(agreementDto);
        return TResult.success();
    }

    @ApiOperation("删除内容")
    @PostMapping("del/{id}")
    public TResult<String> del(@PathVariable int id){
        agreementService.removeById(id);
        return TResult.success();
    }

    @ApiOperation("查询详细内容")
    @GetMapping("get/{id}")
    public TResult<ChatAgreement> get(@PathVariable int id){
        ChatAgreement chatAgreement = agreementService.getById(id);
        return TResult.success(agreementService.getById(id));
    }

    @ApiOperation("查询协议列表")
    @GetMapping("list")
    public TResult<TPage<ChatAgreement>> page(AgreementSearch agreementQuery){
        return TResult.success(agreementService.page(agreementQuery));
    }

    @ApiOperation("修改内容")
    @PostMapping("update")
    public TResult<String> update(@RequestBody ChatAgreement agreementDto){
        agreementService.updateById(agreementDto);
        return TResult.success();
    }
}
