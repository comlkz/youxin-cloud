package com.youxin.admin.basic.controller;

import com.youxin.admin.annotation.LoginUser;
import com.youxin.admin.basic.entity.req.SensitiveReq;
import com.youxin.admin.basic.entity.search.SensitiveSearch;
import com.youxin.admin.basic.entity.vo.SensitiveVo;
import com.youxin.admin.basic.model.Sensitive;
import com.youxin.admin.basic.service.SensitiveService;
import com.youxin.admin.model.SysUser;
import com.youxin.base.TPage;
import com.youxin.base.TResult;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * description: SensitiveController <br>
 * date: 2020/3/3 16:09 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
@RequestMapping("sensitive")
public class SensitiveController {

    @Resource
    private SensitiveService sensitiveService;

    /**
     * 敏感词添加
     *
     * @param sensitiveDto
     * @return
     */
    @PostMapping("add")
    public TResult<String> addSensitive(@LoginUser SysUser sysUser, @RequestBody SensitiveReq sensitiveReq) {
        sensitiveService.addSensitive(sensitiveReq, sysUser.getUserName());
        return TResult.success();
    }

    /**
     * 敏感词删除
     *
     * @param id
     * @return
     */
    @PostMapping("del/{id}")
    public TResult<String> removeSensitive(@PathVariable String id) {
        sensitiveService.removeSensitive(id);
        return TResult.success();
    }

    @GetMapping("page")
    public TResult<TPage<SensitiveVo>> page(SensitiveSearch sensitiveSearch) {
        TPage<SensitiveVo> page = sensitiveService.list(sensitiveSearch);
        return TResult.success(page);
    }


}
