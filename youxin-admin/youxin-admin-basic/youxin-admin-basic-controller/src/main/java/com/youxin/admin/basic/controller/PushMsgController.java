package com.youxin.admin.basic.controller;

import com.alibaba.fastjson.JSONObject;
import com.youxin.admin.annotation.LoginUser;
import com.youxin.admin.basic.entity.req.PushMsgReq;
import com.youxin.admin.basic.entity.search.PushMsgSearch;
import com.youxin.admin.basic.model.PushMsg;
import com.youxin.admin.basic.service.PushMsgService;
import com.youxin.admin.model.SysUser;
import com.youxin.base.TPage;
import com.youxin.base.TResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * description: PushMsgController <br>
 * date: 2020/3/4 17:08 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@RestController
public class PushMsgController {

    @Resource
    private PushMsgService pushMsgService;

    @ApiOperation("广播消息")
    @PostMapping("push/send")
    public TResult<String> pushMsg(@LoginUser SysUser sysUser, @RequestBody PushMsgReq pushMsgDto){
        pushMsgService.setPushMsg(sysUser.getUserName(),pushMsgDto);
        return TResult.success();
    }

    @ApiOperation("广播消息列表")
    @GetMapping("list/push")
    public TResult<TPage<PushMsg>> pushList(PushMsgSearch pushMsgDto){

        return TResult.success(pushMsgService.getList(pushMsgDto));
    }
}
