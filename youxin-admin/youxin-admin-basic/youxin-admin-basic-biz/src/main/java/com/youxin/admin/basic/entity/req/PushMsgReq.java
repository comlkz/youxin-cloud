package com.youxin.admin.basic.entity.req;



import com.youxin.base.TQuery;

import java.util.List;

public class PushMsgReq  {

    private List<Integer> terminalType;//0：安卓 1：IOS

    private String message;

    private String title;

    private List<String> userNos;

    private Integer toAll;



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getUserNos() {
        return userNos;
    }

    public void setUserNos(List<String> userNos) {
        this.userNos = userNos;
    }

    public List<Integer> getTerminalType() {
        return terminalType;
    }

    public void setTerminalType(List<Integer> terminalType) {
        this.terminalType = terminalType;
    }

    public Integer getToAll() {
        return toAll;
    }

    public void setToAll(Integer toAll) {
        this.toAll = toAll;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
