package com.youxin.admin.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.basic.model.Helper;


import java.util.List;

public interface HelperMapper extends BaseMapper<Helper> {
}
