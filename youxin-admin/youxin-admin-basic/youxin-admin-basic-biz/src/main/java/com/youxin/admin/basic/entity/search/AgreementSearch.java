package com.youxin.admin.basic.entity.search;

import com.youxin.base.TQuery;
import io.swagger.annotations.ApiModelProperty;

public class AgreementSearch extends TQuery {
    @ApiModelProperty(value = "键",required = false)
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
