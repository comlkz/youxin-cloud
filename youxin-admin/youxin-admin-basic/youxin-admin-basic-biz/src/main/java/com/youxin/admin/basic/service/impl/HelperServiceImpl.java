package com.youxin.admin.basic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.admin.basic.mapper.HelperMapper;
import com.youxin.admin.basic.model.Helper;
import com.youxin.admin.basic.service.HelperService;
import org.springframework.stereotype.Service;

/**
 * description: HelperServiceImpl <br>
 * date: 2020/3/3 16:36 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class HelperServiceImpl extends ServiceImpl<HelperMapper, Helper> implements HelperService {
}
