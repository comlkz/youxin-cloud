package com.youxin.admin.basic.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.admin.basic.entity.vo.VersionReq;
import com.youxin.admin.basic.mapper.VersionMapper;
import com.youxin.admin.basic.model.Version;
import com.youxin.admin.basic.service.VersionService;
import com.youxin.base.TPage;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * description: VersionServiceImpl <br>
 * date: 2020/3/3 16:57 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class VersionServiceImpl extends ServiceImpl<VersionMapper, Version> implements VersionService {
    @Override
    public TPage<Version> searchByParam(VersionReq versionReq) {
        LambdaQueryWrapper<Version> queryWrapper = new LambdaQueryWrapper<>();
        if(!StringUtils.isEmpty(versionReq.getVersions())){
            queryWrapper.like(Version::getVersions,versionReq.getVersions());
        }
        queryWrapper.orderByDesc(Version::getCrtTime);
        IPage<Version> page = this.baseMapper.selectPage(new Page<Version>(versionReq.getCurrentPage(),versionReq.getShowCount()),queryWrapper);
        return TPage.page(page.getRecords(),page.getTotal());
    }
}
