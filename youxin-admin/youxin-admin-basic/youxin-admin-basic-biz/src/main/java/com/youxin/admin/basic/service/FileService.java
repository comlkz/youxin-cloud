package com.youxin.admin.basic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.basic.model.SysFile;

/**
 * description: FileService <br>
 * date: 2020/3/4 12:54 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface FileService extends IService<SysFile> {
}
