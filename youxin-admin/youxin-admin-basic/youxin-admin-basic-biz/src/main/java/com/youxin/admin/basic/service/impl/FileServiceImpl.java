package com.youxin.admin.basic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.admin.basic.mapper.SysFileMapper;
import com.youxin.admin.basic.model.SysFile;
import com.youxin.admin.basic.service.FileService;
import com.youxin.base.BaseResultCode;
import com.youxin.exception.SystemException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * description: FileServiceImpl <br>
 * date: 2020/3/4 12:55 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class FileServiceImpl extends ServiceImpl<SysFileMapper, SysFile> implements FileService {


}
