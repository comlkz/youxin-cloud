package com.youxin.admin.basic.model;

import java.time.LocalDateTime;

public class PushMsg {
    private Integer id;


    private String terminalType;

    private Integer toAll;

    private LocalDateTime createTime;


    private String userNos;
    private String createUser;

    private String message;

    private String title;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTerminalType() {
        return terminalType;
    }

    public void setTerminalType(String terminalType) {
        this.terminalType = terminalType;
    }

    public Integer getToAll() {
        return toAll;
    }

    public void setToAll(Integer toAll) {
        this.toAll = toAll;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserNos() {
        return userNos;
    }

    public void setUserNos(String userNos) {
        this.userNos = userNos;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
