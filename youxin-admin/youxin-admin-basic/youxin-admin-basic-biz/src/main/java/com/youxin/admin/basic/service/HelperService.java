package com.youxin.admin.basic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.basic.model.Helper;

/**
 * description: HelperService <br>
 * date: 2020/3/3 16:36 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface HelperService extends IService<Helper> {
}
