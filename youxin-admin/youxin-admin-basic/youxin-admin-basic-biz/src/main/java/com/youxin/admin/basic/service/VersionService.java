package com.youxin.admin.basic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.basic.entity.vo.VersionReq;
import com.youxin.admin.basic.model.Version;
import com.youxin.base.TPage;

import java.util.List;

/**
 * description: VersionService <br>
 * date: 2020/3/3 16:56 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface VersionService extends IService<Version> {

    TPage<Version> searchByParam(VersionReq versionReq);
}
