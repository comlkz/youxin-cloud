package com.youxin.admin.basic.entity.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

public class AgreementVo implements Serializable{
    private Integer id;
    @ApiModelProperty(value = "键  0:",required = false)
    private Integer key;
    @ApiModelProperty(value = "值",required = false)
    private String value;
    @ApiModelProperty(value = "描述",required = false)
    private String description;

    private LocalDateTime crtTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCrtTime() {
        return crtTime;
    }

    public void setCrtTime(LocalDateTime crtTime) {
        this.crtTime = crtTime;
    }
}
