package com.youxin.admin.basic.entity.search;

import com.youxin.base.TQuery;

import java.io.Serializable;

/**
 * description: PushMsgSearch <br>
 * date: 2020/3/4 17:02 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class PushMsgSearch extends TQuery {

    private String title;

    private String userNo;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }
}
