package com.youxin.admin.basic.entity.req;

import java.io.Serializable;
import java.time.LocalDateTime;

public class SensitiveReq implements Serializable {

    private String id;

    /**
     * 0: 敏感词替换 1: 敏感词屏蔽
     */
    private Integer wordType;

    /**
     * 关键词
     */
    private String keyWord;

    /**
     * 替换敏感词
     */
    private String replaceWord;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getWordType() {
        return wordType;
    }

    public void setWordType(Integer wordType) {
        this.wordType = wordType;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getReplaceWord() {
        return replaceWord;
    }

    public void setReplaceWord(String replaceWord) {
        this.replaceWord = replaceWord;
    }


}
