package com.youxin.admin.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youxin.admin.basic.model.Sensitive;

public interface SensitiveMapper extends BaseMapper<Sensitive> {
}
