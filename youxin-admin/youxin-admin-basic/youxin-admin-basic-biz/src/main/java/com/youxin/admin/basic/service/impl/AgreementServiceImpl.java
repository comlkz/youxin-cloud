package com.youxin.admin.basic.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.admin.basic.entity.search.AgreementSearch;
import com.youxin.admin.basic.mapper.ChatAgreementMapper;
import com.youxin.admin.basic.model.ChatAgreement;
import com.youxin.admin.basic.service.AgreementService;
import com.youxin.base.TPage;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * description: AgreementServiceImpl <br>
 * date: 2020/3/3 16:44 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class AgreementServiceImpl extends ServiceImpl<ChatAgreementMapper, ChatAgreement> implements AgreementService {
    @Override
    public TPage<ChatAgreement> page(AgreementSearch agreementQuery) {
        LambdaQueryWrapper<ChatAgreement> queryWrapper = new LambdaQueryWrapper<>();
        if(!StringUtils.isEmpty(agreementQuery.getKey())){
            queryWrapper.eq(ChatAgreement::getAgKey,agreementQuery.getKey());
        }
        IPage<ChatAgreement> page =this.baseMapper.selectPage(new Page<>(agreementQuery.getCurrentPage(), agreementQuery.getShowCount()),queryWrapper);
        return TPage.page(page.getRecords(), page.getTotal());
    }
}
