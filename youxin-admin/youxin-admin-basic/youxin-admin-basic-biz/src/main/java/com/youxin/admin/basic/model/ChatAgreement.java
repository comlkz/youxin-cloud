package com.youxin.admin.basic.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ChatAgreement implements Serializable {

    private Integer id;

    private Integer agKey;//键

    private String agValue;//值

    private String description;//描述

    private LocalDateTime crtTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAgKey() {
        return agKey;
    }

    public void setAgKey(Integer agKey) {
        this.agKey = agKey;
    }

    public String getAgValue() {
        return agValue;
    }

    public void setAgValue(String agValue) {
        this.agValue = agValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCrtTime() {
        return crtTime;
    }

    public void setCrtTime(LocalDateTime crtTime) {
        this.crtTime = crtTime;
    }
}
