package com.youxin.admin.basic.entity.vo;

import com.youxin.base.TQuery;

/**
 * description: VersionReq <br>
 * date: 2020/3/3 17:01 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public class VersionReq extends TQuery {

    private String versions;


    public String getVersions() {
        return versions;
    }

    public void setVersions(String versions) {
        this.versions = versions;
    }
}
