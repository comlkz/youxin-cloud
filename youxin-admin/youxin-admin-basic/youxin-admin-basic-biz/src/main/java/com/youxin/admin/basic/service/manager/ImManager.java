package com.youxin.admin.basic.service.manager;

import com.alibaba.fastjson.JSONObject;
import com.youxin.base.BaseResultCode;
import com.youxin.exception.SystemException;
import io.rong.RongCloud;
import io.rong.models.push.Audience;
import io.rong.models.push.BroadcastModel;
import io.rong.models.push.Message;
import io.rong.models.push.Notification;
import io.rong.models.sensitiveword.SensitiveWordModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * description: ImManager <br>
 * date: 2020/3/3 16:15 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Component
public class ImManager {

    private static final Logger logger = LoggerFactory.getLogger(ImManager.class);
    @Resource
    private RongCloud rongCloud;

    public String getHelperNo() {
        String helperNo = "10001";
        return helperNo;
    }

    public void addSensitive(String keyWord, Integer type, String replace) throws SystemException {
        SensitiveWordModel sensitive = new SensitiveWordModel();
        sensitive.setKeyword(keyWord);
        sensitive.setType(type);
        sensitive.setReplace(replace);
        try {
            rongCloud.sensitiveword.add(sensitive);
        } catch (Exception e) {
            logger.error("添加敏感词失败，keyWord={},type={},replace={}error=", keyWord, type, replace, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void removeSensitive(String keyWords) throws SystemException {
        try {
            rongCloud.sensitiveword.remove(keyWords);
        } catch (Exception e) {
            logger.error("删除敏感词失败，keyWords={}error=", keyWords, e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }

    public void pushCustomMsg(String title, String message, List<Integer> type, List<String> userIds, Integer isToAll) throws SystemException {
        BroadcastModel broadcastModel = new BroadcastModel();
        broadcastModel.setFromuserid(getHelperNo());
        List<String> platform = new ArrayList<>();
        if (type.contains(0)) {
            platform.add("android");
        }
        if (type.contains(1)) {
            platform.add("ios");
        }
        broadcastModel.setPlatform(platform.toArray(new String[platform.size()]));
        Audience audience = new Audience();
        if (isToAll == 1) {
            audience.setIs_to_all(true);
        } else {
            audience.setIs_to_all(false);
        }
        if (!CollectionUtils.isEmpty(userIds)) {
            audience.setUserid(userIds.toArray(new String[userIds.size()]));
        }
        broadcastModel.setAudience(audience);
        Message msgModel = new Message();
        Map<String, String> contentMap = new HashMap<>();
        contentMap.put("content", message);
        contentMap.put("title", title);
        contentMap.put("time", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        msgModel.setContent(JSONObject.toJSONString(contentMap));
        msgModel.setObjectName("HR:CustomNtf");
        broadcastModel.setMessage(msgModel);
        Notification notification = new Notification();
        Map<String, String> notifyMap = new HashMap<>();
        notification.setAlert(title);
        broadcastModel.setNotification(notification);
        try {
            logger.info("广播消息，broadcastModel={}", JSONObject.toJSONString(broadcastModel));
            rongCloud.push.message(broadcastModel);
        } catch (Exception e) {
            logger.error("广播消息，broadcastModel={}error=", JSONObject.toJSONString(broadcastModel), e);
            throw new SystemException(BaseResultCode.INTERNAL_ERROR, "系统内部异常，请联系技术人员");
        }
    }
}
