package com.youxin.admin.basic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.basic.entity.req.PushMsgReq;
import com.youxin.admin.basic.entity.search.PushMsgSearch;
import com.youxin.admin.basic.model.PushMsg;
import com.youxin.base.TPage;

/**
 * description: PushMsgService <br>
 * date: 2020/3/4 17:00 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface PushMsgService extends IService<PushMsg> {

    TPage<PushMsg> getList(PushMsgSearch pushMsgSearch);

    void setPushMsg(String userName, PushMsgReq pushMsg);

}
