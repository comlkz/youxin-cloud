package com.youxin.admin.basic.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youxin.admin.basic.entity.req.SensitiveReq;
import com.youxin.admin.basic.entity.search.SensitiveSearch;
import com.youxin.admin.basic.entity.vo.SensitiveVo;
import com.youxin.admin.basic.mapper.SensitiveMapper;
import com.youxin.admin.basic.model.Sensitive;
import com.youxin.admin.basic.service.SensitiveService;
import com.youxin.admin.basic.service.manager.ImManager;
import com.youxin.base.BaseResultCode;
import com.youxin.base.TPage;
import com.youxin.dozer.DozerUtils;
import com.youxin.exception.SystemException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * description: BankInfoServiceImpl <br>
 * date: 2020/3/3 16:04 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
@Service
public class SensitiveServiceImpl extends ServiceImpl<SensitiveMapper, Sensitive> implements SensitiveService {

    @Resource
    private ImManager imManager;

    @Resource
    private DozerUtils dozerUtils;

    @Override
    public void addSensitive(SensitiveReq sensitiveReq, String account) {
        if (sensitiveReq.getKeyWord().contains(",")) {
            String str[] = StringUtils.tokenizeToStringArray(sensitiveReq.getKeyWord(),",");
            List<Sensitive> list = new ArrayList<>();
            for (int i = 0; i < str.length; i++) {
                int count = count(
                        new LambdaQueryWrapper<Sensitive>().eq(Sensitive::getKeyWord, str[i]));
                if (count > 0) {
                    throw new SystemException(BaseResultCode.RECORD_EXISTS, "敏感词不能重复");
                }
                imManager.addSensitive(str[i], sensitiveReq.getWordType(), sensitiveReq.getReplaceWord());
                list.add(buildModel(sensitiveReq, account, str[i]));
            }
            saveBatch(list, str.length);
        } else {
            int count = count(
                    new LambdaQueryWrapper<Sensitive>().eq(Sensitive::getKeyWord, sensitiveReq.getKeyWord()));
            if (count > 0) {
                throw new SystemException(BaseResultCode.RECORD_EXISTS, "敏感词不能重复");
            }
            imManager.addSensitive(sensitiveReq.getKeyWord(), sensitiveReq.getWordType(), sensitiveReq.getReplaceWord());
            Sensitive sensitive = buildModel(sensitiveReq, account, sensitiveReq.getKeyWord());
            save(sensitive);
        }
    }

    @Override
    public void removeSensitive(String id) {
        Sensitive sensitive = getById(id);
        if (sensitive == null) {
            throw new SystemException(BaseResultCode.RECORD_NOT_EXISTS, "敏感词不存在");
        }
        imManager.removeSensitive(sensitive.getKeyWord());
        removeById(id);
    }

    @Override
    public TPage<SensitiveVo> list(SensitiveSearch sensitiveSearch) {
        LambdaQueryWrapper<Sensitive> queryWrapper = new LambdaQueryWrapper<Sensitive>();
        if (!StringUtils.isEmpty(sensitiveSearch.getKeyWord())) {
            queryWrapper.like(Sensitive::getKeyWord, sensitiveSearch.getKeyWord());
        }
        IPage<Sensitive> page = page(new Page<>(sensitiveSearch.getCurrentPage(), sensitiveSearch.getShowCount()), queryWrapper);
        List<SensitiveVo> list = page.getRecords().stream().map(item->{return dozerUtils.map(item,SensitiveVo.class);}).collect(Collectors.toList());
        return TPage.page(list, page.getTotal());
    }

    private Sensitive buildModel(SensitiveReq sensitiveDto, String userName, String keyWord) {
        Sensitive sensitive = new Sensitive();
        sensitive.setKeyWord(keyWord);
        sensitive.setReplaceWord(sensitiveDto.getReplaceWord());
        sensitive.setWordType(sensitiveDto.getWordType());
        sensitive.setCreateTime(LocalDateTime.now());
        sensitive.setUpdateTime(LocalDateTime.now());
        sensitive.setCreateUser(userName);
        return sensitive;
    }
}
