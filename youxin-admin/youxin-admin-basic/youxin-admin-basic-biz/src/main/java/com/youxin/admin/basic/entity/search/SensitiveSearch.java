package com.youxin.admin.basic.entity.search;


import com.youxin.base.TQuery;

public class SensitiveSearch extends TQuery {

    private String keyWord;

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }
}
