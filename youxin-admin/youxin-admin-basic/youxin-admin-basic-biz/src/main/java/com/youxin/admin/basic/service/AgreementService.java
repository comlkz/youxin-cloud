package com.youxin.admin.basic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.basic.entity.search.AgreementSearch;
import com.youxin.admin.basic.entity.vo.AgreementVo;
import com.youxin.admin.basic.model.ChatAgreement;
import com.youxin.base.TPage;

/**
 * description: AgreementService <br>
 * date: 2020/3/3 16:43 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface AgreementService extends IService<ChatAgreement> {

    TPage<ChatAgreement> page(AgreementSearch agreementQuery);

}
