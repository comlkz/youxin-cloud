package com.youxin.admin.basic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youxin.admin.basic.entity.req.SensitiveReq;
import com.youxin.admin.basic.entity.search.SensitiveSearch;
import com.youxin.admin.basic.entity.vo.SensitiveVo;
import com.youxin.admin.basic.model.Sensitive;
import com.youxin.base.TPage;

/**
 * description: BankInfoService <br>
 * date: 2020/3/3 16:04 <br>
 * author: llkj <br>
 * version: 1.0 <br>
 */
public interface SensitiveService extends IService<Sensitive> {

    void addSensitive(SensitiveReq sensitive, String account);

    void removeSensitive(String id);

    TPage<SensitiveVo> list(SensitiveSearch sensitiveSearch);


}
